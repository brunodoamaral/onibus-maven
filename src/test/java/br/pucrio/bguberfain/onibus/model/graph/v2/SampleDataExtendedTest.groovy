package br.pucrio.bguberfain.onibus.model.graph.v2

import com.sun.xml.internal.messaging.saaj.util.ByteInputStream
import spock.lang.Specification

import java.text.ParseException
import java.text.SimpleDateFormat

/**
 * Created by Bruno on 04/05/2015.
 */
class SampleDataExtendedTest extends Specification {
    def "test date format"() {
        when:
        new SampleDataExtended("04-05-2015 21:17:45", "any-order", "any-line-number", 1, 2, 3, 4)
        then:
        notThrown(ParseException)

        when:
        new SampleDataExtended("04-05-2015 21:17", "any-order", "any-line-number", 1, 2, 3, 4)
        then:
        thrown(ParseException)
    }

    def "test timestamp with BRL time"() {
        setup:
        TimeZone brazilianTimeZone = TimeZone.getTimeZone("America/Sao_Paulo")
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss")
        dateFormat.setTimeZone(brazilianTimeZone)
        long timestamp = 381666010000L // 02/04/1982 @ 7:20:10am (BRL)
        def strDate = dateFormat.format(new Date(timestamp))

        when:
        def sample = new SampleDataExtended(strDate, "any-order", "any-line-number", 1, 2, 3, 4)
        then:
        sample.timestamp == timestamp
    }

    def "test day flags"() {
        when:'is a regular monday'
        def s1 = new SampleDataExtended("04-05-2015 12:00:00", "any-order", "any-line-number", 1, 2, 3, 4)
        then:
        s1.isWorkday()
        !s1.isWeekend()
        !s1.isHoliday()

        when:'is a national holiday'
        def s2 = new SampleDataExtended("25-12-2015 12:00:00", "any-order", "any-line-number", 1, 2, 3, 4)
        then:
        !s2.isWorkday()
        !s2.isWeekend()
        s2.isHoliday()

        when:'is a regional holiday'
        def s3 = new SampleDataExtended("23-04-2015 12:00:00", "any-order", "any-line-number", 1, 2, 3, 4)
        then:
        !s3.isWorkday()
        !s3.isWeekend()
        s3.isHoliday()

        when:'is saturday'
        def s4 = new SampleDataExtended("02-05-2015 12:00:00", "any-order", "any-line-number", 1, 2, 3, 4)
        then:
        !s4.isWorkday()
        s4.isWeekend()
        !s4.isHoliday()

        when:'is sunday'
        def s5 = new SampleDataExtended("03-05-2015 12:00:00", "any-order", "any-line-number", 1, 2, 3, 4)
        then:
        !s5.isWorkday()
        s5.isWeekend()
        !s5.isHoliday()

        when:'is the beginning of a national holiday'
        def s6 = new SampleDataExtended("21-04-2015 00:01:00", "any-order", "any-line-number", 1, 2, 3, 4)
        then:
        s6.isHoliday()

        when:'is at the end of a national holiday'
        def s7 = new SampleDataExtended("21-04-2015 23:59:00", "any-order", "any-line-number", 1, 2, 3, 4)
        then:
        s7.isHoliday()

        when:'is just before a national holiday'
        def s8 = new SampleDataExtended("20-04-2015 23:59:59", "any-order", "any-line-number", 1, 2, 3, 4)
        then:
        !s8.isHoliday()

        when:'is just after a national holiday'
        def s9 = new SampleDataExtended("22-04-2015 00:00:01", "any-order", "any-line-number", 1, 2, 3, 4)
        then:
        !s9.isHoliday()
    }

    def "test date fields"() {
        when:
        SampleDataExtended sample = new SampleDataExtended("04-05-2015 21:17:45", "any-order", "any-line-number", 1, 2, 3, 4)
        then:
        sample.dayOfMonth == 4
        sample.month == Calendar.MAY
        sample.year == 2015
        sample.dayOfWeek == Calendar.MONDAY
        sample.secondsSinceMidnight == 45 +17 * 60 + 21 * 60*60

        when:
        SampleDataExtended sample2 = new SampleDataExtended("04-05-2015 00:00:00", "any-order", "any-line-number", 1, 2, 3, 4)
        then:
        sample2.dayOfMonth == 4
        sample2.month == Calendar.MAY
        sample2.year == 2015
        sample2.dayOfWeek == Calendar.MONDAY
        sample2.secondsSinceMidnight == 0

        when:
        SampleDataExtended sample3 = new SampleDataExtended("04-05-2015 23:59:59", "any-order", "any-line-number", 1, 2, 3, 4)
        then:
        sample3.dayOfMonth == 4
        sample3.month == Calendar.MAY
        sample3.year == 2015
        sample3.dayOfWeek == Calendar.MONDAY
        sample3.secondsSinceMidnight == 24 * 60 * 60 - 1
    }

    def "test stream out and in"() {
        ByteArrayOutputStream arrayOut = new ByteArrayOutputStream()
        DataOutputStream dataOut = new DataOutputStream(arrayOut)

        def sampleA = new SampleDataExtended("04-05-2015 21:17:45", "any-order", "any-line-number", 1, 2, 3, 4)

        when:
        sampleA.writeTo(dataOut)
        byte [] sampleABytes = arrayOut.toByteArray()
        DataInputStream dataIn = new DataInputStream(new ByteInputStream(sampleABytes, sampleABytes.length))
        def sampleAFromStream = SampleDataExtended.readFrom(dataIn)
        then:
        sampleA == sampleAFromStream
        dataIn.available() == 0
    }

    def "test equals"() {
        when:
        def sampleA = new SampleDataExtended("04-05-2015 21:17:45", "any-order", "any-line-number", 1, 2, 3, 4)
        def sampleACopy = new SampleDataExtended("04-05-2015 21:17:45", "any-order", "any-line-number", 1, 2, 3, 4)
        then:
        sampleA == sampleACopy
    }
}
