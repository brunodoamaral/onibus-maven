package br.pucrio.bguberfain.onibus.util

import spock.lang.Specification

import java.awt.Color

/**
 * Created by Bruno on 13/05/2015.
 */
class UtilTest extends Specification {
    def "GradientColor"() {
        def color1 = new Color(0, 0, 0)
        def color2 = new Color(255, 0, 0)
        def color3 = new Color(0, 255, 0)
        def color4 = new Color(0, 0, 255)
        def color5 = new Color(255, 255, 255)

        def colors = [color1, color2, color3, color4, color5] as Color[]

        expect:
        Util.gradientColor(colors, 0.00f, 1.0f) == color1
        Util.gradientColor(colors, 0.25f, 1.0f) == color2
        Util.gradientColor(colors, 0.50f, 1.0f) == color3
        Util.gradientColor(colors, 0.75f, 1.0f) == color4
        Util.gradientColor(colors, 1.00f, 1.0f) == color5
        Util.gradientColor(colors, 0.125f, 1.0f) == new Color(127, 0, 0)
        Util.gradientColor(colors, 0.875f, 1.0f) == new Color(127, 127, 255)
    }

    def "other GradientColor"() {
        def colors = [
            new Color(0x440000),    // 0.0
            new Color(0x440000),    // 0.1
            new Color(0xff0000),    // 0.2
            new Color(0xff0000),    // 0.3
            new Color(0xff9000),    // 0.4
            new Color(0xff9000),    // 0.5
            new Color(0x00b22d),    // 0.6
            new Color(0x00b22d),    // 0.7
            new Color(0x00b22d),    // 0.8
            new Color(0x00b22d),    // 0.9
            new Color(0x00b22d)     // 1.0
        ] as Color[] ;

        expect:
        Util.gradientColor(colors, 0.0f, 1.0f) == colors[0]
        Util.gradientColor(colors, 0.1f, 1.0f) == colors[1]
        Util.gradientColor(colors, 0.2f, 1.0f) == colors[2]
        Util.gradientColor(colors, 0.3f, 1.0f) == colors[3]
        Util.gradientColor(colors, 0.4f, 1.0f) == colors[4]
        Util.gradientColor(colors, 0.5f, 1.0f) == colors[5]
        Util.gradientColor(colors, 0.6f, 1.0f) == colors[6]
        Util.gradientColor(colors, 0.7f, 1.0f) == colors[7]
        Util.gradientColor(colors, 0.8f, 1.0f) == colors[8]
        Util.gradientColor(colors, 0.9f, 1.0f) == colors[9]
        Util.gradientColor(colors, 1.0f, 1.0f) == colors[10]
    }
}
