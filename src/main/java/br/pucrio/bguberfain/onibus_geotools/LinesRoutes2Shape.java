package br.pucrio.bguberfain.onibus_geotools;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.geotools.data.DataStore;
import org.geotools.data.DataStoreFinder;
import org.geotools.data.DefaultTransaction;
import org.geotools.data.Transaction;
import org.geotools.data.collection.ListFeatureCollection;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.data.simple.SimpleFeatureStore;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.feature.simple.SimpleFeatureTypeBuilder;
import org.geotools.geometry.jts.JTSFactoryFinder;
import org.geotools.jdbc.JDBCDataStoreFactory;
import org.geotools.referencing.crs.DefaultGeographicCRS;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;

import br.pucrio.bguberfain.onibus_geotools.Util.TrajectoryReadDataExtended;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;

/**
 * This example reads data for point locations and associated attributes from a 
 * comma separated text (CSV) file and exports them as a new shapefile. It illustrates how to build a feature type.
 * <p>
 * Note: to keep things simple in the code below the input file should not have additional spaces or
 * tabs between fields.
 */
public class LinesRoutes2Shape {

    public static void main(String[] args) throws Exception {

        /*
         * We use the DataUtilities class to create a FeatureType that will describe the data in our
         * shapefile.
         * 
         * See also the createFeatureType method below for another, more flexible approach.
         */
        final SimpleFeatureType TYPE = createFeatureType() ;
        /*
         * A list to collect features as we create them.
         */
        List<SimpleFeature> features = new ArrayList<SimpleFeature>();
        
        /*
         * GeometryFactory will be used to create the geometry attribute of each feature,
         * using a Point object for the location.
         */
        GeometryFactory geometryFactory = JTSFactoryFinder.getGeometryFactory();

        SimpleFeatureBuilder featureBuilder = new SimpleFeatureBuilder(TYPE);

        System.out.println("Reading CSV file...");
    	Util.readTrajectoriesExtended(new TrajectoryReadDataExtended() {
    		String lastShapeId ;
    		List<Coordinate> coordinates ;
    		Coordinate lastCoordinate ;
    		String lastLineNumber ;
    		String lastLineName ;
    		
    		@Override
    		public void beginFile(File originFile)
    		{
    			lastShapeId = null ;
    			lastLineNumber = null ;
    			lastLineName = null ;
    			coordinates = null ;
    			lastCoordinate = null ;
    		}
    		
			@Override
			public void readData(File originFile, String lineNumber, String lineName, String agency, int sequence, String shapeId, double latitude, double longitude)
			{
				if ( ! shapeId.equals(lastShapeId) ) {
					saveLast();
					coordinates = new LinkedList<Coordinate>();
				}
				Coordinate newCoordinate = new Coordinate(longitude, latitude) ;
				
				// Evita coordendas iguais
				if ( !newCoordinate.equals(lastCoordinate) ) {
					coordinates.add(newCoordinate) ;
				}
				lastShapeId = shapeId ;
				lastLineNumber = lineNumber ;
				lastLineName = lineName ;
				
				lastCoordinate = newCoordinate ;
			}
			
			private void saveLast()
			{
				if ( coordinates != null ) {
					Coordinate [] lsCoordinates = coordinates.toArray(new Coordinate[0]);
					LineString ls = geometryFactory.createLineString(lsCoordinates) ;
	                featureBuilder.add(ls);
	                featureBuilder.add(lastLineNumber);
	                featureBuilder.add(lastLineName);
	                featureBuilder.add(lastShapeId);
	                SimpleFeature feature = featureBuilder.buildFeature(lastLineNumber + "-" + lastShapeId);
	                features.add(feature);
				}
			}
			
			@Override
			public void endFile(File originFile)
			{
				saveLast();
			}
			
		});

        System.out.println("Saving Shape file...");

        Map<String, Object> params = new HashMap<String, Object>(); 
        params.put(JDBCDataStoreFactory.DBTYPE.key, "postgis"); 
        params.put(JDBCDataStoreFactory.HOST.key, "localhost"); 
        params.put(JDBCDataStoreFactory.PORT.key, 5432); 
        params.put(JDBCDataStoreFactory.SCHEMA.key, "public"); 
        params.put(JDBCDataStoreFactory.DATABASE.key, "bus-routes"); 
        params.put(JDBCDataStoreFactory.USER.key, "postgres"); 
        params.put(JDBCDataStoreFactory.PASSWD.key, "postgres"); 
        DataStore newDataStore = DataStoreFinder.getDataStore(params); 
        
        /*
         * TYPE is used as a template to describe the file contents
         */
        newDataStore.createSchema(TYPE);
        /*
         * Write the features to the shapefile
         */
        Transaction transaction = new DefaultTransaction("create");

        String typeName = TYPE.getTypeName();
        SimpleFeatureSource featureSource = newDataStore.getFeatureSource(typeName);

        if (featureSource instanceof SimpleFeatureStore) {
            SimpleFeatureStore featureStore = (SimpleFeatureStore) featureSource;
            /*
             * SimpleFeatureStore has a method to add features from a
             * SimpleFeatureCollection object, so we use the ListFeatureCollection
             * class to wrap our list of features.
             */
            SimpleFeatureCollection collection = new ListFeatureCollection(TYPE, features);
            featureStore.setTransaction(transaction);
            try {
                featureStore.addFeatures(collection);
                transaction.commit();
                System.out.println("Commited");
            } catch (Exception problem) {
                problem.printStackTrace();
                transaction.rollback();
            } finally {
                transaction.close();
            }
            System.exit(0); // success!
        } else {
            System.out.println(typeName + " does not support read/write access");
            System.exit(1);
        }
    }
    
    /**
     * Here is how you can use a SimpleFeatureType builder to create the schema for your shapefile
     * dynamically.
     * <p>
     * This method is an improvement on the code used in the main method above (where we used
     * DataUtilities.createFeatureType) because we can set a Coordinate Reference System for the
     * FeatureType and a a maximum field length for the 'name' field dddd
     */
    private static SimpleFeatureType createFeatureType() {
        SimpleFeatureTypeBuilder builder = new SimpleFeatureTypeBuilder();
        builder.setName("routes");
        builder.setCRS(DefaultGeographicCRS.WGS84); // <- Coordinate reference system

        // add attributes in order
        builder.add("the_geom", LineString.class);
        builder.add("line_number", String.class);
        builder.add("line_name", String.class);
        builder.add("shape_id",String.class);
        
        // build the type
        return builder.buildFeatureType();
    }
}