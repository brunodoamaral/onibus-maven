package br.pucrio.bguberfain.onibus_geotools;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.TimeZone;

import org.geotools.geometry.jts.JTS;
import org.geotools.referencing.GeodeticCalculator;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.opengis.referencing.operation.TransformException;

import ch.hsr.geohash.GeoHash;

import com.csvreader.CsvReader;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.CoordinateSequence;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.linearref.LinearLocation;

public class Util {
	@FunctionalInterface
	public interface TrajectoryReadData {
		void readData(File originFile, String lineNumber, String lineName, String agency, int sequence, String shapeId, double latitude, double longitude) ;
	}

	public interface TrajectoryReadDataExtended extends TrajectoryReadData {
		void beginFile(File originFile);
		void endFile(File originFile);
	}
	
	public static String getEnv(String envName)
	{
		String envValue = System.getenv(envName) ;
		if ( envValue == null ) {
			throw new RuntimeException("Favor definir a variavel de ambiente '" + envName + "'") ;
		}
		return envValue ;
	}

	public static void readTrajectoriesExtended(TrajectoryReadDataExtended callback) throws IOException
	{
		File trjectoriesFolder = new File( getEnv("TRAJETOS_FOLDER") );
		File[] trajectoires = trjectoriesFolder.listFiles(pathname -> pathname.getName().endsWith(".csv"));
		
		for (File file : trajectoires) {
			CsvReader trajectoryData = new CsvReader(file.getAbsolutePath());
			
			callback.beginFile(file);
			trajectoryData.readHeaders() ;
			while( trajectoryData.readRecord() ) {
				String lineNumber = trajectoryData.get("linha") ;
				String lineName = trajectoryData.get("descricao") ;
				String agency = trajectoryData.get("agencia") ;
				int sequence = Integer.parseInt(trajectoryData.get("sequencia")) ;
				String shapeId = trajectoryData.get("shape_id") ;
				double latitude = Double.parseDouble(trajectoryData.get("latitude")) ;
				double longitude = Double.parseDouble(trajectoryData.get("longitude")) ;
				
				callback.readData(file, lineNumber, lineName, agency, sequence, shapeId, latitude, longitude);
			}
			callback.endFile(file);
			
			trajectoryData.close() ;
		}
	}
	
	public static void readTrajectories(TrajectoryReadData callback) throws IOException
	{
		TrajectoryReadDataExtended extendedCallback = new TrajectoryReadDataExtended() {
			public void beginFile(File originFile) { /* Does nothing */ } ;
			public void endFile(File originFile) { /* Does nothing */ } ;
			public void readData(File originFile, String lineNumber, String lineName, String agency, int sequence, String shapeId, double latitude, double longitude)
			{
				callback.readData(originFile, lineNumber, lineName, agency, sequence, shapeId, latitude, longitude);
			}
		};
		
		readTrajectoriesExtended(extendedCallback);
	}
	
	public static final long MILISECONDS_PER_HOUR = 1000*60*60 ;
	private static final TimeZone TIME_ZONE = TimeZone.getTimeZone("America/Sao_Paulo") ;

	public static double timeStampToDayHour(long timestamp, boolean ajustToBrazilTimezone)
	{
		long offset = ajustToBrazilTimezone ? TIME_ZONE.getOffset(timestamp) : 0 ;
		return
			(
				(timestamp + offset)							// Adjust timezone
				% (MILISECONDS_PER_HOUR*24)						// Truncate for daily timestamp
			) / MILISECONDS_PER_HOUR;							// Convert ms to hour
	}
	
	private static Map<Integer, String> lineNumberById ;
	private static Map<String, Integer> idForLineNumber ;

	private static void loadId2Line() throws IOException
	{
		File logFolder = new File(Util.getEnv("LOG_FOLDER")) ;
		InputStream id2linhaIn = new FileInputStream(new File(logFolder, "id2linha.properties"));

		lineNumberById = new HashMap<>() ;
		idForLineNumber = new HashMap<>() ;

		Properties id2linha = new Properties() ;
		id2linha.load( id2linhaIn );


		for(Entry<Object, Object> prop : id2linha.entrySet() ) {
			lineNumberById.put( new Integer((String)prop.getKey()), (String)prop.getValue() );
			idForLineNumber.put( (String)prop.getValue(), new Integer((String)prop.getKey()) );
		}
	}

	public static String lineNumberForId(int lineId) throws IOException
	{
		if ( lineNumberById == null ) {
			loadId2Line();
		}
		
		return lineNumberById.get(lineId) ;
	}

	public static int idForLineNumber(String lineNumber) throws IOException
	{
		if ( idForLineNumber == null ) {
			loadId2Line();
		}

		return idForLineNumber.get(lineNumber) ;
	}

	private static Map<Integer, String> orderNumberById ;

	public static String orderNumberForId(int orderId) throws IOException
	{
		// Avoid early synchronized to check for orderNumberById==null
		if ( orderNumberById == null ){
			synchronized(Util.class) {
				if ( orderNumberById == null ) {
					File logFolder = new File(Util.getEnv("LOG_FOLDER")) ;
					InputStream id2orderIn = new FileInputStream(new File(logFolder, "id2ordem.properties"));
					
					orderNumberById = new HashMap<>() ;
					
					Properties id2order = new Properties() ;
					id2order.load( id2orderIn );
					
					
					for(Entry<Object, Object> prop : id2order.entrySet() ) {
						orderNumberById.put( new Integer((String)prop.getKey()), (String)prop.getValue() );
					}
				}
			}
		}
		
		return orderNumberById.get(orderId) ;
	}
	
	public static double distanceInMetersBetween(LinearLocation location1, LinearLocation location2, LineString ls, CoordinateReferenceSystem crs) throws TransformException
	{
        CoordinateSequence coords = ls.getCoordinateSequence();

        GeodeticCalculator geoCalculator = new GeodeticCalculator(crs) ;
        
        // Ponto inicial
        geoCalculator.setStartingPosition(JTS.toDirectPosition(location1.getCoordinate(ls), crs));
        
        // Pontos intermediarios
        double length = 0.0;
        for (int i = location1.getSegmentIndex()+1; i <= location2.getSegmentIndex(); i++) {
        	geoCalculator.setDestinationPosition(JTS.toDirectPosition(coords.getCoordinate(i), crs));
        	
        	length += geoCalculator.getOrthodromicDistance() ;
        	
            geoCalculator.setStartingPosition(geoCalculator.getDestinationPosition());
        }
        
        // Ponto final
    	geoCalculator.setDestinationPosition(JTS.toDirectPosition(location2.getCoordinate(ls), crs));
    	length += geoCalculator.getOrthodromicDistance() ;
        
        return length ;
	}
	
	public static String edgeIdForEdge(Coordinate from, Coordinate to)
	{
		String hashFrom = GeoHash.withCharacterPrecision(from.y, from.x, 9).toBase32();
		String hashTo = GeoHash.withCharacterPrecision(to.y, to.x, 9).toBase32();
		
		return hashFrom + "-" + hashTo ;
	}
}
