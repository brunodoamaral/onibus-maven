package br.pucrio.bguberfain.onibus_geotools;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.geotools.data.DataStore;
import org.geotools.data.DataStoreFinder;
import org.geotools.data.DefaultTransaction;
import org.geotools.data.Transaction;
import org.geotools.data.collection.ListFeatureCollection;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.data.simple.SimpleFeatureStore;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.feature.simple.SimpleFeatureTypeBuilder;
import org.geotools.geometry.jts.JTSFactoryFinder;
import org.geotools.jdbc.JDBCDataStoreFactory;
import org.geotools.referencing.crs.DefaultGeographicCRS;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;

import br.pucrio.bguberfain.onibus_geotools.Util.TrajectoryReadDataExtended;
import ch.hsr.geohash.GeoHash;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineSegment;
import com.vividsolutions.jts.geom.LineString;

/**
 * Converte as linhas nos arquivos .csv da prefeitura para .shp
 */
public class LinesRoutes2EdgesShape {

    public static void main(String[] args) throws Exception {

        /*
         * We use the DataUtilities class to create a FeatureType that will describe the data in our
         * shapefile.
         * 
         * See also the createFeatureType method below for another, more flexible approach.
         */
        final SimpleFeatureType featureType = createFeatureType() ;
        /*
         * A list to collect features as we create them.
         */
        List<SimpleFeature> features = new ArrayList<SimpleFeature>();
        
        /*
         * GeometryFactory will be used to create the geometry attribute of each feature,
         * using a Point object for the location.
         */
        GeometryFactory geometryFactory = JTSFactoryFinder.getGeometryFactory();

        SimpleFeatureBuilder featureBuilder = new SimpleFeatureBuilder(featureType);

        System.out.println("Reading CSV file...");
    	Util.readTrajectoriesExtended(new TrajectoryReadDataExtended() {
    		String lastShapeId ;
    		Coordinate lastCoordinate ;
    		String lastLineNumber ;
    		Map<String, SimpleFeature> featureByEdgeId = new HashMap<>();
    		
    		@Override
    		public void beginFile(File originFile)
    		{
    			lastShapeId = null ;
    			lastLineNumber = null ;
    			lastCoordinate = null ;
    		}
    		
			@Override
			public void readData(File originFile, String lineNumber, String lineName, String agency, int sequence, String shapeId, double latitude, double longitude)
			{
				Coordinate newCoordinate = new Coordinate(longitude, latitude) ;
				if ( shapeId.equals(lastShapeId) && lastCoordinate != null ) {
					addEdgeTo(newCoordinate, sequence);
				}
				lastCoordinate = newCoordinate ;
				lastShapeId = shapeId ;
				lastLineNumber = lineNumber ;
			}
			
			private void addEdgeTo(Coordinate newCoordinate, int sequence)
			{
				// Evita edge na mesma coordenada
				if ( !newCoordinate.equals(lastCoordinate) ) {
					// Cria o edge-id
					String fromHash = GeoHash.withCharacterPrecision(lastCoordinate.y, lastCoordinate.x, 9).toBase32() ;
					String toHash = GeoHash.withCharacterPrecision(newCoordinate.y, newCoordinate.x, 9).toBase32() ;
					String edgeId = String.format("%s-%s", fromHash, toHash) ;
					
					SimpleFeature feature = featureByEdgeId.get(edgeId) ;
					if ( feature == null ) {
						Coordinate [] lsCoordinates = new Coordinate[] { lastCoordinate, newCoordinate } ;
						LineString ls = geometryFactory.createLineString(lsCoordinates) ;
		                featureBuilder.add(ls);							// the_geom
		                featureBuilder.add(edgeId);						// edgeId
		                featureBuilder.add(","+lastLineNumber+",");				// linesNumbers
		                featureBuilder.add(","+lastShapeId+",");				// shapesId
		                featureBuilder.add(","+sequence+",");	// sequences
		                feature = featureBuilder.buildFeature(edgeId);
		                features.add(feature);
		                
		                featureByEdgeId.put(edgeId, feature) ;
					} else {
						// Adiciona informações ao feature
						feature.setAttribute("lines_numbers", feature.getAttribute("lines_numbers") + lastLineNumber + ",");
						feature.setAttribute("shapes_ids", feature.getAttribute("shapes_ids") + lastShapeId + ",");
						feature.setAttribute("sequences", feature.getAttribute("sequences") + String.valueOf(sequence) + ",");
					}
				}
			}
			
			@Override
			public void endFile(File originFile)
			{
			}
			
		});

    	// Save to file
//        System.out.println("Saving Shape file...");
//        File newFile = new File("data/trajetos-shape/routes-segmented.shp") ;
//
//        ShapefileDataStoreFactory dataStoreFactory = new ShapefileDataStoreFactory();
//        Map<String, Serializable> params = new HashMap<String, Serializable>();
//        params.put(ShapefileDataStoreFactory.URLP.getName(), newFile.toURI().toURL());
//        params.put(ShapefileDataStoreFactory.CREATE_SPATIAL_INDEX.getName(), Boolean.TRUE);
//        
//        DataStore newDataStore = (ShapefileDataStore) dataStoreFactory.createNewDataStore(params);
    	
    	// Save to PostGIS
        Map<String, Object> params = new HashMap<String, Object>(); 
        params.put(JDBCDataStoreFactory.DBTYPE.key, "postgis"); 
        params.put(JDBCDataStoreFactory.HOST.key, "localhost"); 
        params.put(JDBCDataStoreFactory.PORT.key, 5432); 
        params.put(JDBCDataStoreFactory.SCHEMA.key, "public"); 
        params.put(JDBCDataStoreFactory.DATABASE.key, "bus-routes"); 
        params.put(JDBCDataStoreFactory.USER.key, "postgres"); 
        params.put(JDBCDataStoreFactory.PASSWD.key, "postgres"); 
        DataStore newDataStore = DataStoreFinder.getDataStore(params); 

        /*
         * TYPE is used as a template to describe the file contents
         */
        newDataStore.createSchema(featureType);
        /*
         * Write the features to the shapefile
         */
        Transaction transaction = new DefaultTransaction("create");

        SimpleFeatureSource featureSource = newDataStore.getFeatureSource(featureType.getName().getLocalPart());


        if (featureSource instanceof SimpleFeatureStore) {
            SimpleFeatureStore featureStore = (SimpleFeatureStore) featureSource;
            /*
             * SimpleFeatureStore has a method to add features from a
             * SimpleFeatureCollection object, so we use the ListFeatureCollection
             * class to wrap our list of features.
             */
            SimpleFeatureCollection collection = new ListFeatureCollection(featureType, features);
            featureStore.setTransaction(transaction);
            try {
            	System.out.printf("Adding %d features to database...\n", collection.size());
                featureStore.addFeatures(collection);
                System.out.println("Commiting...");
                transaction.commit();
            } catch (Exception problem) {
                problem.printStackTrace();
                transaction.rollback();
            } finally {
                transaction.close();
            }
            System.exit(0); // success!
        } else {
            System.out.println(featureType.getName() + " does not support read/write access");
            System.exit(1);
        }
    }
    
    /**
     * Here is how you can use a SimpleFeatureType builder to create the schema for your shapefile
     * dynamically.
     * <p>
     * This method is an improvement on the code used in the main method above (where we used
     * DataUtilities.createFeatureType) because we can set a Coordinate Reference System for the
     * FeatureType and a a maximum field length for the 'name' field dddd
     */
    private static SimpleFeatureType createFeatureType() {
        SimpleFeatureTypeBuilder builder = new SimpleFeatureTypeBuilder();
        builder.setName("routes_segmented");
        builder.setCRS(DefaultGeographicCRS.WGS84); // <- Coordinate reference system

        // add attributes in order
        builder.add("the_geom", LineString.class);
        builder.add("edge_id", String.class);
        builder.add("lines_numbers", String.class);
        builder.add("shapes_ids", String.class);
        builder.add("sequences", String.class);
        
        // build the type
        return builder.buildFeatureType();
    }
}