package br.pucrio.bguberfain.onibus_geotools;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import org.geotools.data.FeatureSource;
import org.geotools.data.collection.CollectionFeatureSource;
import org.geotools.data.memory.MemoryFeatureCollection;
import org.geotools.feature.FeatureCollection;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.feature.simple.SimpleFeatureTypeBuilder;
import org.geotools.geometry.jts.JTS;
import org.geotools.geometry.jts.JTSFactoryFinder;
import org.geotools.geometry.jts.ReferencedEnvelope;
import org.geotools.referencing.GeodeticCalculator;
import org.geotools.referencing.crs.DefaultGeographicCRS;
import org.geotools.util.NullProgressListener;
import org.opengis.feature.Feature;
import org.opengis.feature.FeatureVisitor;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.referencing.crs.CoordinateReferenceSystem;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.MultiLineString;
import com.vividsolutions.jts.index.SpatialIndex;
import com.vividsolutions.jts.index.quadtree.Quadtree;
import com.vividsolutions.jts.linearref.LinearLocation;
import com.vividsolutions.jts.linearref.LocationIndexedLine;

public class TestGeoSnap {

	private static SimpleFeature createSimpleLineStringAt(double x1, double y1, double x2, double y2, String identifier, GeometryFactory geometryFactory, SimpleFeatureBuilder featureBuilder) {
		LineString ls = geometryFactory.createLineString(new Coordinate[] { new Coordinate(x1, y1), new Coordinate(x2, y2) });
		featureBuilder.add(ls);
		return featureBuilder.buildFeature(identifier);
	}

	private static final double SOURCE_LATITUDE = -70;

	public static void main(String[] args) throws Exception {
		final SimpleFeatureType TYPE = createFeatureType();
		MemoryFeatureCollection collection = new MemoryFeatureCollection(TYPE);
		SimpleFeatureBuilder featureBuilder = new SimpleFeatureBuilder(TYPE);
		GeometryFactory geometryFactory = JTSFactoryFinder.getGeometryFactory();
		CoordinateReferenceSystem crs = TYPE.getCoordinateReferenceSystem() ; 

		collection.add(createSimpleLineStringAt(-1, SOURCE_LATITUDE, 0, SOURCE_LATITUDE, "horizontal", geometryFactory, featureBuilder));
		collection.add(createSimpleLineStringAt(0, SOURCE_LATITUDE - 1, 0, SOURCE_LATITUDE, "vertical", geometryFactory, featureBuilder));

		FeatureSource<SimpleFeatureType, SimpleFeature> source = new CollectionFeatureSource(collection);

		// Check that we have line features
		Class<?> geomBinding = source.getSchema().getGeometryDescriptor().getType().getBinding();
		boolean isLine = geomBinding != null && (LineString.class.isAssignableFrom(geomBinding) || MultiLineString.class.isAssignableFrom(geomBinding));

		if (!isLine) {
			System.out.println("This example needs a shapefile with line features");
			return;
		}

		final SpatialIndex index = new Quadtree();
		FeatureCollection<SimpleFeatureType, SimpleFeature> features = source.getFeatures();
		System.out.println("Slurping in features ...");
		Map<LineString, Integer> countByLines = new HashMap<>();
		features.accepts(new FeatureVisitor() {

			@Override
			public void visit(Feature feature) {
				SimpleFeature simpleFeature = (SimpleFeature) feature;
				LineString geom = (LineString) simpleFeature.getDefaultGeometry();
				// Just in case: check for null or empty geometry
				if (geom != null) {
					Envelope env = geom.getEnvelopeInternal();
					if (!env.isNull()) {
//						LocationIndexedLine indexedLine = new LocationIndexedLine(geom);
						countByLines.put(geom, 0);
						index.insert(env, geom);
					}
				}
			}
		}, new NullProgressListener());

		/*
		 * For test data, we generate a large number of points placed randomly
		 * within the bounding rectangle of the features.
		 */
		final int NUM_POINTS = 10000;
		ReferencedEnvelope bounds = features.getBounds();
		Coordinate[] points = new Coordinate[NUM_POINTS];
		Random rand = new Random();
		for (int i = 0; i < NUM_POINTS; i++) {
			points[i] = new Coordinate(bounds.getMinX() + rand.nextDouble() * bounds.getWidth(), bounds.getMinY() + rand.nextDouble() * bounds.getHeight());
		}

		/*
		 * We defined the maximum distance that a line can be from a point to be
		 * a candidate for snapping (1% of the width of the feature bounds for
		 * this example).
		 */
		final double MAX_SEARCH_DISTANCE = Math.max(bounds.getSpan(0), bounds.getSpan(1));

		// Maximum time to spend running the snapping process (milliseconds)
		final long DURATION = 5000;

		int pointsProcessed = 0;
		int pointsSnapped = 0;
		long elapsedTime = 0;
		long startTime = System.currentTimeMillis();
		while (pointsProcessed < NUM_POINTS && (elapsedTime = System.currentTimeMillis() - startTime) < DURATION) {

			// Get point and create search envelope
			Coordinate pt = points[pointsProcessed++];
			Envelope search = new Envelope(pt);
			search.expandBy(MAX_SEARCH_DISTANCE);

			/*
			 * Query the spatial index for objects within the search envelope.
			 * Note that this just compares the point envelope to the line
			 * envelopes so it is possible that the point is actually more
			 * distant than MAX_SEARCH_DISTANCE from a line.
			 */
			List<LineString> lines = index.query(search);

			// Initialize the minimum distance found to our maximum acceptable
			// distance plus a little bit
			double minDist = Double.MAX_VALUE ;// MAX_SEARCH_DISTANCE + 1.0e-6;
			Coordinate minDistPoint = null;

			LineString minLine = null;
			for (LineString ls : lines) {
				LocationIndexedLine line = new LocationIndexedLine(ls) ;
				LinearLocation here = line.project(pt);
				Coordinate point = line.extractPoint(here);
//				double dist = point.distance(pt);
				GeodeticCalculator geodeticCalculator = new GeodeticCalculator(crs) ;
				geodeticCalculator.setStartingPosition( JTS.toDirectPosition(point, crs) );
				geodeticCalculator.setDestinationPosition( JTS.toDirectPosition(pt, crs) );
				double dist = geodeticCalculator.getOrthodromicDistance() ;

				if (dist < minDist) {
					minDist = dist;
					minDistPoint = point;
					minLine = ls;
				}
			}

			int thisLineCount = countByLines.get(minLine);
			countByLines.put(minLine, thisLineCount + 1);

			if (minDistPoint == null) {
				// No line close enough to snap the point to
				System.out.println(pt + "- X");

			} else {
				System.out.printf("%s - snapped by moving %.4f\n", pt.toString(), minDist);
				pointsSnapped++;
			}
		}

		System.out.printf("Processed %d points (%.2f points per second). \n" + "Snapped %d points.\n\n", pointsProcessed, 1000.0 * pointsProcessed / elapsedTime, pointsSnapped);
		
		Iterator<Entry<LineString, Integer>> iCounts = countByLines.entrySet().iterator() ;
		double first = iCounts.next().getValue() ;
		double second = iCounts.next().getValue() ;
		
		System.out.printf("Relation = %.5f", Math.max(first/second, second/first)) ;
	}

	/**
	 * Here is how you can use a SimpleFeatureType builder to create the schema
	 * for your shapefile dynamically.
	 * <p>
	 * This method is an improvement on the code used in the main method above
	 * (where we used DataUtilities.createFeatureType) because we can set a
	 * Coordinate Reference System for the FeatureType and a a maximum field
	 * length for the 'name' field dddd
	 */
	private static SimpleFeatureType createFeatureType() {
		SimpleFeatureTypeBuilder builder = new SimpleFeatureTypeBuilder();
		builder.setName("Location");
		builder.setCRS(DefaultGeographicCRS.WGS84); // <- Coordinate reference
													// system

		// add attributes in order
		builder.add("the_geom", LineString.class);
		// builder.length(15).add("lineNumber", String.class); // <- 15 chars
		// width for name field
		// builder.add("shapeId",String.class);

		// build the type
		return builder.buildFeatureType();
	}
}
