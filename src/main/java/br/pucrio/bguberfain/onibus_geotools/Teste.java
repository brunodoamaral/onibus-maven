package br.pucrio.bguberfain.onibus_geotools;

import org.geotools.graph.build.line.BasicDirectedLineGraphGenerator;
import org.geotools.graph.build.line.LineGraphGenerator;
import org.geotools.graph.structure.Graph;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.LineSegment;

import org.geotools.graph.structure.Edge;
import org.geotools.graph.structure.Node;
import org.geotools.graph.structure.basic.BasicDirectedEdge;
import org.geotools.graph.structure.basic.BasicDirectedNode;

public class Teste {

	public static void main(String[] args) {
		LineGraphGenerator generator = new BasicDirectedLineGraphGenerator() ;
		generator.add(new LineSegment(10, 20, 30, 40)) ;
		generator.add(new LineSegment(30, 40, 50, 30)) ;
		generator.add(new LineSegment(50, 30, 20, 10)) ;
		generator.add(new LineSegment(15, 25, 77, 34)) ;
		
//		generator.
		Coordinate c1 = new Coordinate(20, 10) ;
		Coordinate c2 = new Coordinate(50, 30) ;
		BasicDirectedNode n1 = (BasicDirectedNode) generator.getNode(c1) ;
		BasicDirectedNode n2 = (BasicDirectedNode) generator.getNode(c2) ;
		BasicDirectedEdge e1 = (BasicDirectedEdge) generator.getEdge(c1, c2) ;
		BasicDirectedEdge e2 = (BasicDirectedEdge) generator.getEdge(c2, c1) ;
		System.out.println(n1);
		System.out.println(n2);
		System.out.println(e1);
		System.out.println(e2);
		
		Graph graph = generator.getGraph() ;
		System.out.println(graph.getNodes()) ;
//		node.
//		CoordinateSequenceFactory pointsFactory = new 
//		CoordinateSequence points ;
//		graphGenerator.add(new LineString(points, factory))
	}
}
