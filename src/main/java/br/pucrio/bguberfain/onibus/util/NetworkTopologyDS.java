package br.pucrio.bguberfain.onibus.util;

import br.pucrio.bguberfain.onibus.model.graph.v2.BusStop;
import br.pucrio.bguberfain.onibus.model.graph.v2.Edge;
import br.pucrio.bguberfain.onibus.model.graph.v2.Route;
import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.MultiLineString;
import com.vividsolutions.jts.geom.Point;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.geotools.data.DataStore;
import org.geotools.data.FileDataStoreFinder;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.factory.CommonFactoryFinder;
import org.geotools.geometry.jts.ReferencedEnvelope;
import org.geotools.referencing.crs.DefaultGeographicCRS;
import org.geotools.util.NullProgressListener;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.type.FeatureType;
import org.opengis.filter.Filter;
import org.opengis.filter.FilterFactory2;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

public class NetworkTopologyDS {
	private static final Logger logger = LogManager.getLogger(NetworkTopologyDS.class);

	private final static NetworkTopologyDS _instance = new NetworkTopologyDS();
	
	public static NetworkTopologyDS getInstance() {
		return _instance;
	}
	
	public Map<Integer, Route> getNetworkByShapeId(Envelope region, boolean loadBusStops) throws IOException
	{
		DataStore storeRoutes = getDataStore("routes") ;

		try {
			SimpleFeatureSource routesSource = storeRoutes.getFeatureSource("routes");

			SimpleFeatureCollection routesFeatures = grabFeaturesInBoundingBox(routesSource, region) ;
	
			final Map<Integer, Route> routeByShapeId = new HashMap<>() ;
			
			routesFeatures.accepts((feature) -> {
				// Grab edge attributes...
				SimpleFeature simpleFeature = (SimpleFeature) feature;
				LineString lsRoute = (LineString) ((MultiLineString) simpleFeature.getDefaultGeometry()).getGeometryN(0);
				Integer shapeId = new Integer((String) simpleFeature.getAttribute("shape_id"));
				String lineNumber = (String) simpleFeature.getAttribute("line_numbe");

				Route route;
				try {
					route = new Route(lsRoute, lineNumber, shapeId, DefaultGeographicCRS.WGS84);
					routeByShapeId.put(shapeId, route);
				} catch (Exception e) {
					logger.error("Error while loading routes", e);
				}

			}, new NullProgressListener()) ;

			DataStore storeBusStops = getDataStore("bus-stops");
			try {
				SimpleFeatureSource busStopsSource = storeBusStops.getFeatureSource("bus-stops");

				SimpleFeatureCollection busStops = busStopsSource.getFeatures();

				AutoInstanceMap<Integer, BusStop> stopsByShape = new AutoInstanceMap<>(new HashMap<>(), LinkedList::new);

				busStops.accepts(busStopFeature -> {
					try {
						SimpleFeature busStopSimpleFeature = (SimpleFeature) busStopFeature;
						Integer stopShapeId = ((Long) busStopSimpleFeature.getAttribute("shape_id")).intValue();
						Point location = (Point) busStopSimpleFeature.getDefaultGeometry();
						Integer sequence = ((Long) busStopSimpleFeature.getAttribute("sequence")).intValue();
						Double distanceSinceBegin = (Double) busStopSimpleFeature.getAttribute("distance_s");    // .shp file truncates name

						Route route = routeByShapeId.get(stopShapeId) ;

						BusStop busStop = new BusStop(route, sequence, location.getCoordinate(), distanceSinceBegin);
						stopsByShape.putValue(stopShapeId, busStop);
					} catch (Exception e) {
						logger.error("Error while loading bus stops", e);
					}

				}, new NullProgressListener());

				stopsByShape.forEach((stopShapeId, stops) ->{
					Route route = routeByShapeId.get(stopShapeId) ;
					route.setBusStopCount(stops.size());
					stops.forEach(route::addBusStop);
				});

				return routeByShapeId;
			} finally {
				storeBusStops.dispose();
			}
		} finally {
			storeRoutes.dispose();
		}
	}

	private DataStore getDataStore(String fileName) throws IOException
	{
		try {
			URI urlRoutes = getClass().getClassLoader().getResource("data/" + fileName + ".shp").toURI();
			File routesShapeFile = new File(urlRoutes.getPath());
			logger.info("Loading " + fileName + " from shapefile file {}", routesShapeFile);
			return FileDataStoreFinder.getDataStore(routesShapeFile);
		} catch (URISyntaxException e) {
			logger.error("Error while loading routes", e);
			throw new IOException(e);
		}
	}

	// Reference code from http://docs.geotools.org/latest/userguide/library/main/filter.html
	private SimpleFeatureCollection grabFeaturesInBoundingBox(SimpleFeatureSource featureSource, Envelope bbox) throws IOException {
		if ( bbox == null ) {
			return featureSource.getFeatures() ;
		}
		FilterFactory2 ff = CommonFactoryFinder.getFilterFactory2();
		FeatureType schema = featureSource.getSchema();

		// usually "THE_GEOM" for shapefiles
		String geometryPropertyName = schema.getGeometryDescriptor().getLocalName();

		ReferencedEnvelope refbbox = ReferencedEnvelope.create(bbox, DefaultGeographicCRS.WGS84) ;

		Filter filter = ff.bbox(ff.property(geometryPropertyName), refbbox);
		return featureSource.getFeatures(filter);
	}

	private SimpleFeatureCollection getBusStopsForShape(SimpleFeatureSource featureSource, Integer shapeId) throws IOException {
		FilterFactory2 ff = CommonFactoryFinder.getFilterFactory2();
		Filter filter = ff.equal(ff.property("shape_id"), ff.literal(shapeId)) ;

		return featureSource.getFeatures(filter) ;
	}
}
