package br.pucrio.bguberfain.onibus.util;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * Created by Bruno on 01/02/2015.
 */
public class AutoInstanceMap<K, T> implements Map<K, Collection<T>>
{
    Map<K, Collection<T>> originalMap ;
    Supplier<Collection<T>> supplier ;

    public AutoInstanceMap(Map<K, Collection<T>> originalMap, Supplier<Collection<T>> supplier) {
        this.originalMap = originalMap;
        this.supplier = supplier;
    }

    public Collection<T> putValue(K key, T value)
    {
        Collection<T> collection = originalMap.get(key) ;
        if (collection == null) {
            collection = supplier.get() ;
            originalMap.put(key, collection) ;
        }
        collection.add(value) ;

        return collection ;
    }

    @Override
    public int size() {
        return originalMap.size();
    }

    @Override
    public boolean isEmpty() {
        return originalMap.isEmpty();
    }

    @Override
    public boolean containsKey(Object key) {
        return originalMap.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
        return originalMap.containsValue(value);
    }

    @Override
    public Collection<T> get(Object key) {
        return originalMap.get(key);
    }

    @Override
    public Collection<T> put(K key, Collection<T> value) {
        return originalMap.put(key, value);
    }

    @Override
    public Collection<T> remove(Object key) {
        return originalMap.remove(key);
    }

    @Override
    public void putAll(Map<? extends K, ? extends Collection<T>> m) {
        originalMap.putAll(m);
    }

    @Override
    public void clear() {
        originalMap.clear();
    }

    @Override
    public Set<K> keySet() {
        return originalMap.keySet();
    }

    @Override
    public Collection<Collection<T>> values() {
        return originalMap.values();
    }

    @Override
    public Set<Entry<K, Collection<T>>> entrySet() {
        return originalMap.entrySet();
    }

    @Override
    public boolean equals(Object o) {
        return originalMap.equals(o);
    }

    @Override
    public int hashCode() {
        return originalMap.hashCode();
    }

    @Override
    public Collection<T> getOrDefault(Object key, Collection<T> defaultValue) {
        return originalMap.getOrDefault(key, defaultValue);
    }

    @Override
    public void forEach(BiConsumer<? super K, ? super Collection<T>> action) {
        originalMap.forEach(action);
    }

    @Override
    public void replaceAll(BiFunction<? super K, ? super Collection<T>, ? extends Collection<T>> function) {
        originalMap.replaceAll(function);
    }

    @Override
    public Collection<T> putIfAbsent(K key, Collection<T> value) {
        return originalMap.putIfAbsent(key, value);
    }

    @Override
    public boolean remove(Object key, Object value) {
        return originalMap.remove(key, value);
    }

    @Override
    public boolean replace(K key, Collection<T> oldValue, Collection<T> newValue) {
        return originalMap.replace(key, oldValue, newValue);
    }

    @Override
    public Collection<T> replace(K key, Collection<T> value) {
        return originalMap.replace(key, value);
    }

    @Override
    public Collection<T> computeIfAbsent(K key, Function<? super K, ? extends Collection<T>> mappingFunction) {
        return originalMap.computeIfAbsent(key, mappingFunction);
    }

    @Override
    public Collection<T> computeIfPresent(K key, BiFunction<? super K, ? super Collection<T>, ? extends Collection<T>> remappingFunction) {
        return originalMap.computeIfPresent(key, remappingFunction);
    }

    @Override
    public Collection<T> compute(K key, BiFunction<? super K, ? super Collection<T>, ? extends Collection<T>> remappingFunction) {
        return originalMap.compute(key, remappingFunction);
    }

    @Override
    public Collection<T> merge(K key, Collection<T> value, BiFunction<? super Collection<T>, ? super Collection<T>, ? extends Collection<T>> remappingFunction) {
        return originalMap.merge(key, value, remappingFunction);
    }
}
