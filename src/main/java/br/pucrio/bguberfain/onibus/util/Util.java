package br.pucrio.bguberfain.onibus.util;

import ch.hsr.geohash.WGS84Point;
import com.csvreader.CsvReader;
import com.jhlabs.map.Ellipsoid;
import com.jhlabs.map.proj.MercatorProjection;
import com.jhlabs.map.proj.Projection;
import com.vividsolutions.jts.geom.Coordinate;

import java.awt.*;
import java.awt.geom.Point2D;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;


public class Util {

	public static float cheapDistance(double lat1, double lon1, double lat2, double lon2)
	{
	    // Converte coordenadas de graus para radianos.
	    double rLat1 = lat1 * (Math.PI / 180);
	    double rLon1 = lon1 * (Math.PI / 180);
	    double rLat2 = lat2 * (Math.PI / 180);
	    double rLon2 = lon2 * (Math.PI / 180);
	    // Calcula distancia equiretangular aproximada.
	    double x = (rLon2 - rLon1) * Math.cos((rLat1 + rLat2) / 2);
	    double y = (rLat2 - rLat1);
	    double d = Math.sqrt(x * x + y * y) * 6371; // 6371 e a circunferencia do globo.
	    return (float) d * 1000; // Em metros.
	}

	public static double distanceFrom(WGS84Point pt1, WGS84Point pt2)
	{
		// Use precise distance
//		return VincentyGeodesy.distanceInMeters(pt1, pt2) ;

		// User "cheap" distance
		return cheapDistance(pt1.getLatitude(), pt1.getLongitude(), pt2.getLatitude(), pt2.getLongitude()) ;
	}

	public static double distanceFrom(Coordinate pt1, Coordinate pt2)
	{
		// Use precise distance
//		return VincentyGeodesy.distanceInMeters(pt1, pt2) ;

		// User "cheap" distance
		return cheapDistance(pt1.y, pt1.x, pt2.y, pt2.x) ;
	}

	//
//	// Src:
//	// http://stackoverflow.com/questions/837872/calculate-distance-in-meters-when-you-know-longitude-and-latitude-in-java
//	public static float distFrom(double lat1, double lng1, double lat2, double lng2) {
//		double earthRadius = 6371000; // meters
//		double dLat = Math.toRadians(lat2 - lat1);
//		double dLng = Math.toRadians(lng2 - lng1);
//		double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) * Math.sin(dLng / 2) * Math.sin(dLng / 2);
//		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
//		float dist = (float) (earthRadius * c);
//
//		return dist;
//	}
//	
	public static final long MILISECONDS_PER_HOUR = 1000*60*60 ;
	public static final int DEFAULT_TIMEZONE = -3 ;	// Brazilian timezone

	public static double timeStampToDayHour(long timestamp)
	{
		return timeStampToDayHour(timestamp, DEFAULT_TIMEZONE);
	}

	public static double timeStampToDayHour(long timestamp, int timezone)
	{
		return
			(
				(timestamp + MILISECONDS_PER_HOUR*timezone)		// Ajust timezone
				% (MILISECONDS_PER_HOUR*24)						// Truncate for daily timestamp
			) / MILISECONDS_PER_HOUR;							// Convert ms to hour
	}

	private static Map<Integer, String> lineNumberById ;

	public static String lineNumberForId(int lineId) throws IOException
	{
		if ( lineNumberById == null ) {
			InputStream id2linhaIn = Util.class.getClassLoader().getResourceAsStream("data/id2linha.properties");

			lineNumberById = new HashMap<>() ;

			Properties id2linha = new Properties() ;
			id2linha.load( id2linhaIn );


			for(Entry<Object, Object> prop : id2linha.entrySet() ) {
				lineNumberById.put( new Integer((String)prop.getKey()), (String)prop.getValue() );
			}
		}

		return lineNumberById.get(lineId) ;
	}

	@FunctionalInterface
	public interface TrajectoryReadData {
		void readData(File originFile, String lineNumber, String lineName, String agency, int sequence, String shapeId, double latitude, double longitude) ;
	}
	
	public static void readTrajectories(TrajectoryReadData callback) throws IOException
	{
		File trjectoriesFolder = new File("data/trajetos");
		File[] trajectoires = trjectoriesFolder.listFiles(pathname -> pathname.getName().endsWith(".csv"));
		
		for (File file : trajectoires) {
			CsvReader trajectoryData = new CsvReader(file.getAbsolutePath());
			
			trajectoryData.readHeaders() ;
			while( trajectoryData.readRecord() ) {
				String lineNumber = trajectoryData.get("linha") ;
				String lineName = trajectoryData.get("descricao") ;
				String agency = trajectoryData.get("agencia") ;
				int sequence = Integer.parseInt(trajectoryData.get("sequencia")) ;
				String shapeId = trajectoryData.get("shape_id") ;
				double latitude = Double.parseDouble(trajectoryData.get("latitude")) ;
				double longitude = Double.parseDouble(trajectoryData.get("longitude")) ;
				
				callback.readData(file, lineNumber, lineName, agency, sequence, shapeId, latitude, longitude);
			}
			
			trajectoryData.close() ;
		}
	}
	

	private static Projection defaultProjection ;
	static {
		defaultProjection = new MercatorProjection();
		defaultProjection.setEllipsoid(Ellipsoid.WGS_1984);
		defaultProjection.initialize();
	}

	public static Projection getDefaultProjection() {
		return defaultProjection;
	}

	// Project a lon/lat point (in degrees), producing a result in metres
	public static Point2D.Double projectedCoordinate(WGS84Point coordinate)
	{
		Point2D.Double projected = new Point2D.Double(0,0) ;
		defaultProjection.transform(coordinate.getLatitude(), coordinate.getLongitude(), projected) ;
		return projected ;
	}

	public static double distanceFrom(Point2D.Double pt1, Point2D.Double pt2) {
		double dx = pt2.x-pt1.x ;
		double dy = pt2.y-pt1.y ;
		return Math.sqrt(dx*dx+dy*dy);
	}
	
	public static String getEnv(String envName)
	{
		return getEnv(envName, true) ;
	}

	public static String getEnv(String envName, boolean failOnNull)
	{
		String envValue = System.getenv(envName) ;
		if ( failOnNull && envValue == null ) {
			throw new RuntimeException("Favor definir a variavel de ambiente '" + envName + "'") ;
		}
		return envValue ;
	}

	public static Color gradientColor(Color color1, Color color2, float percent, float alpha) {
		float[] components1 = color1.getComponents(null) ;
		float[] components2 = color2.getComponents(null) ;

		for(int i=0; i<components1.length; i++) {
			components1[i] = components2[i] * percent + components1[i] * (1-percent) ;
		}
		return new Color( color1.getColorSpace(), components1, alpha ) ;
	}

	public static Color gradientColor(Color colors[], float percent, float alpha) {
		int numColors = colors.length-1 ;
		int fromColor = (int) Math.floor(numColors * percent);
		int toColor = fromColor + 1 ;


		if (toColor > numColors) {
			float[] lastColor = colors[numColors].getComponents(null) ;
			return new Color(colors[numColors].getColorSpace(), lastColor, alpha) ;
		}

		// Adjust percent to became between fromColor and toColor
		percent = percent * numColors - fromColor ;
		float[] components1 = colors[fromColor].getComponents(null) ;
		float[] components2 = colors[toColor].getComponents(null) ;

		for(int i=0; i<components1.length; i++) {
			components1[i] = components2[i] * percent + components1[i] * (1-percent) ;
		}
		return new Color( colors[0].getColorSpace(), components1, alpha ) ;
	}
}
