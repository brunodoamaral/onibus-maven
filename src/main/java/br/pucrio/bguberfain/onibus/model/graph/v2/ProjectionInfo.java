package br.pucrio.bguberfain.onibus.model.graph.v2;

import br.pucrio.bguberfain.onibus.util.Util;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.linearref.LinearLocation;

/**
 * Created by Bruno on 20/01/2015.
 */
public class ProjectionInfo<T extends Localizable>
{
    LinearLocation location ;
    Route route ;
    double projectedDistance;
    Coordinate projectedCoordinate ;
    T sampleData ;

    public ProjectionInfo(Route route, T sampleData, LinearLocation location, double projectedDistance, Coordinate projectedCoordinate) {
        this.route = route ;
        this.sampleData = sampleData ;
        this.location = location;
        this.projectedCoordinate = projectedCoordinate;
        this.projectedDistance = projectedDistance;
    }

    public static <L extends Localizable> ProjectionInfo<L> project(Route route, L sample)
    {
        return projectAfter(route, sample, null) ;
    }

    public static <L extends Localizable> ProjectionInfo<L> projectAfter(Route route, L sample, ProjectionInfo previousProjection)
    {
        LinearLocation projection = route.indexOfAfter(sample.getCoordinate(), (previousProjection == null) ? null : previousProjection.getLocation()) ;
        if (projection == null) {
            return null ;
        }
        Coordinate projectedCoordinate = route.extractPoint(projection) ;
        double distance = Util.distanceFrom(projectedCoordinate, sample.getCoordinate()) ;
        return new ProjectionInfo(route, sample, projection, distance, projectedCoordinate) ;
    }

    public double distanceInMetersTraveledSince(ProjectionInfo source)
    {
        if ( source.getRoute().equals(this.getRoute()) ) {
            return route.distanceInMetersBetween(source.getLocation(), this.getLocation()) ;
        } else {
            throw new IllegalArgumentException("Source is from a different route") ;
        }
    }

    public double distanceTraveledOnRoute() {
        return route.distanceInMetersUntil(location) ;
    }

    public Route getRoute() {
        return route;
    }

    public LinearLocation getLocation() {
        return location;
    }

    public double getProjectedDistance() {
        return projectedDistance;
    }

    public Coordinate getProjectedCoordinate() {
        return projectedCoordinate;
    }

    public T getLocationData() {
        return sampleData;
    }
}
