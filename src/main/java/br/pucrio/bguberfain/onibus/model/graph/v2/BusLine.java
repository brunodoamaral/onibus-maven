package br.pucrio.bguberfain.onibus.model.graph.v2;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

import br.pucrio.bguberfain.onibus.model.graph.v2.Vector;
import br.pucrio.bguberfain.onibus.model.graph.v2.BusRoute.ClosestEdge;
import br.pucrio.bguberfain.onibus.model.graph.v2.BusRoute.ClosestProjectedEdge;
import br.pucrio.bguberfain.onibus.util.Util;
import ch.hsr.geohash.WGS84Point;

public class BusLine {

	List<BusRoute> routes ;
	String lineNumber ;
	
	public BusLine(String lineNumber) {
		super();
		this.lineNumber = lineNumber;
		this.routes = new ArrayList<>() ;
	}
	
	public List<BusRoute> getRoutes() {
		return routes;
	}
	
	public String getLineNumber() {
		return lineNumber;
	}
	
	public ClosestBusRoute findMostProbableProjectedRoute(WGS84Point fromPoint, WGS84Point toPoint)
	{
		double smallestDifference = Double.MAX_VALUE ;
		ClosestProjectedEdge closestFromEdge = null ;
		ClosestProjectedEdge closestToEdge = null ;
		BusRoute closestRoute = null ;
		double closestMeanDistance = 0 ;
		
		Point2D.Double projectedFromPoint = Util.projectedCoordinate(fromPoint) ;
		Point2D.Double projectedToPoint = Util.projectedCoordinate(toPoint) ;
		
		Vector sampleVector = new Vector(projectedFromPoint, projectedToPoint);
		
		for (BusRoute route : routes) {
			ClosestProjectedEdge fromEdge = route.findClosestEdgeFromProjectedCoordinate(projectedFromPoint) ;
			ClosestProjectedEdge toEdge = route.findClosestEdgeFromProjectedCoordinate(projectedToPoint) ;
			
			// Ensure that 'toEdge' is after 'fromEdge' on the current route
			if( toEdge.getEdgeSequenceIndex() < fromEdge.getEdgeSequenceIndex() )
			{
				ClosestProjectedEdge tmp = fromEdge ;
				fromEdge = toEdge ;
				toEdge = tmp ;
			}
				
			// Calculate the difference between the sample vector and edge-projected vector
			Point2D.Double projectedFromEdge = fromEdge.getProjectedPoint() ;
			Point2D.Double projectedToEdge = toEdge.getProjectedPoint() ;
			Vector projectedVector = new Vector(projectedFromEdge, projectedToEdge) ;
			double differenceModulus = projectedVector.minus(sampleVector).modulus() ;
			
			// Calculate the mean projectedDistance between sample and projected vector
			double meanDistance = (Util.distanceFrom(projectedFromPoint, fromEdge.getProjectedPoint()) + Util.distanceFrom(projectedToPoint, toEdge.getProjectedPoint())) / 2 ;
			
			// Calculate a number that relates the vector modulus and the mean projectedDistance, giving that:
			// - smaller projectedDistance means greater route-probability
			// - smaller difference modulus means greater route-probability (add +1 because the difference could be zero) 
			double difference = (differenceModulus+1) * meanDistance ;
			
			if (difference < smallestDifference) {
				smallestDifference = difference ;
				closestFromEdge = fromEdge ;
				closestToEdge = toEdge ;
				closestRoute = route ;
				closestMeanDistance = meanDistance ;
			}
		}
		return null ;//new ClosestBusRoute(closestFromEdge, closestToEdge, closestRoute, closestMeanDistance) ;
	}
	
	public ClosestBusRoute findMostProbableRoute(WGS84Point fromPoint, WGS84Point toPoint)
	{
		double smallestDifference = Double.MAX_VALUE ;
		ClosestEdge closestFromEdge = null ;
		ClosestEdge closestToEdge = null ;
		BusRoute closestRoute = null ;
		double closestMeanDistance = 0 ;
		
		Point2D.Double projectedFromPoint = Util.projectedCoordinate(fromPoint) ;
		Point2D.Double projectedToPoint = Util.projectedCoordinate(toPoint) ;
		
		Vector sampleVector = new Vector(projectedFromPoint, projectedToPoint);
		
		for (BusRoute route : routes) {
			ClosestEdge fromEdge = route.findClosestEdgeFrom(fromPoint) ;
			ClosestEdge toEdge = route.findClosestEdgeFrom(toPoint) ;
			
			// Ensure that 'toEdge' is after 'fromEdge' on the current route
			if( toEdge.getEdgeSequenceIndex() < fromEdge.getEdgeSequenceIndex() )
			{
				ClosestEdge tmp = fromEdge ;
				fromEdge = toEdge ;
				toEdge = tmp ;
			}
				
			// Calculate the difference between the sample vector and edge-projected vector
			Point2D.Double projectedFromEdge = Util.projectedCoordinate(fromEdge.getProjectedPoint()) ;
			Point2D.Double projectedToEdge = Util.projectedCoordinate(toEdge.getProjectedPoint()) ;
			Vector projectedVector = new Vector(projectedFromEdge, projectedToEdge) ;
			double differenceModulus = projectedVector.minus(sampleVector).modulus() ;
			
			// Calculate the mean projectedDistance between sample and projected vector
			double meanDistance = (Util.distanceFrom(fromPoint, fromEdge.getProjectedPoint()) + Util.distanceFrom(toPoint, toEdge.getProjectedPoint())) / 2 ;
			
			// Calculate a number that relates the vector modulus and the mean projectedDistance, giving that:
			// - smaller projectedDistance means greater route-probability
			// - smaller difference modulus means greater route-probability (add +1 because the difference could be zero) 
			double difference = (differenceModulus+1) * meanDistance ;
			
			if (difference < smallestDifference) {
				smallestDifference = difference ;
				closestFromEdge = fromEdge ;
				closestToEdge = toEdge ;
				closestRoute = route ;
				closestMeanDistance = meanDistance ;
			}
		}
		return new ClosestBusRoute(closestFromEdge, closestToEdge, closestRoute, closestMeanDistance) ;
	}
	
	
	public static class ClosestBusRoute {
		ClosestEdge closestFromEdge = null ;
		ClosestEdge closestToEdge = null ;
		BusRoute busRoute ;
		double meanDistance ;
		
		protected ClosestBusRoute(ClosestEdge closestFromEdge, ClosestEdge closestToEdge, BusRoute busRoute, double meanDistance) {
			super();
			this.closestFromEdge = closestFromEdge;
			this.closestToEdge = closestToEdge;
			this.busRoute = busRoute;
			this.meanDistance = meanDistance ;
		}
		
		public ClosestEdge getClosestFromEdge() {
			return closestFromEdge;
		}
		public ClosestEdge getClosestToEdge() {
			return closestToEdge;
		}
		public BusRoute getBusRoute() {
			return busRoute;
		}
		public double getMeanDistance() {
			return meanDistance;
		}
	}
}
