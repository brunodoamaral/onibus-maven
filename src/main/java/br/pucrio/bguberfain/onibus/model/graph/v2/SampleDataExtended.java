package br.pucrio.bguberfain.onibus.model.graph.v2;

import br.pucrio.bguberfain.onibus.reader.sequence.filter.sampledata.HolidayFilter;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

/**
 * Created by Bruno on 04/05/2015.
 */
public class SampleDataExtended {
    private static final int FLAG_WORKDAY = 1 << 0 ;
    private static final int FLAG_HOLIDAY = 1 << 1 ;
    private static final int FLAG_WEEKEND = 1 << 2 ;

    private static final TimeZone TIME_ZONE = TimeZone.getTimeZone("America/Sao_Paulo") ;
    private static final SimpleDateFormat DATE_PARSER ;

    static {
        DATE_PARSER = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss") ;
        DATE_PARSER.setTimeZone(TIME_ZONE);
        DATE_PARSER.setLenient(false);
    }

    private long timestamp ;
    private String order ;
    private String lineNumber ;
    private double latitude ;
    private double longitude ;
    private float speed ;
    private int delay ;
    private int flags ;
    private int secondsSinceMidnight;
    private int dayOfWeek ;
    private int dayOfMonth ;
    private int month ;
    private int year ;

    public SampleDataExtended(String strSampleHour, String order, String lineNumber, double latitude, double longitude, float speed, int delay) throws ParseException {
        this.timestamp = DATE_PARSER.parse(strSampleHour).getTime() ;
        this.order = order ;
        this.lineNumber = lineNumber ;
        this.latitude = latitude ;
        this.longitude = longitude ;
        this.speed = speed ;
        this.delay = delay ;

        // Derived fields from calendar
        Calendar sampleTime = Calendar.getInstance(TIME_ZONE) ;
        sampleTime.setTimeInMillis(timestamp);

        secondsSinceMidnight = sampleTime.get(Calendar.HOUR_OF_DAY) * 60 * 60 + sampleTime.get(Calendar.MINUTE) * 60 + sampleTime.get(Calendar.SECOND) ;
        dayOfWeek = sampleTime.get(Calendar.DAY_OF_WEEK) ;
        dayOfMonth = sampleTime.get(Calendar.DAY_OF_MONTH) ;
        month = sampleTime.get(Calendar.MONTH) ;
        year = sampleTime.get(Calendar.YEAR) ;

        // Flags
        boolean isWeekend = (dayOfWeek == Calendar.SUNDAY) || (dayOfWeek == Calendar.SATURDAY) ;
        boolean isHoliday = HolidayFilter.isHoliday(timestamp + TIME_ZONE.getOffset(timestamp)) ;

        flags |= isWeekend ? FLAG_WEEKEND : 0 ;
        flags |= isHoliday ? FLAG_HOLIDAY : 0 ;
        flags |= (!isHoliday && !isWeekend) ? FLAG_WORKDAY : 0 ;
    }

    private SampleDataExtended() {
    }

    public void writeTo(DataOutput dataOut) throws IOException {
        dataOut.writeLong(timestamp);
        dataOut.writeUTF(order);
        dataOut.writeUTF(lineNumber);
        dataOut.writeDouble(latitude);
        dataOut.writeDouble(longitude);
        dataOut.writeFloat(speed);
        dataOut.writeInt(delay);
        dataOut.writeInt(flags);

        dataOut.writeInt(secondsSinceMidnight);
        dataOut.writeInt(dayOfWeek);
        dataOut.writeInt(dayOfMonth);
        dataOut.writeInt(month);
        dataOut.writeInt(year);
    }

    public static SampleDataExtended readFrom(DataInput dataIn) throws IOException {
        SampleDataExtended newSample = new SampleDataExtended() ;

        newSample.timestamp = dataIn.readLong() ;
        newSample.order = dataIn.readUTF() ;
        newSample.lineNumber = dataIn.readUTF() ;
        newSample.latitude = dataIn.readDouble() ;
        newSample.longitude = dataIn.readDouble() ;
        newSample.speed = dataIn.readFloat() ;
        newSample.delay = dataIn.readInt() ;
        newSample.flags = dataIn.readInt() ;
        newSample.secondsSinceMidnight = dataIn.readInt() ;
        newSample.dayOfWeek = dataIn.readInt() ;
        newSample.dayOfMonth = dataIn.readInt() ;
        newSample.month = dataIn.readInt() ;
        newSample.year = dataIn.readInt() ;

        return newSample ;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SampleDataExtended that = (SampleDataExtended) o;

        if (dayOfMonth != that.dayOfMonth) return false;
        if (dayOfWeek != that.dayOfWeek) return false;
        if (delay != that.delay) return false;
        if (flags != that.flags) return false;
        if (Double.compare(that.latitude, latitude) != 0) return false;
        if (Double.compare(that.longitude, longitude) != 0) return false;
        if (month != that.month) return false;
        if (secondsSinceMidnight != that.secondsSinceMidnight) return false;
        if (Float.compare(that.speed, speed) != 0) return false;
        if (timestamp != that.timestamp) return false;
        if (year != that.year) return false;
        if (lineNumber != null ? !lineNumber.equals(that.lineNumber) : that.lineNumber != null) return false;
        if (!order.equals(that.order)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = (int) (timestamp ^ (timestamp >>> 32));
        result = 31 * result + order.hashCode();
        result = 31 * result + (lineNumber != null ? lineNumber.hashCode() : 0);
        temp = Double.doubleToLongBits(latitude);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(longitude);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (speed != +0.0f ? Float.floatToIntBits(speed) : 0);
        result = 31 * result + delay;
        result = 31 * result + flags;
        result = 31 * result + secondsSinceMidnight;
        result = 31 * result + dayOfWeek;
        result = 31 * result + dayOfMonth;
        result = 31 * result + month;
        result = 31 * result + year;
        return result;
    }

    private boolean hasFlag(long flag) {
        return (flags & flag) == flag ;
    }

    public boolean isHoliday() {
        return hasFlag(FLAG_HOLIDAY) ;
    }

    public boolean isWorkday() {
        return hasFlag(FLAG_WORKDAY) ;
    }

    public boolean isWeekend() {
        return hasFlag(FLAG_WEEKEND) ;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public String getOrder() {
        return order;
    }

    public String getLineNumber() {
        return lineNumber;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public float getSpeed() {
        return speed;
    }

    public int getDelay() {
        return delay;
    }

    public int getFlags() {
        return flags;
    }

    public int getSecondsSinceMidnight() {
        return secondsSinceMidnight;
    }

    public int getDayOfWeek() {
        return dayOfWeek;
    }

    public int getDayOfMonth() {
        return dayOfMonth;
    }

    public int getMonth() {
        return month;
    }

    public int getYear() {
        return year;
    }
}
