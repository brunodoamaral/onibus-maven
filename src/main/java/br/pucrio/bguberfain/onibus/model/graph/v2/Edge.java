package br.pucrio.bguberfain.onibus.model.graph.v2;

import java.awt.geom.Point2D;
import java.util.HashSet;
import java.util.Set;

import br.pucrio.bguberfain.onibus.util.Util;
import ch.hsr.geohash.WGS84Point;

public class Edge {
	
	Set<BusLine> lines ;
	Node from, to ;
	double length, projectedLength ;
	EdgeStatistics statistics ;
	
	String edgeName ;
	
	public Edge(Node from, Node to) {
		super();
		this.from = from;
		this.to = to;
		
		edgeName = "edge_" + from.getGeoHash() + "-" + to.getGeoHash() ;
		
		lines = new HashSet<>();
		
		length = Util.distanceFrom(from.getCoordinate(), to.getCoordinate()) ;
		projectedLength = Util.distanceFrom(from.getProjectedCoordinate(), to.getProjectedCoordinate()) ;
		statistics = new EdgeStatistics() ;
		
		from.getNextEdges().add(this) ;
		to.getPrevEdges().add(this) ;
	}
	
	public Node getTo() {
		return to;
	}
	
	public Node getFrom() {
		return from;
	}
	
	public void addLineNumber(BusLine line)
	{
		lines.add(line) ;
	}
	
	public Set<BusLine> getLines() {
		return lines;
	}
	
	public double getLength() {
		return length;
	}

	public double getProjectedLength() {
		return projectedLength;
	}
	
	public EdgeStatistics getStatistics() {
		return statistics;
	}

	public Point2D.Double closestProjectedCoordinateTo(Point2D.Double point)
	{
		Point2D.Double ls1 = from.getProjectedCoordinate() ;
		Point2D.Double ls2 = to.getProjectedCoordinate() ;
		
		double A = point.y - ls1.y;
		double B = point.x - ls1.x;
		double C = ls2.y - ls1.y;
		double D = ls2.x - ls1.x;

		double dot = A * C + B * D;
		double len_sq = C * C + D * D;
		double param = dot / len_sq;

		double ptX, ptY ;

		if (param < 0 || (ls1.y == ls2.y && ls1.x == ls2.x)) {
			ptY = ls1.y;
			ptX = ls1.x;
		}
		else if (param > 1) {
			ptY = ls2.y;
			ptX = ls2.x;
		}
		else {
			ptY = ls1.y + param * C;
			ptX = ls1.x + param * D;
		}

		return new Point2D.Double(ptY, ptX) ;
	}
	
	public WGS84Point closestCoordinateTo(WGS84Point coordinate)
	{
		WGS84Point ls1 = from.getCoordinate() ;
		WGS84Point ls2 = to.getCoordinate() ;
		
		double A = coordinate.getLatitude() - ls1.getLatitude();
		double B = coordinate.getLongitude() - ls1.getLongitude();
		double C = ls2.getLatitude() - ls1.getLatitude();
		double D = ls2.getLongitude() - ls1.getLongitude();

		double dot = A * C + B * D;
		double len_sq = C * C + D * D;
		double param = dot / len_sq;

		double ptLat, ptLon;

		if (param < 0 || (ls1.getLatitude() == ls2.getLatitude() && ls1.getLongitude() == ls2.getLongitude())) {
			ptLat = ls1.getLatitude();
			ptLon = ls1.getLongitude();
		}
		else if (param > 1) {
			ptLat = ls2.getLatitude();
			ptLon = ls2.getLongitude();
		}
		else {
			ptLat = ls1.getLatitude() + param * C;
			ptLon = ls1.getLongitude() + param * D;
		}

		return new WGS84Point(ptLat, ptLon) ;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((from == null) ? 0 : from.hashCode());
		result = prime * result + ((to == null) ? 0 : to.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Edge other = (Edge) obj;
		if (from == null) {
			if (other.from != null)
				return false;
		} else if (!from.equals(other.from))
			return false;
		if (to == null) {
			if (other.to != null)
				return false;
		} else if (!to.equals(other.to))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return edgeName;
	}
	
}
