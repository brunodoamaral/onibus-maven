package br.pucrio.bguberfain.onibus.model.graph.v2;

import java.awt.geom.Point2D;
import java.util.HashSet;
import java.util.Set;

import br.pucrio.bguberfain.onibus.util.Util;
import ch.hsr.geohash.GeoHash;
import ch.hsr.geohash.WGS84Point;

public class Node {
	
	GeoHash geoHash ;
	WGS84Point coordinate ;
	Set<Edge> nextEdges ;
	Set<Edge> prevEdges ;
	String geoHashBase32 ;
	Point2D.Double projectedCoordinate ;
	
	public Node(String geoHashStr) {
		this(GeoHash.fromGeohashString(geoHashStr)) ;
	}

	public Node(GeoHash geoHash) {
		this.geoHash = geoHash ;
		coordinate = geoHash.getBoundingBoxCenterPoint();
		nextEdges = new HashSet<>();
		prevEdges = new HashSet<>();
		geoHashBase32 = this.geoHash.toBase32() ;
		projectedCoordinate = Util.projectedCoordinate(this.coordinate) ;
	}

	public String getGeoHash() {
		return geoHashBase32;
	}
	
	public WGS84Point getCoordinate() {
		return coordinate;
	}
	
	public Point2D.Double getProjectedCoordinate() {
		return projectedCoordinate;
	}

	public Set<Edge> getNextEdges() {
		return nextEdges;
	}
	
	public Set<Edge> getPrevEdges() {
		return prevEdges;
	}
	
	public Edge getNextEdgeTo(Node node) {
		for (Edge edge : nextEdges) {
			if ( edge.getTo().equals(node) ) {
				return edge ;
			}
		}
		
		return null ;
	}

	public Edge getPrevEdgeTo(Node node) {
		for (Edge edge : prevEdges) {
			if ( edge.getFrom().equals(node) ) {
				return edge ;
			}
		}
		
		return null ;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((geoHash == null) ? 0 : geoHash.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Node other = (Node) obj;
		if (geoHash == null) {
			if (other.geoHash != null)
				return false;
		} else if (!geoHash.equals(other.geoHash))
			return false;
		return true;
	}

	
	
}
