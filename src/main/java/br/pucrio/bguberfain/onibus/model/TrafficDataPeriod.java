package br.pucrio.bguberfain.onibus.model;

/**
 * Created by bguberfain on 4/26/2015.
 */
public enum TrafficDataPeriod {
    YEARLY(365*24*60, 1388530800000L),  // 1388530800000 = timestamp equals to "Tue Dec 31 21:00:00 BRST 2013" (minus 3h of timezone) (obs: ignore leap years)
    WEEKLY  (7*24*60, 259200000),       //     270000000 = timestamp equals to "Sat Jan 03 21:00:00 BRT 1970" (3h before 'an old sunday'; 3h = timezone)
    DAILY     (24*60, 0) ;

    private static final long MS_IN_A_MINUTE = 1000 * 60 ;

    int minutes ;
    long zeroTime ;

    TrafficDataPeriod(int minutes, long zeroTime) {
        this.minutes = minutes ;
        this.zeroTime = zeroTime;
    }

    public int getMinutes() {
        return minutes;
    }

    public int timeSliceForTimeStamp(long timestamp, int precisionMinutes)
    {
        return (int) ((timestamp - zeroTime) / MS_IN_A_MINUTE % minutes / precisionMinutes);
    }

}
