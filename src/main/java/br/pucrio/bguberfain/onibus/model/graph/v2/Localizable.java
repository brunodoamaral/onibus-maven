package br.pucrio.bguberfain.onibus.model.graph.v2;

import com.vividsolutions.jts.geom.Coordinate;

/**
 * Created by Bruno on 01/02/2015.
 */
public interface Localizable {
    Coordinate getCoordinate();
}
