package br.pucrio.bguberfain.onibus.model.graph.v2;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import br.pucrio.bguberfain.onibus.util.Util;
import br.pucrio.bguberfain.onibus.util.Util.TrajectoryReadData;
import ch.hsr.geohash.GeoHash;

public class NewNodeUtils {
	
	public static Map<String, BusLine> readNewGraph() throws IOException
	{
		Map<String, BusLine> busLineByNumber = new HashMap<>() ;
		Map<GeoHash, Node> nodeById = new HashMap<>() ;
		Set<Edge> edges = new HashSet<Edge>();
		Util.readTrajectories(new TrajectoryReadData() {
			String lasShapeId = null ;
			GeoHash lastGeoHash = null ;
			Node lastNode = null ;
			BusRoute currentRoute = null ;
			
			@Override
			public void readData(File originFile, String lineNumber, String lineName, String agency, int sequence, String shapeId, double latitude, double longitude)
			{
				// Find bus line
				BusLine busLine = busLineByNumber.get(lineNumber) ;
				if ( busLine == null ) {
					busLine = new BusLine(lineNumber) ;
					busLineByNumber.put(lineNumber, busLine) ;
				}

				if ( shapeId.equals(lasShapeId) )
				{
					GeoHash geoHash = GeoHash.withCharacterPrecision(latitude, longitude, 9) ;
					
					// Avoid creating a edge from/to the same point
					if ( !geoHash.equals(lastGeoHash) )
					{
						// Get/create node
						Node node = nodeById.get(geoHash) ;
						if( node == null ) {
							node = new Node(geoHash) ;
							nodeById.put(geoHash, node) ;
						}
						
						// Create edge (if so)
						if( lastNode != null ) {
							Edge edge = lastNode.getNextEdgeTo(node) ;
							if ( edge == null ) {
								edge = new Edge(lastNode, node) ;
								edges.add(edge);
//								lastNode.getNextEdges().add(edge) ;
							}
							edge.addLineNumber(busLine);
							currentRoute.add(edge) ;
						}
					
						lastNode = node ;
					}
					
					lastGeoHash = geoHash ; 
				} else {
					// New shapeId
					lastNode = null ;
					lastGeoHash = null ;
					currentRoute = new BusRoute(busLine) ;
				}
				lasShapeId = shapeId ;
			}
		});
		
		System.out.println(edges.size());
		
		return busLineByNumber ;
	}

}
