package br.pucrio.bguberfain.onibus.model.graph.v2;

import com.vividsolutions.jts.geom.*;
import com.vividsolutions.jts.index.SpatialIndex;
import com.vividsolutions.jts.index.strtree.STRtree;
import com.vividsolutions.jts.linearref.LinearIterator;
import com.vividsolutions.jts.linearref.LinearLocation;
import com.vividsolutions.jts.linearref.LocationIndexedLine;
import org.geotools.geometry.jts.JTS;
import org.geotools.referencing.GeodeticCalculator;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.opengis.referencing.operation.TransformException;

import java.util.ArrayList;
import java.util.List;

public class Route extends LocationIndexedLine {

	private static final double EXPANDED_SEARCH_RADIUS = 0.01 ;
	
	LineString geometry;
	String lineNumber ;
	Integer shapeId ;
	List<Double> distanceOfSegmentsToOrigin ;
	CoordinateReferenceSystem crs ;
	BusStop busStops[] ;
	final SpatialIndex segmentsIndex = new STRtree();

	public Route(LineString geometry, String lineNumber, Integer shapeId, CoordinateReferenceSystem crs) throws TransformException {
		super(geometry);
		this.geometry = geometry;
		this.lineNumber = lineNumber;
		this.shapeId = shapeId;
		this.crs = crs;

		busStops = new BusStop[0] ;

		// Calcula as distancias de cada ponto ate a origem e cria index dos segmentos
		distanceOfSegmentsToOrigin = new ArrayList<>(geometry.getNumPoints()-1) ;
		GeodeticCalculator geoDistance = new GeodeticCalculator(crs) ;
		double currDistance = 0.0 ;
		distanceOfSegmentsToOrigin.add(currDistance) ;

		for (LinearIterator it = new LinearIterator(geometry); it.hasNext(); it.next()) {
			if (! it.isEndOfLine()) {
				geoDistance.setStartingPosition(JTS.toDirectPosition(it.getSegmentStart(), crs));
				geoDistance.setDestinationPosition(JTS.toDirectPosition(it.getSegmentEnd(), crs));

				currDistance += geoDistance.getOrthodromicDistance();

				distanceOfSegmentsToOrigin.add(currDistance);

				LineSegmentWithIndex segment = new LineSegmentWithIndex(it.getSegmentStart(), it.getSegmentEnd(), it.getComponentIndex(), it.getVertexIndex());
				Envelope searchArea = new Envelope(segment.p0, segment.p1);
				searchArea.expandBy(EXPANDED_SEARCH_RADIUS);
				segmentsIndex.insert(searchArea, segment);

				geoDistance.setStartingPosition(geoDistance.getDestinationPosition());
			}
		}
	}

	private static class LineSegmentWithIndex extends LineSegment {
		int componentIndex, segmentIndex ;
		public LineSegmentWithIndex(Coordinate p0, Coordinate p1, int componentIndex, int segmentIndex) {
			super(p0, p1);

			this.componentIndex = componentIndex ;
			this.segmentIndex = segmentIndex ;
		}
	}

	@Override
	public LinearLocation indexOfAfter(Coordinate pt, LinearLocation fromIndex) {
		Envelope searchPt = new Envelope(pt) ;
		List<LineSegmentWithIndex> result = segmentsIndex.query(searchPt) ;

		double minDistance = Double.MAX_VALUE ;
		int minIndex = -1 ;

		for(int i=0; i<result.size(); i++) {
			LineSegmentWithIndex curr = result.get(i) ;
			double distance = curr.distance(pt) ;
			if (distance < minDistance) {
				// Check for minimum location (the second double-check is an small optimization avoiding a call to 'segmentFraction', witch is slow)
				if (fromIndex == null ||
						(fromIndex.compareLocationValues(curr.componentIndex, curr.segmentIndex, 1.0) < 0 && fromIndex.compareLocationValues(curr.componentIndex, curr.segmentIndex, curr.segmentFraction(pt)) < 0)) {
					minIndex = i;
					minDistance = distance;
				}
			}
		}

		if (minIndex == -1) {
			return null ;
		}

		LineSegmentWithIndex closestMatch = result.get(minIndex) ;
		LinearLocation indexAnswer = new LinearLocation(closestMatch.componentIndex, closestMatch.segmentIndex, closestMatch.segmentFraction(pt)) ;

		return indexAnswer ;
	}

	public Coordinate coordinateAtLength(double lengthInMeters) {
		if (lengthInMeters >= geometryLengthInMeters() ) {
			return geometry.getCoordinateN(geometry.getNumPoints()-1) ;
		}

		int lastSegment = 0 ;
		for(; lastSegment<distanceOfSegmentsToOrigin.size()-1; lastSegment++) {
			if(lengthInMeters < distanceOfSegmentsToOrigin.get(lastSegment+1)) {
				break ;
			}
		}

		double distanceToLastSegment = distanceOfSegmentsToOrigin.get(lastSegment) ;
		double distanceToNextSegment = distanceOfSegmentsToOrigin.get(lastSegment+1) ;

		double percent = (lengthInMeters - distanceToLastSegment) / (distanceToNextSegment - distanceToLastSegment) ;
		return LinearLocation.pointAlongSegmentByFraction(geometry.getCoordinateN(lastSegment), geometry.getCoordinateN(lastSegment+1), percent) ;
	}
	
	public String getLineNumber() {
		return lineNumber;
	}

	public Integer getShapeId() {
		return shapeId;
	}

	public LineString getGeometry() {
		return geometry;
	}
	
	public double geometryLengthInMeters()
	{
		return distanceOfSegmentsToOrigin.get(geometry.getCoordinateSequence().size()-1) ;
	}

	public void setBusStopCount(int count)
	{
		busStops = new BusStop[count] ;
	}

	public void addBusStop(BusStop busStop)
	{
		int sequence = busStop.getSequence() ;
		if(sequence+1 > busStops.length) {
			BusStop [] newArray = new BusStop[sequence+1] ;
			System.arraycopy(busStops, 0, newArray, 0, busStops.length);
			busStops = newArray ;
		}
		busStops[sequence] = busStop ;
	}

	public BusStop[] getBusStops() {
		return busStops;
	}

	public double lengthInMetersOfSegment(int segment)
	{
		if ( segment >= distanceOfSegmentsToOrigin.size()-1 ) {
			segment = distanceOfSegmentsToOrigin.size()-2 ;
		}

		double previous = distanceOfSegmentsToOrigin.get(segment) ;
		double current = distanceOfSegmentsToOrigin.get(segment+1) ;

		return current - previous ;
	}

	public double distanceInMetersUntil(LinearLocation location)
	{
		// Caso trivial: segmento eh o ultimo -> retorna a distancia da rota inteira
		if ( location.getSegmentIndex() >= geometry.getCoordinateSequence().size()-1 ) {
			return geometryLengthInMeters() ;
		}
		
		double distanceFromLocationSegment = distanceOfSegmentsToOrigin.get(location.getSegmentIndex()) ;
		double segmentDistance = (distanceOfSegmentsToOrigin.get(location.getSegmentIndex()+1)-distanceFromLocationSegment) * location.getSegmentFraction() ;
		
		return distanceFromLocationSegment + segmentDistance ;
	}
	
	public double distanceInMetersBetween(LinearLocation location1, LinearLocation location2)
	{
		return distanceInMetersUntil(location2) - distanceInMetersUntil(location1) ;
	}
	
	public LinearLocation linearLocationAfter(LinearLocation startLocation, double distanceInMeters)
	{
		int geometrySize = geometry.getCoordinateSequence().size() ;
		
		// Trivial case: segment is the last one -> returns itself
		if ( startLocation.getSegmentIndex() >= geometrySize-1 ) {
			return startLocation ;
		}
		
		double distanceToFinishStartSegment = (distanceOfSegmentsToOrigin.get(startLocation.getSegmentIndex()+1)-distanceOfSegmentsToOrigin.get(startLocation.getSegmentIndex())) * (1-startLocation.getSegmentFraction()) ;
		
		distanceInMeters -= distanceToFinishStartSegment ;
		int currentSegment = startLocation.getSegmentIndex()+1 ;	// Start from the next segment of startLocation
		while ( distanceInMeters > 0 && currentSegment < geometrySize-1 ) {
			distanceInMeters -= (distanceOfSegmentsToOrigin.get(currentSegment+1)-distanceOfSegmentsToOrigin.get(currentSegment)) ;
			currentSegment++ ;
		}
		
		if ( distanceInMeters < 0 ) {
			// Here distanceInMeters is expected to be negative -> thus, we must run "back" from currentSegment-1 distanceInMeters
			double currentSegmentLength = (distanceOfSegmentsToOrigin.get(currentSegment)-distanceOfSegmentsToOrigin.get(currentSegment-1)) ;
			double currentSegmentFraction = (currentSegmentLength+distanceInMeters) / currentSegmentLength ;
			
			return new LinearLocation(currentSegment-1, currentSegmentFraction) ;
		} else {
			// Distance reach the end of route -> returns the last segment
			LinearLocation lastLocation = new LinearLocation() ;
			lastLocation.setToEnd(geometry);
			return lastLocation ;
		}
		
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((lineNumber == null) ? 0 : lineNumber.hashCode());
		result = prime * result + ((shapeId == null) ? 0 : shapeId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Route other = (Route) obj;
		if (lineNumber == null) {
			if (other.lineNumber != null)
				return false;
		} else if (!lineNumber.equals(other.lineNumber))
			return false;
		if (shapeId == null) {
			if (other.shapeId != null)
				return false;
		} else if (!shapeId.equals(other.shapeId))
			return false;
		return true;
	}
}