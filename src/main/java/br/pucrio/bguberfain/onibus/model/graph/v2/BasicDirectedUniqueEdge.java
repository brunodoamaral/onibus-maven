package br.pucrio.bguberfain.onibus.model.graph.v2;

import org.geotools.graph.structure.DirectedNode;
import org.geotools.graph.structure.basic.BasicDirectedEdge;

/**
 * Created by Bruno on 10/03/2015.
 */
public class BasicDirectedUniqueEdge<T> extends BasicDirectedEdge {

    T userObject ;

    /**
     * Constructs a new edge.
     *
     * @param nodeA A node of edge.
     * @param nodeB B node of edge.
     */
    public BasicDirectedUniqueEdge(DirectedNode nodeA, DirectedNode nodeB) {
        super(nodeA, nodeB);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BasicDirectedUniqueEdge that = (BasicDirectedUniqueEdge) o;

        if (getNodeA() != null ? !getNodeA().equals(that.getNodeA()) : that.getNodeA() != null) return false;
        if (getNodeB() != null ? !getNodeB().equals(that.getNodeB()) : that.getNodeB() != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = getNodeA() != null ? getNodeA().hashCode() : 0;
        result = 31 * result + (getNodeB() != null ? getNodeB().hashCode() : 0);
        return result;
    }

    public T getUserObject() {
        return userObject;
    }

    public void setUserObject(T userObject) {
        this.userObject = userObject;
    }

}
