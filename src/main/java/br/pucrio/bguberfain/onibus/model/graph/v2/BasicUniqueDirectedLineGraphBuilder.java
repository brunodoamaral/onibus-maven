package br.pucrio.bguberfain.onibus.model.graph.v2;

import org.geotools.graph.build.basic.BasicDirectedGraphBuilder;
import org.geotools.graph.structure.DirectedNode;
import org.geotools.graph.structure.Edge;
import org.geotools.graph.structure.Node;

/**
 * Created by Bruno on 10/03/2015.
 */
public class BasicUniqueDirectedLineGraphBuilder extends BasicDirectedGraphBuilder
{

    @Override
    public Edge buildEdge(Node nodeA, Node nodeB) {
        return new BasicDirectedUniqueEdge((DirectedNode)nodeA, (DirectedNode)nodeB);
    }

}
