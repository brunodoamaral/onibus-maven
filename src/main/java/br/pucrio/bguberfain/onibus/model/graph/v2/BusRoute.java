package br.pucrio.bguberfain.onibus.model.graph.v2;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import br.pucrio.bguberfain.onibus.util.Util;
import ch.hsr.geohash.GeoHash;
import ch.hsr.geohash.WGS84Point;

public class BusRoute extends ArrayList<Edge> {

	private static final long serialVersionUID = 7046951147307576965L;
	
	BusLine busLine ;
	
	public BusRoute(BusLine busLine) {
		super() ;
		this.busLine = busLine ;
		
		this.busLine.getRoutes().add(this) ;
	}
	
	public BusLine getBusLine() {
		return busLine;
	}
	
//	// Save closest nodes information using geohash
//	private BusRouteNode [] sortedNodes ;
//	
//	private static class BusRouteNode implements Comparable<Object>
//	{
//		public Node node ;
//		public BusRouteNode nextNode ;
//		public BusRouteNode prevNode ;
//		public int index ;
//		protected BusRouteNode(Node node, int index) {
//			super();
//			this.node = node;
//			this.index = index ;
//		}
//		@Override
//		public int compareTo(Object o) {
//			if ( o instanceof BusRouteNode ) {
//				return node.getGeoHash().compareTo(((BusRouteNode)o).node.getGeoHash());
//			} else if ( o instanceof String ) {
//				return node.getGeoHash().compareTo( (String)o );
//			} else {
//				throw new RuntimeException("Cannot compare BusRouteNode to " + o.getClass().getName()) ;
//			}
//		}
//	}
//	
//	private void indexNodes() {
//		if ( sortedNodes == null ) {
//			int indexSize = this.size()+1 ;
//			sortedNodes = new BusRouteNode[indexSize] ;
//			
//			// Add all nodes from all edges of this route
//			int curr = 0 ;
//			sortedNodes[curr] = new BusRouteNode(this.get(0).getFrom(), curr);
//			curr++ ;
//			for (Edge edge : this) {
//				sortedNodes[curr] = new BusRouteNode(edge.getTo(), curr) ;
//				curr++ ;
//			}
//			
//			// Update prev/next node of sortedNodes
//			for( int i=0; i<sortedNodes.length; i++) {
//				if ( i > 0 ) {
//					sortedNodes[i].prevNode = sortedNodes[i-1] ; 
//				}
//				if ( i < sortedNodes.length-1 ) {
//					sortedNodes[i].nextNode = sortedNodes[i+1] ;
//				}
//			}
//			
//			// Sort these nodes by geohash
//			Arrays.sort(sortedNodes, (BusRouteNode a, BusRouteNode b) -> { return a.node.getGeoHash().compareTo(b.node.getGeoHash()) ; } );
//		}
//	}
//	
//	private int findClosestNodeTo(WGS84Point point)
//	{
//		// Ensure index is done
//		indexNodes() ;
//		
//		String geoHash = GeoHash.withCharacterPrecision(point.getLatitude(), point.getLongitude(), 9).toBase32() ;
//		int idx = Arrays.binarySearch(sortedNodes, geoHash) ;
//		
//		// Not found (most common case) -> use the closest match
//		if ( idx < 0 ) {
//			idx = -(idx+1) ;
//			if ( idx >= sortedNodes.length ) {
//				idx = sortedNodes.length-1 ;
//			}
//		}
//		
//		return idx ;
//	}
//	static int totalFind = 0  ;
//	static int totalErrorFind = 0 ;
	
	public ClosestProjectedEdge findClosestEdgeFromProjectedCoordinate(Point2D.Double point)
	{
		double smallestDistance = Double.MAX_VALUE ;
		Point2D.Double closestPoint = null ;
		Edge closestEdge = null ;
		int closestIndex = -1 ;
		int idx = 0 ;
		for (Edge edge : this) {
			Point2D.Double edgePoint = edge.closestProjectedCoordinateTo(point) ;
			double distance = Util.distanceFrom(point, edgePoint) ;
			
			if ( distance < smallestDistance ) {
				smallestDistance = distance ;
				closestPoint = edgePoint ;
				closestEdge = edge ;
				closestIndex = idx ;
			}
			idx++ ;
		}
		
		return new ClosestProjectedEdge(closestEdge, closestPoint, smallestDistance, closestIndex) ;		
	}


	public ClosestEdge findClosestEdgeFrom(WGS84Point point)
	{
		return slowFindClosestEdgeFrom(point) ;
//		totalFind++ ;
//		int closestNodeIdx = findClosestNodeTo(point) ;
//		
//		int idxCandidateEdge = 0 ;
//		Edge candidateEdges[] = new Edge[4*3] ;
//		int candidateEdgesIndex[] = new int[4*3] ;
//		
//		for( int i=Math.max(0, closestNodeIdx-1); i <= Math.min(closestNodeIdx+1, sortedNodes.length-1); i++ ) {
//			BusRouteNode closestBusRouteNode = sortedNodes[i] ;
//			
//			// Try edge to the next node...
//			if ( closestBusRouteNode.nextNode != null ) {
//				candidateEdgesIndex[idxCandidateEdge] = closestBusRouteNode.nextNode.index ;
//				candidateEdges[idxCandidateEdge++] = closestBusRouteNode.node.getNextEdgeTo(closestBusRouteNode.nextNode.node) ;
//				// Try next-next edge
//				if ( closestBusRouteNode.nextNode.nextNode != null ) {
//					candidateEdgesIndex[idxCandidateEdge] = closestBusRouteNode.nextNode.nextNode.index ;
//					candidateEdges[idxCandidateEdge++] = closestBusRouteNode.nextNode.node.getNextEdgeTo(closestBusRouteNode.nextNode.nextNode.node) ;
//				}
//			}
//			
//			// Try edge to the prev node...
//			if ( closestBusRouteNode.prevNode != null ) {
//				candidateEdgesIndex[idxCandidateEdge] = closestBusRouteNode.prevNode.index ;
//				candidateEdges[idxCandidateEdge++] = closestBusRouteNode.node.getPrevEdgeTo(closestBusRouteNode.prevNode.node) ;
//				// Try prev-prev edge
//				if ( closestBusRouteNode.prevNode.prevNode != null ) {
//					candidateEdgesIndex[idxCandidateEdge] = closestBusRouteNode.prevNode.prevNode.index ;
//					candidateEdges[idxCandidateEdge++] = closestBusRouteNode.prevNode.node.getPrevEdgeTo(closestBusRouteNode.prevNode.prevNode.node) ;
//				}
//			}
//		}
//		
//		
//		// Check each candidate-edge to see which is the closet
//		double smallestDistance = Double.MAX_VALUE ;
//		WGS84Point closestPoint = null ;
//		Edge closestEdge = null ;
//		int closestIndex = -1 ;
//		int idx = 0 ;
//		for (Edge edge : candidateEdges) {
//			if (edge != null) {
//				WGS84Point edgePoint = edge.closestPointTo(point) ;
//				double projectedDistance = Util.distanceFrom(point, edgePoint) ;
//				
//				if ( projectedDistance < smallestDistance ) {
//					smallestDistance = projectedDistance ;
//					closestPoint = edgePoint ;
//					closestEdge = edge ;
//					closestIndex = candidateEdgesIndex[idx] ;
//				}
//				idx++ ;
//			}
//		}
//		
//		ClosestEdge correct = slowFindClosestEdgeFrom(point) ;
//		if ( !correct.getEdge().equals(closestEdge) ) {
//			totalErrorFind++ ;
//			System.out.println(String.format("Error edge found (diff projectedDistance = %.2f, error = %2.1f%%)", Math.abs(smallestDistance-correct.getProjectedDistance()), totalErrorFind*100.0/(double)totalFind ));
//		}
//		return new ClosestEdge(closestEdge, closestPoint, smallestDistance, closestIndex) ;
	}
	
	private ClosestEdge slowFindClosestEdgeFrom(WGS84Point point)
	{
		double smallestDistance = Double.MAX_VALUE ;
		WGS84Point closestPoint = null ;
		Edge closestEdge = null ;
		int closestIndex = -1 ;
		int idx = 0 ;
		for (Edge edge : this) {
			WGS84Point edgePoint = edge.closestCoordinateTo(point) ;
			double distance = Util.distanceFrom(point, edgePoint) ;
			
			if ( distance < smallestDistance ) {
				smallestDistance = distance ;
				closestPoint = edgePoint ;
				closestEdge = edge ;
				closestIndex = idx ;
			}
			idx++ ;
		}
		
		return new ClosestEdge(closestEdge, closestPoint, smallestDistance, closestIndex) ;		
	}
	
	public static class ClosestProjectedEdge {
		Edge edge ;
		Point2D.Double projectedPoint ;
		double percent ;
		double distance ;
		int edgeSequenceIndex ;
		
		protected ClosestProjectedEdge(Edge edge, Point2D.Double edgePoint, double distance, int edgeSequenceIndex) {
			this.edge = edge;
			this.projectedPoint = edgePoint;
			this.percent = Util.distanceFrom(edgePoint, edge.getFrom().getProjectedCoordinate()) / edge.getProjectedLength() ;
			this.distance = distance ;
			this.edgeSequenceIndex = edgeSequenceIndex ;
		}

		public Edge getEdge() {
			return edge;
		}
		
		public Point2D.Double getProjectedPoint() {
			return projectedPoint;
		}
		
		public double getPercent() {
			return percent;
		}
		
		public double getProjectedDistance() {
			return distance;
		}
		
		public int getEdgeSequenceIndex() {
			return edgeSequenceIndex;
		}
	}
	
	public static class ClosestEdge {
		Edge edge ;
		WGS84Point projectedPoint ;
		double percent ;
		double distance ;
		int edgeSequenceIndex ;
		
		protected ClosestEdge(Edge edge, WGS84Point edgePoint, double distance, int edgeSequenceIndex) {
			super();
			this.edge = edge;
			this.projectedPoint = edgePoint;
			this.percent = Util.distanceFrom(edgePoint, edge.getFrom().getCoordinate()) / edge.getLength() ;
			this.distance = distance ;
			this.edgeSequenceIndex = edgeSequenceIndex ;
		}

		public Edge getEdge() {
			return edge;
		}
		
		public WGS84Point getProjectedPoint() {
			return projectedPoint;
		}
		
		public double getPercent() {
			return percent;
		}
		
		public double getDistance() {
			return distance;
		}
		
		public int getEdgeSequenceIndex() {
			return edgeSequenceIndex;
		}
	}
}
