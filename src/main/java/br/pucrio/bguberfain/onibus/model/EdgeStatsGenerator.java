package br.pucrio.bguberfain.onibus.model;

import br.pucrio.bguberfain.onibus.gui.Region;
import br.pucrio.bguberfain.onibus.model.graph.v2.*;
import br.pucrio.bguberfain.onibus.reader.GZipSampleDataReader;
import br.pucrio.bguberfain.onibus.reader.sequence.SequenceGenerator;
import br.pucrio.bguberfain.onibus.reader.sequence.SequenceReader;
import br.pucrio.bguberfain.onibus.reader.sequence.ThreadedSequenceGenerator2;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.SequenceBreaker;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.sampledata.BackwardTimeBreaker;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.sampledata.ChangeLineNumberBreaker;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.sampledata.LeastIntervalBreaker;
import br.pucrio.bguberfain.onibus.reader.sequence.filter.DataFilter;
import br.pucrio.bguberfain.onibus.reader.sequence.filter.sampledata.*;
import br.pucrio.bguberfain.onibus.reader.transformer.SampleDataSequence2ProjectionSequence;
import br.pucrio.bguberfain.onibus.util.AutoInstanceMap;
import br.pucrio.bguberfain.onibus.util.NetworkTopologyDS;
import br.pucrio.bguberfain.onibus.util.Util;
import com.vividsolutions.jts.geom.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.geotools.geometry.jts.JTSFactoryFinder;
import org.geotools.graph.build.GraphBuilder;
import org.geotools.graph.build.line.BasicLineGraphGenerator;
import org.geotools.graph.build.line.LineStringGraphGenerator;
import org.geotools.graph.structure.Edge;
import org.geotools.graph.structure.Graph;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

/**
 * Created by bguberfain on 3/20/2015.
 */
public class EdgeStatsGenerator {
    private static final Logger logger = LogManager.getLogger(EdgeStatsGenerator.class);
    private final static long MAX_INTERVAL_BETWEEN_SEQUENCES = 60 * 15; // 15 minutes in seconds
    private final static double MAX_SPEED = 80 / 3.6;     // km/h in m/s
    private final static double MAX_DISTANCE_BETWEEN_SEQUENCES = MAX_SPEED * 5 * 60; // Distance with max speed in 5 minutes
    private final static double MAX_DISTANCE_TO_ENDPOINTS = 200;   // In meters

    private static final int PRECISION_MINUTES = 15;    // Compute statistics for every 'PRECISION_MINUTES' minutes among the day (must be a number that divides 60)

    private static final int MINUTES_PER_DAY = 24 * 60;
    private static final int MILLISECONDS_PER_MINUTE = 1000 * 60;
    private static final int TIME_SLICES_PER_DAY = MINUTES_PER_DAY / PRECISION_MINUTES;

    private final static AutoInstanceMap<Edge, LineSegment> lsSegmentsForEdge = new AutoInstanceMap<>(new HashMap<>(), LinkedList::new);
    private static final SimpleDateFormat fileInputFormat = new SimpleDateFormat("yyyyMMdd'\\.txt\\.gz'") ;

    Envelope region = Region.RIO_DE_JANEIRO;
    Date fromDate;
    Date toDate;

    public EdgeStatsGenerator() {
    }

    public void setRegion(Envelope region) {
        this.region = region;
    }

    public void setDateRange(Date fromDate, Date toDate) {
        this.fromDate = fromDate;
        this.toDate = toDate;
    }

    public Map<LineSegment, EdgeStats> process() throws IOException, ClassNotFoundException {
        // Load routes
        Map<Integer, Route> routesByShapeId = NetworkTopologyDS.getInstance().getNetworkByShapeId(null, false);

        // Expanded region
        Envelope filterRegion = new Envelope(region);
        filterRegion.expandBy(0.01);

        // Generate graph
        logger.info("Generating graph...");
        Graph graph = getGraphForRoutes(routesByShapeId.values(), filterRegion);

        // Generate map of LineSegment for Edge
        logger.info("Generating edge-by-segment map...");
        Map<LineSegment, BasicDirectedUniqueEdge<DynamicEdgeStats>> edgeBySegment = new HashMap<>();
        for (BasicDirectedUniqueEdge edge : (Collection<BasicDirectedUniqueEdge>) graph.getEdges()) {
            Coordinate from = ((Point) edge.getNodeA().getObject()).getCoordinate();
            Coordinate to = ((Point) edge.getNodeB().getObject()).getCoordinate();

            edgeBySegment.put(new LineSegment(from, to), edge);
        }

        // Detect two-way edges
        int countReverse = 0;
        for (Map.Entry<LineSegment, BasicDirectedUniqueEdge<DynamicEdgeStats>> edgeEntry : edgeBySegment.entrySet()) {
            LineSegment segment = edgeEntry.getKey();
            BasicDirectedUniqueEdge<DynamicEdgeStats> edge = edgeEntry.getValue();
            LineSegment reverseSegment = new LineSegment(segment.p1, segment.p0);

            if (edgeBySegment.containsKey(reverseSegment)) {
                countReverse++;

                // Move the segment to the "right" (-pi/2)
                double moveAngle = segment.angle() - Math.PI / 2;
                double plusX = Math.cos(moveAngle) * 0.00005;
                double plusY = Math.sin(moveAngle) * 0.00005;

                Coordinate from = ((Point) edge.getNodeA().getObject()).getCoordinate();
                Coordinate to = ((Point) edge.getNodeB().getObject()).getCoordinate();

                LineSegment lsMoved = new LineSegment(from.x + plusX, from.y + plusY, to.x + plusX, to.y + plusY);

                lsSegmentsForEdge.putValue(edge, lsMoved);
            } else {
                lsSegmentsForEdge.putValue(edge, segment);
            }
        }

        logger.info("Found {} two-way segments", countReverse);

        String filePattern = "\\d{8}\\.txt\\.gz" ;

        // Configure filters...
        DataFilter filter = new WorkdayFilter();
        filter = new RegionFilter(filterRegion, filter);
        filter = new MaxDelayFilter(15 * 60, filter);
        filter = new DaylightSavingTimeFixFilter(filter);

        if (fromDate != null && toDate != null) {
            filter = new DateRangeFilter(fromDate, toDate, filter) ;

            // Try to optimize file loading...
            String fromDateFile = fileInputFormat.format(fromDate) ;
            String toDateFile = fileInputFormat.format(toDate) ;

            // Check for 'from' and to 'date' being the same
            if (fromDateFile.equals(toDateFile)) {
                filePattern = fromDateFile ;
                logger.info("Only one file to load: '{}'", filePattern);
            }
        }

        // Configure data loader
        Iterator<SampleData> samples = new GZipSampleDataReader(filePattern);
        SequenceBreaker sequenceBreakers = new LeastIntervalBreaker(MAX_INTERVAL_BETWEEN_SEQUENCES * 1000);    // Ignore samples that are 5min apart
        sequenceBreakers = new ChangeLineNumberBreaker(sequenceBreakers);    // Keep only same line number in the sequence
        sequenceBreakers = new BackwardTimeBreaker(sequenceBreakers);
        SequenceGenerator<SampleData> samplesSequence = new ThreadedSequenceGenerator2<>(sequenceBreakers, filter, samples, SampleData::getOrder);

        // Convert Sample to Projection (snapping)
        SequenceGenerator<ProjectionInfo<SampleData>> projectionSequence = new SampleDataSequence2ProjectionSequence(routesByShapeId.values(), samplesSequence);

        logger.info("Starting loading data...");
        long start = System.currentTimeMillis();
        projectionSequence.readData(new SequenceReader<ProjectionInfo<SampleData>>() {
            AtomicLong count = new AtomicLong();

            @Override
            public void beginSequence(ProjectionInfo<SampleData> newData) {
            }

            @Override
            public void sequence(ProjectionInfo<SampleData> lastData, ProjectionInfo<SampleData> newData) {
                SampleData newSample = newData.getLocationData();
                SampleData lastSample = lastData.getLocationData();
                int timeDiff = (int) ((newSample.getTimestamp() - lastSample.getTimestamp()) / 1000);
                if (timeDiff > 0) {
                    double distanceDiff = newData.distanceInMetersTraveledSince(lastData);

                    if (distanceDiff > 0 && distanceDiff < MAX_DISTANCE_BETWEEN_SEQUENCES) {
                        double avgSpeed = Math.min(MAX_SPEED, distanceDiff / timeDiff);

                        // Check for buses on the beginning or end of route
                        Route route = newData.getRoute();
                        double distanceSinceBegin = route.distanceInMetersUntil(newData.getLocation());
                        if (distanceSinceBegin < MAX_DISTANCE_TO_ENDPOINTS ||
                                route.geometryLengthInMeters() - distanceSinceBegin < MAX_DISTANCE_TO_ENDPOINTS) {
                            return;
                        }

                        // Update average speed of each edge
                        int imgIdx = timeSliceForTimeStamp(newSample.getTimestamp());
                        LineString lsRoute = newData.getRoute().getGeometry();

                        if (lastData.getLocation().getSegmentFraction() > 0.5) {
                            updateSegmentWithSpeed(lastData.getLocation().getSegmentIndex(), imgIdx, lsRoute, avgSpeed);
                        }

                        if (newData.getLocation().getSegmentFraction() > 0.5) {
                            updateSegmentWithSpeed(newData.getLocation().getSegmentIndex(), imgIdx, lsRoute, avgSpeed);
                        }

                        for (int segment = lastData.getLocation().getSegmentIndex() + 1; segment < newData.getLocation().getSegmentIndex(); segment++) {
                            updateSegmentWithSpeed(segment, imgIdx, lsRoute, avgSpeed);
                        }

//                        if (count.incrementAndGet() % 99999 == 0 ) {
//                            projectionSequence.stop();
//                        }
                    }
                }
            }

            @Override
            public void endSequence(ProjectionInfo<SampleData> lastData) {
            }

            private synchronized void updateSegmentWithSpeed(int segment, int idx, LineString lsRoute, double speed) {
                BasicDirectedUniqueEdge<DynamicEdgeStats> edge = edgeBySegment.get(getLineSegmentFrom(segment, lsRoute));
                if (edge != null) {
                    edge.getUserObject().increment(idx, speed);
                }
            }
        });

        long end = System.currentTimeMillis();
        logger.info("Finished loading all data in {} s", (end - start) / 1000);

        return edgeBySegment.entrySet().stream()
                .map(entry -> new AbstractMap.SimpleEntry<>(entry.getKey(), entry.getValue().getUserObject().snapshot()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)) ;
    }

    private static Graph getGraphForRoutes(Collection<Route> routes, Envelope region) {
        // Graph builders....
        GraphBuilder graphBuilder = new BasicUniqueDirectedLineGraphBuilder();
        BasicLineGraphGenerator graphGenerator = new LineStringGraphGenerator();
        graphGenerator.setGraphBuilder(graphBuilder);

        // Geometry builder...
        GeometryFactory geometryFactory = JTSFactoryFinder.getGeometryFactory();

        // Create graph for each edge
        for (Route route : routes) {
            LineString lsRoute = route.getGeometry();
            Coordinate lastCoordinate = lsRoute.getCoordinateN(0);
            for (int i = 0; i < lsRoute.getNumPoints(); i++) {
                Coordinate currentCoordinate = lsRoute.getCoordinateN(i);

                if (region.contains(currentCoordinate) && region.contains(lastCoordinate)) {
                    LineString lsSegment = geometryFactory.createLineString(new Coordinate[]{lastCoordinate, currentCoordinate});

                    graphGenerator.add(lsSegment);
                }

                lastCoordinate = currentCoordinate;
            }
        }

        // Generate graph and add DynamicEdgeStats to edges
        Graph graph = graphGenerator.getGraph();
        graph.visitEdges(edge -> {
            ((BasicDirectedUniqueEdge) edge).setUserObject(new DynamicEdgeStats(TIME_SLICES_PER_DAY));
            return Graph.PASS_AND_CONTINUE;
        });


        return graph;
    }

    private static LineSegment getLineSegmentFrom(int segment, LineString ls) {
        return new LineSegment(ls.getCoordinateN(segment), ls.getCoordinateN(segment+1)) ;
    }

    public static int timeSliceForTimeStamp(long timestamp)
    {
        return (int)
                ( (
                        (timestamp + Util.MILISECONDS_PER_HOUR*Util.DEFAULT_TIMEZONE)		// Ajust timezone
                                % (Util.MILISECONDS_PER_HOUR*24)									// Truncate for daily timestamp
                ) / MILLISECONDS_PER_MINUTE)											// Convert to minutes
                / PRECISION_MINUTES;													// Convert to y slice
    }

    public static String hourForSegment(int segment) {
        int totalMinutes = segment * PRECISION_MINUTES ;
        int hours = totalMinutes / 60 ;
        int minutes = totalMinutes % 60 ;

        return String.format("%02d-%02d", hours, minutes) ;
    }

}
