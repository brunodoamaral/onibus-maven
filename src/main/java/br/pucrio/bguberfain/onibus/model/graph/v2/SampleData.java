package br.pucrio.bguberfain.onibus.model.graph.v2;

import br.pucrio.bguberfain.onibus.util.Util;
import ch.hsr.geohash.util.VincentyGeodesy;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.geotools.geometry.jts.JTS;
import org.geotools.referencing.GeodeticCalculator;
import org.geotools.referencing.crs.DefaultGeographicCRS;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.opengis.referencing.operation.TransformException;

import com.vividsolutions.jts.geom.Coordinate;

public class SampleData implements Localizable {
	private static final Logger logger = LogManager.getLogger(SampleData.class);

	// Data-Hora
	private long timestamp ;
	private String order ;
	private String lineNumber ;
	private Coordinate coordinate ;
	private float speed ;
	private int delay ;
	private final static CoordinateReferenceSystem DEFAULT_CRS = DefaultGeographicCRS.WGS84 ;
	private boolean annotation ;

	public SampleData(long timestamp, String order, String lineNumber, Coordinate coordinate, float speed, int delay, boolean annotation) {
		super();
		this.timestamp = timestamp;
		this.order = order;
		this.lineNumber = lineNumber;
		this.speed = speed;
		this.delay = delay;
		this.annotation = annotation ;

		this.setCoordinate(coordinate);
	}

	protected SampleData() {
		super() ;
	}

	public long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	public String getOrder() {
		return order;
	}
	public void setOrder(String order) {
		this.order = order;
	}
	public String getLineNumber() {
		return lineNumber;
	}
	public void setLineNumber(String lineNumber) {
		this.lineNumber = lineNumber;
	}
	public boolean isAnnotation() {
		return annotation;
	}

	public void setAnnotation(boolean annotation) {
		this.annotation = annotation;
	}

	@Override
	public Coordinate getCoordinate() {
		return coordinate;
	}
	public void setCoordinate(Coordinate coordinate) {
		this.coordinate = coordinate;
	}
	public float getSpeed() {
		return speed;
	}
	public void setSpeed(float speed) {
		this.speed = speed;
	}
	public int getDelay() {
		return delay;
	}
	public void setDelay(int dalay) {
		this.delay = dalay;
	}

	public double distanceTo(SampleData other)
	{
		try {
			return JTS.orthodromicDistance(this.coordinate, other.coordinate, DEFAULT_CRS) ;
		} catch (TransformException e) {
			logger.error("Error calculating projectedDistance", e);
			return 0 ;
		}
	}

	public double fastDistanceTo(SampleData other)
	{
		return Util.cheapDistance(getCoordinate().y, getCoordinate().x, other.getCoordinate().y, other.getCoordinate().x) ;
	}

	@Override
	public String toString() {
		return "SampleData [timestamp=" + timestamp + ", order=" + order + ", lineNumber=" + lineNumber + ", latitude=" + coordinate.y + ", longitude=" + coordinate.x + ", speed=" + speed
				+ ", delay=" + delay + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((order == null) ? 0 : order.hashCode());
		result = prime * result + (int) (timestamp ^ (timestamp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SampleData other = (SampleData) obj;
		if (order == null) {
			if (other.order != null)
				return false;
		} else if (!order.equals(other.order))
			return false;
		if (timestamp != other.timestamp)
			return false;
		return true;
	}
}
