package br.pucrio.bguberfain.onibus.model.graph.v2;

import br.pucrio.bguberfain.onibus.util.Util;

import java.awt.*;

/**
 * Created by Bruno on 14/03/2015.
 */
public enum SpeedColors
{
    LIGHT               (0x00b22d, 1.0, -1),
    MODERATE            (0xff9e00, 0.5, -1),
    HEAVY               (0xff0000, 0.3, -1),
    VERY_HEAVY          (0xbe0000, 0.1, -1),
    BUMPER_TO_BUMPER    (0x440000,  -1, 6 / 3.6 ) ;  // 6 km/h

    private final static Color[] colors = new Color[] {
        new Color(0x440000),    // 0.0
        new Color(0x440000),    // 0.1
        new Color(0xff0000),    // 0.2
        new Color(0xff0000),    // 0.3
        new Color(0xff9e00),    // 0.4
        new Color(0xff9e00),    // 0.5
        new Color(0x00b22d),    // 0.6
        new Color(0x00b22d),    // 0.7
        new Color(0x00b22d),    // 0.8
        new Color(0x00b22d),    // 0.9
        new Color(0x00b22d),    // 1.0
    } ;

    private Color color ;
    private double maxRelativeSpeed ;
    private double maxAbsoluteSpeed ;

    private SpeedColors(int color, double maxRelativeSpeed, double maxAbsoluteSpeed) {
        this.color = new Color(color) ;
        this.maxRelativeSpeed = maxRelativeSpeed ;
        this.maxAbsoluteSpeed = maxAbsoluteSpeed;
    }

    public Color getColor() {
        return color;
    }

    public double getMaxRelativeSpeed() {
        return maxRelativeSpeed;
    }

    public double getMaxAbsoluteSpeed() {
        return maxAbsoluteSpeed;
    }

    /**
     * Find the best speed color for parameters
     *
     * @param absoluteSpeedInMps    absolute speed in meters per seconds
     * @param relativeSpeed         relative speed (ie, current speed divided by max speed) in range 0..1
     * @return                      the closest SpeedColors to parameters (rounded down)
     */
    public static Color speedForRelativeOrAbsolute(double absoluteSpeedInMps, double relativeSpeed) {
//        SpeedColors speedColors[] = SpeedColors.values() ;

        // Check for absolute speed first in reverse order (worst to best traffic)
//        for(int i=speedColors.length-1; i > 0; i--) {
//            SpeedColors speedColor = speedColors[i] ;
//            // First check for absolute match
//            if (absoluteSpeedInMps <= speedColor.maxAbsoluteSpeed) {
//                return speedColor.color ;
//            }
//        }

        // Returns a "smooth transition"
        return gradientColorFor(relativeSpeed) ;
    }

    public static Color gradientColorFor(double relativeSpeed) {
        return Util.gradientColor(colors, (float) relativeSpeed, 1.0f) ;
    }
}
