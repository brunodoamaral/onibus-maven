package br.pucrio.bguberfain.onibus.model.graph.v2;

/**
 * Created by Bruno on 25/01/2015.
 */
public class XYSample
{
    double y;
    double x;
    float colorPercent ;

    public XYSample(double x, double y, float colorPercent) {
        this.y = y;
        this.x = x;
        this.colorPercent = colorPercent ;
    }

    public double getY() {
        return y;
    }

    public double getX() {
        return x;
    }

    public float getColorPercent() {
        return colorPercent;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    public void setColorPercent(float colorPercent) {
        this.colorPercent = colorPercent;
    }

    public static class XYSampleBoundary
    {
        double minY, maxY ;
        double minX, maxX ;

        public XYSampleBoundary() {
        }

        public XYSampleBoundary(double minX, double minY, double maxX, double maxY) {
            this.minY = minY;
            this.maxY = maxY;
            this.minX = minX;
            this.maxX = maxX;
        }

        public boolean updateBoundary(XYSample sample)
        {
            boolean isModified = false ;
            if (sample.getX() < minX) {
                minX = sample.getX() ;
                isModified = true;
            }
            if (sample.getX() > maxX) {
                maxX = sample.getX() ;
                isModified = true;
            }
            if (sample.getY() < minY) {
                minY = sample.getY() ;
                isModified = true;
            }
            if (sample.getY() > maxY) {
                maxY = sample.getY() ;
                isModified = true;
            }

            return isModified ;
        }

        public void fitInside(XYSampleBoundary other) {
            if(this.minY < other.minY) this.minY = other.minY ;
            if(this.maxY > other.maxY) this.maxY = other.maxY ;
            if(this.minX < other.minX) this.minX = other.minX ;
            if(this.maxX > other.maxX) this.maxX = other.maxX ;
        }

        public double getMinY() {
            return minY;
        }

        public double getMaxY() {
            return maxY;
        }

        public double getMinX() {
            return minX;
        }

        public double getMaxX() {
            return maxX;
        }

        public double getArea() {
            return (maxY-minY) * (maxX-minX) ;
        }
    }
}
