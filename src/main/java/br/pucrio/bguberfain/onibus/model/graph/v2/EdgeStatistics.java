package br.pucrio.bguberfain.onibus.model.graph.v2;

import br.pucrio.bguberfain.onibus.util.Util;


public class EdgeStatistics {
	
	private static final int PRECISION_MINUTES = 15 ;	// Compute statistics for every 15 minutes among the day (must be a number that divides 60)
	
	private static final int MINUTES_PER_DAY = 24 * 60 ;
	public static final int MILISSECONDS_PER_MINUTE = 1000*60 ;
	private static final int TIME_SLICES_PER_DAY = MINUTES_PER_DAY / PRECISION_MINUTES ;
	
	double meanSpeedByTimeSlice[] ;
	long sampleSpeedCountByTimeSlice[] ;
	
	public EdgeStatistics()
	{
		meanSpeedByTimeSlice = new double[TIME_SLICES_PER_DAY] ;
		sampleSpeedCountByTimeSlice = new long[TIME_SLICES_PER_DAY] ;
	}
	
	public static int timeSliceForTimeStamp(long timestamp)
	{
		return (int)
			( (
				(timestamp + Util.MILISECONDS_PER_HOUR*Util.DEFAULT_TIMEZONE)		// Ajust timezone
				% (Util.MILISECONDS_PER_HOUR*24)									// Truncate for daily timestamp
			) / MILISSECONDS_PER_MINUTE	)											// Convert to minutes
			/ PRECISION_MINUTES;													// Convert to y slice
	}

	public void updateSpeed( long timestamp, double speed )
	{
		// Find correct slice
		int timeSlice = timeSliceForTimeStamp(timestamp) ;
		
		updateSpeedSlice(timeSlice, speed) ;
	}

	public void updateSpeedSlice( int timeSlice, double speed )
	{
		// Update mean and count for this y slice
		meanSpeedByTimeSlice[timeSlice] = (sampleSpeedCountByTimeSlice[timeSlice] * meanSpeedByTimeSlice[timeSlice] + speed) / (sampleSpeedCountByTimeSlice[timeSlice]+1) ;
		sampleSpeedCountByTimeSlice[timeSlice]++ ;
	}

	public double[] getMeanSpeedByTimeSlice() {
		return meanSpeedByTimeSlice;
	}
	
	public long[] getSampleSpeedCountByTimeSlice() {
		return sampleSpeedCountByTimeSlice;
	}
}
