package br.pucrio.bguberfain.onibus.model.graph.v2;

import com.vividsolutions.jts.geom.Coordinate;

/**
 * Created by Bruno on 01/02/2015.
 */
public class BusStop
{
    Route route ;
    int sequence ;
    Coordinate coordinate ;
    double distanceFromBegin ;

    public BusStop(Route route, int sequence, Coordinate coordinate, double distanceFromBegin) {
        this.route = route;
        this.sequence = sequence;
        this.coordinate = coordinate;
        this.distanceFromBegin = distanceFromBegin;
    }

    public Route getRoute() {
        return route;
    }

    public int getSequence() {
        return sequence;
    }

    public Coordinate getCoordinate() {
        return coordinate;
    }

    public double getDistanceFromBegin() {
        return distanceFromBegin;
    }
}
