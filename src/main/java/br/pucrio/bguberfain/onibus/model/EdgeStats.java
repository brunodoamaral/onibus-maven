package br.pucrio.bguberfain.onibus.model;

import com.vividsolutions.jts.geom.LineSegment;

import java.io.*;
import java.util.Map;

/**
 * Created by bguberfain on 3/20/2015.
 */
public class EdgeStats implements Serializable {

    private static final long serialVersionUID = -3878104254466613398L;

    public double meanByTimeSlice[] ;
    public double varianceByTimeSlice[] ;
    public int numSamples[] ;
    public double maxSpeed ;

    public EdgeStats(int timeSlices) {
        meanByTimeSlice = new double[timeSlices] ;
        varianceByTimeSlice = new double[timeSlices] ;
        numSamples = new int[timeSlices] ;
    }

    public static Map<LineSegment, EdgeStats> loadEdgeStatsByLineString(File folder, TrafficDataPeriod period, int precision) throws IOException, ClassNotFoundException
    {
        File file = new File(folder, "traffic-data-" + period.name().toLowerCase() + "-" + precision + ".dat") ;
        ObjectInputStream in = new ObjectInputStream(new BufferedInputStream(new FileInputStream(file))) ;

        try {
            return (Map<LineSegment, EdgeStats>) in.readObject() ;
        } finally {
            in.close();
        }
    }
}
