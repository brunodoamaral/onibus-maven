package br.pucrio.bguberfain.onibus.model;

/**
 * Created by Bruno on 12/04/2015.
 */
public class MapAverage
{
    int width, height ;
    double sumData[][] ;
    double sumWeight[][] ;
    int count[][] ;

    public MapAverage(int width, int height) {
        this.width = width;
        this.height = height;

        sumData = new double[width][height] ;
        sumWeight = new double[width][height] ;
        count = new int[width][height] ;
    }

    public void add(double x, double y, double value)
    {
        int ix = (int) Math.round(x);
        int iy = (int) Math.round(y);

        if (ix <= 0) {
            ix = 1 ;
        } else if (ix >= width) {
            ix = width-1 ;
        }

        if (iy <= 0) {
            iy = 1 ;
        } else if (iy >= height) {
            iy = height-1 ;
        }

        double wTopLeft =  Math.abs(ix - (x-0.5)) * Math.abs((y+0.5) - iy) ;
        double wTopRight =  Math.abs((x+0.5) - ix) * Math.abs((y+0.5) - iy) ;
        double wBottomLeft =  Math.abs(ix - (x-0.5)) * Math.abs(iy - (y-0.5)) ;
        double wBottomRight =  Math.abs((x+0.5) - ix) * Math.abs(iy - (y-0.5)) ;

        sumData[ix][iy] += value * wBottomRight ;
        sumWeight[ix][iy] += wBottomRight ;
        count[ix][iy]++ ;

        sumData[ix-1][iy-1] += value * wTopLeft ;
        sumWeight[ix-1][iy-1] += wTopLeft ;
        count[ix-1][iy-1]++ ;

        sumData[ix-1][iy] += value * wTopRight ;
        sumWeight[ix-1][iy] += wTopRight ;
        count[ix-1][iy]++ ;

        sumData[ix][iy-1] += value * wBottomLeft ;
        sumWeight[ix][iy-1] += wBottomLeft ;
        count[ix][iy-1]++ ;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public double getResult(int x, int y) {
        return sumData[x][y] / sumWeight[x][y] ;
    }
    public int getCount(int x, int y) {
        return count[x][y] ;
    }
}
