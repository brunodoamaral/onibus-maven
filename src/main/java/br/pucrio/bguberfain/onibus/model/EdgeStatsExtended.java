package br.pucrio.bguberfain.onibus.model;

import com.vividsolutions.jts.geom.LineSegment;

import java.io.*;
import java.util.Map;

/**
 * Created by bguberfain on 3/20/2015.
 */
public class EdgeStatsExtended implements Serializable {

    private static final long serialVersionUID = -1231286488301870188L;

    public double meanByTimeSlice[] ;
    public double varianceByTimeSlice[] ;
    public int numSamples[] ;
    public double percentile10 ;
    public double percentile20 ;
    public double percentile30 ;
    public double percentile40 ;
    public double percentile50 ;
    public double percentile60 ;
    public double percentile70 ;
    public double percentile80 ;
    public double percentile90 ;
    public double percentile95 ;
    public double percentile98 ;

    public EdgeStatsExtended(int timeSlices) {
        meanByTimeSlice = new double[timeSlices] ;
        varianceByTimeSlice = new double[timeSlices] ;
        numSamples = new int[timeSlices] ;
    }

    public static Map<LineSegment, EdgeStatsExtended> loadEdgeStatsByLineString(File folder, TrafficDataPeriod period, int precision) throws IOException, ClassNotFoundException
    {
        File file = new File(folder, "traffic-data-extended-" + period.name().toLowerCase() + "-" + precision + ".dat") ;
        ObjectInputStream in = new ObjectInputStream(new BufferedInputStream(new FileInputStream(file))) ;

        try {
            return (Map<LineSegment, EdgeStatsExtended>) in.readObject() ;
        } finally {
            in.close();
        }
    }
}
