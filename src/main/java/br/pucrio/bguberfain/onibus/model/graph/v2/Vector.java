package br.pucrio.bguberfain.onibus.model.graph.v2;

import java.awt.geom.Point2D;

public class Vector {
	private double x;
	private double y;

	public Vector(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	public Vector(Point2D.Double from, Point2D.Double to)
	{
		this(to.x-from.x, to.y-from.y) ;
	}
	
	public Vector add(Vector vector) {
		return new Vector(this.x + vector.x, this.y + vector.y);
	}

	public Vector minus(Vector vector) {
		return new Vector(this.x - vector.x, this.y - vector.y);
	}

	public double dot(Vector vector) {
		return this.x * vector.x + this.y * vector.y;
	}

	public Vector scale(double scalar) {
		return new Vector(this.x * scalar, this.y * scalar);
	}
	
	public double modulus()
	{
		return Math.sqrt(x*x + y*y) ;
	}
}