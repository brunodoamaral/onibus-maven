package br.pucrio.bguberfain.onibus.model;

import org.apache.commons.math3.stat.descriptive.StorelessUnivariateStatistic;
import org.apache.commons.math3.stat.descriptive.moment.Mean;
import org.apache.commons.math3.stat.descriptive.moment.Variance;
import org.apache.commons.math3.stat.descriptive.rank.PSquarePercentile;

/**
 * Created by bguberfain on 3/20/2015.
 */
public class DynamicEdgeStats {
    StorelessUnivariateStatistic percentile10 ;
    StorelessUnivariateStatistic percentile20 ;
    StorelessUnivariateStatistic percentile30 ;
    StorelessUnivariateStatistic percentile40 ;
    StorelessUnivariateStatistic percentile50 ;
    StorelessUnivariateStatistic percentile60 ;
    StorelessUnivariateStatistic percentile70 ;
    StorelessUnivariateStatistic percentile80 ;
    StorelessUnivariateStatistic percentile90 ;
    StorelessUnivariateStatistic percentile95 ;
    StorelessUnivariateStatistic percentile98 ;

    StorelessUnivariateStatistic meanSpeed[] ;
    StorelessUnivariateStatistic variance[] ;

    public DynamicEdgeStats(int timeSlices) {
        percentile10 = new PSquarePercentile(10) ;
        percentile20 = new PSquarePercentile(20) ;
        percentile30 = new PSquarePercentile(30) ;
        percentile40 = new PSquarePercentile(40) ;
        percentile50 = new PSquarePercentile(50) ;
        percentile60 = new PSquarePercentile(60) ;
        percentile70 = new PSquarePercentile(70) ;
        percentile80 = new PSquarePercentile(80) ;
        percentile90 = new PSquarePercentile(90) ;
        percentile95 = new PSquarePercentile(95) ;
        percentile98 = new PSquarePercentile(98) ;

        meanSpeed = new StorelessUnivariateStatistic[timeSlices] ;
        variance = new StorelessUnivariateStatistic[timeSlices] ;
        for(int i=0; i<timeSlices; i++) {
            meanSpeed[i] = new Mean() ;
            variance[i] = new Variance() ;
        }
    }

    public double getMeanSpeed(int idx) {
        return meanSpeed[idx].getResult() ;
    }

    public double getVariance(int idx) {
        return variance[idx].getResult() ;
    }

    public double getPercentile10() {
        return percentile10.getResult();
    }

    public double getPercentile20() {
        return percentile20.getResult();
    }

    public double getPercentile30() {
        return percentile30.getResult();
    }

    public double getPercentile40() {
        return percentile40.getResult();
    }

    public double getPercentile50() {
        return percentile50.getResult();
    }

    public double getPercentile60() {
        return percentile60.getResult();
    }

    public double getPercentile70() {
        return percentile70.getResult();
    }

    public double getPercentile80() {
        return percentile80.getResult();
    }

    public double getPercentile90() {
        return percentile90.getResult();
    }

    public double getPercentile95() {
        return percentile95.getResult();
    }

    public double getPercentile98() {
        return percentile98.getResult();
    }


    public long getN(int idx) {
        return meanSpeed[idx].getN() ;
    }

    public synchronized void increment(int idx, double value) {
        percentile10.increment(value);
        percentile20.increment(value);
        percentile30.increment(value);
        percentile40.increment(value);
        percentile50.increment(value);
        percentile60.increment(value);
        percentile70.increment(value);
        percentile80.increment(value);
        percentile90.increment(value);
        percentile95.increment(value);
        percentile98.increment(value);

        meanSpeed[idx].increment(value);
        variance[idx].increment(value);
    }

    public EdgeStats snapshot() {
        int timeSlices = meanSpeed.length ;
        EdgeStats stats = new EdgeStats(timeSlices);

        for( int i=0; i<timeSlices; i++) {
            stats.meanByTimeSlice[i] = getMeanSpeed(i) ;
            stats.varianceByTimeSlice[i] = getVariance(i) ;
            stats.numSamples[i] = (int) getN(i) ;
        }

        stats.maxSpeed = getPercentile98() ;

        return stats ;
    }

    public EdgeStatsExtended snapshotExtended() {
        int timeSlices = meanSpeed.length ;
        EdgeStatsExtended stats = new EdgeStatsExtended(timeSlices);

        for( int i=0; i<timeSlices; i++) {
            stats.meanByTimeSlice[i] = getMeanSpeed(i) ;
            stats.varianceByTimeSlice[i] = getVariance(i) ;
            stats.numSamples[i] = (int) getN(i) ;
        }

        stats.percentile10 = getPercentile10() ;
        stats.percentile20 = getPercentile20();
        stats.percentile30 = getPercentile30();
        stats.percentile40 = getPercentile40();
        stats.percentile50 = getPercentile50();
        stats.percentile60 = getPercentile60();
        stats.percentile70 = getPercentile70();
        stats.percentile80 = getPercentile80();
        stats.percentile90 = getPercentile90();
        stats.percentile95 = getPercentile95();
        stats.percentile98 = getPercentile98();

        return stats ;
    }
}
