package br.pucrio.bguberfain.onibus.model.graph.v2;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.LineSegment;

import java.util.*;

/**
 * Created by Bruno on 10/03/2015.
 */
public class SharedRouteEdge<T>
{
    private static final Map<LineSegment, SharedRouteEdge> edgeById = new HashMap<>() ;

    LineSegment id ;
    Coordinate from ;
    Coordinate to ;
    T data ;
    Set<Route> routes ;

    public SharedRouteEdge(Coordinate from, Coordinate to) {
        this.id = generateId(from, to);
        this.from = from;
        this.to = to;
        routes = new HashSet<>() ;
    }

    public static synchronized <K> SharedRouteEdge<K> getInstance(Coordinate from, Coordinate to)
    {
        LineSegment id = generateId(from, to) ;
        SharedRouteEdge foundEdge = edgeById.get(id) ;
        if (foundEdge == null) {
            foundEdge = new SharedRouteEdge(from, to) ;
            edgeById.put(id, foundEdge) ;
        }

        return foundEdge ;
    }

    private static LineSegment generateId(Coordinate from, Coordinate to)
    {
        return new LineSegment(from, to) ;
    }

    public LineSegment getId() {
        return id;
    }

    public Coordinate getFrom() {
        return from;
    }

    public Coordinate getTo() {
        return to;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public static Collection<SharedRouteEdge> allEdges()
    {
        return edgeById.values() ;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SharedRouteEdge that = (SharedRouteEdge) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
