package br.pucrio.bguberfain.onibus.reader.sequence;

import java.util.Collection;

import br.pucrio.bguberfain.onibus.model.graph.v2.SampleData;

public class MulticastSequenceReader<T extends SequenceReader> implements SequenceReader<SampleData> {
	
	Collection<T> sequenceReaders ;

	public MulticastSequenceReader(Collection<T> sequenceReaders) {
		super();
		this.sequenceReaders = sequenceReaders;
	}

	@Override
	public void beginSequence(SampleData firstData) {
		for (SequenceReader seqReader : sequenceReaders) {
			seqReader.beginSequence(firstData) ;
		}
	}

	@Override
	public void sequence(SampleData lastData, SampleData newData) {
		for (SequenceReader seqReader : sequenceReaders) {
			seqReader.sequence(lastData, newData) ;
		}
	}

	@Override
	public void endSequence(SampleData lastData) {
		for (SequenceReader seqReader : sequenceReaders) {
			seqReader.endSequence(lastData) ;
		}
	}

}
