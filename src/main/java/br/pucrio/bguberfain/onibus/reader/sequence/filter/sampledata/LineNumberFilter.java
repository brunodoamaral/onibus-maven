package br.pucrio.bguberfain.onibus.reader.sequence.filter.sampledata;

import br.pucrio.bguberfain.onibus.model.graph.v2.SampleData;
import br.pucrio.bguberfain.onibus.reader.sequence.filter.DataFilter;

/**
 * Created by Bruno on 25/01/2015.
 */
public class LineNumberFilter extends DataFilter<SampleData>
{
    private String lineNumber ;

    public LineNumberFilter(DataFilter<SampleData> nextFilter, String lineNumber) {
        super(nextFilter);
        this.lineNumber = lineNumber;
    }

    public LineNumberFilter(String lineNumber) {
        this.lineNumber = lineNumber;
    }

    @Override
    protected SampleData doFilter(SampleData data) {
        if (lineNumber.equals(data.getLineNumber())) {
            return data;
        } else {
            return null ;
        }
    }
}
