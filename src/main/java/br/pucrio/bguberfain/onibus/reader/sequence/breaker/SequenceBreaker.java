package br.pucrio.bguberfain.onibus.reader.sequence.breaker;

public abstract class SequenceBreaker<T> {
	
	private SequenceBreaker<T> nextBreaker ;
	
	public SequenceBreaker(SequenceBreaker<T> nextBreaker)
	{
		this() ;
		this.nextBreaker = nextBreaker ;
	}
	
	public SequenceBreaker() {
		super() ;
	}
	
	public final boolean sequenceShouldContinueBetween(T last, T current)
	{
		if ( doSequenceShouldContinueBetween(last, current) ) {
			if( nextBreaker != null ) {
				return nextBreaker.sequenceShouldContinueBetween(last, current) ;
			} else {
				return true ;
			}
		}
		
		return false ;
	}
	
	protected abstract boolean doSequenceShouldContinueBetween(T last, T current) ;
}
