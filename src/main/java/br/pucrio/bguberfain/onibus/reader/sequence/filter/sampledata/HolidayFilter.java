package br.pucrio.bguberfain.onibus.reader.sequence.filter.sampledata;

import br.pucrio.bguberfain.onibus.model.graph.v2.SampleData;
import br.pucrio.bguberfain.onibus.reader.sequence.filter.DataFilter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by Bruno on 26/04/2015.
 */
public class HolidayFilter extends DataFilter<SampleData>
{
    private static final long MS_IN_AN_HOUR = 1000 * 60 * 60 ;
    private static final long MS_IN_A_DAY = MS_IN_AN_HOUR * 24 ;
    TimeZone timeZone ;
    static final long [] dayTimestampHolidays ;

    static {
        String [] holidaysInRio = new String[] { "01/01/2014", "20/01/2014", "28/02/2014", "03/03/2014", "04/03/2014", "05/03/2014", "17/04/2014", "18/04/2014",
                "21/04/2014", "23/04/2014", "01/05/2014", "18/06/2014", "19/06/2014", "25/06/2014", "04/07/2014", "15/10/2014", "20/10/2014", "28/10/2014",
                "20/11/2014", "24/12/2014", "25/12/2014", "31/12/2014", "01/01/2015", "20/01/2015", "13/02/2015", "16/02/2015", "17/02/2015", "18/02/2015",
                "02/04/2015", "03/04/2015", "21/04/2015", "23/04/2015", "01/05/2015", "04/06/2015", "07/09/2015", "12/10/2015", "15/10/2015", "19/10/2015",
                "28/10/2015", "02/11/2015", "20/11/2015", "24/12/2015", "25/12/2015", "31/12/2015" } ;

        long[] tmpTimestampHolidays = new long[holidaysInRio.length] ;

        SimpleDateFormat holidayParse = new SimpleDateFormat("dd/MM/yyyy") ;
        try {
            for( int i=0; i<holidaysInRio.length; i++ ) {
                tmpTimestampHolidays[i] = (((Date) holidayParse.parseObject(holidaysInRio[i])).getTime()) / MS_IN_A_DAY ;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Arrays.sort(tmpTimestampHolidays);

        dayTimestampHolidays = tmpTimestampHolidays ;
    }

    public HolidayFilter(DataFilter<SampleData> nextFilter) {
        super(nextFilter);
        timeZone = TimeZone.getTimeZone("America/Sao_Paulo") ;
    }

    public HolidayFilter() {
        this(null) ;
    }

    public static boolean isHoliday(long timestamp) {
        return Arrays.binarySearch(dayTimestampHolidays, timestamp / MS_IN_A_DAY) >= 0 ;
    }

    @Override
    protected SampleData doFilter(SampleData data) {
        long timestamp = data.getTimestamp() + timeZone.getOffset(data.getTimestamp()) ;
        if ( isHoliday(timestamp) ) {
            return null ;
        } else {
            return data;
        }
    }
}
