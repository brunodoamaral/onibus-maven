package br.pucrio.bguberfain.onibus.reader.sequence.breaker.sampledata;

import br.pucrio.bguberfain.onibus.model.graph.v2.SampleData;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.SequenceBreaker;

public class LeastIntervalBreaker extends SequenceBreaker<SampleData> {
	
	private long longestInterval ;
	
	public LeastIntervalBreaker(SequenceBreaker nextBreaker, long longestInterval) {
		super(nextBreaker);
		this.longestInterval = longestInterval;
	}

	public LeastIntervalBreaker(long longestInterval) {
		super();
		this.longestInterval = longestInterval;
	}

	@Override
	protected boolean doSequenceShouldContinueBetween(SampleData last, SampleData current) {
		if ( last != null ) {
			long interval = current.getTimestamp() - last.getTimestamp() ;
			// Check interval and validate it (it should be positive) 
			if ( interval > 0 && interval <= longestInterval ) {
				return true ;
			} else {
				return false ;
			}
		} else {
			return true ;
		}
	}
}
