package br.pucrio.bguberfain.onibus.reader.sequence;

import br.pucrio.bguberfain.onibus.reader.sequence.breaker.SequenceBreaker;
import br.pucrio.bguberfain.onibus.reader.sequence.filter.DataFilter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.function.Function;

/**
 * Created by bguberfain on 1/7/2015.
 */
public class SimpleSequenceGenerator<T> implements SequenceGenerator<T>
{
    private static final Logger logger = LogManager.getLogger(SimpleSequenceGenerator.class);

    SequenceBreaker sequenceBreaker ;
    Iterator<T> sampleReader ;
    DataFilter<T>filter ;
    Function<T, ? extends Object> idGetter ;
    Function<T, Boolean> isAnnotation ;
    boolean stopped ;

    private long logEvery = 10000000 ;

    public SimpleSequenceGenerator(SequenceBreaker sequenceBreaker, DataFilter<T> filter, Iterator<T> sampleReader, Function<T, ? extends Object> idGetter, Function<T, Boolean> isAnnotation) {
        super();

        if (sequenceBreaker == null) throw new NullPointerException("Must specify a SequenceBreaker") ;
        if (sampleReader == null) throw new NullPointerException("Must specify a SampleData Reader") ;

        this.sequenceBreaker = sequenceBreaker;
        this.sampleReader = sampleReader;
        this.filter = filter ;
        this.idGetter = idGetter ;
        this.isAnnotation = isAnnotation ;
    }

    public void setLogEvery(long logEvery) {
        this.logEvery = logEvery;
    }

    public void readData(SequenceReader sequenceReader)
    {
        Map<Object, T> lastById = new HashMap<>() ;

        long cnt = 0 ;
        long startTime = System.currentTimeMillis() ;

        while( sampleReader.hasNext() && !stopped ) {
            if(cnt++ % logEvery == logEvery-1) {
                logger.info("Read {} samples at {} samples/ms", cnt, cnt / (System.currentTimeMillis() - startTime));
            }

            T current = sampleReader.next() ;

            if(isAnnotation.apply(current)) {
                sequenceReader.beginAnnotation(current);
            } else {
                if (filter != null) {
                    current = filter.filter(current);
                    if (current == null) {
                        continue;
                    }
                }

                Object currentId = idGetter.apply(current);

                T last = lastById.get(currentId);

                lastById.put(currentId, current);

                // Detect begin of sequence
                if (last == null) {
                    sequenceReader.beginSequence(current);
                } else {
                    if (sequenceBreaker.sequenceShouldContinueBetween(last, current)) {
                        sequenceReader.sequence(last, current);
                    } else {
                        sequenceReader.endSequence(last);
                        sequenceReader.beginSequence(current);
                    }
                }
            }
        }

        // Finalize all sequences
        for( T lastSample : lastById.values() ) {
            sequenceReader.endSequence(lastSample) ;
        }
    }

    @Override
    public void stop() {
        stopped = true ;
    }
}
