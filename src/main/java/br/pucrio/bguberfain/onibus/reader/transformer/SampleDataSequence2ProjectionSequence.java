package br.pucrio.bguberfain.onibus.reader.transformer;

import br.pucrio.bguberfain.onibus.model.graph.v2.ProjectionInfo;
import br.pucrio.bguberfain.onibus.model.graph.v2.Route;
import br.pucrio.bguberfain.onibus.model.graph.v2.SampleData;
import br.pucrio.bguberfain.onibus.reader.sequence.SequenceGenerator;
import br.pucrio.bguberfain.onibus.reader.sequence.SequenceReader;
import com.vividsolutions.jts.algorithm.Angle;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

/**
 * Created by Bruno on 25/01/2015.
 */
public class SampleDataSequence2ProjectionSequence implements SequenceGenerator<ProjectionInfo<SampleData>>
{
    private static final double MAX_SEARCH_DISTANCE = 40 ;		// 40m
    private static final Logger logger = LogManager.getLogger(SampleDataSequence2ProjectionSequence.class);

    Map<String, Collection<Route>> routesByLineNumber ;
    Map<String, ProjectionInfo<SampleData>> lastProjectionByOrder ;
    SequenceGenerator<SampleData> sampleGenerator ;

    public SampleDataSequence2ProjectionSequence(Collection<Route> routes, SequenceGenerator<SampleData> sampleGenerator)
    {
        this.sampleGenerator = sampleGenerator ;
        lastProjectionByOrder = new HashMap<>() ;

        // Create map of routes by line number
        routesByLineNumber = new HashMap<>();
        for( Route route : routes ) {
            Collection<Route> otherRoutesForThisLineNumber = routesByLineNumber.get(route.getLineNumber()) ;
            if ( otherRoutesForThisLineNumber == null ) {
                otherRoutesForThisLineNumber = new ArrayList<>(2) ;
                routesByLineNumber.put(route.getLineNumber(), otherRoutesForThisLineNumber) ;
            }
            otherRoutesForThisLineNumber.add(route) ;
        }
    }

    @Override
    public void stop() {
        sampleGenerator.stop() ;
    }

    @Override
    public void readData(SequenceReader<ProjectionInfo<SampleData>> sequenceReader) {
        sampleGenerator.readData(new Sample2ProjectionTransformer(sequenceReader));
    }

    private class Sample2ProjectionTransformer implements SequenceReader<SampleData> {
        SequenceReader<ProjectionInfo<SampleData>> outReader;

        Sample2ProjectionTransformer(SequenceReader<ProjectionInfo<SampleData>> outReader)
        {
            this.outReader = outReader ;
        }

        @Override
        public void beginSequence(SampleData firstData) {
        }

        @Override
        public void sequence(SampleData lastData, SampleData newData) {
            try {
                Collection<Route> routes = routesByLineNumber.get(newData.getLineNumber());
                if (routes == null) {
                    //logger.debug("Line number has no routes defined: " + newData.getLineNumber());
                } else {
                    // Make a prediction for this sample to evaluate the error
                    double sequenceAngle = Angle.angle(lastData.getCoordinate(), newData.getCoordinate());
                    double minAngle = Double.MAX_VALUE;
                    ProjectionInfo<SampleData> lastProjection = null;
                    ProjectionInfo<SampleData> currentProjection = null;

                    for (Route route : routes) {
                        ProjectionInfo<SampleData> lastRouteProjection = ProjectionInfo.project(route, lastData);

                        if (lastRouteProjection != null) {
                            ProjectionInfo<SampleData> currentRouteProjection = ProjectionInfo.projectAfter(route, newData, lastRouteProjection);

                            if (currentRouteProjection != null && currentRouteProjection.getProjectedDistance() < MAX_SEARCH_DISTANCE) {
                                // Make the projected angle relative to route direction
                                double projectedAngle;
                                if (currentRouteProjection.getLocation().compareTo(lastRouteProjection.getLocation()) < 0) {
                                    projectedAngle = Angle.angle(currentRouteProjection.getProjectedCoordinate(), lastRouteProjection.getProjectedCoordinate());
                                } else {
                                    projectedAngle = Angle.angle(lastRouteProjection.getProjectedCoordinate(), currentRouteProjection.getProjectedCoordinate());
                                }

                                double diffAngle = Angle.diff(sequenceAngle, projectedAngle);    // Note: both angles are already normalized by Angle.angle
                                if (diffAngle < minAngle) {
                                    minAngle = diffAngle;
                                    currentProjection = currentRouteProjection;
                                    lastProjection = lastRouteProjection;
                                }
                            }
                        }
                    }

                    // Found a valid projection for the new sample
                    if (currentProjection != null) {
                        // Save this for later use
                        synchronized (lastProjectionByOrder) {
                            // Starts a sequence if it is the first projection for this order
                            if ( !lastProjectionByOrder.containsKey(newData.getOrder()) ) {
                                outReader.beginSequence(lastProjection);
                            }
                            lastProjectionByOrder.put(newData.getOrder(), currentProjection);
                        }

                        outReader.sequence(lastProjection, currentProjection);
                    }
                }
            } catch (Exception e) {
                logger.error("Error while updating model", e);
                e.printStackTrace();
            }
        }

        @Override
        public void endSequence(SampleData lastData) {
            // Remove location when finishing this sequence
            ProjectionInfo<SampleData> lastProjection;
            synchronized (lastProjectionByOrder) {
                lastProjection = lastProjectionByOrder.remove(lastData.getOrder());
            }

            if (lastProjection != null) {
                outReader.endSequence(lastProjection);
            }
        }

        @Override
        public void beginAnnotation(SampleData annotation) {
            ProjectionInfo pi = new ProjectionInfo(null, annotation, null, 0, null) ;
            outReader.beginAnnotation(pi);
        }
    }
}
