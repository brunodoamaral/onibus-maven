package br.pucrio.bguberfain.onibus.reader.sequence.breaker.sampledata;

import br.pucrio.bguberfain.onibus.model.graph.v2.SampleData;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.SequenceBreaker;

public class LongDistanceBreaker extends SequenceBreaker<SampleData> {

	private long longuestDistance;

	public LongDistanceBreaker(SequenceBreaker nextBreaker, long longuestDistance) {
		super(nextBreaker);
		this.longuestDistance = longuestDistance;
	}

	public LongDistanceBreaker(long longuestDistance) {
		super();
		this.longuestDistance = longuestDistance;
	}

	@Override
	protected boolean doSequenceShouldContinueBetween(SampleData last, SampleData current) {
		if (last != null) {
			// Assume next is newer...
			return current.distanceTo(last) <= longuestDistance ;
		} else {
			return true;
		}
	}
}
