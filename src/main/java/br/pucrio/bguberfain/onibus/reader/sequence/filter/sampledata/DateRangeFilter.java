package br.pucrio.bguberfain.onibus.reader.sequence.filter.sampledata;

import br.pucrio.bguberfain.onibus.model.graph.v2.SampleData;
import br.pucrio.bguberfain.onibus.reader.sequence.filter.DataFilter;

import java.util.Date;

/**
 * Created by bguberfain on 3/20/2015.
 */
public class DateRangeFilter extends DataFilter<SampleData> {
    long fromDate, toDate ;

    public DateRangeFilter(Date fromDate, Date toDate) {
        this(fromDate, toDate, null) ;
    }

    public DateRangeFilter(Date fromDate, Date toDate, DataFilter<SampleData> nextFilter) {
        super(nextFilter) ;

        this.fromDate = fromDate.getTime() ;
        this.toDate = toDate.getTime() ;
    }

    @Override
    protected SampleData doFilter(SampleData data) {
        if (data.getTimestamp() < fromDate || data.getTimestamp() > toDate ) {
            return null;
        } else {
            return data ;
        }
    }
}
