package br.pucrio.bguberfain.onibus.reader.sequence.filter.sampledata;

import br.pucrio.bguberfain.onibus.model.graph.v2.SampleData;
import br.pucrio.bguberfain.onibus.reader.sequence.filter.DataFilter;

public class MaxDelayFilter extends DataFilter<SampleData> {

    int maxDelayInSeconds;

	public MaxDelayFilter(int maxDelayInSeconds) {
		this(maxDelayInSeconds, null) ;
	}

	public MaxDelayFilter(int maxDelayInSeconds, DataFilter nextFilter) {
        super(nextFilter) ;
        this.maxDelayInSeconds = maxDelayInSeconds ;
	}

	@Override
	protected SampleData doFilter(SampleData data) {
		int delay = data.getDelay() ;	// Delay is expressed in seconds

        if (delay > maxDelayInSeconds || delay < -maxDelayInSeconds) {
            return null ;
        }

		return data ;
	}

}
