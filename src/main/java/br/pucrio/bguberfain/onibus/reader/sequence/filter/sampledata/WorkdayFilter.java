package br.pucrio.bguberfain.onibus.reader.sequence.filter.sampledata;

import br.pucrio.bguberfain.onibus.model.graph.v2.SampleData;
import br.pucrio.bguberfain.onibus.reader.sequence.filter.DataFilter;
import org.apache.commons.lang3.time.FastDateFormat;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Bruno on 25/01/2015.
 */
public class WorkdayFilter extends HolidayFilter
{
    private static final long MS_IN_AN_HOUR = 1000 * 60 * 60 ;
    private static final long MS_IN_A_DAY = MS_IN_AN_HOUR * 24 ;
    private static final long MS_IN_A_WEEK = MS_IN_A_DAY * 7 ;
    private long firstTimeDOW ;
    TimeZone timeZone ;

    public WorkdayFilter(DataFilter<SampleData> nextFilter) {
        super(nextFilter);

        timeZone = TimeZone.getTimeZone("America/Sao_Paulo") ;
        Calendar zeroTimestamp = Calendar.getInstance(timeZone) ;
        int offTime = timeZone.getOffset(0) ;
        zeroTimestamp.setTimeInMillis(0 - offTime);
        firstTimeDOW = zeroTimestamp.get(Calendar.DAY_OF_WEEK) ;
    }

    public WorkdayFilter() {
        this(null) ;
    }

    private int timestampDOW(long timestamp)
    {
        int dow = (int)(firstTimeDOW + timestamp % MS_IN_A_WEEK / MS_IN_A_DAY) % 7 ;
        if (dow == 0) {
            return 7 ;
        }
        return dow ;
    }

    @Override
    protected SampleData doFilter(SampleData data) {
        long timestamp = data.getTimestamp() + timeZone.getOffset(data.getTimestamp()) ;
        int dayOfWeek = timestampDOW(timestamp) ;

        if ( dayOfWeek == Calendar.SATURDAY || dayOfWeek == Calendar.SUNDAY ) {
            return null ;
        } else {
            return super.doFilter(data) ;
        }
    }

    public static void main(String []args) throws Exception
    {
        WorkdayFilter me = new WorkdayFilter();
        Calendar current = Calendar.getInstance() ;
        FastDateFormat format = FastDateFormat.getInstance("dd-MM-yyyy HH:mm:ss ZZ", TimeZone.getTimeZone("America/Sao_Paulo")) ;
        Random rnd = new Random() ;
        long cnt = 0 ;
        long correct = 0 ;
        for (int i=0; i<1000; i++) {
            for(int j=0; j<24; j++) {
                int correctDOW = current.get(Calendar.DAY_OF_WEEK);
                int myDOW = me.timestampDOW(current.getTimeInMillis() + me.timeZone.getOffset(current.getTimeInMillis()));

                cnt ++;
                if (correctDOW != myDOW) {
                    System.out.printf("Correct = %d, my = %d (%.2f%% correct) (%s)\n", correctDOW, myDOW, correct * 100.0 / cnt, format.format(current.getTime()));
                } else {
                    correct++ ;
                }

                long v = rnd.nextLong() ;
                while (v < 0) {
                    v = rnd.nextLong() ;
                }
                current.setTimeInMillis(v);
            }
            current.add(Calendar.DAY_OF_MONTH, 1);
        }
        System.out.printf("%.2f%% were correct\n", correct * 100.0 / cnt);

        // Test holidays
        String [] holidays = new String[] { "01/01/2014", "20/01/2014", "28/02/2014", "03/03/2014", "04/03/2014", "05/03/2014", "17/04/2014", "18/04/2014",
                "21/04/2014", "23/04/2014", "01/05/2014", "18/06/2014", "19/06/2014", "25/06/2014", "04/07/2014", "15/10/2014", "20/10/2014", "28/10/2014",
                "20/11/2014", "24/12/2014", "25/12/2014", "31/12/2014", "01/01/2015", "20/01/2015", "13/02/2015", "16/02/2015", "17/02/2015", "18/02/2015",
                "02/04/2015", "03/04/2015", "21/04/2015", "23/04/2015", "01/05/2015", "04/06/2015", "07/09/2015", "12/10/2015", "15/10/2015", "19/10/2015",
                "28/10/2015", "02/11/2015", "20/11/2015", "24/12/2015", "25/12/2015", "31/12/2015" } ;
        SimpleDateFormat holidayParse = new SimpleDateFormat("dd/MM/yyyy") ;

        correct = 0 ;
        cnt = 0 ;
        for(int i=0; i<1000; i++) {
            String strDate = holidays[rnd.nextInt(holidays.length)] ;
            Date date = holidayParse.parse(strDate) ;
            Calendar calendar = Calendar.getInstance() ;
            calendar.setTime(date);

            calendar.set(Calendar.HOUR_OF_DAY, rnd.nextInt(24));


            if ( me.isHoliday(calendar.getTimeInMillis() + me.timeZone.getOffset(calendar.getTimeInMillis())) ) {
                correct++ ;
            }
            cnt++ ;
        }

        System.out.printf("%.2f%% holidays were correct\n", correct * 100.0 / cnt);
    }
}
