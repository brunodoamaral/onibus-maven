package br.pucrio.bguberfain.onibus.reader.sequence;

import br.pucrio.bguberfain.onibus.reader.sequence.breaker.SequenceBreaker;
import br.pucrio.bguberfain.onibus.reader.sequence.filter.DataFilter;
import org.apache.commons.math3.stat.descriptive.moment.Mean;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Array;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.function.Function;

public class ThreadedSequenceGenerator<T> implements SequenceGenerator<T>
{
	private static final Logger logger = LogManager.getLogger(ThreadedSequenceGenerator.class);
	
	SequenceBreaker sequenceBreaker ;
	Iterator<T> sampleReader ;
	DataFilter<T>filter ;
	Function<T, ? extends Object> idGetter ;
	private static final int MAX_QUEUE_SIZE = 1000 ;
	private static final int CHECK_QUEUE_SIZE_EVERY = 10000 ;
	private long logEvery = 10000000 ;

	public void setLogEvery(long logEvery) {
		this.logEvery = logEvery;
	}

	public ThreadedSequenceGenerator(SequenceBreaker sequenceBreaker, DataFilter<T> filter, Iterator<T> sampleReader, Function<T, ? extends Object> idGetter) {
		super();

		if (sequenceBreaker == null) throw new NullPointerException("Must specify a SequenceBreaker") ;
		if (sampleReader == null) throw new NullPointerException("Must specify a T Reader") ;

		this.sequenceBreaker = sequenceBreaker;
		this.sampleReader = sampleReader;
		this.filter = filter ;
		this.idGetter = idGetter ;
	}

    @Override
    public void stop() {
        // TODO: Implementar este método
    }

    enum ReadDataExecutorType {
		EXECUTOR_BEGIN, EXECUTOR_SEQUENCE, EXECUTOR_END;
	}

	private class ReadDataExecutor extends Thread implements SequenceReader<T>
	{
		private class ReadDataExecutorTask {
			public ReadDataExecutorTask(ReadDataExecutorType type, T param1, T param2) {
				super();
				this.type = type;
				this.param1 = param1;
				this.param2 = param2;
			}
			ReadDataExecutorType type ;
			T param1 ;
			T param2 ;
		}

		private boolean finished = false ;
		Queue<List<ReadDataExecutorTask>> tasks ;
		SequenceReader target ;
		int taskId ;

		public ReadDataExecutor(SequenceReader target, int taskId)
		{
			tasks = new ConcurrentLinkedDeque<>() ;
			this.target = target ;
			this.taskId = taskId ;
		}

		public void finish()
		{
			finished = true ;
		}

		public int queueSize()
		{
			return tasks.size() ;
		}

		@Override
		public void run() {
			try {
				while ( !finished )
				{
					List<ReadDataExecutorTask> myTasks = tasks.poll() ;
					if ( myTasks != null ) {
							myTasks.forEach( task -> {
								switch (task.type) {
									case EXECUTOR_BEGIN:
										target.beginSequence(task.param1);
										break;
									case EXECUTOR_SEQUENCE:
										target.sequence(task.param1, task.param2);
										break;
									case EXECUTOR_END:
										target.endSequence(task.param1);
										break;
								}
							} );
					} else {
						Thread.sleep(1000);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		Map<Object, List<ReadDataExecutorTask>> tasksByOrderId = new HashMap<>();

		List<ReadDataExecutorTask> tasksForOrderId(Object id)
		{
			List<ReadDataExecutorTask> tasks = tasksByOrderId.get(id) ;
			if ( tasks == null ) {
				tasks = new LinkedList<>() ;
				tasksByOrderId.put(id, tasks) ;
			}
			return tasks ;
		}

		@Override
		public void beginSequence(T firstData) {
			try {
				Object currentId = idGetter.apply(firstData) ;
				tasksForOrderId(currentId).add(new ReadDataExecutorTask(ReadDataExecutorType.EXECUTOR_BEGIN, firstData, null)) ;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		@Override
		public void sequence(T lastData, T newData) {
			try {
				Object lastId = idGetter.apply(lastData) ;
				tasksForOrderId(lastId).add(new ReadDataExecutorTask(ReadDataExecutorType.EXECUTOR_SEQUENCE, lastData, newData)) ;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		@Override
		public void endSequence(T lastData) {
			try {
				Object lastId = idGetter.apply(lastData) ;
				List<ReadDataExecutorTask> myTasks = tasksForOrderId(lastId) ;
				myTasks.add(new ReadDataExecutorTask(ReadDataExecutorType.EXECUTOR_END, lastData, null)) ;
				//if ( myTasks.size() > 20 ) {
					tasks.offer(new ArrayList<>(myTasks));
					myTasks.clear();
				//}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public void readData(SequenceReader sequenceReader)
	{
		Map<Object, T> lastByOrderId = new HashMap<>() ;

		long cnt = 0 ;

		long startTime = System.currentTimeMillis() ;

		Map<Object, ReadDataExecutor> executorByOrderId = new HashMap<>() ;

		int numCores = Runtime.getRuntime().availableProcessors();
		// Generate array in runtime to avoid 'generic array creation' error
		ReadDataExecutor executorPool[] = (ReadDataExecutor[]) Array.newInstance(ReadDataExecutor.class, numCores-1) ;

		for( int i=0; i<executorPool.length; i++ ) {
			executorPool[i] = new ReadDataExecutor(sequenceReader, i+1) ;
			executorPool[i].start() ;
		}
		int nextThread = 0 ;

		while( sampleReader.hasNext() ) {
			if(cnt++ % logEvery == 0) {
				logger.info("Read {} samples at {} samples/ms", cnt, cnt / (System.currentTimeMillis() - startTime));
			}

			T current = sampleReader.next() ;
			if (filter != null) {
				current = filter.filter(current) ;
				if ( current == null ) {
					continue ;
				}
			}

			Object currentId = idGetter.apply(current) ;
			T last = lastByOrderId.get(currentId) ;

			ReadDataExecutor executor = executorByOrderId.get(currentId);
			if ( executor == null ) {
				executor = executorPool[nextThread] ;
				nextThread = (nextThread + 1) % executorPool.length ;
				executorByOrderId.put(currentId, executor) ;
			}

			lastByOrderId.put(currentId, current) ;

			// Detect begin of sequence
			if (last == null) {
				executor.beginSequence(current) ;
			} else {
				if ( sequenceBreaker.sequenceShouldContinueBetween(last, current) ) {
					executor.sequence(last, current) ;
				} else {
					executor.endSequence(last) ;
					executor.beginSequence(current) ;
				}
			}

			if ( ++cnt % CHECK_QUEUE_SIZE_EVERY == 0 ) {
				Mean meanQueue = new Mean() ;
				for( int i=0; i<executorPool.length; i++ ) {
					meanQueue.increment(executorPool[i].queueSize()) ;
				}
				logger.info("Mean queue size:\n" + meanQueue.toString());
				while ( meanQueue.getResult() > MAX_QUEUE_SIZE ) {
					logger.info("Waiting for queue to be processed...");
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}

		}

		for( int i=0; i<executorPool.length; i++ ) {
			executorPool[i].finish() ;
			try {
				executorPool[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		// Finalize all sequences
		for( T lastSample : lastByOrderId.values() ) {
			sequenceReader.endSequence(lastSample) ;
		}
	}

}
