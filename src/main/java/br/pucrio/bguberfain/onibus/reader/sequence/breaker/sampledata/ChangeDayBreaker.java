package br.pucrio.bguberfain.onibus.reader.sequence.breaker.sampledata;

import br.pucrio.bguberfain.onibus.model.graph.v2.SampleData;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.SequenceBreaker;
import br.pucrio.bguberfain.onibus_geotools.Util;

public class ChangeDayBreaker extends SequenceBreaker<SampleData> {

	public ChangeDayBreaker() {
		super();
	}

	public ChangeDayBreaker(SequenceBreaker nextBreaker) {
		super(nextBreaker);
	}

	@Override
	protected boolean doSequenceShouldContinueBetween(SampleData last, SampleData current) {
		long msDayOfLast = last.getTimestamp() % (24*60*60*1000) ;
		long msDayOfCurrent = current.getTimestamp() % (24*60*60*1000) ;
		return msDayOfLast < msDayOfCurrent;
	}

}
