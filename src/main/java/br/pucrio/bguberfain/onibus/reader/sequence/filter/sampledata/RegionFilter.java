package br.pucrio.bguberfain.onibus.reader.sequence.filter.sampledata;

import br.pucrio.bguberfain.onibus.model.graph.v2.SampleData;
import br.pucrio.bguberfain.onibus.reader.sequence.filter.DataFilter;
import com.vividsolutions.jts.geom.Envelope;

public class RegionFilter extends DataFilter<SampleData> {

	Envelope region ;

	public RegionFilter(Envelope region, DataFilter prevFilter)
	{
		super(prevFilter) ;
		this.region = region ;
	}

	public RegionFilter(Envelope region)
	{
		this(region, null) ;
	}

	@Override
	protected SampleData doFilter(SampleData data)
	{
		if ( !region.contains(data.getCoordinate()) ) {
			this.increamentTotalSamplesFiltered();	// Update stats
			return null;
		} else {
			return data ;
		}
	}

}
