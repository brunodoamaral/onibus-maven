package br.pucrio.bguberfain.onibus.reader.sequence;

import br.pucrio.bguberfain.onibus.reader.sequence.breaker.SequenceBreaker;
import br.pucrio.bguberfain.onibus.reader.sequence.filter.DataFilter;
import org.apache.commons.math3.stat.descriptive.moment.Mean;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Array;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.function.Function;

public class ThreadedSequenceGenerator2<T> implements SequenceGenerator<T>
{
	private static final Logger logger = LogManager.getLogger(ThreadedSequenceGenerator2.class);

	SequenceBreaker sequenceBreaker ;
	Iterator<T> sampleReader ;
	DataFilter<T>filter ;
	Function<T, ? extends Object> idGetter ;
	private static final int MAX_QUEUE_SIZE = 200000 ;
	private static final int CHECK_QUEUE_SIZE_EVERY = 10000000 ;
	private long logEvery = 10000000 ;
    private boolean stopped = false ;

	public void setLogEvery(long logEvery) {
		this.logEvery = logEvery;
	}

	public ThreadedSequenceGenerator2(SequenceBreaker sequenceBreaker, DataFilter<T> filter, Iterator<T> sampleReader, Function<T, ? extends Object> idGetter) {
		super();

		if (sequenceBreaker == null) throw new NullPointerException("Must specify a SequenceBreaker") ;
		if (sampleReader == null) throw new NullPointerException("Must specify a T Reader") ;

		this.sequenceBreaker = sequenceBreaker;
		this.sampleReader = sampleReader;
		this.filter = filter ;
		this.idGetter = idGetter ;
	}

	private class ReadDataExecutor extends Thread
	{
		private boolean finished = false ;
		Queue<T> tasks ;
		SequenceReader target ;
		int taskId ;

		public ReadDataExecutor(SequenceReader target, int taskId)
		{
			tasks = new ConcurrentLinkedDeque<>() ;
			this.target = target ;
			this.taskId = taskId ;

			this.setName("ReadDataExecutor-" + taskId);
		}

		public void finish()
		{
			finished = true ;
		}

		public int queueSize()
		{
			return tasks.size() ;
		}

		@Override
		public void run() {
			try {
				Map<Object, T> lastByOrderId = new HashMap<>() ;
				while ( !finished )
				{
					T current = tasks.poll() ;

					if (current != null) {
						if (filter != null) {
							current = filter.filter(current) ;
						}

						if ( current != null ) {
							Object currentId = idGetter.apply(current);
							//System.out.printf("Processing %s\n", currentId.toString());
							T last = lastByOrderId.get(currentId);

							lastByOrderId.put(currentId, current);

							// Detect begin of sequence
							if (last == null) {
								target.beginSequence(current);
							} else {
								if (sequenceBreaker.sequenceShouldContinueBetween(last, current)) {
									target.sequence(last, current);
								} else {
									target.endSequence(last);
									target.beginSequence(current);
								}
							}
						}
					} else {
						Thread.sleep(500);
					}
				}

				// Finalize all sequences
				lastByOrderId.values().forEach(target::endSequence);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		public void processSample(T sampleData) {
			tasks.add(sampleData) ;
		}
	}

	public void readData(SequenceReader sequenceReader)
	{
		long cnt = 0 ;

		long startTime = System.currentTimeMillis() ;

		Map<Object, ReadDataExecutor> executorByOrderId = new HashMap<>() ;

		int numCores = Runtime.getRuntime().availableProcessors() ;
		// Generate array in runtime to avoid 'generic array creation' error
		ReadDataExecutor executorPool[] = (ReadDataExecutor[]) Array.newInstance(ReadDataExecutor.class, numCores) ;

		for( int i=0; i<executorPool.length; i++ ) {
			executorPool[i] = new ReadDataExecutor(sequenceReader, i+1) ;
			executorPool[i].start() ;
		}
		int nextThread = 0 ;

		logger.info("Processing data with {} threads", executorPool.length) ;

		while( sampleReader.hasNext() && !stopped ) {
			if(cnt++ % logEvery == 0) {
				logger.info("Read {} samples at {} samples/ms", cnt, cnt / (System.currentTimeMillis() - startTime));
			}

			T current = sampleReader.next() ;
			Object currentId = idGetter.apply(current) ;

			ReadDataExecutor executor = executorByOrderId.get(currentId);
			if ( executor == null ) {
				executor = executorPool[nextThread] ;
				nextThread = (nextThread + 1) % executorPool.length ;
				executorByOrderId.put(currentId, executor) ;
			}

			executor.processSample(current);

			if ( ++cnt % CHECK_QUEUE_SIZE_EVERY == 0 ) {
				Mean meanQueue = new Mean() ;
				Arrays.stream(executorPool).map(ReadDataExecutor::queueSize).forEach(meanQueue::increment);
				logger.info("Mean queue size: {}%", (int) (meanQueue.getResult() * 100 / MAX_QUEUE_SIZE));
				while ( meanQueue.getResult() > MAX_QUEUE_SIZE ) {
					logger.info("Mean queue size: {}% - waiting...", (int)(meanQueue.getResult() * 100 / MAX_QUEUE_SIZE));
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}

					// Recompute queue mean
					meanQueue.clear();
					Arrays.stream(executorPool).map(ReadDataExecutor::queueSize).forEach(meanQueue::increment);
				}
			}
		}

		for( int i=0; i<executorPool.length; i++ ) {
			executorPool[i].finish() ;
			try {
				executorPool[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

    @Override
    public void stop() {
        stopped = true ;
    }
}
