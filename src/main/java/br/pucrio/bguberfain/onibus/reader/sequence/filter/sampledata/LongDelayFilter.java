package br.pucrio.bguberfain.onibus.reader.sequence.filter.sampledata;

import br.pucrio.bguberfain.onibus.model.graph.v2.SampleData;
import br.pucrio.bguberfain.onibus.reader.sequence.filter.DataFilter;

public class LongDelayFilter extends DataFilter<SampleData> {
	
	int maxDelay ;
	
	public LongDelayFilter(int maxDelay, DataFilter prevFilter)
	{
		super(prevFilter) ;
		this.maxDelay = maxDelay ;
	}
	
	public LongDelayFilter(int maxDelay)
	{
		this(maxDelay, null) ;
	}

	@Override
	protected SampleData doFilter(SampleData data)
	{
		if ( data.getDelay() > maxDelay ) {
			this.increamentTotalSamplesFiltered();	// Update stats
			return null;
		} else {
			return data ;
		}
	}

}
