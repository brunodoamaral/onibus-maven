package br.pucrio.bguberfain.onibus.reader.sequence.filter;

public abstract class DataFilter<T> {
	
	private DataFilter<T> nextFilter ;
	long totalSamplesProcessed ;
	long totalSamplesFiltered ;
	
	public DataFilter(DataFilter<T> nextFilter)
	{
		this.nextFilter = nextFilter ;
	}
	
	public DataFilter() {
		this(null) ;
	}
	
	public final T filter(T data)
	{
		totalSamplesProcessed++ ;
		T result = doFilter(data) ;
		if ( result != null && nextFilter != null ) {
			return nextFilter.filter(result) ;
		}
		
		return result ;
	}
	
	protected void increamentTotalSamplesFiltered() {
		totalSamplesFiltered++ ;
	}
	
	protected abstract T doFilter(T data);

	public long getTotalSamplesProcessed() {
		return totalSamplesProcessed;
	}

	public long getTotalSamplesFiltered() {
		return totalSamplesFiltered;
	}
	
	public DataFilter getNextFilter() {
		return nextFilter;
	}
}
