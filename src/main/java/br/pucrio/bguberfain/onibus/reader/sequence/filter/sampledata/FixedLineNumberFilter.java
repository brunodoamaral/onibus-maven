package br.pucrio.bguberfain.onibus.reader.sequence.filter.sampledata;

import br.pucrio.bguberfain.onibus.model.graph.v2.SampleData;
import br.pucrio.bguberfain.onibus.reader.sequence.filter.DataFilter;

/**
 * Created by Bruno on 23/04/2015.
 */
public class FixedLineNumberFilter extends DataFilter<SampleData> {
    String lineNumber ;

    public FixedLineNumberFilter(DataFilter<SampleData> nextFilter, String lineNumber) {
        super(nextFilter);
        this.lineNumber = lineNumber;
    }

    public FixedLineNumberFilter(String lineNumber) {
        this(null, lineNumber) ;
    }

    @Override
    protected SampleData doFilter(SampleData data) {
        data.setLineNumber(lineNumber);
        return data;
    }
}
