package br.pucrio.bguberfain.onibus.reader.sequence.breaker.sampledata;

import br.pucrio.bguberfain.onibus.model.graph.v2.SampleData;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.SequenceBreaker;
import br.pucrio.bguberfain.onibus_geotools.Util;

public class ChangeHourBreaker extends SequenceBreaker<SampleData> {

	public ChangeHourBreaker() {
		super();
	}

	public ChangeHourBreaker(SequenceBreaker nextBreaker) {
		super(nextBreaker);
	}

	@Override
	protected boolean doSequenceShouldContinueBetween(SampleData last, SampleData current) {
		int lastHour = (int) Util.timeStampToDayHour(last.getTimestamp(), false) ;
		int currentHour = (int) Util.timeStampToDayHour(current.getTimestamp(), false) ;
		return lastHour == currentHour;
	}

}
