package br.pucrio.bguberfain.onibus.reader;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.BufferUnderflowException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;

import br.pucrio.bguberfain.onibus.model.graph.v2.SampleData;
import br.pucrio.bguberfain.onibus_geotools.Util;

import com.vividsolutions.jts.geom.Coordinate;

public class SampleDataReader implements Iterator<SampleData>
{
//	private DataInputStream dataIn ;
	private SampleData nextData ;
	private long fileSize ;
	private long currentPosition ;
	MappedByteBuffer inBuffer ;
	FileChannel fileChannel ;
	long totalReadInFile ;
	int currentFile = 0 ;
	File logFiles[] ;
	
	private static final int RECORD_SIZE = 40 ;
	private static final long BUFFER_SIZE = 50*1024*1024 ;

	public SampleDataReader(String yearMonthDayDateMatches) throws IOException {
		File logFolder = new File(Util.getEnv("LOG_FOLDER")) ;
		logFiles = logFolder.listFiles( pathname -> 
			pathname.getName().matches("\\d{8}\\.dat") &&
			(yearMonthDayDateMatches == null || pathname.getName().matches(yearMonthDayDateMatches + "\\.dat")) );
		
		Arrays.sort(logFiles, new Comparator<File>() {
			@Override
			public int compare(File o1, File o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});

		currentFile = -1 ;
		
		nextData = readNextFromStream() ;
	}
	
	public SampleDataReader() throws IOException {
		this(null) ;
	}
	
	public double status()
	{
		return currentPosition / (double) fileSize ;
	}
	
	private boolean startNextFile() throws IOException
	{
		if ( ++currentFile < logFiles.length ) {
			fileSize = logFiles[currentFile].length() ;
			fileChannel = new RandomAccessFile(logFiles[currentFile], "r").getChannel() ;
			
			return true ;
		} else {
			if ( fileChannel != null ) {
				fileChannel.close();
				fileChannel = null ;
				
				inBuffer = null ;
			}
			return false ;
		}
	}
	
	private SampleData readNextFromStream()
	{
		try {
			SampleData sample = null ;
			
			while (sample == null) {
				// Read data in correct order....
				long timestamp ;
				
				// If finishing...
				if (inBuffer == null || inBuffer.remaining() < 40) {
					
					// First file
					long toRead ;
					if (inBuffer == null) {
						totalReadInFile = 0 ;
						// Force load of next file (in this case, the first one)
						toRead = 0 ;
					} else {
						// Move cursor
						totalReadInFile += inBuffer.position() ;
						
						// Calculate bytes remaining in file
						toRead = fileChannel.size() - totalReadInFile ;
					}
					
					// Truncate to buffer size (maximum bytes allowed)
					if (toRead > BUFFER_SIZE) {
						toRead = BUFFER_SIZE ;
						
					// Check for end of file
					} else if (toRead == 0) {
						// Reset bytes read
						totalReadInFile = 0 ;
						
						// Try to start a new one...
						if ( !startNextFile() ) {
							// No more files: return null
							return null ;
						} else {
							// Start loading file
							toRead = Math.min(BUFFER_SIZE, fileChannel.size()) ;
						}
					
					// Sanity check...
					} else if ( toRead % 40 != 0 ) {
						throw new RuntimeException("Arquivo corrompido: " + logFiles[currentFile]) ;
					}
					
					System.out.println("Loading more data from file "  + logFiles[currentFile].getName() + "...");
					inBuffer = fileChannel.map(FileChannel.MapMode.READ_ONLY, totalReadInFile, toRead);
				}
				try {
					timestamp = inBuffer.getLong() /* - Util.DEFAULT_TIMEZONE*Util.MILISECONDS_PER_HOUR */ ;
				} catch (BufferUnderflowException eof) {
					// Last record read
					return null ;
				}
				int orderId = inBuffer.getInt() ;
				int lineNumberId = inBuffer.getInt() ;
				double latitude = inBuffer.getDouble() ;
				double longitude = inBuffer.getDouble() ;
				float speed = inBuffer.getFloat() ;
				int delay = inBuffer.getInt() ;
				
				currentPosition += RECORD_SIZE ;

				// Avoid invalid lat/lon error
				try {
					sample = new SampleData(timestamp, Util.orderNumberForId(orderId), Util.lineNumberForId(lineNumberId), new Coordinate(longitude, latitude), speed, delay, false) ;
				} catch (IllegalArgumentException e) {
					System.err.println("Erro while creating WGS84Point");
					e.printStackTrace(); 
				}
			}
			return sample ;
		} catch( BufferUnderflowException | IOException e ) {
			System.err.println("Error while reading out.dat");
			e.printStackTrace() ;
			return null ;
		}
	}

	@Override
	public boolean hasNext() {
		return nextData != null ;
	}

	@Override
	public SampleData next() {
		SampleData nextToReturn = nextData ;
		nextData = readNextFromStream() ;
		return nextToReturn ;
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException("Cannot remove read-only SampleData") ;
	}
	
}