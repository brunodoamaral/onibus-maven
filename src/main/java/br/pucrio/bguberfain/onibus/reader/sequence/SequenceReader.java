package br.pucrio.bguberfain.onibus.reader.sequence;

public interface SequenceReader<T> {

	public void beginSequence(T firstData) ;
	public void sequence(T lastData, T newData) ;
	public void endSequence(T lastData);

	// Annotation is an 'optional' method
	default void beginAnnotation(T annotation) {

	}
	
}
