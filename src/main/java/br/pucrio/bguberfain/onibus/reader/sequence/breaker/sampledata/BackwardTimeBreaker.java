package br.pucrio.bguberfain.onibus.reader.sequence.breaker.sampledata;

import br.pucrio.bguberfain.onibus.model.graph.v2.SampleData;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.SequenceBreaker;
import br.pucrio.bguberfain.onibus_geotools.Util;

public class BackwardTimeBreaker extends SequenceBreaker<SampleData> {

	public BackwardTimeBreaker() {
		super();
	}

	public BackwardTimeBreaker(SequenceBreaker nextBreaker) {
		super(nextBreaker);
	}

	@Override
	protected boolean doSequenceShouldContinueBetween(SampleData last, SampleData current) {
		return current.getTimestamp() > last.getTimestamp() ;
	}

}
