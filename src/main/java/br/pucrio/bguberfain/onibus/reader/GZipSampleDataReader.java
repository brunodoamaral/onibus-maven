package br.pucrio.bguberfain.onibus.reader;

import br.pucrio.bguberfain.onibus.model.graph.v2.SampleData;
import br.pucrio.bguberfain.onibus.util.Util;
import com.vividsolutions.jts.geom.Coordinate;
import org.apache.commons.lang3.time.FastDateFormat;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.text.ParseException;
import java.util.*;
import java.util.zip.GZIPInputStream;

/**
 * Created by Bruno on 25/01/2015.
 */
public class GZipSampleDataReader implements Iterator<SampleData>
{
    private static final Logger logger = LogManager.getLogger(GZipSampleDataReader.class);
    private static final FastDateFormat DATE_PARSER = FastDateFormat.getInstance("dd-MM-yyyy HH:mm:ss", TimeZone.getTimeZone("America/Sao_Paulo")) ;

    BufferedReader inData ;
    String lastLine ;
    LineTokenizer lineToken = new LineTokenizer("") ;
    boolean includeAnnotations ;

    public GZipSampleDataReader() {
        this(false) ;
    }

    public GZipSampleDataReader(boolean isBrt) {
        this(isBrt ? "\\d{8}-brt\\.txt\\.gz" : "\\d{8}\\.txt\\.gz") ;
    }

    public GZipSampleDataReader(String filePattern)
    {
        File logFolder = new File(Util.getEnv("LOG_FOLDER")) ;
        File logFiles[] = logFolder.listFiles(pathname -> pathname.getName().matches(filePattern));
        Arrays.sort(logFiles, Comparator.comparing(File::getName));

        String filesToLoad = Arrays.stream(logFiles).map(File::getName).reduce("", (a,b) -> a + ", " + b) ;

        logger.info("Files to load: " + filesToLoad);

        final Iterator<File> enumFiles = Arrays.asList(logFiles).iterator() ;

        Enumeration<InputStream> enumFilesIn = new Enumeration<InputStream>() {
            @Override
            public boolean hasMoreElements() {
                return enumFiles.hasNext() ;
            }

            @Override
            public InputStream nextElement() {
                try {
                    File nextFile = enumFiles.next() ;
                    FileInputStream fileIn = new FileInputStream(nextFile);
                    BufferedInputStream buffIn = new BufferedInputStream(fileIn, 64 * 1024 * 1024);
                    GZIPInputStream gzIn = new GZIPInputStream(buffIn);
                    logger.info("Starting to load file {}", nextFile.getName());
                    return gzIn;
                } catch (IOException e) {
                    throw new RuntimeException(e) ;
                }
            }
        } ;

        SequenceInputStream filesIn = new SequenceInputStream(enumFilesIn) ;
        InputStreamReader inReader = new InputStreamReader(filesIn);
        inData = new BufferedReader(inReader, 64 * 1024 * 1024);
    }

    public void setIncludeAnnotations(boolean includeAnnotations) {
        this.includeAnnotations = includeAnnotations;
    }

    @Override
    public boolean hasNext() {
        try {
            if (includeAnnotations) {
                lastLine = inData.readLine() ;

                return lastLine != null ;
            } else {
                String line;
                while ((line = inData.readLine()) != null) {
                    // Ignore comments
                    if (line.charAt(0) != '#') {
                        lastLine = line;
                        return true;
                    }
                }
            }
        } catch (IOException e) {
            // Will return false
            logger.error("Error while reading next line. Will return false on hasNext().", e);
        }
        return false ;
    }

    @Override
    public SampleData next() {
        if (includeAnnotations && lastLine.charAt(0) == '#' ) {
            String strDateSamples = lastLine.substring(lastLine.indexOf(" em ") + 4) ;
            return new DelayedSampleData(strDateSamples, null, null, null, null, 0, 0, true) ;
        }

        lineToken = LineTokenizer.recicleTokenizer(lineToken, lastLine) ;

        String strDataHora = lineToken.nextToken() ;
        String strOrdem = lineToken.nextToken() ;
        String strLinha = lineToken.nextToken() ;
        String strLat = lineToken.nextToken() ;
        String strLon = lineToken.nextToken() ;
        String strSpeed = lineToken.nextToken() ;
        String strDelay = lineToken.nextToken() ;

        // Linha
        // Remove ".0" by the end of string
        if ( strLinha.endsWith(".0") ) {
            strLinha = strLinha.substring(0, strLinha.length()-2) ;
        }

        // Velocidade
        float velocidade = fastParseSpeed(strSpeed) ;

        // Atraso
        int atraso = Integer.parseInt(strDelay) ;

        // Return delayed data (avoid parsing all data)
        return new DelayedSampleData(strDataHora, strOrdem, strLinha, strLat, strLon, velocidade, atraso, false) ;
    }

    private static int fastParseSpeed(String strSpeed)
    {
        int value = 0 ;
        for(int i=0; i<strSpeed.length(); i++) {
            char c = strSpeed.charAt(i) ;
            if (c == '.') break ;
            value = value * 10 + (c-'0') ;
        }

        return value ;
    }

    private static class LineTokenizer
    {
        String line ;
        int currPosition ;
        int len ;

        static LineTokenizer recicleTokenizer(LineTokenizer recicledTokenizer, String line) {
            recicledTokenizer.line = line ;
            recicledTokenizer.currPosition = -1 ;
            recicledTokenizer.len = line.length() ;
            return recicledTokenizer ;
        }

        public LineTokenizer(String line) {
            this.line = line;
            currPosition = -1 ;
            len = line.length() ;
        }

        public String nextToken()
        {
            int lastPosition = currPosition+1 ;
            currPosition = line.indexOf('\t', currPosition+1) ;
            if ( currPosition == -1 ) {
                return line.substring(lastPosition, len);
            } else {
                return line.substring(lastPosition, currPosition);
            }
        }
    }

    private static class DelayedSampleData extends SampleData
    {
        String strTimeStamp ;
        String strLatitude ;
        String strLongitude ;

        private static final TimeZone TIME_ZONE = TimeZone.getTimeZone("America/Sao_Paulo") ;

        public DelayedSampleData(String strTimeStamp, String order, String lineNumber, String strLatitude, String strLongitude, float speed, int delay, boolean annotation) {
            super(-1, order, lineNumber, null, speed, delay, annotation);
            this.strTimeStamp = strTimeStamp ;
            this.strLatitude = strLatitude ;
            this.strLongitude = strLongitude ;
        }

        @Override
        public long getTimestamp() {
            if(super.getTimestamp() == -1){
                try {
                    Date parsedDate = DATE_PARSER.parse(strTimeStamp) ;

                    // Apply a fix for erroneous timezone in log file
                    long realTimeOffset = TIME_ZONE.getOffset(parsedDate.getTime());

                    setTimestamp( parsedDate.getTime() + realTimeOffset );
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            return super.getTimestamp() ;
        }

        @Override
        public Coordinate getCoordinate() {
            if (super.getCoordinate() == null) {
                setCoordinate(new Coordinate(Double.parseDouble(strLongitude), Double.parseDouble(strLatitude)));
            }

            return super.getCoordinate() ;
        }
    }
}
