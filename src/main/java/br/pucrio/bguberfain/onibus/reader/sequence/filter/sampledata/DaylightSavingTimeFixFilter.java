package br.pucrio.bguberfain.onibus.reader.sequence.filter.sampledata;

import br.pucrio.bguberfain.onibus.model.graph.v2.SampleData;
import br.pucrio.bguberfain.onibus.reader.sequence.filter.DataFilter;

public class DaylightSavingTimeFixFilter extends DataFilter<SampleData> {

	public DaylightSavingTimeFixFilter() {
		super();
	}

	public DaylightSavingTimeFixFilter(DataFilter nextFilter) {
		super(nextFilter);
	}

	private static final int MAX_DELAY_TO_ADJUST = 15 * 60 ; // 15 minutes (expressed in seconds)
	private static final int MIN_DELAY_TO_ADJUST = -5 * 60 ; // -5 minutes (expressed in seconds)
	private static final int MILLISECONDS_IN_AN_HOUR = 60*60*1000 ;
	private static final int SECONDS_IN_AN_HOUR = 60*60 ;

	@Override
	protected SampleData doFilter(SampleData data) {
		int delay = data.getDelay() ;	// Delay is expressed in seconds
		
		// Make adjustements when delay is about an hour before or after server time
		if ( delay >= -SECONDS_IN_AN_HOUR+MIN_DELAY_TO_ADJUST && delay <= -SECONDS_IN_AN_HOUR+MAX_DELAY_TO_ADJUST ) {
			data.setTimestamp(data.getTimestamp() - MILLISECONDS_IN_AN_HOUR);	// Timestamp is expressed in milliseconds
			data.setDelay(data.getDelay() + SECONDS_IN_AN_HOUR);
			this.increamentTotalSamplesFiltered();	// Update stats
		} else if ( delay >= SECONDS_IN_AN_HOUR+MIN_DELAY_TO_ADJUST && delay <= SECONDS_IN_AN_HOUR+MAX_DELAY_TO_ADJUST ) {
			data.setTimestamp(data.getTimestamp() + MILLISECONDS_IN_AN_HOUR);
			data.setDelay(data.getDelay() - SECONDS_IN_AN_HOUR);
			this.increamentTotalSamplesFiltered();	// Update stats
		}

		return data ;
	}

}
