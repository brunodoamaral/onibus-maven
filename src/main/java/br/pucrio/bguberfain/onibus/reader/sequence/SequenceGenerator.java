package br.pucrio.bguberfain.onibus.reader.sequence;

/**
 * Created by Bruno on 25/01/2015.
 */
public interface SequenceGenerator<T>
{
    public void readData(SequenceReader<T> sequenceReader) ;
    public void stop();
}
