package br.pucrio.bguberfain.onibus.reader.sequence;

import java.util.Collection;
import java.util.function.Function;

import br.pucrio.bguberfain.onibus.model.graph.v2.SampleData;

public class SwitcherSequenceReader<T extends SequenceReader> implements SequenceReader<SampleData> {
	
	Collection<T> sequenceReaders ;
	Function<SampleData, T> switcher ;

	public SwitcherSequenceReader(Collection<T> sequenceReaders, Function<SampleData, T> switcher) {
		super();
		this.sequenceReaders = sequenceReaders;
		this.switcher = switcher ;
	}

	@Override
	public void beginSequence(SampleData firstData) {
		T choosen = switcher.apply(firstData) ;
		if ( choosen != null ) choosen.beginSequence(firstData);
	}

	@Override
	public void sequence(SampleData lastData, SampleData newData) {
		T choosen = switcher.apply(lastData) ;
		if ( choosen != null ) choosen.sequence(lastData, newData);
	}

	@Override
	public void endSequence(SampleData lastData) {
		T choosen = switcher.apply(lastData) ;
		if ( choosen != null ) choosen.endSequence(lastData);
	}

}
