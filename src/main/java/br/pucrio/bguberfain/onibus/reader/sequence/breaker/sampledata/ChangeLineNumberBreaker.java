package br.pucrio.bguberfain.onibus.reader.sequence.breaker.sampledata;

import br.pucrio.bguberfain.onibus.model.graph.v2.SampleData;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.SequenceBreaker;

public class ChangeLineNumberBreaker extends SequenceBreaker<SampleData> {

	public ChangeLineNumberBreaker() {
		super();
	}

	public ChangeLineNumberBreaker(SequenceBreaker nextBreaker) {
		super(nextBreaker);
	}

	@Override
	protected boolean doSequenceShouldContinueBetween(SampleData last, SampleData current) {
		return last.getLineNumber().equals(current.getLineNumber()) ;
	}

}
