package br.pucrio.bguberfain.onibus.gui;

import com.vividsolutions.jts.geom.Envelope;

public class Region {

	public static final Envelope RIO_DE_JANEIRO = new Envelope( -43.733826, -43.145370, -23.086048, -22.768584) ;
	public static final Envelope ZONA_SUL = new Envelope( -43.231201, -43.159048,-22.992847, -22.926935) ;
	public static final Envelope CENTRO = new Envelope( -43.210430, -43.165541,-22.922982, -22.891437) ;
	public static final Envelope COPACABANA = new Envelope(-43.20526663912445,-43.16303664757927,-22.957354242781374,-22.989182758576515) ;
	public static final Envelope LAGOA = new Envelope(-43.231974,-43.198500,-22.959026,-22.991346) ;
	public static final Envelope BOTAFOGO = new Envelope(-43.201341713483146, -43.18281671348315, -22.959426123595506, -22.948411727528093) ;
	public static final Envelope BOTAFOGO_SAO_CLEMENTE = new Envelope(-43.193565715851385, -43.184526776960084, -22.95133128175742, -22.94840377672643) ;	// Only São Clemente Av.
	public static final Envelope PAINEIRAS = new Envelope( -43.212576, -43.199530,-22.949739, -22.935156) ;
	public static final Envelope FLAMENGO = new Envelope( -43.179885, -43.168083,-22.944616, -22.934489) ;
	public static final Envelope PONTO_SENADOR_VERGUEIRO = new Envelope( -43.177696, -43.176023,-22.940585, -22.938441) ;
	public static final Envelope PONTO_SAO_CLEMENTE = new Envelope( -43.191735, -43.189638,-22.950495, -22.949457) ;
	public static final Envelope PREDIDENTE_VARGAS = new Envelope( -43.211546, -43.174810, -22.911994, -22.899344) ;
    public static final Envelope GAVEA_ULTIL_BOTAFOGO = new Envelope(-43.234034, -43.180990, -22.981014, -22.948040) ;
	public static final Envelope BARRA_UNTIL_LEBLON = new Envelope(-43.371792, -43.224678, -23.019076, -22.977360) ;
	public static final Envelope ATERRO = new Envelope(-43.184338, -43.166656, -22.950332, -22.910650) ;

	public static String regionName(Envelope region) {
		if (region.equals(RIO_DE_JANEIRO)) {
			return "RIO_DE_JANEIRO" ;
		} else if (region.equals(ZONA_SUL)) {
			return "ZONA_SUL" ;
		} else if (region.equals(CENTRO)) {
			return "CENTRO" ;
		} else if (region.equals(COPACABANA)) {
			return "COPACABANA" ;
		} else if (region.equals(LAGOA)) {
			return "LAGOA" ;
		} else if (region.equals(BOTAFOGO)) {
			return "BOTAFOGO" ;
		} else if (region.equals(BOTAFOGO_SAO_CLEMENTE)) {
			return "BOTAFOGO_SAO_CLEMENTE" ;
		} else if (region.equals(PAINEIRAS)) {
			return "PAINEIRAS" ;
		} else if (region.equals(FLAMENGO)) {
			return "FLAMENGO" ;
		} else if (region.equals(PONTO_SENADOR_VERGUEIRO)) {
			return "PONTO_SENADOR_VERGUEIRO" ;
		} else if (region.equals(PONTO_SAO_CLEMENTE)) {
			return "PONTO_SAO_CLEMENTE" ;
		} else if (region.equals(PREDIDENTE_VARGAS)) {
			return "PREDIDENTE_VARGAS" ;
		} else if (region.equals(GAVEA_ULTIL_BOTAFOGO)) {
			return "GAVEA_ULTIL_BOTAFOGO" ;
		} else if (region.equals(BARRA_UNTIL_LEBLON)) {
			return "BARRA_UNTIL_LEBLON" ;
		} else if (region.equals(ATERRO)) {
			return "ATERRO" ;
		} else {
			return "UNKNOWN";
		}
	}

	// Calculate the image width based on the zoom level (web standard)
	// Reference: https://msdn.microsoft.com/en-us/library/bb259689.aspx
	public static int imageWidthForZoomLevel(int level, Envelope region) {
		double longitudeSpan = region.getWidth() ;

		return (int) (256 * Math.pow(2, level-1) * longitudeSpan / 180);
	}
/*

	private double minLat, maxLat, minLon, maxLon ;
	
	private Region(double minLat, double maxLat, double minLon, double maxLon) {
		super();
		this.minLat = minLat;
		this.maxLat = maxLat;
		this.minLon = minLon;
		this.maxLon = maxLon;
	}
	
	public double getMinLat() {
		return minLat;
	}
	public double getMaxLat() {
		return maxLat;
	}
	public double getMinLon() {
		return minLon;
	}
	public double getMaxLon() {
		return maxLon;
	}

	public static final Region RIO_DE_JANEIRO = new Region(-23.086048, -22.768584, -43.733826, -43.145370) ;
	public static final Region ZONA_SUL = new Region(-22.992847, -22.926935, -43.231201, -43.146400) ;
	public static final Region CENTRO = new Region(-22.922982, -22.891437, -43.210430, -43.165541) ;
	public static final Region BOTAFOGO = new Region(-22.963451, -22.946538, -43.204422, -43.171463) ;
	public static final Region PAINEIRAS = new Region(-22.949739, -22.935156, -43.212576, -43.199530) ;
	public static final Region FLAMENGO = new Region(-22.944616, -22.934489, -43.179885, -43.168083) ;
	public static final Region PONTO_SENADOR_VERGUEIRO = new Region(-22.940585, -22.938441, -43.177696, -43.176023) ;
	public static final Region PONTO_SAO_CLEMENTE = new Region(-22.950495, -22.949457, -43.191735, -43.189638) ;
	public static final Region PRACA_GAL_GLICERIO = new Region(-22.943297, -22.940699, -43.192502, -43.190786) ;
	*/
}
