package br.pucrio.bguberfain.onibus.gui;

import java.awt.*;

/**
 * Created by Bruno on 11/04/2015.
 */
public interface ColorProvider
{
    Color colorByPercent(float percent, int alpha) ;
}
