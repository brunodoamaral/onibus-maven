package br.pucrio.bguberfain.onibus.gui;

import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import br.pucrio.bguberfain.onibus.util.Util;
import org.imgscalr.Scalr;

public class MultiImageViewer extends JFrame {

	public interface MultiImageViewerTitle {
		public String getTitle(int row, int col) ;
	}

	private static final long serialVersionUID = 4584273979870063742L;
	List<JLabel> imageIcons ;
	int rows, cols ;
	MultiImageViewerTitle titleFunc ;
	int imgWidth, imgHeight ;
    File saveToFolder ;

	public MultiImageViewer(int rows, int cols, MultiImageViewerTitle titleFunc)
	{
		this.rows = rows ;
		this.cols = cols ;
		this.titleFunc = titleFunc ;
        this.setTitle("Images");
        this.setExtendedState(JFrame.MAXIMIZED_BOTH) ;

        saveToFolder = new File(Util.getEnv("SAVE_TO_DIR")) ;

//        this.setSize(300, 200);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        JPanel jPanel = new JPanel();
        jPanel.setLayout(new GridLayout(rows,cols));
        
        imageIcons = new ArrayList<>( rows * cols ) ;
		for(int row = 0; row < rows; row++ ) {
			for( int col = 0; col < cols; col++ ) {
				JLabel iconLabel = new JLabel();
				jPanel.add(iconLabel);
				imageIcons.add(iconLabel) ;
			}
		}

        this.add(jPanel);
        
        this.pack() ;

		imageIcons.get(0).addComponentListener(new ComponentAdapter() {
			public void componentResized(ComponentEvent evt) {
				Component c = (Component) evt.getSource();
				imgWidth = c.getWidth();
				imgHeight = c.getHeight() ;
			}
		});

	}
	
	ConcurrentMap<Integer, BufferedImage> imagesProcessing = new ConcurrentHashMap<>();
	
	public void setImage(int row, int col, BufferedImage bufferedImage)
	{
		int index = cols * row + col ;
		if ( !imagesProcessing.containsKey(index) ) {
//			System.out.println("Creating thread for index " + index);
			imagesProcessing.put(index, bufferedImage) ;
			
			new Thread(() -> {
//				System.out.println("Starting thread for index " + index);
				JLabel label = imageIcons.get(index) ;
//				System.out.println(label.getWidth() + ", " +  label.getHeight());
				
				BufferedImage resized = Scalr.resize(bufferedImage, Scalr.Method.BALANCED, Scalr.Mode.FIT_EXACT, imgWidth, imgHeight) ;

				String title = titleFunc.getTitle( row, col ) ;
				Graphics2D graphics = (Graphics2D) resized.getGraphics() ;
				graphics.setColor(Color.GRAY);
				graphics.drawString(title, 0, 0);

				try {
                    File outFile = new File(saveToFolder, title + ".png") ;
					ImageIO.write(bufferedImage, "png", outFile) ;
				} catch (IOException e) {
					e.printStackTrace();
				}

				ImageIcon icon = (ImageIcon) label.getIcon() ;
				if ( icon == null ) {
					label.setIcon(new ImageIcon(resized));
				} else {
					icon.setImage(resized);
				}
//				try {
//					Thread.sleep(200) ;
//				} catch (Exception e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
				synchronized(this) {
					this.repaint() ;
				}
				imagesProcessing.remove(index) ;

				
//				System.out.println("Finishing thread for index " + index);
			}).start() ;
		}
		

//		try {
//			Thread.sleep(10);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}
}
