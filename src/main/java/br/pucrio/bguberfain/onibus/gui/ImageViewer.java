package br.pucrio.bguberfain.onibus.gui;

import org.imgscalr.Scalr;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.locks.Lock;

public class ImageViewer extends JFrame {

	private static final long serialVersionUID = 4584273979870063742L;

	ImageIcon iconImage;
	JViewport vport;
	double zoom;

	boolean hasFinished;

	JPopupMenu menu = new JPopupMenu("Popup");
	GeoImage geoImage ;

	public ImageViewer(GeoImage geoImage, boolean autoUpdate) {
		this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		this.setExtendedState(JFrame.MAXIMIZED_BOTH);
		this.setLocationRelativeTo(null);
		this.setVisible(true);

		this.geoImage = geoImage ;

		iconImage = new ImageIcon(geoImage.getImage());
		zoom = 1;

		JLabel label = new JLabel(iconImage);
		JScrollPane scroll = new JScrollPane(label);
		vport = scroll.getViewport();
		MouseAdapter ma = new HandScrollListener();
		vport.addMouseMotionListener(ma);
		vport.addMouseListener(ma);
		this.getContentPane().add(scroll);

		MouseAdapter zoomListener = new WheelListener();
		vport.addMouseWheelListener(zoomListener);

		// Save-as
		JMenuItem item = new JMenuItem("Save graphImage...");
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				geoImage.lock();
				try {
					final JFileChooser fc = new JFileChooser();
					fc.setFileFilter(new FileNameExtensionFilter("PNG File", "png"));
					int ret = fc.showSaveDialog(ImageViewer.this) ;
					if ( ret == JFileChooser.APPROVE_OPTION ) {
						System.out.println("Saving graphImage...");
						File file = fc.getSelectedFile();
						try {
							if ( !file.getName().endsWith(".png") ) {
								file = new File(file.getAbsoluteFile() + ".png") ;
							}
							ImageIO.write(geoImage.getImage(), "png", file) ;
						} catch (IOException e1) {
							e1.printStackTrace();
						}
					}
				} finally {
					geoImage.unlock();
				}
			}
		});
		menu.add(item);
		vport.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent ev) {
				if (ev.isPopupTrigger()) {
					menu.show(ev.getComponent(), ev.getX(), ev.getY());
				}
			}
			
			@Override
			public void mouseReleased(MouseEvent e) {
				this.mousePressed(e);
			}
		});

		hasFinished = false;

		if (autoUpdate) {
			new Thread(() -> {
				while (!hasFinished) {
					updateImage();
					try {
						Thread.sleep(100);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				System.out.println("Finishing...");
			}).start();
		}
	}

	BufferedImage lastImage;

	private void updateImage() {
		if (lastImage != null) {
//			System.out.println("Updating graphImage...");
			BufferedImage resized;
			synchronized (lastImage) {
				resized = Scalr.resize(lastImage, (int) (lastImage.getWidth() * zoom), (int) (lastImage.getHeight() * zoom));
			}
			iconImage.setImage(resized);
			this.repaint();
		}
	}


	public void finish() {
		hasFinished = true;
		updateImage();
	}

	public void setImage(GeoImage geoImage) {
		if (lastImage != null) {
			synchronized (lastImage) {
				lastImage = geoImage.getImage();
			}
		} else {
			this.geoImage = geoImage;
			lastImage = geoImage.getImage() ;
		}
	}

	class WheelListener extends MouseAdapter {
		@Override
		public void mouseWheelMoved(MouseWheelEvent e) {
			super.mouseWheelMoved(e);

			if (e.getWheelRotation() > 0) {
				// Positive zoom (zoom in)
				zoom *= (double) e.getWheelRotation() * 1.2;
			} else {
				// Negative zoom (zoom out)
				zoom /= -(double) e.getWheelRotation() * 1.2;
			}
			updateImage();
			JComponent label = (JComponent) vport.getView();
			Point vp = vport.getViewPosition();
			label.scrollRectToVisible(new Rectangle(vp, vport.getSize()));
		}
	}

	static class HandScrollListener extends MouseAdapter {
		private final Point pp = new Point();

		@Override
		public void mouseDragged(MouseEvent e) {
			JViewport vport = (JViewport) e.getSource();
			JComponent label = (JComponent) vport.getView();
			Point cp = e.getPoint();
			Point vp = vport.getViewPosition();
			vp.translate(pp.x - cp.x, pp.y - cp.y);
			label.scrollRectToVisible(new Rectangle(vp, vport.getSize()));
			// vport.setViewPosition(vp);
			pp.setLocation(cp);
		}

		@Override
		public void mousePressed(MouseEvent e) {
			pp.setLocation(e.getPoint());
		}

	}
}