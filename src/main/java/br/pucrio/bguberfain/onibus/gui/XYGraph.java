package br.pucrio.bguberfain.onibus.gui;

import br.pucrio.bguberfain.onibus.model.graph.v2.XYSample;
import com.vividsolutions.jts.geom.*;
import com.vividsolutions.jts.geom.Polygon;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.geotools.geometry.jts.JTS;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;
import java.util.Timer;
import java.util.function.Function;

/**
 * Created by Bruno on 25/01/2015.
 */
public class XYGraph extends JPanel implements ComponentListener, ColorProvider
{
    private static final Logger logger = LogManager.getLogger(XYGraph.class);
    private static final int LEGEND_TEXT_SPACE = 60 ;
    private static final Insets GRAPH_BORDER = new Insets(15, 40, 40, 15) ;
    private static final Point LEGEND_BORDER = new Point(10, GRAPH_BORDER.top) ;
    private static final Point LEGEND_OFFSET = new Point((int)(LEGEND_TEXT_SPACE+LEGEND_BORDER.getX()), (int)LEGEND_BORDER.getY()) ;
    private static final long FILE_MAGIC = 0xac82066f ;
    private static final int FILE_VERSION = 0x02 ;
    private static final SimpleDateFormat dateFormatAxis = new SimpleDateFormat("EEE dd/MM/yyyy HH:mm") ;
    private static final TimeZone timeZone = TimeZone.getTimeZone("America/Sao_Paulo") ;

    public enum XYGraphAxisType {
        METERS ("m", (m) -> String.format("%.0fm", m)),
        SECONDS("s", (s) -> {
            int seconds = s.intValue() ;
            int min = seconds / 60 ;
            int hour = min / 60 ;
            min %= 60 ;

            return String.format("%dh%02d", hour, min) ;
        }),
        METERS_PER_SECOND("km/h", (ms) -> String.format("%.0fkm/h", ms * 3.6)),
        TIMESTAMP("ms", (ms) -> dateFormatAxis.format(new Date(ms.longValue() - timeZone.getOffset(ms.longValue()))) );

        String name ;
        Function<Double, String> formatFunction ;

        XYGraphAxisType(String name, Function<Double, String> formatFunction) {
            this.name = name;
            this.formatFunction = formatFunction;
        }

        public String format(double value) {
            return formatFunction.apply(value) ;
        }

        public String getName() {
            return name;
        }
    }

    XYGraphAxisType xAxis, yAxis ;

    int currentWidth, currentHeight, currentResolution ;

    BufferedImage graphImage;
    BufferedImage imageLegend ;
    Graphics2D imageGraphics;
    Graphics2D graphicsLegend;
    Envelope graphArea ;
    Polygon graphAreaGeometry ;
    ColorProvider colorProvider ;
    boolean drawAsDots = false ;
    Color backgroundColor = Color.BLACK ;

    long totalSamples ;

    List<Deque<XYSample>> samplesSequences ;
    private XYSample.XYSampleBoundary boundary ;
    boolean shouldUpdateBoundary = true ;
    private XYSample.XYSampleBoundary selectedBoundary ;
    private SortedMap<Float, String> legend ;
    Rectangle legendPosition ;
    Rectangle legendMousePosition ;
    double selectedLegend = -1 ;
    double selectedLegendRange = 0.010 ;
    Function<Double, String> legendForColorPercent ;
    Point graphPoint ;

    Font font ;
    int fontHeight = 15 ;

    int sequenceAlpha = 0x15 ;

    BasicStroke graphLineStroke = new BasicStroke(1.0f) ;
    GeometryFactory geometryFactory = new GeometryFactory();
    Rectangle graphSelection ;

    java.util.List<Double> verticalMarks ;
    float verticalMarksStroke = 2.0f ;

    float graphLineWidthZoom = 1.0f ;
    float graphLineWidthWheel = 1.0f ;
    boolean ctrlPressed = false ;

    File fileLoaded = null ;

    Timer delayedUpdateGraph = new Timer() ;

    public XYGraph(XYGraphAxisType xAxis, XYGraphAxisType yAxis) {
        super();

        this.xAxis = xAxis ;
        this.yAxis = yAxis ;

        colorProvider = this ;

        addComponentListener(this);
        samplesSequences = new LinkedList<>() ;
        verticalMarks = new LinkedList<>() ;

        boundary = new XYSample.XYSampleBoundary() ;

        // Save-as
        JPopupMenu menuSaveAs = new JPopupMenu("Popup");
        JMenuItem item = new JMenuItem("Save data...");
        item.addActionListener(e -> saveData());
        menuSaveAs.add(item);

        item = new JMenuItem("Save image...");
        item.addActionListener(e -> saveImage());
        menuSaveAs.add(item);

        item = new JMenuItem("Save image 2x...");
        item.addActionListener(e -> saveImage2x());
        menuSaveAs.add(item);

        item = new JMenuItem("Save image 4x...");
        item.addActionListener(e -> saveImage4x());
        menuSaveAs.add(item);

        MouseAdapter mouseHandler = new MouseAdapter() {
            boolean freeze = false ;
            Point dragStartPoint ;

            @Override
            public void mouseClicked(MouseEvent e) {
                if ( e.getButton() == MouseEvent.BUTTON1 ) {
                    if (legendPosition.contains(e.getPoint())) {
                        // Click on legend
                        freeze = !freeze;
                    }
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {
                Coordinate where = new Coordinate(e.getPoint().getX(), e.getPoint().getY()) ;
                if (graphArea.contains(where)) {
                    dragStartPoint = e.getPoint() ;
                    graphSelection = new Rectangle((int)dragStartPoint.getX(), (int)dragStartPoint.getY(), 1, 1) ;
                    repaint();
                }
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                if (e.getButton() == MouseEvent.BUTTON3) {
                    if (graphSelection != null) {
                        if (graphSelection.getWidth() == 1 && graphSelection.getHeight() == 1) {
                            // Reset zoom
                            selectedBoundary = null;
                            graphLineWidthZoom = 1f;
                            graphLineStroke = new BasicStroke(graphLineWidthZoom * graphLineWidthWheel);
                            paintGraph();
                        } else {
                            // Save boundary (zoom feature)
                            double minY = getSampleYForGraphY(graphSelection.getY() + graphSelection.getHeight());
                            double maxY = getSampleYForGraphY(graphSelection.getY());
                            double minX = getSampleXForGraphX(graphSelection.getX());
                            double maxX = getSampleXForGraphX(graphSelection.getX() + graphSelection.getWidth());
                            selectedBoundary = new XYSample.XYSampleBoundary(minX, minY, maxX, maxY);

                            // Keep selection inside samples boundary
                            selectedBoundary.fitInside(boundary);

                            // Calculate new stroke width relative to original boundary
                            graphLineWidthZoom = (float) Math.sqrt(boundary.getArea() / selectedBoundary.getArea());
                            logger.info("Storke width changed to {}", graphLineWidthZoom * graphLineWidthWheel);
                            graphLineStroke = new BasicStroke(graphLineWidthZoom * graphLineWidthWheel);

                            paintGraph();
                        }
                    } else {
                        menuSaveAs.show(e.getComponent(), e.getX(), e.getY());
                    }
                }
                dragStartPoint = null ;
                graphSelection = null ;
                repaint();
            }

            @Override
            public void mouseDragged(MouseEvent e) {
                if (dragStartPoint != null) {
                    Point pt = e.getPoint() ;

                    pt.setLocation( Math.min(graphArea.getMaxX(), Math.max(graphArea.getMinX(), pt.getX())), Math.min(graphArea.getMaxY(), Math.max(graphArea.getMinY(), pt.getY())) );

                    Point rectangleOrigin = new Point((int)Math.min(pt.getX(), dragStartPoint.getX()), (int)Math.min(pt.getY(), dragStartPoint.getY())) ;
                    graphSelection.setLocation(rectangleOrigin);
                    graphSelection.setSize((int) Math.abs(pt.getX() - dragStartPoint.getX()), (int) Math.abs(pt.getY() - dragStartPoint.getY()));
                    repaint();
                }
            }

            @Override
            public void mouseMoved(MouseEvent e) {
                if (!freeze) {
                    if (legendMousePosition.contains(e.getPoint())) {
                        selectedLegend = (e.getPoint().getY() - legendMousePosition.getY()) / legendMousePosition.getHeight();
                        paintLegend();
                        repaint();
                    } else if (selectedLegend != -1) {
                        selectedLegend = -1;
                        paintLegend();
                        repaint();
                    }
                }

                Coordinate where = new Coordinate(e.getPoint().getX(), e.getPoint().getY()) ;
                if (graphArea.contains(where)) {
                    graphPoint = e.getPoint() ;
                } else {
                    graphPoint = null ;
                }
                repaint() ;
            }

            @Override
            public void mouseWheelMoved(MouseWheelEvent e) {
                if (legendPosition.contains(e.getPoint())) {
                    if (!freeze) {
                        selectedLegendRange *= Math.pow(1.2, e.getWheelRotation());
                        if (selectedLegend != -1) {
                            paintLegend();
                            repaint();
                        }
                    }
                } else {
                    if (ctrlPressed) {
                        sequenceAlpha += -e.getWheelRotation() * 2 ;

                        if (sequenceAlpha<0) {
                            sequenceAlpha = 0 ;
                        } else if (sequenceAlpha > 255) {
                            sequenceAlpha = 255 ;
                        }
                        logger.info("Sequence alpha changed to {}", sequenceAlpha);
                    } else {
                        graphLineWidthWheel = (float) (graphLineWidthWheel * Math.pow(1.2, -e.getWheelRotation()));
                        logger.info("Storke width changed to {}", graphLineWidthZoom * graphLineWidthWheel);
                        graphLineStroke = new BasicStroke(graphLineWidthZoom * graphLineWidthWheel);
                    }

                    // Make a small delay before update the graph
                    delayedUpdate();
                }
            }
        } ;

        KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(e -> {
            ctrlPressed = ((e.getModifiers() & KeyEvent.CTRL_MASK) != 0);
            return false;
        });

        this.addMouseListener(mouseHandler);
        this.addMouseMotionListener(mouseHandler);
        this.addMouseWheelListener(mouseHandler);
    }

    private synchronized void delayedUpdate() {
        if(delayedUpdateGraph != null) {
            delayedUpdateGraph.cancel();
        }
        delayedUpdateGraph = new Timer();
        delayedUpdateGraph.schedule(new TimerTask() {
            @Override
            public void run() {
                paintGraph();
                repaint();
            }
        }, 250);

    }

    public void setBoundary(XYSample.XYSampleBoundary boundary) {
        this.boundary = boundary;
        delayedUpdate() ;
    }

    public void setColorProvider(ColorProvider colorProvider) {
        this.colorProvider = colorProvider;
        delayedUpdate() ;
    }

    public void setFontSize(int size) {
        setGraphicsFont(new Font(Font.SANS_SERIF, Font.PLAIN, size)) ;
    }

    private void setGraphicsFont(Font font) {
        this.font = font ;

        if(graphicsLegend != null) {
            graphicsLegend.setFont(font);
        }

        if(imageGraphics != null) {
            imageGraphics.setFont(font);
            fontHeight = imageGraphics.getFontMetrics().getHeight() ;
        }

        delayedUpdate() ;
    }

    public void setBackgroundColor(Color backgroundColor) {
        this.backgroundColor = backgroundColor;
        delayedUpdate() ;
    }

    public void setSequenceAlpha(int sequenceAlpha) {
        this.sequenceAlpha = sequenceAlpha;
        delayedUpdate() ;
    }

    public void setStrokeWidth(float width) {
        graphLineWidthWheel = width ;
        logger.info("Storke width changed to {}", graphLineWidthZoom * width);
        graphLineStroke = new BasicStroke(graphLineWidthZoom * width) ;
    }

    public void setDrawAsDots(boolean drawAsDots) {
        this.drawAsDots = drawAsDots;
        delayedUpdate() ;
    }

    public XYSample.XYSampleBoundary getBoundary() {
        return boundary;
    }

    public void addSampleSequence(Deque<XYSample> sequence)
    {
        if (sequence.size() > 1) {
            totalSamples += sequence.size() ;
            synchronized (samplesSequences) {
                samplesSequences.add(sequence);
            }

            // Update min-max
            boolean shouldRepaint = false;
            if (shouldUpdateBoundary) {
                for (XYSample sample : sequence) {
                    if (boundary.updateBoundary(sample)) {
                        shouldRepaint = true;
                    }
                }
            }

            if (shouldRepaint) {
                paintGraph();
            } else {
                if (imageGraphics != null) {
                    synchronized (imageGraphics) {
                        paintSequence(sequence);
                    }
                }
            }
            this.repaint();
        } else {
            logger.warn("Trying to add a sequence with size less then 2: {}", sequence.size());
        }
    }

    public void clean() {
        samplesSequences.clear();
        delayedUpdate() ;
    }

    public Dimension getPreferredSize() {
        return new Dimension(1900, 768);
    }

    @Override
    public void componentResized(ComponentEvent e) {
        updateSize(getWidth(), getHeight(), 1);
    }

    private void updateSize(int newWidth, int newHeight, int resolution) {
        int oldResolution = currentResolution ;
        currentResolution = resolution ;
        currentWidth = newWidth * resolution;
        currentHeight = newHeight * resolution;

        // Update legend dimensions
        int legendHeight = currentHeight - (GRAPH_BORDER.top*resolution+GRAPH_BORDER.bottom*resolution);
        int legendWidth = Math.max((int) (currentWidth * 0.02), 20*resolution) ;
        legendPosition = new Rectangle((int) LEGEND_OFFSET.getX()*resolution, GRAPH_BORDER.top*resolution, legendWidth, legendHeight) ;
        legendMousePosition = new Rectangle(0, GRAPH_BORDER.top*resolution, (int) (legendWidth + 2*LEGEND_OFFSET.getX()*resolution), legendHeight) ;

        int graphX = (int)(legendPosition.getWidth()+legendPosition.getX())+LEGEND_TEXT_SPACE*resolution + GRAPH_BORDER.left*resolution ;
        graphArea = new Envelope(graphX, currentWidth-GRAPH_BORDER.right*resolution, GRAPH_BORDER.top*resolution, currentHeight-GRAPH_BORDER.bottom*resolution) ;

        if (imageGraphics != null) {
            synchronized (imageGraphics) {
                graphImage = new BufferedImage(currentWidth, currentHeight, BufferedImage.TYPE_3BYTE_BGR);
                imageGraphics = (Graphics2D) graphImage.getGraphics();
                imageGraphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

                imageLegend = new BufferedImage(legendWidth + 2 * LEGEND_TEXT_SPACE*resolution + 2*(int) LEGEND_BORDER.getX()*resolution,
                        legendHeight + 2*(int) LEGEND_BORDER.getY()*resolution, BufferedImage.TYPE_4BYTE_ABGR) ;
                graphicsLegend = (Graphics2D) imageLegend.getGraphics() ;
                graphicsLegend.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            }
        } else {
            graphImage = new BufferedImage(currentWidth, currentHeight, BufferedImage.TYPE_3BYTE_BGR);
            imageGraphics = (Graphics2D) graphImage.getGraphics();
            imageGraphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

            imageLegend = new BufferedImage(legendWidth + 2 * LEGEND_TEXT_SPACE*resolution + 2*(int) LEGEND_BORDER.getX()*resolution,
                    legendHeight + 2*(int) LEGEND_BORDER.getY()*resolution, BufferedImage.TYPE_4BYTE_ABGR) ;
            graphicsLegend = (Graphics2D) imageLegend.getGraphics() ;
            graphicsLegend.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        }

        // Use the geometry to make intersections
        graphAreaGeometry = JTS.toGeometry(graphArea) ;

        // Keep font updated on new graphs
        if(font != null) {
            if (oldResolution == 0) {
                setFontSize(font.getSize() * resolution);
            } else {
                setFontSize(font.getSize() * currentResolution / oldResolution);
            }
        }
    }

    private void drawValues(double x, double y, Graphics g)
    {
        String msg = String.format("(%s, %s, %.2f(%s)/(%s))",
                xAxis.format(x),
                yAxis.format(y),
                x / y, xAxis.getName(), yAxis.getName()) ;

        drawTextAlignCenter(g, msg, (int) (graphArea.getMaxX() + graphArea.getMinX()) / 2, currentHeight - fontHeight / 2);
    }

    @Override
    protected void paintComponent(Graphics g)
    {
        ((Graphics2D)g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        if( font != null ) {
            g.setFont(font);
        }

        g.drawImage(graphImage, 0, 0, null) ;

        synchronized (samplesSequences) {
            if (selectedLegend != -1) {
                // Fade the graph...
                g.setColor(new Color(0xddffffff & backgroundColor.getRGB(), true));
                g.fillRect(0, 0, currentWidth, currentHeight);

                for (Collection<XYSample> sequence : samplesSequences) {
                    paintHighlightSequence(sequence, (Graphics2D) g);
                }
            }
        }

        if (graphSelection != null) {
            g.setColor(Color.WHITE);
            g.drawRect((int) graphSelection.getX(), (int) graphSelection.getY(), (int) graphSelection.getWidth(), (int) graphSelection.getHeight());

            double diffX = getSampleXForGraphX(graphSelection.getX() + graphSelection.getWidth()) - getSampleXForGraphX(graphSelection.getX()) ;
            double diffY = getSampleYForGraphY(graphSelection.getY()) - getSampleYForGraphY(graphSelection.getY() + graphSelection.getHeight()) ;
            drawValues(diffX, diffY, g);

        } else if (graphPoint != null) {
            double x = getSampleXForGraphX(graphPoint.getX()) ;
            double y = getSampleYForGraphY(graphPoint.getY()) ;
            g.setColor(Color.LIGHT_GRAY);

            drawValues(x, y, g);
        }

        g.drawImage(imageLegend, 0, 0, null) ;

        // Paint footer
        g.setColor(Color.WHITE);
        if (graphArea != null && graphSelection == null && graphPoint == null) {
            drawTextAlignCenter(g, "Total samples: " + totalSamples, (int) (graphArea.getMaxX() + graphArea.getMinX()) / 2, currentHeight - fontHeight/2);
        }
    }

    private void drawTextAlignCenter(Graphics g, String text, double x, double y)
    {
        int textWidth = g.getFontMetrics().stringWidth(text);
        g.drawString(text, (int) (x - textWidth / 2), (int) y);
    }

    private void drawTextAlignRight(Graphics g, String text, double x, double y)
    {
        int textWidth = g.getFontMetrics().stringWidth(text);
        g.drawString(text, (int) (x - textWidth), (int) y);
    }

    private void paintGraph()
    {
        if (imageGraphics != null) {
            synchronized (imageGraphics) {
                long start = System.currentTimeMillis();

                // Clean with backgroundColor
                imageGraphics.setColor(backgroundColor);
                imageGraphics.fillRect(0, 0, graphImage.getWidth(), graphImage.getHeight());

                // Vertical lines
                imageGraphics.setColor(Color.WHITE);
                imageGraphics.setStroke(new BasicStroke(verticalMarksStroke * currentResolution));
                for (Double mark : verticalMarks) {
                    int x = getGraphXForSampleX(mark) ;
                    if ( x > graphArea.getMinX() && x < graphArea.getMaxX() ) {
                        imageGraphics.drawLine(x, (int) graphArea.getMaxY()-5 * currentResolution, x, (int) graphArea.getMaxY()+2 * currentResolution);
                    }
                }
                imageGraphics.setStroke(new BasicStroke(1.0f * currentResolution));

                // Graph border
                imageGraphics.setColor(Color.LIGHT_GRAY);
                imageGraphics.drawRect((int)graphArea.getMinX(), (int)graphArea.getMinY(), (int)graphArea.getWidth(), (int)graphArea.getHeight());

                // Min/Max X
                imageGraphics.drawString(String.format("%s", xAxis.format(getCurrentBoundary().getMinX())), (int)graphArea.getMinX(), (int)(graphArea.getMaxY())+fontHeight);
                drawTextAlignRight(imageGraphics, String.format("%s", xAxis.format(getCurrentBoundary().getMaxX())), graphArea.getMaxX(), graphArea.getMaxY()+ fontHeight);
                // Min/Max Y

                double diffY = getCurrentBoundary().getMaxY() - getCurrentBoundary().getMinY() ;
                for( int i=0; i<=24; i++ ) {
                    double value = getCurrentBoundary().getMinY() + i * diffY / 24 ;
                    double y = graphArea.getMinY() + (24-i)*(graphArea.getMaxY()-graphArea.getMinY())/24 ;
                    drawTextAlignRight(imageGraphics, String.format("%s", yAxis.format(value)), graphArea.getMinX()-5 * currentResolution, y+fontHeight * 1 / 4);
                    imageGraphics.drawLine((int)(graphArea.getMinX() - 3), (int) y, (int) graphArea.getMinX(), (int) y);
                }

//                drawTextAlignRight(imageGraphics, String.format("%s", yAxis.format(getCurrentBoundary().getMinY())), graphArea.getMinX()-5 * currentResolution, graphArea.getMaxY());
//                drawTextAlignRight(imageGraphics, String.format("%s", yAxis.format(getCurrentBoundary().getMaxY())), graphArea.getMinX()-5 * currentResolution, graphArea.getMinY()+fontHeight * 3 / 4);

                // Draw Sequences
                long totalSamples ;
                synchronized (samplesSequences) {
                    totalSamples = samplesSequences.stream().mapToInt(sequence ->sequence.size()).sum() ;
                    samplesSequences.forEach(this::paintSequence);
                }

                paintLegend();

                logger.info("XYGraph full redraw of {} samples and {} sequences in {}ms", totalSamples, samplesSequences.size(), System.currentTimeMillis() - start);
            }
        }
    }

    private int getGraphXForSampleX(double x)
    {
        float offset = graphLineStroke.getLineWidth()/2 ;
        return (int) ((graphArea.getWidth()-2*offset-2) * (x-getCurrentBoundary().getMinX()) / (getCurrentBoundary().getMaxX()-getCurrentBoundary().getMinX()) + graphArea.getMinX()+offset+1) ;
    }

    private double getSampleXForGraphX(double x)
    {
        return (x-graphArea.getMinX()-1) * (getCurrentBoundary().getMaxX()-getCurrentBoundary().getMinX()) / (graphArea.getWidth()-2) + getCurrentBoundary().getMinX() ;
    }

    private int getGraphYForSampleY(double y)
    {
        float offset = graphLineStroke.getLineWidth()/2 ;
        return (int) ((graphArea.getHeight()-2*offset-2) * (1 - (y-getCurrentBoundary().getMinY()) / (getCurrentBoundary().getMaxY()-getCurrentBoundary().getMinY())) + graphArea.getMinY()+offset+1) ;
    }

    private double getSampleYForGraphY(double y)
    {
        return getCurrentBoundary().getMaxY()-(y - graphArea.getMinY() - 1) * (getCurrentBoundary().getMaxY()-getCurrentBoundary().getMinY()) / (graphArea.getHeight()-2) + 0 ;
    }

    private Coordinate getCoordinateForSample(XYSample sample)
    {
        return new Coordinate(getGraphXForSampleX(sample.getX()), getGraphYForSampleY(sample.getY())) ;
    }

    public Color colorByPercent(float percent, int alpha)
    {
        int color = Color.HSBtoRGB(percent, 1.0f, 1.0f) & 0xffffff | (alpha<<24) ;
        return new Color(color, true) ;
    }

    private XYSample.XYSampleBoundary getCurrentBoundary()
    {
        return (selectedBoundary != null) ? selectedBoundary : boundary ;
    }

    private void paintSequence(Collection<XYSample> sequence)
    {
        Stroke originalStroke = imageGraphics.getStroke() ;
        imageGraphics.setStroke(graphLineStroke);
        Iterator<XYSample> iSamples = sequence.iterator() ;

        if( drawAsDots ) {
            while ( iSamples.hasNext() ) {
                XYSample newSample = iSamples.next();
                Coordinate current = getCoordinateForSample(newSample);
                imageGraphics.setColor(colorProvider.colorByPercent(newSample.getColorPercent(), sequenceAlpha));
                drawLine(imageGraphics, current, current);
            }
        } else {
            XYSample lastSample = iSamples.next() ;
            Coordinate last = getCoordinateForSample(lastSample) ;
            while ( iSamples.hasNext() ) {
                XYSample newSample = iSamples.next() ;
                Coordinate current = getCoordinateForSample(newSample) ;
                imageGraphics.setColor(colorProvider.colorByPercent(newSample.getColorPercent(), sequenceAlpha));
                drawLine(imageGraphics, last, current);
                last = current ;
            }
            imageGraphics.setStroke(originalStroke);
        }

    }

    private void paintHighlightSequence(Collection<XYSample> sequence, Graphics2D g)
    {
        if (selectedLegend == -1) return ;

        Iterator<XYSample> iSamples = sequence.iterator() ;
        XYSample lastSample = iSamples.next() ;
        Coordinate last = getCoordinateForSample(lastSample) ;

        Stroke blackOutlineStroke = new BasicStroke(graphLineStroke.getLineWidth() + 1) ;

        while ( iSamples.hasNext() ) {
            XYSample newSample = iSamples.next() ;
            Coordinate current = getCoordinateForSample(newSample) ;

            double diff = Math.abs(selectedLegend - newSample.getColorPercent()) ;

            int alpha ;
            if (diff < selectedLegendRange) {
                alpha = (int) (0xff * (selectedLegendRange - diff) / selectedLegendRange);

                // Draw black outline
                g.setStroke(blackOutlineStroke);
                g.setColor(new Color(alpha >> 2 << 24, true));
                if (drawAsDots) {
                    drawLine(g, current, current);
                } else {
                    drawLine(g, last, current);
                }

                // Draw line
                g.setStroke(graphLineStroke);
                g.setColor(colorProvider.colorByPercent(newSample.getColorPercent(), alpha));
                if (drawAsDots) {
                    drawLine(g, current, current);
                } else {
                    drawLine(g, last, current);
                }
            }
            last = current ;
        }
    }

    private void drawLine(Graphics2D g, Coordinate from, Coordinate to)
    {
        boolean containsFirst = graphArea.contains(from) ;
        boolean containsSecond = graphArea.contains(to) ;

        if (containsFirst != containsSecond) {
            LineString ls = geometryFactory.createLineString(new Coordinate[]{from, to}) ;

            Geometry result = graphAreaGeometry.intersection(ls) ;
            if (result instanceof LineString) {
                LineString lsResult = (LineString) result ;
                if (!lsResult.isEmpty()) {
                    containsFirst = true;
                    containsSecond = true;

                    from = lsResult.getCoordinateN(0);
                    to = lsResult.getCoordinateN(lsResult.getNumPoints() - 1);
                }
            }
        }

        if (containsFirst && containsSecond) {
            g.drawLine((int) from.x, (int) from.y, (int) to.x, (int) to.y);
        }
    }

    public void setLegend(SortedMap<Float, String> legend) {
        this.legend = legend;
        delayedUpdate() ;
    }

    private void paintLegend()
    {
        if(legend != null) {
            int fontOffset = graphicsLegend.getFontMetrics(graphicsLegend.getFont()).getHeight() / 4;
            int legendHeight = (int) legendPosition.getHeight() ;
            int legendWidth = (int) legendPosition.getWidth() ;
            int legendX = (int) legendPosition.getX();

            // Clear imageGraphics
            graphicsLegend.setBackground(new Color(255, 255, 255, 0));
            graphicsLegend.clearRect(0,0, imageLegend.getWidth(), imageLegend.getHeight());

            // Move to border
            AffineTransform originalGraphics = graphicsLegend.getTransform() ;
            graphicsLegend.translate(LEGEND_BORDER.getX() * currentResolution, LEGEND_BORDER.getY() * currentResolution);
            graphicsLegend.setStroke(new BasicStroke(1.0f * currentResolution));

            Iterator<Float> iKeys = legend.keySet().iterator();
            Float lastKey = iKeys.next();
            for(int y=0; y<=legendHeight; y++) {
                float colorPercent = y / (float) legendHeight ;
                graphicsLegend.setColor(colorProvider.colorByPercent(colorPercent, 0xff));
                if (lastKey != null && colorPercent >= lastKey) {
//                    imageGraphics.setColor(Color.WHITE);
                    graphicsLegend.drawLine(legendX - 5 * currentResolution, y, legendX, y);
                    String str = legend.get(lastKey) ;
                    drawTextAlignRight(graphicsLegend, str, legendX - 7 * currentResolution, y + fontOffset);
                    if(iKeys.hasNext()) {
                        lastKey = iKeys.next();
                    }
                }
                graphicsLegend.drawLine(legendX, y, legendWidth+legendX, y);
            }

            if ( selectedLegend >= 0 ) {
                graphicsLegend.setColor(Color.LIGHT_GRAY);
                double minLegend = Math.max(0.0, selectedLegend - selectedLegendRange) ;
                double maxLegend = Math.min(1.0, selectedLegend + selectedLegendRange) ;
                double minLegendY = legendPosition.getHeight() * minLegend ;
                double maxLegendY = legendPosition.getHeight() * maxLegend + 1 * currentResolution ;
                graphicsLegend.drawRect( legendX-1 * currentResolution, (int)minLegendY, legendWidth+2 * currentResolution, (int)(maxLegendY-minLegendY) );

                if ( legendForColorPercent != null ) {
                    String minLegendName = legendForColorPercent.apply(minLegend) ;
                    String maxLegendName = legendForColorPercent.apply(maxLegend) ;
                    graphicsLegend.drawString(minLegendName, legendWidth + legendX + 10 * currentResolution, (int) minLegendY+fontOffset);
                    graphicsLegend.drawString(maxLegendName, legendWidth + legendX + 10 * currentResolution, (int) maxLegendY+fontOffset);
                }
            }

            graphicsLegend.setTransform(originalGraphics);
        }
    }

    public void setLegendForColorPercent(Function<Double, String> legendForColorPercent) {
        this.legendForColorPercent = legendForColorPercent;
        delayedUpdate() ;
    }

    public void saveData() {
        final JFileChooser fc = new JFileChooser();
        fc.setFileFilter(new FileNameExtensionFilter("XYGraph Data", "xyd"));
        int ret = fc.showSaveDialog(XYGraph.this) ;
        if ( ret == JFileChooser.APPROVE_OPTION ) {
            logger.info("Saving data...");
            File file = fc.getSelectedFile();

            if ( !file.getName().endsWith(".xyd") ) {
                file = new File(file.getAbsoluteFile() + ".xyd") ;
            }

            saveData(file) ;
        }
    }
    public void saveImage() {
        saveImage(1);
    }

    public void saveImage2x() {
        saveImage(2);
    }

    public void saveImage4x() {
        saveImage(4);
    }

    public void saveImage(int resolution) {
        final JFileChooser fc = new JFileChooser(fileLoaded);
        if (fileLoaded != null) {
            File suggestedName ;
            if ( resolution == 1 ) {
                suggestedName = new File(fileLoaded.getAbsolutePath().replace(".xyd", ".png")) ;
            } else {
                suggestedName = new File(fileLoaded.getAbsolutePath().replace(".xyd", "x" + resolution +  ".png")) ;
            }
            fc.setSelectedFile(suggestedName);
        }
        fc.setFileFilter(new FileNameExtensionFilter("PNG Image", "png"));
        int ret = fc.showSaveDialog(XYGraph.this) ;
        if ( ret == JFileChooser.APPROVE_OPTION ) {
            logger.info("Saving image...");
            File file = fc.getSelectedFile();

            if ( !file.getName().endsWith(".png") ) {
                file = new File(file.getAbsoluteFile() + ".png") ;
            }

            try {
                saveImage(file, resolution) ;
                logger.info("Image saved");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void saveImage(File file, int resolution) throws IOException {
        logger.info("Saving to file {}", file.getPath()) ;
        int oldWidth = currentWidth ;
        int oldHeight = currentHeight ;
        int oldResolution = currentResolution ;

        updateSize(currentWidth, currentHeight, resolution);

        BufferedImage img = new BufferedImage(currentWidth, currentHeight, BufferedImage.TYPE_3BYTE_BGR) ;

        if (img == null) {
            throw new NullPointerException("Image is null") ;
        }

        // Remove all mouse-over info
        selectedLegend = -1 ;
        graphPoint = null ;
        graphSelection = null ;
        synchronized (imageGraphics) {
            this.paintGraph();
            this.paintComponent(img.getGraphics());
        }
        ImageIO.write(img, "png", file) ;

        updateSize(oldWidth, oldHeight, oldResolution);
    }

    public void saveData(File file)
    {
        try {
            DataOutputStream dataOut = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(file))) ;
            try {
                synchronized (samplesSequences) {
                    // Header
                    dataOut.writeLong(FILE_MAGIC);
                    dataOut.writeInt(FILE_VERSION);

                    // Axis order
                    dataOut.writeInt(xAxis.ordinal());
                    dataOut.writeInt(yAxis.ordinal());

                    // We must write these numbers in the same order of the constructor (so loading is easier)
                    dataOut.writeDouble(boundary.getMinX());
                    dataOut.writeDouble(boundary.getMinY());
                    dataOut.writeDouble(boundary.getMaxX());
                    dataOut.writeDouble(boundary.getMaxY());

                    // Write the vertical ines
                    dataOut.writeInt(verticalMarks.size()) ;
                    for (Double mark : verticalMarks) {
                        dataOut.writeDouble(mark);
                    }

                    // Sequences data
                    for (Deque<XYSample> sequence : samplesSequences) {
                        dataOut.writeInt(sequence.size());
                        for (XYSample xySample : sequence) {
                            dataOut.writeDouble(xySample.getX());
                            dataOut.writeDouble(xySample.getY());
                            dataOut.writeFloat(xySample.getColorPercent());
                        }
                    }
                }

                dataOut.flush();
                logger.info("Done saving data...");
            } finally {
                dataOut.close();
            }


        } catch (IOException e1) {
            logger.error("Error while saving data...", e1);
        }
    }

    public static XYGraph loadFromFile(File file) throws IOException {
        logger.info("Loading graph from file {}...", file.getName());
        DataInputStream dataIn = new DataInputStream(new BufferedInputStream(new FileInputStream(file))) ;
        try {
            int fileVersion = 0x1 ;
            // Header
            dataIn.mark(64);
            if (dataIn.readLong() == FILE_MAGIC) {
                fileVersion = dataIn.readInt() ;
            } else {
                dataIn.reset();
            }

            // Read axis
            XYGraphAxisType xAxis = XYGraphAxisType.values()[ dataIn.readInt() ] ;
            XYGraphAxisType yAxis = XYGraphAxisType.values()[ dataIn.readInt() ] ;

            // Read boundaries
            XYGraph me = new XYGraph(xAxis, yAxis);
            me.fileLoaded = file ;
            me.setBoundary(new XYSample.XYSampleBoundary(dataIn.readDouble(), dataIn.readDouble(), dataIn.readDouble(), dataIn.readDouble()) ) ;

            // Read vertical lines
            if (fileVersion >= 2) {
                int numVerticalLines = dataIn.readInt();
                for (; numVerticalLines > 0; numVerticalLines--) {
                    me.addVerticalMark(dataIn.readDouble());
                }
            }

            // Read sequences data
            while (dataIn.available() > 0 ) {
                int sequenceSize = dataIn.readInt() ;
                Deque<XYSample> sequence = new ArrayDeque<>(sequenceSize) ;
                for(; sequenceSize > 0; sequenceSize-- ) {
                    double x = dataIn.readDouble() ;
                    double y = dataIn.readDouble() ;
                    float colorPercent = dataIn.readFloat() ;
                    sequence.add(new XYSample(x, y, colorPercent)) ;
                }

                me.addSampleSequence(sequence) ;
            }

            return me ;
        } finally {
            dataIn.close() ;
        }
    }

    public XYGraphAxisType getxAxis() {
        return xAxis;
    }

    public XYGraphAxisType getyAxis() {
        return yAxis;
    }

    public void setShouldUpdateBoundary(boolean shouldUpdateBoundary) {
        this.shouldUpdateBoundary = shouldUpdateBoundary;
    }

    @Override
    public void componentMoved(ComponentEvent e) {

    }

    @Override
    public void componentShown(ComponentEvent e) {

    }

    @Override
    public void componentHidden(ComponentEvent e) {

    }

    public void addVerticalMark(Double mark) {
        verticalMarks.add(mark) ;
    }
}
