package br.pucrio.bguberfain.onibus.gui;

import br.pucrio.bguberfain.onibus.util.Util;
import com.jhlabs.map.proj.Projection;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Envelope;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class GeoImage {

	protected Projection projection;
	private Point2D.Double minLonLat;
	private Point2D.Double maxLonLat;
	private double diffLat, diffLon;
	protected Graphics2D graph;
	private BufferedImage image;
	Envelope region ;
	Lock lock = new ReentrantLock();

	int imgWidth, imgHeight;

	public GeoImage(Envelope region, int imgWidth) {
		super();
		this.imgWidth = imgWidth;
		this.region = region ;
		this.projection = Util.getDefaultProjection() ;

		minLonLat = new Point2D.Double(0, 0);
		maxLonLat = new Point2D.Double(0, 0);
		this.projection.transform(region.getMinX(), region.getMinY(), minLonLat);
		this.projection.transform(region.getMaxX(), region.getMaxY(), maxLonLat);

		diffLon = maxLonLat.getX() - minLonLat.getX();
		diffLat = maxLonLat.getY() - minLonLat.getY();

		imgHeight = (int) (imgWidth * diffLat / diffLon);

//		System.out.println("img: " + imgWidth + ", " + imgHeight);

		image = new BufferedImage(imgWidth, imgHeight, BufferedImage.TYPE_3BYTE_BGR);
		graph = (Graphics2D) image.getGraphics();
		graph.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	}

	private int convertLatitudeToY(double lat) {
		return (int) (imgHeight - (imgHeight * (lat - minLonLat.getY()) / diffLat));
	}

	private int convertLongitudeToX(double lon) {
		return (int) (imgWidth * (lon - minLonLat.getX()) / diffLon);
	}
	
	protected Point projectedLatLon(double lon, double lat, double z)
	{
		Point2D.Double lonLat = new Point2D.Double(0,0) ;
		projection.transform(lon, lat, lonLat);

		return new Point(convertLongitudeToX(lonLat.x), convertLatitudeToY(lonLat.y)) ;
	}
	
	public Projection getProjection() {
		return projection;
	}
	
	public BufferedImage getImage() {
		return image;
	}
	
	public void setColor(Color c)
	{
		graph.setColor(c) ;
	}
	
	public void setStrokeWidth(float width)
	{
		graph.setStroke(new BasicStroke(width, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
	}

    public void clear() {
        graph.fillRect(0, 0, imgWidth, imgHeight);
    }
	
	public boolean drawArrow(double lat1, double lon1, double lat2, double lon2, int arrowSize)
	{
		Point pt1 = projectedLatLon(lon1, lat1, 0) ;
		Point pt2 = projectedLatLon(lon2, lat2, 0) ;

		if (pt1 == null || pt2 == null) return false ;

		drawArrow(pt1.x, pt1.y, pt2.x, pt2.y, arrowSize);
		
		return
			(pt1.x >= 0 && pt1.x <= image.getWidth() && pt1.y >= 0 && pt1.y <= image.getHeight()) ||
			(pt2.x >= 0 && pt2.x <= image.getWidth() && pt2.y >= 0 && pt2.y <= image.getHeight()) ;
	}
	
	public void drawString(String text, double lat, double lon)
	{
		Point pt = projectedLatLon(lon, lat, 0) ;
		if (pt == null) return ;

		graph.drawString(text, pt.x, pt.y);
	}

	public void drawStringRotated(String text, double angle, double lat, double lon)
	{
		Point pt = projectedLatLon(lon, lat, 0) ;
		if (pt == null) return ;

		AffineTransform orig = graph.getTransform();
		graph.translate(pt.x, pt.y);
		graph.rotate(angle);
		graph.drawString(text, 0, 0);
		graph.setTransform(orig);
	}

	public boolean drawLine(double lat1, double lon1, double z1, double lat2, double lon2, double z2)
	{
		lock();
		try {
			Point pt1 = projectedLatLon(lon1, lat1, z1);
			Point pt2 = projectedLatLon(lon2, lat2, z2);

			if (pt1 == null || pt2 == null) return false;

			graph.drawLine(pt1.x, pt1.y, pt2.x, pt2.y);

			return
					(pt1.x >= 0 && pt1.x <= image.getWidth() && pt1.y >= 0 && pt1.y <= image.getHeight()) ||
							(pt2.x >= 0 && pt2.x <= image.getWidth() && pt2.y >= 0 && pt2.y <= image.getHeight());
		} finally {
			unlock();
		}
	}

	private void drawArrow(int x1, int y1, int x2, int y2, int arrowSize) {
		int dx = x2 - x1, dy = y2 - y1;
		double angle = Math.atan2(dy, dx);
		int len = (int) Math.sqrt(dx * dx + dy * dy);
		AffineTransform at = AffineTransform.getTranslateInstance(x1, y1);
		at.concatenate(AffineTransform.getRotateInstance(angle));
		Graphics2D g2 = (Graphics2D) graph.create() ;
		
		g2.transform(at);

		// Draw horizontal arrow starting in (0, 0)
		g2.drawLine(0, 0, len, 0);
		g2.fillPolygon(new int[] { len, len - arrowSize, len - arrowSize, len }, new int[] { 0, -arrowSize / 2, arrowSize / 2, 0 }, 4);
		g2.dispose();
	}

	public boolean drawOval(double latitude, double longitude, int width) {
		Point centerLonLat = projectedLatLon(longitude, latitude, 0) ;
		if (centerLonLat == null) return false ;

		int radius = width / 2 ;
		graph.drawOval(centerLonLat.x-radius, centerLonLat.y-radius, width, width);
		
		return
				(centerLonLat.x >= 0 && centerLonLat.x <= image.getWidth()) &&
				(centerLonLat.y >= 0 && centerLonLat.y <= image.getHeight()) ;
	}

	public boolean fillOval(double latitude, double longitude, int width) {
		Point centerLonLat = projectedLatLon(longitude, latitude, 0) ;
		if (centerLonLat == null) return false ;
		int radius = width / 2 ;
		graph.fillOval(centerLonLat.x-radius, centerLonLat.y-radius, width, width);
		
		return
				(centerLonLat.x >= 0 && centerLonLat.x <= image.getWidth()) &&
				(centerLonLat.y >= 0 && centerLonLat.y <= image.getHeight()) ;
	}

	public boolean drawLine(Coordinate coordinate1, Coordinate coordinate2) {
		return drawLine(coordinate1.y, coordinate1.x, coordinate1.z, coordinate2.y, coordinate2.x, coordinate2.z);
	}

	public void drawArrow(Coordinate coordinate1, Coordinate coordinate2, int arrowSize) {
		drawArrow(coordinate1.y, coordinate1.x, coordinate2.y, coordinate2.x, arrowSize);
	}

	public void setPixel(Coordinate coordinate, int color) {
		if(region.contains(coordinate)) {
			Point pt = projectedLatLon(coordinate.x, coordinate.y, 0);
			if (pt == null) return;

			int fgAlpha = color >> 24 & 0xff ;
			int current = image.getRGB(pt.x, pt.y) ;
			int bgAlpha = 255 - fgAlpha  ;
			int newColor =
					((((color & 0xff) * fgAlpha) / 255 + ((current & 0xff) * (bgAlpha)) / 255) & 0xff) |	// red
					((((((color >> 8) & 0xff) * fgAlpha) / 255 + (((current >> 8) & 0xff) * (bgAlpha)) / 255 ) & 0xff) << 8) |	// green
					((((((color >> 16) & 0xff) * fgAlpha) / 255 + (((current >> 16) & 0xff) * (bgAlpha)) / 255) & 0xff) << 16) ;	// blue

			image.setRGB(pt.x, pt.y, newColor);
		}
	}


	public void lock() {
		lock.lock();
	}

	public void unlock() {
		lock.unlock();
	}
}
