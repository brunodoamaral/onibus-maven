package br.pucrio.bguberfain.onibus.gui;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.math.Vector2D;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

import java.awt.*;
import java.awt.geom.Point2D;

import static org.apache.commons.math3.geometry.euclidean.threed.Vector3D.crossProduct;

public class GeoImage3D extends GeoImage {

	double angle = Math.PI / 2 ;
	double viewHeight = 0.07 ;
	Vector3D a1, a2, a3, at, from ;
	double offX, offY, offZ, dVal, A, B, C, D ;

	public GeoImage3D(Envelope region, int imgWidth) {
		super(region, imgWidth);

		angle = Math.PI / 4.5 ;
		dVal = Math.cos(angle/2) / Math.sin(angle/2) ;

//		from = new Vector3D(region.centre().x + region.getWidth() * 1.20, region.centre().y - region.getHeight() * 1.40, viewHeight) ;
//		at = new Vector3D(region.centre().x, region.centre().y, 0) ;

//		from = new Vector3D(region.getMaxX()+region.getWidth()*0.1, region.getMinY()-region.getHeight()*0.1, viewHeight) ;
//		at = new Vector3D(region.centre().x+region.getWidth()*0.2, region.centre().y, 0) ;

		from = new Vector3D(region.getMaxX()+region.getWidth()*0.2, region.centre().y+region.getHeight()*0.2, viewHeight) ;
		at = new Vector3D(region.getMinX()-region.getWidth()*0.2, region.centre().y, -0.2) ;

		Vector3D up = new Vector3D(0, 0, 1) ;
		Vector3D dist = at.subtract(from) ;
		double amm = dist.getNorm() ;
		a3 = dist.scalarMultiply(1/amm) ;

		Vector3D tmp = crossProduct(dist, up) ;
		double tm = tmp.getNorm() ;
		a1 = tmp.scalarMultiply(1/tm) ;

		tmp = crossProduct(a1, a3) ;
		tm = tmp.getNorm() ;
		a2 = tmp.scalarMultiply(1/tm) ;

		offX = -a1.getX() * from.getX() - a1.getY() * from.getY() - a1.getZ() * from.getZ();
		offY = -a2.getX() * from.getX() - a2.getY() * from.getY() - a2.getZ() * from.getZ();
		offZ = -a3.getX() * from.getX() - a3.getY() * from.getY() - a3.getZ() * from.getZ();

		A = (imgWidth/2 < imgHeight/2) ? imgWidth/2 : imgHeight/2;
		B = imgWidth/2;
		C = -A;
		D = imgHeight/2;
	}

	private Vector2D get2dPoint(double x, double y, double z) {
		double z1 =  x*a3.getX() + y*a3.getY() + z*a3.getZ() + offZ;
		if (z1 == 0) return null ;
		double x1 = (x*a1.getX() + y*a1.getY() + z*a1.getZ() + offX) * dVal;
		double y1 = (x*a2.getX() + y*a2.getY() + z*a2.getZ() + offY) * dVal;

		if (Double.isFinite(x1) && Double.isFinite(y1))
			return new Vector2D(A * x1 / z1 + B, C * y1 / z1 + D);
		else
			return null ;
	}

	@Override
	protected Point projectedLatLon(double lon, double lat, double z)
	{
		Point2D.Double lonLat = new Point2D.Double(0,0) ;
		projection.transform(lon, lat, lonLat);

		Vector2D pt = get2dPoint(lon, lat, Double.isNaN(z) ? 0 : z) ;

		if (pt == null) return null ;

		if ( pt.getX() > 0 && pt.getX() < super.imgWidth && pt.getY() > 0 && pt.getY() < imgHeight )
			return new Point((int)pt.getX(), (int)pt.getY()) ;
		else
			return null ;
	}

	public void setColorWithDistanceAlpha(Color c, int maxAlpha, Coordinate coordinate)
	{
		Vector3D pt = new Vector3D(coordinate.x, coordinate.y, viewHeight) ;
		double distanceToView = pt.subtract(from).getNorm() ;
		double relativeDistance = (region.getArea() / (distanceToView * distanceToView)) * 0.5 ;


		int alpha = maxAlpha ;
		if (relativeDistance < 1 ) {
			alpha *= relativeDistance ;
		} else {
			relativeDistance = 1 ;
		}
		graph.setStroke(new BasicStroke((float)relativeDistance / 10));

		super.setColor(new Color(c.getRGB() & 0xffffff | alpha << 24, true));
	}
}
