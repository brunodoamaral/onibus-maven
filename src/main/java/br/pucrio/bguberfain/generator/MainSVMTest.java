package br.pucrio.bguberfain.generator;

import br.pucrio.bguberfain.onibus.gui.XYGraph;
import br.pucrio.bguberfain.onibus.model.graph.v2.Route;
import br.pucrio.bguberfain.onibus.model.graph.v2.XYSample;
import br.pucrio.bguberfain.onibus.util.NetworkTopologyDS;
import de.bwaldvogel.liblinear.Feature;
import de.bwaldvogel.liblinear.FeatureNode;
import de.bwaldvogel.liblinear.Linear;
import de.bwaldvogel.liblinear.Model;

import java.io.File;
import java.util.*;

/**
 * Created by Bruno on 18/07/2015.
 */
public class MainSVMTest extends MainGraphBase {
    protected static final int SHAPE_TO_LOAD = 17573358 ;
    protected static final int PRECISION_MINUTES = 5 ; // must divide by 24h
    protected static final int PRECISION_METERS = 10 ;
    protected static final double MAX_TRAVEL_IN_DISCRETE_TIME = 1500 ;
    protected static final int NUM_SAMPLES = 500 ;

    public static void main(String [] args) throws Exception {
        Map<Integer, Route> routesByShapeId = NetworkTopologyDS.getInstance().getNetworkByShapeId(null, true) ;
        final Route route = routesByShapeId.get(SHAPE_TO_LOAD) ;
        final double routeLength = route.geometryLengthInMeters() ;
        XYGraph xyGraph = createAndShowGUI("Space Time Graph", XYGraph.XYGraphAxisType.METERS, XYGraph.XYGraphAxisType.SECONDS);
        XYSample.XYSampleBoundary graphBoundary = new XYSample.XYSampleBoundary(0, 0, route.geometryLengthInMeters(), 2*60*60 + 10*60) ;
        xyGraph.setBoundary(graphBoundary);
        xyGraph.setShouldUpdateBoundary(false);

        // Configure font size
        xyGraph.setFontSize(20);
        xyGraph.setSequenceAlpha(40);

        Model model = Model.load(new File(args[0]));
        System.out.println("Model loaded");

        for(int i=0; i<NUM_SAMPLES; i++) {
            double time = i / (double)NUM_SAMPLES * (23-5) / 24.0 + 5 / 24.0 ;    // between 5:00 and 23:00
            double departureTime = time ;
            double position = 100  ;
            Deque<XYSample> samples = new LinkedList<>() ;
            while(position < routeLength && samples.size() < 200) {
                List<Feature> features = new LinkedList<>() ;
//                features.add(new FeatureNode(1, position)) ;
//                features.add(new FeatureNode(2, time)) ;

                int sampleMinutes = (int) ( 60*24 * time );
                int attrIndex = sampleMinutes / PRECISION_MINUTES ;
                for(int j=0; j<=24*60/PRECISION_MINUTES; j++) {
                    int idxDiff = attrIndex-j ;
                    double value = Math.exp( -idxDiff*idxDiff ) ;
                    if (value > 1e-7) {
                        features.add(new FeatureNode(j + 1, value)) ;
                    }
                }

                int firstDistanceAttr = 24*60/PRECISION_MINUTES ;
                int dIdx = 0 ;
                while(dIdx * PRECISION_METERS < routeLength) {
                    int idxDiff = (int) ((position - dIdx * PRECISION_METERS) / PRECISION_METERS);
                    double value = Math.exp( -idxDiff*idxDiff ) ;
                    if (value > 1e-7) {
                        features.add(new FeatureNode(dIdx + firstDistanceAttr, value)) ;
                    }
                    dIdx++ ;
                }

                Feature[] instance = features.toArray(new Feature[0]) ;
                double predicted = Linear.predict(model, instance) ;
                position += predicted * MAX_TRAVEL_IN_DISCRETE_TIME ;

//                double speed = (predicted - position) * routeLength / 60 ;
                samples.add(new XYSample(position, (time-departureTime) * 24*60*60, (float) departureTime)) ;

                time += 1.0 / (24*60) ; // +1 minute in 24h
                if (time > 1) {
                    time = 0 ;
                }
            }

            xyGraph.addSampleSequence(samples);
        }
    }
}
