package br.pucrio.bguberfain.generator;

import br.pucrio.bguberfain.onibus.gui.GeoImage;
import br.pucrio.bguberfain.onibus.gui.ImageViewer;
import br.pucrio.bguberfain.onibus.gui.Region;
import br.pucrio.bguberfain.onibus.util.Util;
import com.vividsolutions.jts.geom.*;
import com.vividsolutions.jts.geom.Point;
import org.geotools.data.DataStore;
import org.geotools.data.DataStoreFinder;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.factory.CommonFactoryFinder;
import org.geotools.geometry.jts.ReferencedEnvelope;
import org.geotools.graph.build.basic.BasicDirectedGraphBuilder;
import org.geotools.graph.build.line.DirectedLineStringGraphGenerator;
import org.geotools.graph.path.DijkstraShortestPathFinder;
import org.geotools.graph.path.Path;
import org.geotools.graph.structure.*;
import org.geotools.graph.traverse.standard.DijkstraIterator;
import org.geotools.graph.util.geom.GeometryUtil;
import org.geotools.jdbc.JDBCDataStoreFactory;
import org.geotools.referencing.crs.DefaultGeographicCRS;
import org.geotools.util.NullProgressListener;
import org.opengis.feature.Feature;
import org.opengis.feature.FeatureVisitor;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.type.FeatureType;
import org.opengis.filter.Filter;
import org.opengis.filter.FilterFactory2;

import java.awt.*;
import java.io.IOException;
import java.util.*;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicIntegerArray;

/**
 * Created by bguberfain on 1/11/2015.
 */
public class MainOSMGraph
{
    private static final Envelope region = Region.RIO_DE_JANEIRO ;

    public static void main(String args[]) throws Exception
    {
        BasicDirectedGraphBuilder graphBuilder = new BasicDirectedGraphBuilder();
        DirectedLineStringGraphGenerator graphGenerator = new DirectedLineStringGraphGenerator() ;
        graphGenerator.setGraphBuilder(graphBuilder);

        System.out.print("Loading OSM...");
        visitOSM(region, (Feature feature) -> {
            SimpleFeature simpleFeature = (SimpleFeature) feature;
            LineString featureLS = (LineString) simpleFeature.getDefaultGeometry();

            graphGenerator.add(featureLS) ;

            // Add reverse direction
//            if (((String) simpleFeature.getAttribute("osm_tags")).contains("oneway=no")) {
//                LineString reverseLS = (LineString) GeometryUtil.reverseGeometry(featureLS, false);
//                graphGenerator.add(reverseLS) ;
//            }
        });
        System.out.println("done");

        GeoImage img = new GeoImage(region, 2048);

        Graph osmGraph = graphGenerator.getGraph() ;
        Node destinationNode = findNodeAtCoordinate(new Coordinate(-43.1783604, -22.9401549), osmGraph) ;
        Node homeNode = findNodeAtCoordinate( new Coordinate(-43.1753714, -22.9389088), osmGraph ) ;

        osmGraph.visitEdges(component -> {
            drawEdgeOnImage((Edge) component, img);
            return Graph.PASS_AND_CONTINUE ;
        });

        final Map<Node, DijkstraShortestPathFinder> pathFinderForNode = Collections.synchronizedMap(new HashMap<>()) ;
        DijkstraIterator.EdgeWeighter weighter = new EdgeWeighterByDistance(osmGraph) ;

        System.out.print("Calculating all Dijkstras...");
        long start = System.currentTimeMillis() ;
        int total = osmGraph.getNodes().size() ;
        AtomicInteger count = new AtomicInteger(0) ;
        osmGraph.getNodes().stream().forEach(component -> {
            Node node = (Node) component;
            DijkstraShortestPathFinder pathFinder = new DijkstraShortestPathFinder(osmGraph, node, weighter);
            pathFinder.calculate();
            pathFinderForNode.put(node, pathFinder);

            int current = count.incrementAndGet();
            synchronized (System.out) {
                System.out.printf("Processed %d nodes (%5.1f%%) at rate %4.1f nodes/s\r", current, current * 100.0 / total, current * 1000.0 / (System.currentTimeMillis() - start));
            }
        });
        System.out.println("done");

        ImageViewer imgViewer = new ImageViewer(img, true);
        imgViewer.setVisible(true);

        System.out.print("Calculating Dijkstra...");
//        DijkstraShortestPathFinder pathFinder = pathFinderForNode.get(homeNode) ;
        DijkstraShortestPathFinder pathFinder = new DijkstraShortestPathFinder(osmGraph, homeNode, weighter);
        pathFinder.calculate();
        Path source2Destination = pathFinder.getPath(destinationNode) ;
        System.out.println("done");
        img.setColor(Color.RED);
        if (source2Destination!= null) {
            Iterator iPath = source2Destination.iterator();
            int current = 0 ;
            while ( iPath.hasNext() ) {
                Node node = (Node) iPath.next() ;
                current++ ;
                Coordinate coordinate = ((Point) node.getObject()).getCoordinate() ;
                img.drawOval(coordinate.y, coordinate.x, 10);
                img.drawString("" + current, coordinate.y + 0.00005, coordinate.x + 0.00005);
            }
            imgViewer.setImage(img);    // Update image
        } else {
            System.out.println("No route found");
        }
    }

    private static void drawEdgeOnImage(Edge edge, GeoImage img)
    {
        Coordinate from = ((Point)edge.getNodeA().getObject()).getCoordinate() ;
        Coordinate to = ((Point)edge.getNodeB().getObject()).getCoordinate() ;

        img.drawArrow( from, to, 6 ) ;
    }

    private static Node findNodeAtCoordinate(Coordinate home, Graph osmGraph)
    {
        if ( !region.contains(home) ) {
            throw new RuntimeException("Coordinate " + home + " is outside region " + region) ;
        }
        List<Node> result = osmGraph.queryNodes(component -> {
            Node node = (Node) component;
            if (home.equals(((Point) node.getObject()).getCoordinate())) {
                return Graph.PASS_AND_STOP;
            } else {
                return Graph.FAIL_QUERY;
            }
        }) ;
        return result.isEmpty() ? null : result.get(0) ;
    }

    private static class EdgeWeighterByDistance implements DijkstraIterator.EdgeWeighter
    {
        Map<Edge, Double> lenghtByEdge = new HashMap<>() ;

        public EdgeWeighterByDistance(Graph osmGraph) {
            osmGraph.visitEdges(component -> {
                LineString ls = (LineString) component.getObject();
                double lengthInMeters = 0 ;
                Coordinate last = ls.getCoordinateN(0) ;
                for( int i=1; i<ls.getNumPoints(); i++ ) {
                    Coordinate current = ls.getCoordinateN(i) ;
                    lengthInMeters += Util.distanceFrom(last, current) ;
                    current = last ;
                }

                lenghtByEdge.put((Edge) component, lengthInMeters) ;
                return Graph.PASS_AND_CONTINUE ;
            });
        }

        @Override
        public double getWeight(Edge e) {
            Double distance = lenghtByEdge.get(e) ;
            if ( distance == null ) throw new RuntimeException("Edge not in graph") ;
            return distance ;
        }
    }

    // Reference code from http://docs.geotools.org/latest/userguide/library/main/filter.html
    private static SimpleFeatureCollection grabFeaturesInBoundingBox(SimpleFeatureSource featureSource, Envelope bbox) throws IOException {
        FilterFactory2 ff = CommonFactoryFinder.getFilterFactory2();
        FeatureType schema = featureSource.getSchema();

        // usually "THE_GEOM" for shapefiles
        String geometryPropertyName = schema.getGeometryDescriptor().getLocalName();

        ReferencedEnvelope refbbox = ReferencedEnvelope.create(bbox, DefaultGeographicCRS.WGS84) ;

        Filter filter = ff.bbox(ff.property(geometryPropertyName), refbbox);
        return featureSource.getFeatures(filter);
    }

    private static void visitOSM(Envelope bbox, FeatureVisitor visitor) throws IOException {
        Map<String, Object> params = new HashMap<>();
        params.put(JDBCDataStoreFactory.DBTYPE.key, "postgis");
        params.put(JDBCDataStoreFactory.HOST.key, "localhost");
        params.put(JDBCDataStoreFactory.PORT.key, 5432);
        params.put(JDBCDataStoreFactory.SCHEMA.key, "public");
        params.put(JDBCDataStoreFactory.DATABASE.key, "bus-routes");
        params.put(JDBCDataStoreFactory.USER.key, "postgres");
        params.put(JDBCDataStoreFactory.PASSWD.key, "postgres");
        DataStore store = DataStoreFinder.getDataStore(params);
        try {
            SimpleFeatureSource featureSource = store.getFeatureSource("osm_rj_roads_segments");
            grabFeaturesInBoundingBox(featureSource, bbox).accepts(visitor, new NullProgressListener());
        } finally {
            store.dispose();
        }
    }
}
