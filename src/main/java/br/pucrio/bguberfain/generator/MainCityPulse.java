package br.pucrio.bguberfain.generator;

import br.pucrio.bguberfain.onibus.gui.XYGraph;
import br.pucrio.bguberfain.onibus.model.graph.v2.SampleData;
import br.pucrio.bguberfain.onibus.model.graph.v2.XYSample;
import br.pucrio.bguberfain.onibus.reader.GZipSampleDataReader;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.apache.commons.math3.stat.descriptive.moment.Mean;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Date;
import java.util.Deque;
import java.util.LinkedList;

/**
 * Created by Bruno on 26/04/2015.
 */
public class MainCityPulse extends MainGraphBase {

    private static final Logger logger = LogManager.getLogger(MainCityPulse.class);

    private static final long MS_IN_A_DAY = 1000 * 60 * 60 * 24 ;

    public static void main(String [] args) {
        GZipSampleDataReader sampleReader = new GZipSampleDataReader() ;

        XYGraph graph = createAndShowGUI("DiffTime", XYGraph.XYGraphAxisType.TIMESTAMP, XYGraph.XYGraphAxisType.METERS_PER_SECOND) ;

        graph.setSequenceAlpha(0x255);

        boolean shouldUpdateBoundaries = true ;
        Deque<XYSample> samples = new LinkedList<>() ;
        Mean meanSpeed = null;
        Mean meanTime = null;
        DescriptiveStatistics movingAverage = new DescriptiveStatistics(30) ;

        long lastServerTime = -1 ;

        while (sampleReader.hasNext()) {
            SampleData sample = sampleReader.next() ;

            // Update mean speed and mean time
            if (meanSpeed!= null && sample.getSpeed() > 2 && sample.getSpeed() < 100) {
                meanSpeed.increment(sample.getSpeed() / 3.6);
                meanTime.increment(sample.getTimestamp()/(1000*60));
            }

            long serverTime = sample.getTimestamp() + sample.getDelay() * 1000 ;
            if (Math.abs(lastServerTime-serverTime) > 1000 * 30 ) {
                // System.out.println("serverTime = " + serverTime + " with " + numSamples + " samples");
                lastServerTime = serverTime ;

                if (shouldUpdateBoundaries) {
                    shouldUpdateBoundaries = false ;
                    // Update boundaries on first run
                    graph.setBoundary(new XYSample.XYSampleBoundary(serverTime, 0, new Date().getTime(), 100 / 3.6));
                    graph.setShouldUpdateBoundary(false);

                } else if ( meanSpeed != null && meanSpeed.getN() > 10) {
                    // Create a new point on the graph, using a moving average
                    movingAverage.addValue(meanSpeed.getResult());
                    long meanTimestamp = (long) (meanTime.getResult() * 1000 * 60);
                    samples.add(new XYSample(meanTimestamp, movingAverage.getMean(), meanTimestamp % MS_IN_A_DAY / (float) MS_IN_A_DAY)) ;

                    // Add to graph every 100 samples
                    if (samples.size() >= 100) {
                        graph.addSampleSequence(samples);
                        samples = new LinkedList<>() ;
                    }
                }

                meanSpeed = new Mean() ;
                meanTime = new Mean() ;
            }
        }

        graph.addSampleSequence(samples);

        logger.info("Finished load all data");
    }

}
