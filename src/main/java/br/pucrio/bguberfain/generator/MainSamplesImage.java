package br.pucrio.bguberfain.generator;

import br.pucrio.bguberfain.onibus.gui.GeoImage;
import br.pucrio.bguberfain.onibus.gui.ImageViewer;
import br.pucrio.bguberfain.onibus.gui.Region;
import br.pucrio.bguberfain.onibus.model.graph.v2.SampleData;
import br.pucrio.bguberfain.onibus.reader.GZipSampleDataReader;
import br.pucrio.bguberfain.onibus.reader.sequence.SequenceGenerator;
import br.pucrio.bguberfain.onibus.reader.sequence.SequenceReader;
import br.pucrio.bguberfain.onibus.reader.sequence.SimpleSequenceGenerator;
import br.pucrio.bguberfain.onibus.reader.sequence.ThreadedSequenceGenerator2;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.SequenceBreaker;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.sampledata.BackwardTimeBreaker;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.sampledata.LeastIntervalBreaker;

import java.util.Iterator;

/**
 * Created by Bruno on 01/03/2015.
 */
public class MainSamplesImage {

    public static void main(String [] args)
    {
        Iterator<SampleData> samples = new GZipSampleDataReader();
        SequenceBreaker sequenceBreakers = new LeastIntervalBreaker(5 * 60 * 1000) ;	// Ignore samples that are 5min apart
        sequenceBreakers = new BackwardTimeBreaker(sequenceBreakers) ;
        SequenceGenerator<SampleData> samplesSequence = new ThreadedSequenceGenerator2<>(sequenceBreakers, null, samples, SampleData::getOrder) ;

        GeoImage img = new GeoImage(Region.COPACABANA, 800) ;
        ImageViewer viewer = new ImageViewer(img, true) ;
        viewer.setVisible(true);

        samplesSequence.readData(new SequenceReader<SampleData>() {
            long cnt = 0;
            @Override
            public void beginSequence(SampleData firstData) {

            }

            @Override
            public void sequence(SampleData lastData, SampleData newData) {
                double diffX = newData.getCoordinate().x - lastData.getCoordinate().x ;
                double diffY = newData.getCoordinate().y - lastData.getCoordinate().y ;
                int color = headingToColor(diffX, diffY, 0x10) ;
                img.setPixel(newData.getCoordinate(), color);

                if (cnt++ % 10000 == 0) {
                    viewer.setImage(img);
                }
            }

            @Override
            public void endSequence(SampleData lastData) {

            }
        });

        viewer.finish();
    }

    private static int headingToColor(double diffX, double diffY, int alpha) {
        double length = Math.sqrt(diffX*diffX + diffY*diffY) ;

        if ( length == 0 ) return 0 ;

        int byte1 = (int) (128 + diffX * 127.0f / length);
        int byte2 = (int) (128 + diffY * 127.0f / length);

        return 0xff0000 | byte1 << 8 | byte2 | (alpha << 24);
    }
}
