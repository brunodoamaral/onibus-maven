package br.pucrio.bguberfain.generator;

import br.pucrio.bguberfain.onibus.model.EdgeStats;
import br.pucrio.bguberfain.onibus.model.graph.v2.SharedRouteEdge;
import com.csvreader.CsvReader;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.LineSegment;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by bguberfain on 3/20/2015.
 */
public class MainTrafficConverter {

    private static final int PRECISION_MINUTES = 15 ;	// Compute statistics for every 'PRECISION_MINUTES' minutes among the day (must be a number that divides 60)
    private static final int MINUTES_PER_DAY = 24 * 60 ;
    private static final int TIME_SLICES_PER_DAY = MINUTES_PER_DAY / PRECISION_MINUTES ;


    public static void main(String [] args) throws Exception {
        // Load traffic data
        System.out.println("Loading traffic statistics...");
        File trafficDataFile = new File("F:\\www-pub\\processed-data\\graph.csv") ;
        CsvReader trajectoryData = new CsvReader(trafficDataFile.getAbsolutePath(), ';');
        Map<LineSegment, EdgeStats> edgeStatsByLs = new HashMap<>() ;
        trajectoryData.readHeaders() ;
        while( trajectoryData.readRecord() ) {
            double from_lat = Double.parseDouble(trajectoryData.get("from_lat"));
            double from_lon = Double.parseDouble(trajectoryData.get("from_lon"));
            double to_lat = Double.parseDouble(trajectoryData.get("to_lat"));
            double to_lon = Double.parseDouble(trajectoryData.get("to_lon"));

            Coordinate from = new Coordinate(from_lon, from_lat) ;
            Coordinate to = new Coordinate(to_lon, to_lat) ;
            LineSegment lsEdge = new LineSegment(from, to) ;
            EdgeStats edgeStats = edgeStatsByLs.get(lsEdge) ;
            if (edgeStats == null) {
                edgeStats = new EdgeStats(TIME_SLICES_PER_DAY);
                edgeStatsByLs.put(lsEdge, edgeStats) ;
            }
            int timeSlice = Integer.parseInt(trajectoryData.get("time_segment_per_15_minutes"));
            edgeStats.maxSpeed = Double.parseDouble(trajectoryData.get("max_speed"));
            edgeStats.meanByTimeSlice[timeSlice] = Double.parseDouble(trajectoryData.get("mean_speed"));
            edgeStats.varianceByTimeSlice[timeSlice] = Double.parseDouble(trajectoryData.get("variance"));
            edgeStats.numSamples[timeSlice] = Integer.parseInt(trajectoryData.get("num_samples"));
        }
        trajectoryData.close() ;

        System.out.println("Saving serialized class...");
        ObjectOutputStream out = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream("F:\\www-pub\\processed-data\\graph.dat"))) ;
        out.writeObject(edgeStatsByLs);
        out.flush();
        out.close();
    }
}
