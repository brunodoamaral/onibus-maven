package br.pucrio.bguberfain.generator;

import br.pucrio.bguberfain.onibus.gui.XYGraph;
import br.pucrio.bguberfain.onibus.model.graph.v2.*;
import br.pucrio.bguberfain.onibus.reader.GZipSampleDataReader;
import br.pucrio.bguberfain.onibus.reader.sequence.SequenceGenerator;
import br.pucrio.bguberfain.onibus.reader.sequence.SequenceReader;
import br.pucrio.bguberfain.onibus.reader.sequence.SimpleSequenceGenerator;
import br.pucrio.bguberfain.onibus.reader.sequence.ThreadedSequenceGenerator2;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.SequenceBreaker;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.sampledata.BackwardTimeBreaker;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.sampledata.ChangeLineNumberBreaker;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.sampledata.LeastIntervalBreaker;
import br.pucrio.bguberfain.onibus.reader.sequence.filter.DataFilter;
import br.pucrio.bguberfain.onibus.reader.sequence.filter.sampledata.DaylightSavingTimeFixFilter;
import br.pucrio.bguberfain.onibus.reader.sequence.filter.sampledata.FixedLineNumberFilter;
import br.pucrio.bguberfain.onibus.reader.sequence.filter.sampledata.LineNumberFilter;
import br.pucrio.bguberfain.onibus.reader.sequence.filter.sampledata.WorkdayFilter;
import br.pucrio.bguberfain.onibus.reader.transformer.SampleDataSequence2ProjectionSequence;
import br.pucrio.bguberfain.onibus.util.NetworkTopologyDS;
import br.pucrio.bguberfain.onibus.util.Util;
import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.geom.LineString;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.io.File;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Bruno on 25/01/2015.
 */
public class MainGraphTimeSpace extends MainGraphBase {
    private static final Logger logger = LogManager.getLogger(MainGraphTimeSpace.class);

    /*
    Max shapes after 8740001 samples:
        17376210, line 638 has 132573 entries
        17378017, line 371 has 113376 entries
        17613062, line 371 has 100290 entries
        17389243, line 397 has 93517 entries  - 397-CAMPO GRANDE X CARIOCA
        17726374, line 624 has 90469 entries
        17541675, line 638 has 86068 entries
        17339354, line 393 has 85308 entries
        17416262, line 607 has 80032 entries
        17378253, line 753 has 75054 entries
        17362666, line 607 has 74823 entries

    - Linha: 2336-CAMPO GRANDE X CASTELO (VIA AV. BRASIL)
        - Sentido Campo Grande: 17594567 (ou 17375751)
        - Sentido Centro: 17761930 (ou 17556438)
    */
    private final static double FINAL_AND_FIRST_POINT_RANGE = 0.0020;
    private final static long MS_IN_A_MINUTE = 60 * 1000 ;
    private final static long MAX_INTERVAL_ON_ROUTE = MS_IN_A_MINUTE * 60 * 4 ;  // 4 hours in ms
    private final static int MAX_INTERVAL_BETWEEN_SEQUENCES = 30 * 60 ; // 30 minutes in seconds

    public static void main(String [] args) throws Exception {
        Stream<Integer> keysToLoad ;
        Map<Integer, Route> routesByShapeId = NetworkTopologyDS.getInstance().getNetworkByShapeId(null, true) ;

        String strShapeIdToLoad = Util.getEnv("SHAPE_TO_LOAD", false);
        if(strShapeIdToLoad != null) {
            keysToLoad = Arrays.stream(strShapeIdToLoad.split(",")).map(Integer::new) ;
        } else {
            keysToLoad = routesByShapeId.keySet().stream() ;
        }
        keysToLoad.sorted()
                .parallel()
                .forEach((shapeId) -> {
                    try {
                        Route route = routesByShapeId.get(shapeId) ;
                        String lineNumber = route.getLineNumber() ;
                        Collection<Route> routesForLineNumber = routesByShapeId.entrySet().stream()
                                .filter( item -> item.getValue().getLineNumber().equals(lineNumber) )
                                .map(Map.Entry::getValue)
                                .collect(Collectors.toList()) ;
                        MainGraphTimeSpace.generateGraph(route, routesForLineNumber, ".*\\.txt\\.gz", new LineNumberFilter(lineNumber), null);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });
    }

    public static XYGraph generateGraph(Route route, Collection<Route> routesForLineNumber, String datePattern, DataFilter filter, SequenceGenerator<ProjectionInfo<SampleData>> projectionSequence) throws Exception {
        long start = System.currentTimeMillis() ;
        XYGraph xyGraph = createAndShowGUI("Space Time Graph", XYGraph.XYGraphAxisType.METERS, XYGraph.XYGraphAxisType.SECONDS);

        String saveToFolder = Util.getEnv("SAVE_TO_FOLDER", false);

        String lineNumber = route.getLineNumber() ;
        logger.info("Considering line # {} for shape id {}", lineNumber, route.getShapeId());

        // Add bus-stops strips
        for (BusStop busStop : route.getBusStops()) {
            xyGraph.addVerticalMark(busStop.getDistanceFromBegin());
        }

        // Update graph boundary to full route length
        XYSample.XYSampleBoundary graphBoundary = new XYSample.XYSampleBoundary(0, 0, route.geometryLengthInMeters(), 40*60) ;
        xyGraph.setBoundary(graphBoundary);
        xyGraph.setShouldUpdateBoundary(false);

        // Configure font size
        xyGraph.setFontSize(20);
        xyGraph.setSequenceAlpha(0x10);

        // Get the first and last point envelope
        LineString lineShape = route.getGeometry() ;
        Envelope startPoint = new Envelope(lineShape.getCoordinateN(0)) ;
        Envelope endPoint = new Envelope(lineShape.getCoordinateN(lineShape.getNumPoints()-1)) ;
        startPoint.expandBy(FINAL_AND_FIRST_POINT_RANGE);
        endPoint.expandBy(FINAL_AND_FIRST_POINT_RANGE);

        if(projectionSequence == null) {
            // Create Sample Reader...
            Iterator<SampleData> samples = new GZipSampleDataReader(datePattern);
            //        Iterator<SampleData> samples = new GZipSampleDataReader("2014((06[0-9][0-9])|(07[0-9][0-9])|(080[1-8]))\\.txt\\.gz");
            //        Iterator<SampleData> samples = new GZipSampleDataReader();
            SequenceBreaker sequenceBreakers = new LeastIntervalBreaker(5 * 60 * 1000);    // Ignore samples that are 5min apart
            sequenceBreakers = new ChangeLineNumberBreaker(sequenceBreakers);    // Keep only same line number in the sequence
            sequenceBreakers = new BackwardTimeBreaker(sequenceBreakers);
            filter = new WorkdayFilter(filter);
            filter = new DaylightSavingTimeFixFilter(filter);

            SequenceGenerator<SampleData> samplesSequence = new SimpleSequenceGenerator<>(sequenceBreakers, filter, samples, SampleData::getOrder, SampleData::isAnnotation);

            // Convert Sample to Projection (snapping)
            projectionSequence = new SampleDataSequence2ProjectionSequence(routesForLineNumber, samplesSequence);
        }

        logger.info("Starting loading data...");
        Map<String, ProjectionInfo<SampleData>> startPointForOrder = new HashMap<>() ;
        Map<String, Deque<XYSample>> spaceTimeSequenceByOrder = new HashMap<>() ;
        projectionSequence.readData(new SequenceReader<ProjectionInfo<SampleData>>() {
            @Override
            public void beginSequence(ProjectionInfo<SampleData> newData) {
                SampleData newSample = newData.getLocationData();
                if (newData.getRoute().equals(route)) {
                    // Check for older sample
                    ProjectionInfo<SampleData> startData = startPointForOrder.get(newSample.getOrder());
                    if (startData != null && !startPoint.contains(newData.getProjectedCoordinate())) {
                        SampleData startSample = startData.getLocationData();
                        boolean continueSequence = false;
                        // Check for long interval or different route between start and new sample
                        if ((newSample.getTimestamp() - startSample.getTimestamp() > MAX_INTERVAL_ON_ROUTE)
                                || !newData.getRoute().equals(startData.getRoute())) {
                            startPointForOrder.remove(newSample.getOrder());
                        } else {
                            continueSequence = true;
                        }

                        // Remove this sequence
                        Deque<XYSample> lastSequence = spaceTimeSequenceByOrder.remove(newSample.getOrder());
                        if (lastSequence != null && lastSequence.size() > 1) {
                            addSampleSequenceToGraph(lastSequence, newSample.getOrder());
                        }

                        Deque<XYSample> newSequence = new LinkedList<>();
                        double timeDiff = newSample.getTimestamp() - startData.getLocationData().getTimestamp();
                        double distanceDiff = newData.distanceTraveledOnRoute();
                        newSequence.add(new XYSample(distanceDiff, timeDiff / 1000, timeToColorPercent(startSample.getTimestamp())));
                        spaceTimeSequenceByOrder.put(newSample.getOrder(), newSequence);

                    } else {
                        // Check if newData is on start point
                        if (startPoint.contains(newData.getProjectedCoordinate())) {
                            // Save it for latter and create the new sequence
                            startPointForOrder.put(newSample.getOrder(), newData);
                            Deque<XYSample> newSequence = new LinkedList<>();
                            newSequence.add(new XYSample(newData.distanceTraveledOnRoute(), 0, timeToColorPercent(newSample.getTimestamp())));
                            spaceTimeSequenceByOrder.put(newSample.getOrder(), newSequence);
                        }
                    }
                } else {
                    startPointForOrder.remove(newSample.getOrder());
                }
            }

            @Override
            public void sequence(ProjectionInfo<SampleData> lastData, ProjectionInfo<SampleData> newData) {
                // Check for older sample
                SampleData newSample = newData.getLocationData();
                if (newData.getRoute().equals(route)) {
                    ProjectionInfo<SampleData> startData = startPointForOrder.get(newSample.getOrder());
                    if (startData != null && !startPoint.contains(newData.getProjectedCoordinate())) {
                        SampleData startSample = startData.getLocationData();
                        // Check for long interval, different route or newer sample in start or end point
                        if ((newSample.getTimestamp() - startSample.getTimestamp() > MAX_INTERVAL_ON_ROUTE)
                                || !newData.getRoute().equals(startData.getRoute())
                                || startPoint.contains(newData.getProjectedCoordinate())
                                || endPoint.contains(newData.getProjectedCoordinate())) {
                            startPointForOrder.remove(newSample.getOrder());

                            // Remove this sequence
                            Deque<XYSample> lastSequence = spaceTimeSequenceByOrder.remove(newSample.getOrder());
                            if (lastSequence != null && lastSequence.size() > 1 && !startPoint.contains(newData.getProjectedCoordinate())) {
                                addSampleSequenceToGraph(lastSequence, newSample.getOrder());
                            }
                        } else {
                            // Add this sample to sequence
                            // Save it for latter and create the new sequence
                            Deque<XYSample> lastSequence = spaceTimeSequenceByOrder.get(newSample.getOrder());
                            if (lastSequence != null) {
                                XYSample lastSequenceData = lastSequence.getLast();

                                double timeDiff = newSample.getTimestamp() - startData.getLocationData().getTimestamp();
                                double distanceDiff = newData.getRoute().distanceInMetersUntil(newData.getLocation());

                                // Avoid adding a backward sample to the sequence
                                if (lastSequenceData.getX() < distanceDiff && lastSequenceData.getY() < timeDiff) {
                                    lastSequence.add(new XYSample(distanceDiff, timeDiff / 1000, timeToColorPercent(startSample.getTimestamp())));
                                } else {
                                    // Terminate this sequence
                                    endSequence(lastData);
                                    startPointForOrder.remove(newSample.getOrder());
                                }
                            }
                        }
                    } else {
                        // Check if newData is on start point
                        if (startPoint.contains(newData.getProjectedCoordinate())) {
                            // Save it for latter and create the new sequence
                            startPointForOrder.put(newSample.getOrder(), newData);
                            Deque<XYSample> newSequence = new LinkedList<>();
                            newSequence.add(new XYSample(newData.distanceTraveledOnRoute(), 0, timeToColorPercent(newSample.getTimestamp())));
                            spaceTimeSequenceByOrder.put(newSample.getOrder(), newSequence);
                        }
                    }
                } else {
                    startPointForOrder.remove(newSample.getOrder());
                }
            }

            @Override
            public void endSequence(ProjectionInfo<SampleData> lastData) {
                SampleData lastSample = lastData.getLocationData();
                if (lastData.getRoute().equals(route)) {
                    Deque<XYSample> lastSequence = spaceTimeSequenceByOrder.remove(lastSample.getOrder());
                    if (lastSequence != null && lastSequence.size() > 1 && !startPoint.contains(lastData.getProjectedCoordinate())) {
                        addSampleSequenceToGraph(lastSequence, lastSample.getOrder());
                    }
                } else {
                    startPointForOrder.remove(lastSample.getOrder());
                }
            }

            Map<String, XYSample> lastSampleForOrder = new HashMap<>();

            private synchronized void addSampleSequenceToGraph(Deque<XYSample> sequence, String order) {
                XYSample lastSample = lastSampleForOrder.get(order);
                if (lastSample != null) {
                    // Check for maximum diff
                    XYSample firstNewSequenceSample = sequence.getFirst();
                    if (firstNewSequenceSample.getY() - lastSample.getY() > MAX_INTERVAL_BETWEEN_SEQUENCES) {
                        // Avoid adding an new sequence whose time is too far from previous
                        return;
                    }
                }

                xyGraph.addSampleSequence(sequence);
                lastSampleForOrder.put(order, sequence.getLast());
            }
        });

        long end = System.currentTimeMillis() ;

        logger.info("Finished loading all data in {} s", (end - start) / 1000);

        // Save file
        if (saveToFolder != null) {
            String fileBase = String.format("space-time-%s-%d", route.getLineNumber(), route.getShapeId()) ;

            xyGraph.saveData(new File(saveToFolder, fileBase + ".xyd"));
            xyGraph.saveImage(new File(saveToFolder, fileBase + ".png"), 1);

            JFrame frame = (JFrame) SwingUtilities.getRoot(xyGraph);
            frame.setVisible(false);
            frame.dispose();
        }

        return xyGraph ;
    }
}
