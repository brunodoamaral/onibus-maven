package br.pucrio.bguberfain.generator;

import br.pucrio.bguberfain.onibus.gui.GeoImage;
import br.pucrio.bguberfain.onibus.gui.GeoImage3D;
import br.pucrio.bguberfain.onibus.gui.ImageViewer;
import br.pucrio.bguberfain.onibus.gui.Region;
import br.pucrio.bguberfain.onibus.model.graph.v2.SampleData;
import br.pucrio.bguberfain.onibus.reader.GZipSampleDataReader;
import br.pucrio.bguberfain.onibus.reader.SampleDataReader;
import br.pucrio.bguberfain.onibus.reader.sequence.SequenceReader;
import br.pucrio.bguberfain.onibus.reader.sequence.SimpleSequenceGenerator;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.sampledata.ChangeHourBreaker;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.sampledata.ChangeLineNumberBreaker;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.sampledata.LeastIntervalBreaker;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.SequenceBreaker;
import br.pucrio.bguberfain.onibus_geotools.Util;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.CoordinateSequence;
import com.vividsolutions.jts.geom.LineString;
import org.geotools.data.DataStore;
import org.geotools.data.DataStoreFinder;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.jdbc.JDBCDataStoreFactory;
import org.geotools.util.NullProgressListener;
import org.opengis.feature.Feature;
import org.opengis.feature.simple.SimpleFeature;

import java.awt.*;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by bguberfain on 6/jan/2015.
 */
public class GenerateImageData {

    static final long MS_IN_AN_HOUR = 1000 * 60 * 60 ;
    static final long MS_IN_A_DAY = MS_IN_AN_HOUR * 24 ;

    public static void main(String[] args) throws Exception {
        Iterator<SampleData> sampleReader = new GZipSampleDataReader("2015040(8|9)\\.txt\\.gz") ;
        SequenceBreaker qualityFilter = new LeastIntervalBreaker((int) 5 * 60 * 1000);    // Ignore samples that are 2min apart
//		qualityFilter = new LongDistanceBreaker(qualityFilter, 1000) ;	// Ignore samples of more than 1km
        qualityFilter = new ChangeLineNumberBreaker(qualityFilter);    // Keep only same line number in the sequence
        qualityFilter = new ChangeHourBreaker(qualityFilter);            // Break between hour-change

        SimpleSequenceGenerator sequenceGenerator = new SimpleSequenceGenerator<>(qualityFilter, null, sampleReader, SampleData::getOrder, SampleData::isAnnotation);

        GeoImage3D img = new GeoImage3D(Region.RIO_DE_JANEIRO, 1024*4);

        drawOSMLayer(img);

        ImageViewer imgViewer = new ImageViewer(img, true);
        imgViewer.setVisible(true);

        final int lineId = Util.idForLineNumber("485") ;

        System.out.println("Reading file...");

        sequenceGenerator.readData(new SequenceReader<SampleData>() {
            @Override
            public void beginSequence(SampleData firstData) {

            }

            @Override
            public void sequence(SampleData lastData, SampleData newData) {
                double speed = 3.6 * newData.fastDistanceTo(lastData) * 1000 / (newData.getTimestamp() - lastData.getTimestamp()); // In km/h
                double normSpeed = speed > 60 ? 1.0 : speed / 60;
                double normTime = (lastData.getTimestamp() % MS_IN_A_DAY - MS_IN_AN_HOUR*12) / (double) MS_IN_A_DAY;

                double z = normTime * 0.01;
                Color color = colorByPercent(normSpeed / 2, 255);

                img.setColorWithDistanceAlpha(color, 20, newData.getCoordinate());

                lastData.getCoordinate().setOrdinate(Coordinate.Z, z);
                newData.getCoordinate().setOrdinate(Coordinate.Z, z);

                if (img.drawLine(lastData.getCoordinate(), newData.getCoordinate())) {
                    imgViewer.setImage(img);
                }
            }

            @Override
            public void endSequence(SampleData lastData) {

            }
        });

        imgViewer.finish();
        System.out.println("Finished");
    }

    private static Color colorByPercent(double percent, int alpha)
    {
        int color = Color.HSBtoRGB((float)percent, 1.0f, 1.0f) & 0xffffff | (alpha<<24) ;
        return new Color(color, true) ;
    }

    private static void drawOSMLayer(GeoImage3D img) throws Exception
    {
        System.out.print("Drawing OSM layer...");
        Color color = new Color(0x12ffffff, true) ;
        DataStore store ;
        SimpleFeatureSource featureSource ;
        // Load line shapes (from postgis)
        Map<String, Object> params = new HashMap<String, Object>();
        params.put(JDBCDataStoreFactory.DBTYPE.key, "postgis");
        params.put(JDBCDataStoreFactory.HOST.key, "localhost");
        params.put(JDBCDataStoreFactory.PORT.key, 5432);
        params.put(JDBCDataStoreFactory.SCHEMA.key, "public");
        params.put(JDBCDataStoreFactory.DATABASE.key, "bus-routes");
        params.put(JDBCDataStoreFactory.USER.key, "postgres");
        params.put(JDBCDataStoreFactory.PASSWD.key, "postgres");
        store = DataStoreFinder.getDataStore(params);
        featureSource = store.getFeatureSource("osm_rj_roads_segments");
        SimpleFeatureCollection featureCollection = featureSource.getFeatures() ;

        img.setStrokeWidth(1);
        img.setColor(color) ;

        // Desenha os features
        featureCollection.accepts((Feature feature) -> {
            SimpleFeature simpleFeature = (SimpleFeature) feature;
            LineString geom = (LineString) simpleFeature.getDefaultGeometry();

            CoordinateSequence coordinates =  geom.getCoordinateSequence() ;
            Coordinate last = coordinates.getCoordinate(0) ;
            for( int i=1; i<coordinates.size(); i++ ) {
                Coordinate current = coordinates.getCoordinate(i) ;
                //img.setColorWithDistanceAlpha(color, 255, current);
                img.drawLine(last, current) ;
                last = current ;
            }

        }, new NullProgressListener() );

        System.out.println(" done!");
        store.dispose();
    }

    private static int samplesToColor(SampleData lastData, SampleData newData)
    {
        // Based on headeing...
//        double heading = Math.atan2(newData.getCoordinate().y - lastData.getCoordinate().y, newData.getCoordinate().x - lastData.getCoordinate().x);
//        return headingToColor(heading) ;

        // Based on speed
        double speed = 3.6 * newData.fastDistanceTo(lastData) * 1000 / (newData.getTimestamp() - lastData.getTimestamp()) ; // In km/h
        int byteSpeed = ((int) (speed / 5 * 255)) & 0xff ;
        return 0x20ff0000 | byteSpeed << 8 | byteSpeed ;
    }

    private static int headingToColor(double heading) {
        int byte1 = (int) (128 + Math.sin(heading) * 127);
        int byte2 = (int) (128 + Math.cos(heading) * 127);

        return 0x20ff0000 | byte1 << 8 | byte2;
    }
}