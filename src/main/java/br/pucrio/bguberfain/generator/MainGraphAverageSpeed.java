package br.pucrio.bguberfain.generator;

import br.pucrio.bguberfain.onibus.gui.XYGraph;
import br.pucrio.bguberfain.onibus.model.graph.v2.*;
import br.pucrio.bguberfain.onibus.reader.GZipSampleDataReader;
import br.pucrio.bguberfain.onibus.reader.sequence.SequenceGenerator;
import br.pucrio.bguberfain.onibus.reader.sequence.SequenceReader;
import br.pucrio.bguberfain.onibus.reader.sequence.SimpleSequenceGenerator;
import br.pucrio.bguberfain.onibus.reader.sequence.ThreadedSequenceGenerator2;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.SequenceBreaker;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.sampledata.BackwardTimeBreaker;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.sampledata.ChangeLineNumberBreaker;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.sampledata.LeastIntervalBreaker;
import br.pucrio.bguberfain.onibus.reader.sequence.filter.DataFilter;
import br.pucrio.bguberfain.onibus.reader.sequence.filter.sampledata.DaylightSavingTimeFixFilter;
import br.pucrio.bguberfain.onibus.reader.sequence.filter.sampledata.FixedLineNumberFilter;
import br.pucrio.bguberfain.onibus.reader.sequence.filter.sampledata.LineNumberFilter;
import br.pucrio.bguberfain.onibus.reader.sequence.filter.sampledata.WorkdayFilter;
import br.pucrio.bguberfain.onibus.reader.transformer.SampleDataSequence2ProjectionSequence;
import br.pucrio.bguberfain.onibus.util.NetworkTopologyDS;
import br.pucrio.bguberfain.onibus.util.Util;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Bruno on 25/01/2015.
 */
public class MainGraphAverageSpeed extends MainGraphBase {
    private static final Logger logger = LogManager.getLogger(MainGraphAverageSpeed.class);

    /*
    Max shapes after 8740001 samples:
        17376210, line 638 has 132573 entries
        17378017, line 371 has 113376 entries
        17613062, line 371 has 100290 entries
        17389243, line 397 has 93517 entries  - 397-CAMPO GRANDE X CARIOCA
        17726374, line 624 has 90469 entries
        17541675, line 638 has 86068 entries
        17339354, line 393 has 85308 entries
        17416262, line 607 has 80032 entries
        17378253, line 753 has 75054 entries
        17362666, line 607 has 74823 entries

    - Linha: 2336-CAMPO GRANDE X CASTELO (VIA AV. BRASIL)
        - Sentido Campo Grande: 17594567 (ou 17375751)
        - Sentido Centro: 17761930 (ou 17556438)
    */
    private final static long MAX_INTERVAL_BETWEEN_SEQUENCES = 60 * 15 ; // 15 minutes in seconds
    private final static double MAX_SPEED = 100 / 3.6 ;     // 100 km/h in ms/s
    private final static double MAX_DISTANCE_BETWEEN_SEQUENCES = MAX_SPEED * 2 * 60 ; // Distance with max speed in 2 minutes

    public static void main(String [] args) throws Exception {
        Stream<Integer> keysToLoad ;
        Map<Integer, Route> routesByShapeId = NetworkTopologyDS.getInstance().getNetworkByShapeId(null, true) ;

        String strShapeIdToLoad = Util.getEnv("SHAPE_TO_LOAD", false);
        if(strShapeIdToLoad != null) {
            keysToLoad = Arrays.stream(strShapeIdToLoad.split(",")).map(Integer::new) ;
        } else {
            keysToLoad = routesByShapeId.keySet().stream() ;
        }
        keysToLoad.sorted()
                .parallel()
                .forEach((shapeId) -> {
                    try {
                        Route route = routesByShapeId.get(shapeId) ;
                        String lineNumber = route.getLineNumber() ;
                        Collection<Route> routesForLineNumber = routesByShapeId.entrySet().stream()
                                .filter( item -> item.getValue().getLineNumber().equals(lineNumber) )
                                .map(Map.Entry::getValue)
                                .collect(Collectors.toList()) ;
                        MainGraphAverageSpeed.generateGraph(route, routesForLineNumber, ".*\\.txt\\.gz", new LineNumberFilter(lineNumber), null);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });
    }

    public static XYGraph generateGraph(Route route, Collection<Route> routesForLineNumber, String datePattern, DataFilter filter, SequenceGenerator<ProjectionInfo<SampleData>> projectionSequence) throws Exception {
        long start = System.currentTimeMillis() ;
        XYGraph xyGraph = MainGraphBase.createAndShowGUI("Space-Speed Graph", XYGraph.XYGraphAxisType.METERS, XYGraph.XYGraphAxisType.METERS_PER_SECOND);

        String lineNumber = route.getLineNumber() ;
        logger.info("Considering line # {} for shape id {}", lineNumber, route.getShapeId());
        logger.info("MAX_DISTANCE_BETWEEN_SEQUENCES = {}", MAX_DISTANCE_BETWEEN_SEQUENCES) ;
        String saveToFolder = Util.getEnv("SAVE_TO_FOLDER", false);

        // Configure color for samples
//        final Color[] heatColors = new Color[] {
//                new Color(0xbe0000),    // 0.1
//                new Color(0xff0000),    // 0.2
//                new Color(0xff0000),    // 0.3
//                new Color(0xff9e00),    // 0.4
//                new Color(0xff9e00),    // 0.5
//                new Color(0xff9e00),    // 0.6
//                new Color(0x00b22d),    // 0.7
//                new Color(0x00b22d),    // 0.8
//                new Color(0x00b22d),    // 0.9
//                new Color(0x00b22d),    // 1.0
//        } ;
//        xyGraph.setColorProvider((float percent, int alpha) -> Util.gradientColor(heatColors, percent, alpha / 255.0f));

        // Add bus-stops strips
        for (BusStop busStop : route.getBusStops()) {
            xyGraph.addVerticalMark(busStop.getDistanceFromBegin());
        }

        // Update graph boundary to full route length
        XYSample.XYSampleBoundary graphBoundary = new XYSample.XYSampleBoundary(0, 0, route.geometryLengthInMeters(), 100/3.6) ;
        xyGraph.setBoundary(graphBoundary);
        xyGraph.setShouldUpdateBoundary(false);
        xyGraph.setFontSize(20);
        xyGraph.setSequenceAlpha(0x06);

        if (projectionSequence == null) {
            // Create Sample Reader...
            // Before BRS
            //        Iterator<SampleData> samples = new GZipSampleDataReader("2014((06[0-9][0-9])|(07[0-9][0-9])|(080[1-8]))\\.txt\\.gz");
            // After BRS
            //        Iterator<SampleData> samples = new GZipSampleDataReader("2014((08[1-3][0-9])|(09[0-9][0-9])|((100[0-9])))\\.txt\\.gz");
            Iterator<SampleData> samples = new GZipSampleDataReader(datePattern);

            SequenceBreaker sequenceBreakers = new LeastIntervalBreaker(5 * 60 * 1000);    // Ignore samples that are 5min apart
            sequenceBreakers = new ChangeLineNumberBreaker(sequenceBreakers);    // Keep only same line number in the sequence
            sequenceBreakers = new BackwardTimeBreaker(sequenceBreakers);
            filter = new WorkdayFilter(filter);
            filter = new DaylightSavingTimeFixFilter(filter);

            SequenceGenerator<SampleData> samplesSequence = new SimpleSequenceGenerator<>(sequenceBreakers, filter, samples, SampleData::getOrder, SampleData::isAnnotation);

            // Convert Sample to Projection (snapping)
            projectionSequence = new SampleDataSequence2ProjectionSequence(routesForLineNumber, samplesSequence);
        }

        logger.info("Starting loading data...");
        Map<String, Deque<XYSample>> spaceTimeSequenceByOrder = new HashMap<>() ;
        projectionSequence.readData(new SequenceReader<ProjectionInfo<SampleData>>() {
            @Override
            public void beginSequence(ProjectionInfo<SampleData> newData) {
                if (newData.getRoute().equals(route)) {
                    Deque<XYSample> newSequence = new LinkedList<>();
                    SampleData newSample = newData.getLocationData();

                    spaceTimeSequenceByOrder.put(newSample.getOrder(), newSequence);
                }
            }

            @Override
            public void sequence(ProjectionInfo<SampleData> lastData, ProjectionInfo<SampleData> newData) {
                if (newData.getRoute().equals(route) && lastData.getRoute().equals(route)) {
                    SampleData newSample = newData.getLocationData();
                    SampleData lastSample = lastData.getLocationData();
                    int timeDiff = (int) ((newSample.getTimestamp() - lastSample.getTimestamp()) / 1000);
                    if (timeDiff > 0 && timeDiff < MAX_INTERVAL_BETWEEN_SEQUENCES) {
                        double distanceDiff = newData.distanceInMetersTraveledSince(lastData);

                        if (distanceDiff > 0 && distanceDiff < MAX_DISTANCE_BETWEEN_SEQUENCES) {
                            Deque<XYSample> currentSequence = spaceTimeSequenceByOrder.get(newSample.getOrder());

                            if (currentSequence == null) {
                                return;
                            }

                            double distanceTraveledSinceBeginRoute = newData.getRoute().distanceInMetersUntil(newData.getLocation());

                            double distanceTraveledSinceLastSample = 0;

                            if (!currentSequence.isEmpty()) {
                                double lastSampleDistanceTraveledSinceBeginRoute = currentSequence.peekLast().getX();

                                distanceTraveledSinceLastSample = distanceTraveledSinceBeginRoute - lastSampleDistanceTraveledSinceBeginRoute;
                            }

                            if (distanceTraveledSinceBeginRoute > 0 && distanceTraveledSinceLastSample >= 0 && distanceTraveledSinceLastSample < MAX_DISTANCE_BETWEEN_SEQUENCES) {
                                double avgSpeed = distanceDiff / timeDiff;

                                if (avgSpeed < MAX_SPEED) {
                                    XYSample sequenceSample = new XYSample(distanceTraveledSinceBeginRoute, avgSpeed, MainGraphBase.timeToColorPercent(newSample.getTimestamp()));
                                    currentSequence.add(sequenceSample);
                                }
                            }
                        }
                    }
                }
            }

            @Override
            public void endSequence(ProjectionInfo<SampleData> lastData) {
                SampleData lastSample = lastData.getLocationData();
                Deque<XYSample> newSequence = spaceTimeSequenceByOrder.get(lastSample.getOrder());

                if (newSequence != null) {
                    addSampleSequenceToGraph(newSequence, lastSample.getOrder());

                    newSequence.remove(lastSample.getOrder());
                }
            }

            private synchronized void addSampleSequenceToGraph(Deque<XYSample> sequence, String order) {
                if (sequence.size() > 1) {
                    xyGraph.addSampleSequence(sequence);
                }
            }
        });

        long end = System.currentTimeMillis() ;

        logger.info("Finished loading all data in {} s", (end - start) / 1000);

        // Save file
        if (saveToFolder != null) {
            String fileBase = String.format("space-speed-%s-%d", route.getLineNumber(), route.getShapeId()) ;

            xyGraph.saveData(new File(saveToFolder, fileBase + ".xyd"));
            xyGraph.saveImage(new File(saveToFolder, fileBase + ".png"), 1);

            JFrame frame = (JFrame) SwingUtilities.getRoot(xyGraph);
            frame.setVisible(false);
            frame.dispose();
        }

        return xyGraph ;
    }


}
