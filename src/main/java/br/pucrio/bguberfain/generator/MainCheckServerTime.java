package br.pucrio.bguberfain.generator;

import br.pucrio.bguberfain.onibus.gui.XYGraph;
import br.pucrio.bguberfain.onibus.model.graph.v2.SampleData;
import br.pucrio.bguberfain.onibus.model.graph.v2.XYSample;
import br.pucrio.bguberfain.onibus.reader.GZipSampleDataReader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Date;
import java.util.Deque;
import java.util.LinkedList;

/**
 * Created by Bruno on 26/04/2015.
 */
public class MainCheckServerTime extends MainGraphBase {

    private static final Logger logger = LogManager.getLogger(MainCheckServerTime.class);

    public static void main(String [] args) {
        GZipSampleDataReader sampleReader = new GZipSampleDataReader("((2014[^0].*)|(2015.*))\\.txt\\.gz") ;
        sampleReader.setIncludeAnnotations(true);

        XYGraph graph = createAndShowGUI("DiffTime", XYGraph.XYGraphAxisType.SECONDS, XYGraph.XYGraphAxisType.SECONDS) ;

        graph.setSequenceAlpha(0x255);

        long cityHallServerTime = -1 ;
        long myServerTime = -1 ;
        long firstMyServerTime = -1 ;
        Deque<XYSample> samples = new LinkedList<>() ;
        while (sampleReader.hasNext()) {
            SampleData sample = sampleReader.next() ;

            if (sample.isAnnotation()) {
                myServerTime = sample.getTimestamp() ;
                cityHallServerTime = -1 ;
                if (firstMyServerTime == -1) {
                    firstMyServerTime = myServerTime ;
                    graph.setBoundary(new XYSample.XYSampleBoundary(0, -4 * 60 * 60, (new Date().getTime()-firstMyServerTime)/1000, 4 * 60 * 60));
                    graph.setShouldUpdateBoundary(false);
                }
            } else if (cityHallServerTime == -1) {
                cityHallServerTime = sample.getTimestamp() + sample.getDelay() * 1000 ;

                if (myServerTime != -1) {
                    // Compare difference
                    //System.out.printf("%d: diff(cityHall-myServer) = %d\n", myServerTime, (myServerTime - cityHallServerTime) / 1000);

                    samples.add(new XYSample((myServerTime-firstMyServerTime)/1000, (myServerTime - cityHallServerTime) / 1000, 1.0f)) ;

                    if (samples.size() >= 100) {
                        graph.addSampleSequence(samples);
                        samples = new LinkedList<>() ;
                    }
                }
            }
        }

        graph.addSampleSequence(samples);

        logger.info("Finished load all data");
    }

}
