package br.pucrio.bguberfain.generator;

import br.pucrio.bguberfain.onibus.gui.XYGraph;
import br.pucrio.bguberfain.onibus.model.graph.v2.*;
import br.pucrio.bguberfain.onibus.reader.GZipSampleDataReader;
import br.pucrio.bguberfain.onibus.reader.sequence.SequenceGenerator;
import br.pucrio.bguberfain.onibus.reader.sequence.SequenceReader;
import br.pucrio.bguberfain.onibus.reader.sequence.ThreadedSequenceGenerator2;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.SequenceBreaker;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.sampledata.BackwardTimeBreaker;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.sampledata.ChangeLineNumberBreaker;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.sampledata.LeastIntervalBreaker;
import br.pucrio.bguberfain.onibus.reader.sequence.filter.DataFilter;
import br.pucrio.bguberfain.onibus.reader.sequence.filter.sampledata.DaylightSavingTimeFixFilter;
import br.pucrio.bguberfain.onibus.reader.sequence.filter.sampledata.LineNumberFilter;
import br.pucrio.bguberfain.onibus.reader.sequence.filter.sampledata.WorkdayFilter;
import br.pucrio.bguberfain.onibus.reader.transformer.SampleDataSequence2ProjectionSequence;
import br.pucrio.bguberfain.onibus.util.NetworkTopologyDS;
import br.pucrio.bguberfain.onibus.util.Util;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.io.File;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Bruno on 25/01/2015.
 */
public class MainGraphAllAverageSpeed extends MainGraphBase {
    private static final Logger logger = LogManager.getLogger(MainGraphAllAverageSpeed.class);

    private final static long MAX_INTERVAL_BETWEEN_SEQUENCES = 60 * 15 ; // 15 minutes in seconds
    private final static double MAX_SPEED = 100 / 3.6 ;     // 100 km/h in ms/s
    private final static double MAX_DISTANCE_BETWEEN_SEQUENCES = MAX_SPEED * 2 * 60 ; // Distance with max speed in 2 minutes

    public static void main(String [] args) throws Exception {
        long start = System.currentTimeMillis() ;
        XYGraph xyGraph = MainGraphBase.createAndShowGUI("Space-Speed Graph", XYGraph.XYGraphAxisType.METERS, XYGraph.XYGraphAxisType.METERS_PER_SECOND);

        Map<Integer, Route> routesByShapeId = NetworkTopologyDS.getInstance().getNetworkByShapeId(null, true) ;

        String saveToFolder = Util.getEnv("SAVE_TO_FOLDER", false);

        // Add bus-stops strips
//        for (BusStop busStop : route.getBusStops()) {
//            xyGraph.addVerticalMark(busStop.getDistanceFromBegin());
//        }

        // Update graph boundary to full route length
//        XYSample.XYSampleBoundary graphBoundary = new XYSample.XYSampleBoundary(0, 0, route.geometryLengthInMeters(), 100/3.6) ;
//        xyGraph.setBoundary(graphBoundary);
//        xyGraph.setShouldUpdateBoundary(false);
//        xyGraph.setFontSize(20);
//        xyGraph.setSequenceAlpha(0x06);


        // Create Sample Reader...
        Iterator<SampleData> samples = new GZipSampleDataReader();

        SequenceBreaker sequenceBreakers = new LeastIntervalBreaker(5 * 60 * 1000) ;	// Ignore samples that are 5min apart
        sequenceBreakers = new ChangeLineNumberBreaker(sequenceBreakers) ;	// Keep only same line number in the sequence
        sequenceBreakers = new BackwardTimeBreaker(sequenceBreakers) ;
        DataFilter filter = new WorkdayFilter() ;
        filter = new DaylightSavingTimeFixFilter(filter) ;

        SequenceGenerator<SampleData> samplesSequence = new ThreadedSequenceGenerator2<>(sequenceBreakers, filter, samples, SampleData::getOrder) ;

        // Convert Sample to Projection (snapping)
        SequenceGenerator<ProjectionInfo<SampleData>> projectionSequence = new SampleDataSequence2ProjectionSequence(routesByShapeId.values(), samplesSequence) ;

        logger.info("Starting loading data...");
        Map<String, Deque<XYSample>> spaceTimeSequenceByOrder = new HashMap<>() ;
        projectionSequence.readData(new SequenceReader<ProjectionInfo<SampleData>>() {
            ThreadLocal<Map<String, Route>> tlLastRouteByOrder = new ThreadLocal<Map<String, Route>>() {
                @Override
                protected Map<String, Route> initialValue() {
                    return new HashMap<>() ;
                }
            };

            @Override
            public void beginSequence(ProjectionInfo<SampleData> newData) {

                if (newData.getRoute().equals(route)) {
                    Deque<XYSample> newSequence = new LinkedList<>();
                    SampleData newSample = newData.getLocationData();

                    spaceTimeSequenceByOrder.put(newSample.getOrder(), newSequence);
                }
            }

            @Override
            public void sequence(ProjectionInfo<SampleData> lastData, ProjectionInfo<SampleData> newData)
            {
                if (newData.getRoute().equals(route) && lastData.getRoute().equals(route)) {
                    SampleData newSample = newData.getLocationData();
                    SampleData lastSample = lastData.getLocationData();
                    int timeDiff = (int) ((newSample.getTimestamp() - lastSample.getTimestamp()) / 1000);
                    if (timeDiff > 0 && timeDiff < MAX_INTERVAL_BETWEEN_SEQUENCES) {
                        double distanceDiff = newData.distanceInMetersTraveledSince(lastData);

                        if (distanceDiff > 0 && distanceDiff < MAX_DISTANCE_BETWEEN_SEQUENCES) {
                            Deque<XYSample> currentSequence = spaceTimeSequenceByOrder.get(newSample.getOrder());

                            if(currentSequence == null) {
                                return ;
                            }

                            double distanceTraveledSinceBeginRoute = newData.getRoute().distanceInMetersUntil(newData.getLocation());

                            double distanceTraveledSinceLastSample = 0;

                            if (!currentSequence.isEmpty()) {
                                double lastSampleDistanceTraveledSinceBeginRoute = currentSequence.peekLast().getX();

                                distanceTraveledSinceLastSample = distanceTraveledSinceBeginRoute - lastSampleDistanceTraveledSinceBeginRoute;
                            }

                            if (distanceTraveledSinceBeginRoute > 0 && distanceTraveledSinceLastSample >= 0 && distanceTraveledSinceLastSample < MAX_DISTANCE_BETWEEN_SEQUENCES) {
                                double avgSpeed = distanceDiff / timeDiff;

                                if (avgSpeed < MAX_SPEED) {
                                    XYSample sequenceSample = new XYSample(distanceTraveledSinceBeginRoute, avgSpeed, MainGraphBase.timeToColorPercent(newSample.getTimestamp()));
                                    currentSequence.add(sequenceSample);
                                }
                            }
                        }
                    }
                }
            }

            @Override
            public void endSequence(ProjectionInfo<SampleData> lastData) {
                SampleData lastSample = lastData.getLocationData();
                Deque<XYSample> newSequence = spaceTimeSequenceByOrder.get(lastSample.getOrder());

                if(newSequence != null) {
                    addSampleSequenceToGraph(newSequence, lastSample.getOrder());

                    newSequence.remove(lastSample.getOrder());
                }
            }

            private synchronized void addSampleSequenceToGraph(Deque<XYSample> sequence, String order)
            {
                if (sequence.size() > 1) {
                    xyGraph.addSampleSequence(sequence);
                }
            }
        });

        long end = System.currentTimeMillis() ;

        logger.info("Finished loading all data in {} s", (end-start) / 1000);

        // Save file
        if (saveToFolder != null) {
            String fileBase = String.format("space-speed-%s-%d", route.getLineNumber(), route.getShapeId()) ;

            xyGraph.saveData(new File(saveToFolder, fileBase + ".xyd"));
            xyGraph.saveImage(new File(saveToFolder, fileBase + ".png"));

            JFrame frame = (JFrame) SwingUtilities.getRoot(xyGraph);
            frame.setVisible(false);
            frame.dispose();
        }

        return xyGraph ;
    }


}
