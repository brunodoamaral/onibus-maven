package br.pucrio.bguberfain.generator;

import br.pucrio.bguberfain.onibus.model.graph.v2.ProjectionInfo;
import br.pucrio.bguberfain.onibus.model.graph.v2.Route;
import br.pucrio.bguberfain.onibus.model.graph.v2.SampleData;
import br.pucrio.bguberfain.onibus.reader.GZipSampleDataReader;
import br.pucrio.bguberfain.onibus.reader.sequence.SequenceGenerator;
import br.pucrio.bguberfain.onibus.reader.sequence.SequenceReader;
import br.pucrio.bguberfain.onibus.reader.sequence.SimpleSequenceGenerator;
import br.pucrio.bguberfain.onibus.reader.sequence.ThreadedSequenceGenerator2;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.SequenceBreaker;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.sampledata.BackwardTimeBreaker;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.sampledata.ChangeLineNumberBreaker;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.sampledata.LeastIntervalBreaker;
import br.pucrio.bguberfain.onibus.reader.sequence.filter.DataFilter;
import br.pucrio.bguberfain.onibus.reader.sequence.filter.sampledata.DaylightSavingTimeFixFilter;
import br.pucrio.bguberfain.onibus.reader.sequence.filter.sampledata.LineNumberFilter;
import br.pucrio.bguberfain.onibus.reader.sequence.filter.sampledata.WorkdayFilter;
import br.pucrio.bguberfain.onibus.reader.transformer.SampleDataSequence2ProjectionSequence;
import br.pucrio.bguberfain.onibus.util.NetworkTopologyDS;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Bruno on 18/07/2015.
 */
public class MainGenerateSVMDataset {
    private final static int DISCRETE_TIME_SECONDS = 60 ; // 60 seconds
    protected static final long SECONDS_IN_AN_HOUR = 60 * 60 ;
    protected static final long SECONDS_IN_A_DAY = SECONDS_IN_AN_HOUR * 24 ;
    protected static final long MS_IN_A_DAY = SECONDS_IN_A_DAY * 1000 ;
    protected static final Locale EN_US = Locale.US ;
    protected static final int PRECISION_MINUTES = 5 ; // must divide by 24h
    protected static final int PRECISION_METERS = 10 ;
    protected static final int SHAPE_TO_LOAD = 17573358 ;
    protected static final double MAX_TRAVEL_IN_DISCRETE_TIME = 1500 ;
    protected static final int INITIAL_ATTRIBUTES = 1 ;

    public static void main(String [] args) throws Exception {
        Map<Integer, Route> routesByShapeId = NetworkTopologyDS.getInstance().getNetworkByShapeId(null, true) ;
        final Route route = routesByShapeId.get(SHAPE_TO_LOAD) ;
        final double routeLength = route.geometryLengthInMeters() ;
        String lineNumber = route.getLineNumber() ;
        Collection<Route> routesForLineNumber = routesByShapeId.entrySet().stream()
                .filter( item -> item.getValue().getLineNumber().equals(lineNumber) )
                .map(Map.Entry::getValue)
                .collect(Collectors.toList()) ;

        // Create Sample Reader...
        Iterator<SampleData> samples = new GZipSampleDataReader("2015.*\\.txt\\.gz");
        //        Iterator<SampleData> samples = new GZipSampleDataReader("2014((06[0-9][0-9])|(07[0-9][0-9])|(080[1-8]))\\.txt\\.gz");
        //        Iterator<SampleData> samples = new GZipSampleDataReader();
        SequenceBreaker sequenceBreakers = new LeastIntervalBreaker(3 * 60 * 1000);    // Ignore samples that are 2min apart
        sequenceBreakers = new ChangeLineNumberBreaker(sequenceBreakers);    // Keep only same line number in the sequence
        sequenceBreakers = new BackwardTimeBreaker(sequenceBreakers);
        DataFilter filter = new LineNumberFilter(lineNumber) ;
        filter = new WorkdayFilter(filter);
        filter = new DaylightSavingTimeFixFilter(filter);

        BufferedWriter out = new BufferedWriter(new FileWriter("H:\\onibus-data\\train-brt\\train-" + SHAPE_TO_LOAD + "-complex.svm")) ;

        SequenceGenerator<SampleData> samplesSequence = new ThreadedSequenceGenerator2<>(sequenceBreakers, filter, samples, SampleData::getOrder);

        // Convert Sample to Projection (snapping)
        SequenceGenerator<ProjectionInfo<SampleData>> projectionSequence = new SampleDataSequence2ProjectionSequence(routesForLineNumber, samplesSequence);

        // Read data
        projectionSequence.readData(new SequenceReader<ProjectionInfo<SampleData>>() {
            ThreadLocal<StringBuffer> sbLocal = new ThreadLocal<StringBuffer>() {
                @Override
                protected StringBuffer initialValue() {
                    return new StringBuffer(1024);
                }
            } ;

            @Override
            public void beginSequence(ProjectionInfo<SampleData> firstData) {
            }

            @Override
            public void sequence(ProjectionInfo<SampleData> lastData, ProjectionInfo<SampleData> newData) {
                if (lastData.getRoute().equals(route) && newData.getRoute().equals(route)) {
                    double currentPosition = newData.distanceTraveledOnRoute();
                    if (currentPosition > 500 && currentPosition < routeLength - 500) {
                        double lastPosition = lastData.distanceTraveledOnRoute();
                        int diffTimeSeconds = (int) ((newData.getLocationData().getTimestamp() - lastData.getLocationData().getTimestamp()) / 1000);
                        double meanSpeed = (currentPosition - lastPosition) / diffTimeSeconds;

                        if (meanSpeed > 0) {
                            double projectedLastPosition = Math.max(0, currentPosition - DISCRETE_TIME_SECONDS * meanSpeed);

                            try {
                                StringBuffer sb = sbLocal.get();
                                sb.setLength(0);

                                double traveled = currentPosition - projectedLastPosition;
                                if (traveled > MAX_TRAVEL_IN_DISCRETE_TIME) {
                                    traveled = MAX_TRAVEL_IN_DISCRETE_TIME;
                                }
                                sb.append(String.format(EN_US, "%.7f",
                                        traveled / MAX_TRAVEL_IN_DISCRETE_TIME
                                ));

                                int sampleMinutes = (int) (((newData.getLocationData().getTimestamp() - DISCRETE_TIME_SECONDS * 1000) % MS_IN_A_DAY) / (1000 * 60));
                                int attrIndex = sampleMinutes / PRECISION_MINUTES;
                                for (int i = 0; i < 24 * 60 / PRECISION_MINUTES; i++) {
                                    int idxDiff = attrIndex - i;
                                    double value = Math.exp(-idxDiff * idxDiff);
                                    if (value > 1e-7) {
                                        sb.append(String.format(EN_US, " %d:%.7f", i + INITIAL_ATTRIBUTES, value));
                                    }
                                }

                                int firstDistanceAttr = 24 * 60 / PRECISION_MINUTES + INITIAL_ATTRIBUTES;
                                int dIdx = 0;
                                while (dIdx * PRECISION_METERS < routeLength) {
                                    int idxDiff = (int) ((currentPosition - dIdx * PRECISION_METERS) / PRECISION_METERS);
                                    double value = Math.exp(-idxDiff * idxDiff);
                                    if (value > 1e-7) {
                                        sb.append(String.format(EN_US, " %d:%.7f", dIdx + firstDistanceAttr, value));
                                    }
                                    dIdx++;
                                }

                                sb.append("\n");
                                synchronized (out) {
                                    out.write(sb.toString());
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }

            @Override
            public void endSequence(ProjectionInfo<SampleData> lastData) {

            }
        });

        out.flush();
        out.close();
    }

    protected static double timeToColorPercent(long time)
    {
        return Math.min(1.0, (time % MS_IN_A_DAY) / (double) MS_IN_A_DAY);
    }
}
