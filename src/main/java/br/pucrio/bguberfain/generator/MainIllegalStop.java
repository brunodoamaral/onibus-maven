package br.pucrio.bguberfain.generator;

import br.pucrio.bguberfain.onibus.gui.XYGraph;
import br.pucrio.bguberfain.onibus.model.graph.v2.ProjectionInfo;
import br.pucrio.bguberfain.onibus.model.graph.v2.Route;
import br.pucrio.bguberfain.onibus.model.graph.v2.SampleData;
import br.pucrio.bguberfain.onibus.reader.GZipSampleDataReader;
import br.pucrio.bguberfain.onibus.reader.sequence.SequenceGenerator;
import br.pucrio.bguberfain.onibus.reader.sequence.SequenceReader;
import br.pucrio.bguberfain.onibus.reader.sequence.SimpleSequenceGenerator;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.SequenceBreaker;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.sampledata.BackwardTimeBreaker;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.sampledata.ChangeDayBreaker;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.sampledata.ChangeLineNumberBreaker;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.sampledata.LeastIntervalBreaker;
import br.pucrio.bguberfain.onibus.reader.sequence.filter.DataFilter;
import br.pucrio.bguberfain.onibus.reader.sequence.filter.sampledata.DaylightSavingTimeFixFilter;
import br.pucrio.bguberfain.onibus.reader.sequence.filter.sampledata.FixedLineNumberFilter;
import br.pucrio.bguberfain.onibus.reader.sequence.filter.sampledata.WorkdayFilter;
import br.pucrio.bguberfain.onibus.reader.transformer.SampleDataSequence2ProjectionSequence;
import br.pucrio.bguberfain.onibus.util.Util;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.PrecisionModel;
import com.vividsolutions.jts.io.WKTReader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.geotools.referencing.crs.DefaultGeographicCRS;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.*;

/**
 * Created by Bruno on 22/04/2015.
 */
public class MainIllegalStop {
    private static final Logger logger = LogManager.getLogger(MainIllegalStop.class);

    private static final String linhaAmarelaSentidoCentro =
            "LINESTRING ( -43.3059 -22.9064, -43.3064 -22.9067, -43.3064 -22.9067, -43.3069 -22.9069, -43.3069 -22.9069," +
            "	-43.3074 -22.9071, -43.3074 -22.9071, -43.3082 -22.9071, -43.3082 -22.9071, -43.3096 -22.9069, -43.3096 -22.9069, " +
            "	-43.3096 -22.9069, -43.3096 -22.9069, -43.3105 -22.9069, -43.3105 -22.9069, -43.3105 -22.9069, -43.3105 -22.9069, " +
            "	-43.3106 -22.9068, -43.3106 -22.9068, -43.311 -22.9068, -43.311 -22.9068, -43.3116 -22.9067, -43.3116 -22.9067, " +
            "	-43.3122 -22.9068, -43.3122 -22.9068, -43.3125 -22.9068, -43.3125 -22.9068, -43.3129 -22.9069, -43.3129 -22.9069, " +
            "	-43.3134 -22.907, -43.3134 -22.907, -43.3139 -22.9073, -43.3139 -22.9073, -43.3144 -22.9077, -43.3144 -22.9077, " +
            "	-43.3148 -22.9082, -43.3148 -22.9082, -43.315 -22.9086, -43.315 -22.9086, -43.315 -22.9088, -43.315 -22.9088, " +
            "	-43.315 -22.9088, -43.315 -22.9088, -43.3165 -22.9119, -43.3165 -22.9119, -43.3166 -22.9121, -43.3166 -22.9121, " +
            "	-43.3167 -22.9123, -43.3167 -22.9123, -43.3168 -22.9125, -43.3168 -22.9125, -43.317 -22.9128, -43.317 -22.9128, " +
            "	-43.3173 -22.9131, -43.3173 -22.9131, -43.3176 -22.9134, -43.3176 -22.9134, -43.3178 -22.9136, -43.3178 -22.9136, " +
            "	-43.3203 -22.9156, -43.3203 -22.9156, -43.326 -22.9192, -43.326 -22.9192, -43.3307 -22.9215, -43.3307 -22.9215, " +
            "	-43.3307 -22.9215, -43.3307 -22.9215, -43.3312 -22.9217, -43.3312 -22.9217, -43.3321 -22.9221, -43.3321 -22.9221, " +
            "	-43.3321 -22.9221, -43.3321 -22.9221, -43.3325 -22.9223, -43.3325 -22.9223, -43.3337 -22.9227, -43.3337 -22.9227, " +
            "	-43.3337 -22.9227, -43.3337 -22.9227, -43.3356 -22.9235, -43.3356 -22.9235, -43.3362 -22.9237, -43.3362 -22.9237, " +
            "	-43.3365 -22.9239, -43.3365 -22.9239, -43.3372 -22.9242, -43.3372 -22.9242, -43.3382 -22.9247, -43.3382 -22.9247, " +
            "	-43.3384 -22.9249, -43.3384 -22.9249, -43.3384 -22.9249, -43.3384 -22.9249, -43.3387 -22.925, -43.3387 -22.925, " +
            "	-43.3395 -22.9254, -43.3395 -22.9254, -43.3395 -22.9254, -43.3395 -22.9254, -43.3402 -22.9258, -43.3402 -22.9258, " +
            "	-43.3402 -22.9258, -43.3402 -22.9258, -43.3407 -22.9261, -43.3407 -22.9261, -43.341 -22.9264, -43.341 -22.9264, " +
            "	-43.3414 -22.9266, -43.3414 -22.9266, -43.3417 -22.9269, -43.3417 -22.9269, -43.3417 -22.9269, -43.3417 -22.9269, " +
            "	-43.3421 -22.9273, -43.3421 -22.9273, -43.3422 -22.9274, -43.3422 -22.9274, -43.3422 -22.9274, -43.3422 -22.9274, " +
            "	-43.3422 -22.9274, -43.3422 -22.9274, -43.3423 -22.9276, -43.3423 -22.9276, -43.3423 -22.9276, -43.3423 -22.9276, " +
            "	-43.3425 -22.9278, -43.3425 -22.9278, -43.3425 -22.9278, -43.3425 -22.9278, -43.3428 -22.9282, -43.3428 -22.9282, " +
            "	-43.3431 -22.9287, -43.3431 -22.9287, -43.3433 -22.929, -43.3433 -22.929, -43.3436 -22.9295, -43.3436 -22.9295, " +
            "	-43.3438 -22.9299, -43.3438 -22.9299, -43.3438 -22.9299, -43.3438 -22.9299, -43.3441 -22.9303, -43.3441 -22.9303, " +
            "	-43.3442 -22.9306, -43.3442 -22.9306, -43.3444 -22.9309, -43.3444 -22.9309, -43.3445 -22.9313, -43.3445 -22.9313, " +
            "	-43.3445 -22.9313, -43.3445 -22.9313, -43.3447 -22.9317, -43.3447 -22.9317, -43.3449 -22.9321, -43.3449 -22.9321, " +
            "	-43.3451 -22.9324, -43.3451 -22.9324, -43.3455 -22.9333, -43.3455 -22.9333, -43.3455 -22.9333, -43.3455 -22.9333, " +
            "	-43.3459 -22.934, -43.3459 -22.934, -43.3462 -22.9345, -43.3462 -22.9345, -43.3462 -22.9345, -43.3462 -22.9345, " +
            "	-43.3464 -22.9347, -43.3464 -22.9347, -43.3467 -22.9351, -43.3467 -22.9351, -43.3473 -22.9358, -43.3473 -22.9358, " +
            "	-43.3475 -22.9361, -43.3475 -22.9361, -43.3482 -22.9369, -43.3482 -22.9369, -43.3491 -22.9379, -43.3491 -22.9379, " +
            "	-43.3491 -22.9379, -43.3491 -22.9379, -43.3494 -22.9381, -43.3494 -22.9381, -43.3494 -22.9381, -43.3494 -22.9381, " +
            "	-43.3497 -22.9385, -43.3497 -22.9385, -43.3497 -22.9385, -43.3497 -22.9385, -43.3499 -22.9388, -43.3499 -22.9388, " +
            "	-43.3499 -22.9388, -43.3499 -22.9388, -43.3504 -22.9393, -43.3504 -22.9393, -43.3508 -22.9398, -43.3508 -22.9398, " +
            "	-43.3508 -22.9398, -43.3508 -22.9398, -43.3511 -22.9401, -43.3511 -22.9401, -43.3514 -22.9405, -43.3514 -22.9405, " +
            "	-43.3514 -22.9405, -43.3514 -22.9405, -43.3515 -22.9407, -43.3515 -22.9407, -43.3519 -22.9413, -43.3519 -22.9413, " +
            "	-43.3524 -22.9421, -43.3524 -22.9421, -43.3528 -22.9429, -43.3528 -22.9429, -43.3533 -22.9438, -43.3533 -22.9438, " +
            "	-43.3539 -22.9447, -43.3539 -22.9447, -43.3543 -22.9455, -43.3543 -22.9455, -43.3548 -22.9466, -43.3548 -22.9466, " +
            "	-43.355 -22.9469, -43.355 -22.9469, -43.3561 -22.9488, -43.3561 -22.9488, -43.3562 -22.949, -43.3562 -22.949, " +
            "	-43.3563 -22.9493, -43.3563 -22.9493, -43.3565 -22.9497, -43.3565 -22.9497, -43.3566 -22.9499, -43.3566 -22.9499, " +
            "	-43.3567 -22.9503, -43.3567 -22.9503, -43.3569 -22.9512, -43.3569 -22.9512, -43.357 -22.9519, -43.357 -22.9519, " +
            "	-43.357 -22.9519, -43.357 -22.9519, -43.3572 -22.9529, -43.3572 -22.9529, -43.3573 -22.9533, -43.3573 -22.9533, " +
            "	-43.3574 -22.9536, -43.3574 -22.9536, -43.3574 -22.9537, -43.3574 -22.9537, -43.3574 -22.9539, -43.3574 -22.9539, " +
            "	-43.3575 -22.9542, -43.3575 -22.9542, -43.3574 -22.9544, -43.3574 -22.9544, -43.3574 -22.9546, -43.3574 -22.9546, " +
            "	-43.3574 -22.9548, -43.3574 -22.9548, -43.3573 -22.955, -43.3573 -22.955, -43.3571 -22.9554, -43.3571 -22.9554, " +
            "	-43.3571 -22.9554, -43.3571 -22.9554, -43.357 -22.9556, -43.357 -22.9556, -43.357 -22.9556, -43.357 -22.9556, " +
            "	-43.3568 -22.9559, -43.3568 -22.9559, -43.3567 -22.9561, -43.3567 -22.9562, -43.3567 -22.9564, -43.3567 -22.9564, " +
            "	-43.3566 -22.9566, -43.3566 -22.9567, -43.3567 -22.9568, -43.3567 -22.9568, -43.3568 -22.9572, -43.3568 -22.9572, " +
            "	-43.3569 -22.9576, -43.3569 -22.9576, -43.3569 -22.9578, -43.3569 -22.9578, -43.3573 -22.9585, -43.3573 -22.9585, " +
            "	-43.3574 -22.9589, -43.3574 -22.9589, -43.3575 -22.9592, -43.3575 -22.9592, -43.3575 -22.9592, -43.3575 -22.9592, " +
            "	-43.3576 -22.9595, -43.3576 -22.9595, -43.3576 -22.9597, -43.3576 -22.9597, -43.3576 -22.96, -43.3576 -22.96, " +
            "	-43.3575 -22.9607, -43.3575 -22.9607, -43.3573 -22.9617, -43.3573 -22.9617, -43.3573 -22.9621, -43.3573 -22.9621, " +
            "	-43.3573 -22.9624, -43.3573 -22.9624, -43.3572 -22.9633, -43.3572 -22.9633, -43.3572 -22.9647, -43.3572 -22.9647, " +
            "	-43.3572 -22.9651, -43.3572 -22.9651, -43.3573 -22.9662, -43.3573 -22.9662, -43.3573 -22.9662, -43.3573 -22.9662, " +
            "	-43.3573 -22.9665, -43.3573 -22.9665, -43.3574 -22.9667, -43.3574 -22.9667, -43.3575 -22.967, -43.3575 -22.967, " +
            "	-43.3576 -22.9672 )" ;
    ;
    private static final String linhaAmarelaSentidoBarra =
            "LINESTRING ( -43.3574 -22.9672, -43.3573 -22.967, -43.3573 -22.967, -43.3572 -22.9668, -43.3572 -22.9668," +
            "	-43.3571 -22.9663, -43.3571 -22.9663, -43.3571 -22.9663, -43.3571 -22.9663, -43.3571 -22.9661, -43.3571 -22.9661, " +
            "	-43.3571 -22.966, -43.3571 -22.966, -43.3571 -22.9657, -43.3571 -22.9657, -43.3571 -22.9636, -43.3571 -22.9636, " +
            "	-43.3571 -22.9628, -43.3571 -22.9628, -43.3572 -22.9624, -43.3572 -22.9624, -43.3572 -22.9619, -43.3572 -22.9619, " +
            "	-43.3574 -22.9601, -43.3574 -22.9601, -43.3575 -22.9597, -43.3575 -22.9597, -43.3574 -22.9595, -43.3574 -22.9595, " +
            "	-43.3574 -22.9591, -43.3574 -22.9591, -43.3573 -22.9589, -43.3573 -22.9589, -43.3572 -22.9586, -43.3572 -22.9586, " +
            "	-43.3568 -22.9578, -43.3568 -22.9578, -43.3568 -22.9577, -43.3568 -22.9577, -43.3567 -22.9574, -43.3567 -22.9574, " +
            "	-43.3566 -22.957, -43.3566 -22.957, -43.3565 -22.9569, -43.3565 -22.9569, -43.3565 -22.9567, -43.3565 -22.9567, " +
            "	-43.3565 -22.9565, -43.3565 -22.9564, -43.3566 -22.9563, -43.3566 -22.9563, -43.3566 -22.9562, -43.3566 -22.9562, " +
            "	-43.3568 -22.9557, -43.3568 -22.9557, -43.357 -22.9554, -43.357 -22.9554, -43.3571 -22.9551, -43.3571 -22.9551, " +
            "	-43.3572 -22.9549, -43.3572 -22.9549, -43.3572 -22.9548, -43.3572 -22.9548, -43.3573 -22.9546, -43.3573 -22.9546, " +
            "	-43.3573 -22.9545, -43.3573 -22.9545, -43.3573 -22.9543, -43.3573 -22.9543, -43.3573 -22.9539, -43.3573 -22.9539, " +
            "	-43.3573 -22.9538, -43.3573 -22.9538, -43.3573 -22.9537, -43.3573 -22.9537, -43.3572 -22.9534, -43.3572 -22.9534, " +
            "	-43.3571 -22.9529, -43.3571 -22.9529, -43.3569 -22.9519, -43.3569 -22.9519, -43.3569 -22.9519, -43.3569 -22.9519, " +
            "	-43.3568 -22.9509, -43.3568 -22.9509, -43.3568 -22.9509, -43.3568 -22.9509, -43.3566 -22.9503, -43.3566 -22.9503, " +
            "	-43.3565 -22.9501, -43.3565 -22.9501, -43.3564 -22.9496, -43.3564 -22.9496, -43.3563 -22.9494, -43.3563 -22.9494, " +
            "	-43.3562 -22.9492, -43.3562 -22.9492, -43.3561 -22.949, -43.3561 -22.949, -43.3558 -22.9484, -43.3558 -22.9484, " +
            "	-43.3549 -22.947, -43.3549 -22.947, -43.3541 -22.9455, -43.3541 -22.9455, -43.3534 -22.9442, -43.3534 -22.9442, " +
            "	-43.3526 -22.9429, -43.3526 -22.9429, -43.3521 -22.942, -43.3521 -22.942, -43.3521 -22.942, -43.3521 -22.942, " +
            "	-43.3519 -22.9416, -43.3519 -22.9416, -43.3516 -22.9411, -43.3516 -22.9411, -43.3513 -22.9406, -43.3513 -22.9406, " +
            "	-43.3509 -22.9402, -43.3509 -22.9402, -43.3506 -22.9399, -43.3506 -22.9399, -43.3506 -22.9399, -43.3506 -22.9399, " +
            "	-43.35 -22.9391, -43.35 -22.9391, -43.3495 -22.9386, -43.3495 -22.9386, -43.3495 -22.9386, -43.3495 -22.9386, " +
            "	-43.3495 -22.9386, -43.3495 -22.9386, -43.3495 -22.9386, -43.3495 -22.9386, -43.3492 -22.9382, -43.3492 -22.9382, " +
            "	-43.3492 -22.9382, -43.3492 -22.9382, -43.349 -22.9379, -43.349 -22.9379, -43.349 -22.9379, -43.349 -22.9379, " +
            "	-43.3484 -22.9374, -43.3484 -22.9374, -43.3479 -22.9367, -43.3479 -22.9367, -43.3476 -22.9364, -43.3476 -22.9364, " +
            "	-43.3468 -22.9355, -43.3468 -22.9355, -43.3468 -22.9355, -43.3468 -22.9355, -43.3467 -22.9353, -43.3467 -22.9353, " +
            "	-43.3461 -22.9346, -43.3461 -22.9346, -43.3461 -22.9346, -43.3461 -22.9346, -43.346 -22.9344, -43.346 -22.9344, " +
            "	-43.3457 -22.934, -43.3457 -22.934, -43.3456 -22.9338, -43.3456 -22.9338, -43.3454 -22.9333, -43.3454 -22.9333, " +
            "	-43.3454 -22.9333, -43.3454 -22.9333, -43.3451 -22.9329, -43.3451 -22.9329, -43.3449 -22.9325, -43.3449 -22.9325, " +
            "	-43.3446 -22.9318, -43.3446 -22.9318, -43.3443 -22.9313, -43.3443 -22.9313, -43.3443 -22.9313, -43.3443 -22.9313, " +
            "	-43.3442 -22.931, -43.3442 -22.931, -43.344 -22.9307, -43.344 -22.9307, -43.3439 -22.9303, -43.3439 -22.9303, " +
            "	-43.3437 -22.9299, -43.3437 -22.9299, -43.3437 -22.9299, -43.3437 -22.9299, -43.3427 -22.9284, -43.3427 -22.9284, " +
            "	-43.3425 -22.9281, -43.3425 -22.9281, -43.3423 -22.9279, -43.3423 -22.9279, -43.3423 -22.9279, -43.3423 -22.9279, " +
            "	-43.3422 -22.9277, -43.3422 -22.9277, -43.3422 -22.9277, -43.3422 -22.9277, -43.342 -22.9274, -43.342 -22.9274, " +
            "	-43.342 -22.9274, -43.342 -22.9274, -43.3419 -22.9274, -43.3419 -22.9274, -43.3416 -22.927, -43.3416 -22.927, " +
            "	-43.3416 -22.927, -43.3416 -22.927, -43.3414 -22.9269, -43.3414 -22.9269, -43.3412 -22.9267, -43.3412 -22.9267, " +
            "	-43.3411 -22.9266, -43.3411 -22.9266, -43.3405 -22.9262, -43.3405 -22.9262, -43.3405 -22.9262, -43.3405 -22.9262, " +
            "	-43.3397 -22.9257, -43.3397 -22.9257, -43.3395 -22.9255, -43.3395 -22.9255, -43.3395 -22.9255, -43.3395 -22.9255, " +
            "	-43.3386 -22.9251, -43.3386 -22.9251, -43.3384 -22.925, -43.3384 -22.925, -43.3384 -22.925, -43.3384 -22.925, " +
            "	-43.3377 -22.9246, -43.3377 -22.9246, -43.3368 -22.9242, -43.3368 -22.9242, -43.3356 -22.9236, -43.3356 -22.9236, " +
            "	-43.3352 -22.9235, -43.3352 -22.9235, -43.3349 -22.9234, -43.3349 -22.9234, -43.3331 -22.9227, -43.3331 -22.9227, " +
            "	-43.3328 -22.9226, -43.3328 -22.9226, -43.332 -22.9222, -43.332 -22.9222, -43.3313 -22.922, -43.3313 -22.922, " +
            "	-43.3305 -22.9216, -43.3305 -22.9216, -43.3305 -22.9216, -43.3305 -22.9216, -43.325 -22.919, -43.325 -22.919, " +
            "	-43.32 -22.9159, -43.32 -22.9159, -43.3183 -22.9145, -43.3183 -22.9145, -43.3181 -22.9143, -43.3181 -22.9143, " +
            "	-43.3178 -22.9141, -43.3178 -22.9141, -43.3175 -22.9138, -43.3175 -22.9138, -43.3172 -22.9136, -43.3172 -22.9136, " +
            "	-43.3171 -22.9134, -43.3171 -22.9134, -43.317 -22.9133, -43.317 -22.9133, -43.3167 -22.913, -43.3167 -22.913, " +
            "	-43.3165 -22.9126, -43.3165 -22.9126, -43.3164 -22.9124, -43.3164 -22.9124, -43.3148 -22.9088, -43.3148 -22.9088, " +
            "	-43.3148 -22.9088, -43.3148 -22.9088, -43.3147 -22.9085, -43.3147 -22.9085, -43.3146 -22.9083, -43.3146 -22.9083, " +
            "	-43.3143 -22.908, -43.3143 -22.908, -43.3143 -22.9079, -43.3143 -22.9079, -43.3139 -22.9076, -43.3139 -22.9076, " +
            "	-43.3135 -22.9073, -43.3135 -22.9073, -43.313 -22.9071, -43.313 -22.9071, -43.3124 -22.9069, -43.3124 -22.9069, " +
            "	-43.312 -22.9069, -43.312 -22.9069, -43.3115 -22.9069, -43.3115 -22.9069, -43.3109 -22.9069, -43.3109 -22.9069, " +
            "	-43.3107 -22.9069, -43.3107 -22.9069, -43.3106 -22.907, -43.3106 -22.907, -43.3106 -22.907, -43.3106 -22.907, " +
            "	-43.3102 -22.907, -43.3102 -22.907, -43.3096 -22.9072, -43.3096 -22.9072, -43.309 -22.9074, -43.309 -22.9074, " +
            "	-43.3082 -22.9075, -43.3082 -22.9075, -43.3078 -22.9075, -43.3078 -22.9075, -43.3078 -22.9075, -43.3078 -22.9075, " +
            "	-43.3073 -22.9073, -43.3073 -22.9073, -43.3068 -22.9071, -43.3068 -22.9071, -43.3064 -22.9069, -43.3064 -22.9069, " +
            "	-43.306 -22.9066, -43.306 -22.9066, -43.3057 -22.9064 )" ;

    static final File outFolder = new File(Util.getEnv("SAVE_TO_DIR")) ;

    public static void main(String [] args) throws Exception {

        GeometryFactory geometryFactory = new GeometryFactory(new PrecisionModel(), 4326) ;
        WKTReader wktReader = new WKTReader(geometryFactory) ;
        LineString lsSentidoCentro = (LineString) wktReader.read(linhaAmarelaSentidoCentro) ;
        LineString lsSentidoBarra = (LineString) wktReader.read(linhaAmarelaSentidoBarra) ;

        Route laSentidoCentro = new Route(lsSentidoCentro, "Linha-Amarela", 1003, DefaultGeographicCRS.WGS84) ;
        Route laSentidoBarra = new Route(lsSentidoBarra, "Linha-Amarela", 1004, DefaultGeographicCRS.WGS84) ;

        String filePattern = "201504.*\\.txt\\.gz" ;

        SequenceGenerator<ProjectionInfo<SampleData>> sequencer = generatorForDate(filePattern, Arrays.asList(laSentidoCentro, laSentidoBarra));

        // Space x Speed
        // -> Before
        runAsThread(() -> saveXYGraph(MainGraphAverageSpeed.generateGraph(laSentidoCentro, Arrays.asList(laSentidoCentro, laSentidoBarra), filePattern, null, sequencer), "space-speed-illegal-stop"));

        // Space x Time
        // -> Before
        runAsThread(() -> saveXYGraph(MainGraphTimeSpace.generateGraph(laSentidoCentro, Arrays.asList(laSentidoCentro, laSentidoBarra), filePattern, null, sequencer), "space-time-illegal-stop"));

        // -> Before
        runAsThread(() -> saveXYGraph(MainGraphHeatmapDistanceBuses.generateGraph(laSentidoCentro, Arrays.asList(laSentidoCentro, laSentidoBarra), filePattern, null, 5, sequencer), "heatmap-illegal-stop"));

        logger.info("Finished");
    }

    private static SequenceGenerator<ProjectionInfo<SampleData>> generatorForDate(String datePattern, Collection<Route> routesForLineNumber) {
        // Create Sample Reader...
        GZipSampleDataReader samples = new GZipSampleDataReader(datePattern);
        //        samples.setIncludeAnnotations(true);
        SequenceBreaker sequenceBreakers = new LeastIntervalBreaker(5 * 60 * 1000);    // Ignore samples that are 5min apart
        sequenceBreakers = new ChangeDayBreaker(sequenceBreakers);         // Break samples between 0h
        sequenceBreakers = new ChangeLineNumberBreaker(sequenceBreakers);    // Keep only same line number in the sequence
        sequenceBreakers = new BackwardTimeBreaker(sequenceBreakers);
        DataFilter filter = new FixedLineNumberFilter("Linha-Amarela") ;
        filter = new WorkdayFilter(filter);
        filter = new DaylightSavingTimeFixFilter(filter);

        SequenceGenerator<SampleData> samplesSequence = new SimpleSequenceGenerator<>(sequenceBreakers, filter, samples, SampleData::getOrder, SampleData::isAnnotation);
        final SequenceGenerator<ProjectionInfo<SampleData>> realSource = new SampleDataSequence2ProjectionSequence(routesForLineNumber, samplesSequence) ;
        final Collection<SequenceReader<ProjectionInfo<SampleData>>> readers = new LinkedList<>() ;
        final Object lock = new Object() ;

        new Thread(() -> {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            realSource.readData(new SequenceReader<ProjectionInfo<SampleData>>() {
                @Override
                public void beginSequence(ProjectionInfo<SampleData> firstData) {
                    readers.forEach(r -> r.beginSequence(firstData));
                }

                @Override
                public void sequence(ProjectionInfo<SampleData> lastData, ProjectionInfo<SampleData> newData) {
                    readers.forEach(r -> r.sequence(lastData, newData));
                }

                @Override
                public void endSequence(ProjectionInfo<SampleData> lastData) {
                    readers.forEach(r -> r.endSequence(lastData));
                }
            });

            synchronized (lock) {
                lock.notifyAll();
            }
        }).start() ;

        // Convert Sample to Projection (snapping)
        return new SequenceGenerator<ProjectionInfo<SampleData>>() {
            @Override
            public void readData(SequenceReader<ProjectionInfo<SampleData>> sequenceReader) {
                readers.add(sequenceReader) ;
                synchronized (lock) {
                    try {
                        lock.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void stop() {

            }
        };
    }

    private interface ThrowableRunnable {
        void run() throws Exception ;
    }

    private static void runAsThread(ThrowableRunnable task) {
        new Thread(() -> {
            try {
                task.run() ;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).start();
    }

    private static void saveXYGraph(XYGraph xyGraph, String fileBase) throws IOException {
        xyGraph.saveImage(new File(outFolder, fileBase + ".png"), 1);
        xyGraph.saveData(new File(outFolder, fileBase + ".xyd"));

        JFrame frame = (JFrame) SwingUtilities.getRoot(xyGraph);
        frame.setVisible(false);
        frame.dispose();
    }
}
