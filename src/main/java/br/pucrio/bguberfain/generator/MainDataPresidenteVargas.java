package br.pucrio.bguberfain.generator;

import br.pucrio.bguberfain.onibus.model.graph.v2.SampleData;
import br.pucrio.bguberfain.onibus.reader.GZipSampleDataReader;
import com.vividsolutions.jts.geom.*;
import com.vividsolutions.jts.io.WKTReader;

import java.util.Iterator;

/**
 * Created by Bruno on 28/05/2015.
 */
public class MainDataPresidenteVargas {

    // Coordenadas obtidas a partir de http://www.birdtheme.org/useful/v3tool.html
    private static String WKT_PRES_VARGAS = "POLYGON (( -43.208199 -22.909622, -43.208456 -22.911598, -43.176184 -22.901162, -43.17687 -22.899581, -43.208199 -22.909622 ))" ;

    public static void main(String args[]) throws Exception {
        // Constrói um polígono ao redor da Presidente Vargs
        GeometryFactory geometryFactory = new GeometryFactory(new PrecisionModel(), 4326) ;
        WKTReader wktReader = new WKTReader(geometryFactory) ;
        Polygon polyPresVargas = (Polygon) wktReader.read(WKT_PRES_VARGAS) ;
        Envelope envPresVargs = polyPresVargas.getEnvelopeInternal() ;

        Iterator<SampleData> reader = new GZipSampleDataReader() ;
        while(reader.hasNext()) {
            SampleData sample = reader.next() ;

            // Checa se o sample está dentro da "caixa" que delimita a Presidente Vargas
            if (envPresVargs.contains(sample.getCoordinate())) {

                // Converte para Point e verifica se está dentro do polígono
                Point ptSample = geometryFactory.createPoint(sample.getCoordinate()) ;
                if( polyPresVargas.contains(ptSample) ) {
                    // TODO: Fazer algo de útil com o sample que está dentro da Av. Presidente Vargas
                    System.out.printf("%d\t%.5f\t%.5f\n", sample.getTimestamp(), sample.getCoordinate().x, sample.getCoordinate().y);
                }
            }
        }
    }

}
