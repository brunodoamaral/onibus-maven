package br.pucrio.bguberfain.generator;

import br.pucrio.bguberfain.onibus.model.graph.v2.SampleData;
import br.pucrio.bguberfain.onibus.reader.SampleDataReader;
import br.pucrio.bguberfain.onibus.reader.sequence.SequenceGenerator;
import br.pucrio.bguberfain.onibus.reader.sequence.SequenceReader;
import br.pucrio.bguberfain.onibus.reader.sequence.ThreadedSequenceGenerator;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.SequenceBreaker;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.sampledata.ChangeLineNumberBreaker;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.sampledata.LeastIntervalBreaker;
import br.pucrio.bguberfain.onibus.reader.sequence.filter.DataFilter;
import br.pucrio.bguberfain.onibus.reader.sequence.filter.sampledata.DaylightSavingTimeFixFilter;
import br.pucrio.bguberfain.onibus.reader.sequence.filter.sampledata.LongDelayFilter;
import br.pucrio.bguberfain.onibus_geotools.Util;
import com.vividsolutions.jts.algorithm.Angle;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineSegment;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.linearref.LinearLocation;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.apache.commons.math3.stat.descriptive.SummaryStatistics;
import org.apache.commons.math3.stat.descriptive.SynchronizedSummaryStatistics;
import org.geotools.data.*;
import org.geotools.data.collection.ListFeatureCollection;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.data.simple.SimpleFeatureStore;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.feature.simple.SimpleFeatureTypeBuilder;
import org.geotools.geometry.jts.JTS;
import org.geotools.geometry.jts.JTSFactoryFinder;
import org.geotools.jdbc.JDBCDataStoreFactory;
import org.geotools.referencing.crs.DefaultGeographicCRS;
import org.geotools.util.NullProgressListener;
import org.opengis.feature.Feature;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;

public class MainByLineString {
	
    private static final double MAX_SEARCH_DISTANCE = 0.0004 ;	// In degrees (aprox. 40m)

    private static final boolean USE_POSTGIS = true ;
    private static final boolean SAVE_TO_POSTGIS = false ;
    //private static final boolean SAVE_TO_FILE = false ;
    
    private static final int MAX_DELAY_IN_SECONDS = 60 * 50 ;	// Ignore samples with delay above 50 minutes
	
	public static void main(String[] args) throws Exception {
		// Le os dados das linhas
		final SimpleFeatureType projectionType = createProjectionFeatureType() ;
		
		DataStore store ;
		SimpleFeatureSource featureSource ;
		if ( USE_POSTGIS ) {
			// Load line shapes (from postgis)
			Map<String, Object> params = new HashMap<>();
			params.put(JDBCDataStoreFactory.DBTYPE.key, "postgis"); 
			params.put(JDBCDataStoreFactory.HOST.key, "localhost"); 
			params.put(JDBCDataStoreFactory.PORT.key, 5432); 
			params.put(JDBCDataStoreFactory.SCHEMA.key, "public"); 
			params.put(JDBCDataStoreFactory.DATABASE.key, "bus-routes"); 
			params.put(JDBCDataStoreFactory.USER.key, "postgres"); 
			params.put(JDBCDataStoreFactory.PASSWD.key, "postgres"); 
			store = DataStoreFinder.getDataStore(params);
			featureSource = store.getFeatureSource("routes");
		} else {
			// Load line shapes (from file)
			File routesShapeFile = new File("data/trajetos-shape/routes.shp");
			store = FileDataStoreFinder.getDataStore(routesShapeFile);
			featureSource = store.getFeatureSource("routes");
		}

        System.out.println("Loading shapesfile...");
        final Map<String, Set<LocationIndexedLineWithGeometry>> routesByLineNumber = new HashMap<>();
        SimpleFeatureCollection featureCollection = featureSource.getFeatures() ;
        
        // Obtem as rotas agrupadas por linha
        featureCollection.accepts((Feature feature) -> {
            SimpleFeature simpleFeature = (SimpleFeature) feature;
            LineString geom = (LineString) simpleFeature.getDefaultGeometry();
            String lineNumber = (String) simpleFeature.getAttribute("line_number") ;
            String lineName = (String) simpleFeature.getAttribute("line_name") ;
            String shapeId = (String) simpleFeature.getAttribute("shape_id") ;
            
        	LocationIndexedLineWithGeometry locationIndex;
			try {
				locationIndex = new LocationIndexedLineWithGeometry(geom, projectionType.getCoordinateReferenceSystem());
				locationIndex.setLineNumber(lineNumber);
				locationIndex.setLineName(lineName);
				locationIndex.setShapeId(new Integer(shapeId));
				
				// Save LS by line number
				Set<LocationIndexedLineWithGeometry> routesSet = routesByLineNumber.get(lineNumber) ;
				if ( routesSet == null ) {
					routesSet = new HashSet<>();
					routesByLineNumber.put(lineNumber, routesSet);
				}
				routesSet.add(locationIndex) ;
			} catch (Exception e) {
				throw new RuntimeException(e) ;
			}
        	
        }, new NullProgressListener() );
        
        
		// Cria o tabela de projeções
        SimpleFeatureStore projectionsDestination ;
        Transaction transaction ;
        if ( SAVE_TO_POSTGIS ) {
	        try {
	        	store.removeSchema(projectionType.getName().getLocalPart());
	        } catch( IllegalArgumentException e ) { } // Does nothing... relation simply does not exists
			store.createSchema(projectionType);
			// Cria store de saída das projeções
			transaction = new DefaultTransaction("create");
			projectionsDestination = (SimpleFeatureStore) store.getFeatureSource(projectionType.getName().getLocalPart());
			projectionsDestination.setTransaction(transaction);
        } else {
        	projectionsDestination = null ;
        	transaction = null ;
        }
		
		
        // Início do snap!
		SampleDataReader sampleReader = new SampleDataReader() ;
		SequenceBreaker sequenceBreakers = new LeastIntervalBreaker((int) 5 * 60 * 1000) ;	// Ignore samples that are 5min apart
//		sequenceBreakers = new LongDistanceBreaker(sequenceBreakers, 1000) ;	// Ignore samples of more than 1km
		sequenceBreakers = new ChangeLineNumberBreaker(sequenceBreakers) ;	// Keep only same line number in the sequence

		// Apply the fix for DST
		DataFilter filter = new LongDelayFilter(MAX_DELAY_IN_SECONDS) ;
		filter = new DaylightSavingTimeFixFilter(filter) ;

		SequenceGenerator<SampleData> sequenceGenerator = new ThreadedSequenceGenerator<>(sequenceBreakers, filter, sampleReader, SampleData::getOrder) ;

		final long startTime = System.currentTimeMillis() ;
        System.out.println("Reading log file...");
        sequenceGenerator.readData(new SequenceReader<SampleData>() {
        	
        	AtomicLong processed = new AtomicLong(0) ;
        	AtomicLong countIndexNotFound = new AtomicLong(0) ;
        	SummaryStatistics statsForNumRoutes = new SynchronizedSummaryStatistics() ;
        	SummaryStatistics statsForDistance = new SynchronizedSummaryStatistics() ;
        	
        	ThreadLocal<List<SimpleFeature>> projectedFeaturesForThread = new ThreadLocal<List<SimpleFeature>>() {
        		@Override
        		protected List<SimpleFeature> initialValue() {
        			return new LinkedList<SimpleFeature>();
        		}
        	} ;

			ThreadLocal<Map<String, Integer>> lastRouteIdByOrderIdForThread = new ThreadLocal<Map<String, Integer>>() {
				@Override
				protected Map<String, Integer> initialValue() {
					return new HashMap<>() ;
				}
			} ;

			ThreadLocal<Map<String, Map<Integer, DescriptiveStatistics>>> probabilityOfRouteByOrderIdForThread = new ThreadLocal<Map<String, Map<Integer, DescriptiveStatistics>>>() {
				@Override
				protected Map<String, Map<Integer, DescriptiveStatistics>> initialValue() {
					return new HashMap<>() ;
				}
			} ;

			ThreadLocal<SimpleFeatureBuilder> featureBuilderForThread = new ThreadLocal<SimpleFeatureBuilder>() {
        		@Override
        		protected SimpleFeatureBuilder initialValue() {
        			return new SimpleFeatureBuilder(projectionType);
        		}
        	} ;
        	
        	ThreadLocal<GeometryFactory> geometryFactory = new ThreadLocal<GeometryFactory>() {
        		@Override
        		protected GeometryFactory initialValue() {
        			return JTSFactoryFinder.getGeometryFactory() ;
        		}
        	} ;

			@Override
			public void beginSequence(SampleData firstData) {
				// Create/reset stats for this sample's order-id
				Map<String, Map<Integer, DescriptiveStatistics>> probabilityOfRouteByOrderId = probabilityOfRouteByOrderIdForThread.get() ;
				Map<Integer, DescriptiveStatistics> probabilityOfRoute = probabilityOfRouteByOrderId.get(firstData.getOrder()) ;
				if  (probabilityOfRoute == null) {
					probabilityOfRoute = new HashMap<>() ;
					probabilityOfRouteByOrderId.put(firstData.getOrder(), probabilityOfRoute) ;
				} else {
					probabilityOfRoute.clear() ;
				}
			}

			@Override
			public void sequence(SampleData lastData, SampleData newData)
			{
				try {
					String lineNumber = lastData.getLineNumber() ;
					if ( !"".equals(lineNumber) ) {
						long localProcessed = processed.incrementAndGet() ;
						// Get routes for this line (if so)
						Set<LocationIndexedLineWithGeometry> routesSet = routesByLineNumber.get(lineNumber) ;
						if( routesSet == null ) {
							countIndexNotFound.incrementAndGet() ;
							return ;	// Ignore processing of lines without route
						}
						
						// Log and save projectedFeatures
						List<SimpleFeature> projectedFeatures = projectedFeaturesForThread.get() ;
						if ( localProcessed % 20000 == 0 ) {
							long localCountIndexNotFound = countIndexNotFound.get() ;
							System.out.printf("Procesed %10d samples at rate %5.2fsamples/s with %4.2f%% of lines not found\n", localProcessed, localProcessed*1000.0/(System.currentTimeMillis()-startTime), localCountIndexNotFound*100.0/localProcessed);
							System.out.printf("Mean # of candidate routes: %.4f (StdDev: %.4f)\n", statsForNumRoutes.getMean(), statsForNumRoutes.getStandardDeviation());
							System.out.printf("Mean distance (x1000): %.4f (StdDev: %.4f)\n", 1000*statsForDistance.getMean(), 1000*statsForDistance.getStandardDeviation());
							
							if ( SAVE_TO_POSTGIS ) {
								if ( projectedFeatures.size() > 0 ) {
									SimpleFeatureCollection collection = new ListFeatureCollection(projectionType, projectedFeatures);
									System.out.printf("Adding %d features to database...\n", collection.size());
									projectionsDestination.addFeatures(collection);
									projectedFeatures.clear();
								}
								try {
									synchronized(transaction) {
										System.out.println("Commiting...");
										transaction.commit();
									}
								} catch (Exception problem) {
									problem.printStackTrace();
									transaction.rollback();
									throw new RuntimeException(problem);
								}
							}
						}
						statsForNumRoutes.addValue(routesSet.size());

						LineSegment sampleSegment = new LineSegment(lastData.getCoordinate(), newData.getCoordinate()) ;
						String lastRouteId = newData.getOrder() ;

						// Busca a rota mais provavel
						Map<Integer, DescriptiveStatistics> probabilityOfRouteById = probabilityOfRouteByOrderIdForThread.get().get(newData.getOrder()) ;
						double minDistance = Double.MAX_VALUE ;
						LocationIndexedLineWithGeometry minRoute = null ;
						Coordinate minProjectedLocation = null ;
						LinearLocation minNewDataLocation = null ;
						LinearLocation minLastDataLocation = null ;
						double minProbabilityOfRoute = Double.MAX_VALUE ;
						for (LocationIndexedLineWithGeometry route : routesSet) {
							DescriptiveStatistics probabilityOfRoute = probabilityOfRouteById.get(route.getShapeId()) ;
							if ( probabilityOfRoute == null ) {
								probabilityOfRoute = new DescriptiveStatistics(5);
								probabilityOfRoute.addValue(1.0 / routesSet.size());
								probabilityOfRouteById.put(route.getShapeId(), probabilityOfRoute);
							}

							LinearLocation lastDataLocation = route.project(lastData.getCoordinate());
			                Coordinate lastDataProjection = route.extractPoint(lastDataLocation) ;
			                double lastDataProjectionDistance = lastDataProjection.distance(lastData.getCoordinate()) ;
			                statsForDistance.addValue(lastDataProjectionDistance);

			                // So faz a projecao do segundo ponto se o primeiro estiver dentro do range de busca
			                if ( lastDataProjectionDistance < MAX_SEARCH_DISTANCE ) {
			                	LinearLocation newDataLocation = route.project(newData.getCoordinate());
			                	Coordinate newDataProjection = route.extractPoint(newDataLocation) ;

			                	double newDataProjectionDistance = newDataProjection.distance(newData.getCoordinate()) ;
				                if ( newDataProjectionDistance < MAX_SEARCH_DISTANCE ) {
				                	LineSegment projectedSegment ;
				                	// Cria o segmento dos pontos projetados, considerando a direcao da rota
				                	if ( newDataLocation.compareTo(lastDataLocation) < 0 ) {
					                	projectedSegment = new LineSegment(newDataProjection, lastDataProjection);
				                	} else {
					                	projectedSegment = new LineSegment(lastDataProjection, newDataProjection);
				                	}

				                	// Calcula o angulo entre os dois segmentos
				                	double angle = Angle.normalizePositive(sampleSegment.angle() - projectedSegment.angle()) ;
									probabilityOfRoute.addValue( 1 / (angle+1) );
									if( probabilityOfRoute.getMean() < minProbabilityOfRoute ) {
										minDistance = newDataProjectionDistance ;
										minProjectedLocation = newDataProjection ;
										minRoute = route ;
										minNewDataLocation = newDataLocation ;
										minLastDataLocation = lastDataLocation ;
									}
								} else {
									// Decrease the probability of this route
									probabilityOfRoute.addValue(0);
								}
							} else {
								// Decrease the probability of this route
								probabilityOfRoute.addValue(0);
							}
						}

						// Salva o ponto projetado
						if ( minProjectedLocation != null ) {
							// Calcula a data do sample
							Date date = new Date(newData.getTimestamp());
							
							LineString lsRoute = (LineString) minRoute.getGeometry() ;
							
							double metersTraveled = minRoute.distanceInMetersUntil(minNewDataLocation) ;
							double percentTraveled = metersTraveled / minRoute.geometryLengthInMeters() ;

						    // ensure segment index is valid
							int segmentIndex = minNewDataLocation.getSegmentIndex() ;
						    int segIndex = segmentIndex;
						    if (segmentIndex >= lsRoute.getNumPoints() - 1)
						      segIndex = lsRoute.getNumPoints() - 2;

						    Coordinate edgeFrom = lsRoute.getCoordinateN(segIndex);
						    Coordinate edgeTo = lsRoute.getCoordinateN(segIndex + 1);
						    
							// Info between last and new sample
							double distanceTraveled = Util.distanceInMetersBetween(minLastDataLocation, minNewDataLocation, lsRoute, projectionType.getCoordinateReferenceSystem());
							int timeInSeconds = (int)( (newData.getTimestamp()-lastData.getTimestamp())/1000);
							double avgSpeedInKmPerHour = distanceTraveled * 3.6  / timeInSeconds ;
							
							Coordinate [] lsCoordinates = new Coordinate[] { newData.getCoordinate(), minProjectedLocation } ;
							// Distance to route in meters
							double distanceInMeters = JTS.orthodromicDistance(lsCoordinates[0], lsCoordinates[1], projectionType.getCoordinateReferenceSystem());

							// Compute last route id
							lastRouteIdByOrderIdForThread.get().put(newData.getOrder(), minRoute.getShapeId()) ;

			                if ( SAVE_TO_POSTGIS ) {
			                	SimpleFeatureBuilder featureBuilder = featureBuilderForThread.get() ;
			                	
			                	String edgeId = Util.edgeIdForEdge(edgeFrom, edgeTo) ;
			                	
			                	LineString ls = geometryFactory.get().createLineString(lsCoordinates) ;
			                	featureBuilder.add(ls);							// the_geom
			                	featureBuilder.add(minDistance);				// distance
			                	featureBuilder.add(distanceInMeters);			// distance_in_meters
			                	
			                	featureBuilder.add(lineNumber);					// line_number
			                	featureBuilder.add(newData.getOrder());			// order_number
			                	featureBuilder.add(minRoute.getShapeId());		// shape_id
								featureBuilder.add(lastRouteId);		// shape_id
			                	featureBuilder.add(edgeId);						// edge_id
			                	featureBuilder.add(date);						// date
			                	featureBuilder.add(newData.getSpeed());			// instant_speed
			                	
			                	featureBuilder.add(distanceTraveled);			// diff_distance
			                	featureBuilder.add(timeInSeconds);				// diff_seconds
			                	featureBuilder.add(avgSpeedInKmPerHour);		// diff_mean_speed
			                	
			                	SimpleFeature lsProjection = featureBuilder.buildFeature(null);
			                	projectedFeatures.add(lsProjection) ;
			                }
		                	
		                	// Save to file
//			                if( SAVE_TO_FILE ) {
//			                	DataOutputStream projectionsOut = getOutputForDate(date) ;
//
//			                	synchronized(projectionsOut) {
//		                			// Timestamp
//		                			projectionsOut.writeLong(newData.getTimestamp());
//
//		                			// Identificador do onibus (ordem)
//		                			projectionsOut.writeInt(newData.getOrderId());
//
//		                			// Coordenadas originais
//		                			projectionsOut.writeDouble(newData.getCoordinate().y);	// latitude
//		                			projectionsOut.writeDouble(newData.getCoordinate().x);	// longitude
//
//		                			// Velocidade instantanea
//		                			projectionsOut.writeDouble(newData.getSpeed());
//
//		                			// Coordenadas projetadas
//		                			projectionsOut.writeDouble(minProjectedLocation.y);		// latitude
//		                			projectionsOut.writeDouble(minProjectedLocation.x);		// longitude
//
//		                			// Distancia da projecao (m)
//		                			projectionsOut.writeDouble(distanceInMeters);
//
//		                			// Id da linha e rota
//		                			projectionsOut.writeInt(newData.getLineNumberId());
//		                			projectionsOut.writeInt(minRoute.getShapeId()) ;
//
//		                			// Localização linear dentro do trajeto (antes e depois)
//		                			projectionsOut.writeInt(minLastDataLocation.getSegmentIndex());
//									projectionsOut.writeInt(minNewDataLocation.getSegmentIndex());
//									projectionsOut.writeDouble(minLastDataLocation.getSegmentFraction());
//		                			projectionsOut.writeDouble(minNewDataLocation.getSegmentFraction());
//
//		                			// Dados em relacao ao trecho percorrido
//		                			projectionsOut.writeDouble(distanceTraveled) ;
//		                			projectionsOut.writeInt(timeInSeconds) ;
//		                			projectionsOut.writeDouble(avgSpeedInKmPerHour) ;
//
//		                			// Dados relativos ao trajeto ja percorrido
//		                			projectionsOut.writeDouble(metersTraveled);
//		                			projectionsOut.writeDouble(percentTraveled);
//			                	}
//			                }
						}

					} else {
//							System.out.printf("Line route not found for line number %s\n", lineNumber);
						countIndexNotFound.incrementAndGet() ;
					}
				} catch (Exception e) {
					e.printStackTrace();
					throw new RuntimeException(e) ;
				}
			}

			@Override
			public void endSequence(SampleData lastData) {} // Does nothing
        	
        });
        
        closeAllOutputs();
        
        if ( transaction != null ) {
        	transaction.close();
        }
        
        // Print filter stats
        DataFilter filterToPrint = filter ;
        System.out.println("Filter stats:");
        while (filterToPrint != null) {
        	System.out.printf(" Filter %s processed %d samples and filtered %d (%.2f%%)\n", filterToPrint.getClass().getName(),
        			filterToPrint.getTotalSamplesProcessed(), filterToPrint.getTotalSamplesFiltered(),
        			filterToPrint.getTotalSamplesFiltered() * 100.0 / filterToPrint.getTotalSamplesProcessed());
        	
        	filterToPrint = filterToPrint.getNextFilter() ;
        }
	}
	
	private static Map<String, DataOutputStream> outputForDate = new HashMap<String, DataOutputStream>() ;
	private static SimpleDateFormat outFileFormat = new SimpleDateFormat("yyyyMMdd");
	private static DataOutputStream getOutputForDate(Date date) throws IOException
	{
		String strDate = outFileFormat.format(date) ;
//		synchronized(lastTimestampReferenceForDate) {
//			lastTimestampReferenceForDate.put(strDate, System.currentTimeMillis()) ;
//		}
		
		DataOutputStream out = outputForDate.get(strDate) ;
		if ( out == null ) {
			synchronized(outputForDate) {
				// Try again in a synchronized scope
				out = outputForDate.get(strDate) ;
				if ( out == null )
				{
					File logFolder = new File(Util.getEnv("LOG_FOLDER_OUT")) ;
					File outFile = new File(logFolder, strDate + "-projected.dat") ;
					FileOutputStream fileOutStream = new FileOutputStream(outFile) ;
					BufferedOutputStream bufferOutStream = new BufferedOutputStream(fileOutStream, 20*1024*1024) ;
					out = new DataOutputStream(bufferOutStream) ;
					
					outputForDate.put(strDate, out);
					
					// Close older files opened...
					while ( outputForDate.size() > 20 )
					{
						String strToClose = outputForDate.keySet().stream().min((a,b) -> a.compareTo(b)).get() ;
						System.out.println("Closing file " + strToClose);
						DataOutputStream outToClose = outputForDate.get(strToClose) ;
						if ( outToClose != null ) {
							outToClose.flush();
							outToClose.close();
						} else {
							System.err.printf("Trying to close an file not created... %s\n", strToClose);
						}
						outputForDate.remove(strToClose) ;
					}
				}				
			}
		}
		
		return out ;
	}
	
	private static void closeAllOutputs()
	{
		outputForDate.values().forEach((out) -> {
			try {
				out.close() ;
			} catch (Exception e) {
				e.printStackTrace();
			}	
		});
	}
	
    private static SimpleFeatureType createProjectionFeatureType() {
        SimpleFeatureTypeBuilder builder = new SimpleFeatureTypeBuilder();
        builder.setName("projections_optimized_prob");
        builder.setCRS(DefaultGeographicCRS.WGS84); // <- Coordinate reference system

        // add attributes in order
        builder.add("the_geom", LineString.class);
        builder.add("distance", Double.class);
        builder.add("distance_in_meters", Double.class);
        builder.add("line_number", String.class);
        builder.add("order_number", String.class);
        builder.add("shape_id", Integer.class);
		builder.add("last_shape_id", Integer.class);
        builder.add("edge_id", String.class);
        builder.add("date", Date.class);
        builder.add("instant_speed", Double.class);

        builder.add("diff_distance", Double.class);
        builder.add("diff_seconds", Integer.class);
        builder.add("diff_mean_speed", Double.class);

        // build the type
        return builder.buildFeatureType();
    }

}
