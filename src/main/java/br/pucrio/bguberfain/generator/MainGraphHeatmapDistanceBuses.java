package br.pucrio.bguberfain.generator;

import br.pucrio.bguberfain.onibus.gui.XYGraph;
import br.pucrio.bguberfain.onibus.model.MapAverage;
import br.pucrio.bguberfain.onibus.model.graph.v2.*;
import br.pucrio.bguberfain.onibus.reader.GZipSampleDataReader;
import br.pucrio.bguberfain.onibus.reader.sequence.SequenceGenerator;
import br.pucrio.bguberfain.onibus.reader.sequence.SequenceReader;
import br.pucrio.bguberfain.onibus.reader.sequence.SimpleSequenceGenerator;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.SequenceBreaker;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.sampledata.BackwardTimeBreaker;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.sampledata.ChangeDayBreaker;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.sampledata.ChangeLineNumberBreaker;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.sampledata.LeastIntervalBreaker;
import br.pucrio.bguberfain.onibus.reader.sequence.filter.DataFilter;
import br.pucrio.bguberfain.onibus.reader.sequence.filter.sampledata.DaylightSavingTimeFixFilter;
import br.pucrio.bguberfain.onibus.reader.sequence.filter.sampledata.FixedLineNumberFilter;
import br.pucrio.bguberfain.onibus.reader.sequence.filter.sampledata.LineNumberFilter;
import br.pucrio.bguberfain.onibus.reader.sequence.filter.sampledata.WorkdayFilter;
import br.pucrio.bguberfain.onibus.reader.transformer.SampleDataSequence2ProjectionSequence;
import br.pucrio.bguberfain.onibus.util.AutoInstanceMap;
import br.pucrio.bguberfain.onibus.util.NetworkTopologyDS;
import br.pucrio.bguberfain.onibus.util.Util;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Bruno on 25/01/2015.
 */
public class MainGraphHeatmapDistanceBuses extends MainGraphBase {
    private static final Logger logger = LogManager.getLogger(MainGraphHeatmapDistanceBuses.class);

    /*
    Max shapes after 8740001 samples:
        17376210, line 638 has 132573 entries
        17378017, line 371 has 113376 entries
        17613062, line 371 has 100290 entries
        17389243, line 397 has 93517 entries  - 397-CAMPO GRANDE X CARIOCA
        17726374, line 624 has 90469 entries
        17541675, line 638 has 86068 entries
        17339354, line 393 has 85308 entries
        17416262, line 607 has 80032 entries
        17378253, line 753 has 75054 entries
        17362666, line 607 has 74823 entries

    - Linha: 2336-CAMPO GRANDE X CASTELO (VIA AV. BRASIL)
        - Sentido Campo Grande: 17594567 (ou 17375751)
        - Sentido Centro: 17761930 (ou 17556438)
    */

    private final static long MIN_DISTANCE_FROM_BORDER = 50 ;
    private final static long MAX_INTERVAL_FROM_ANNOTATION = 10 * 60 * 1000 ;
    private final static double MAX_SPEED = 60 / 3.6 ;     // 100 km/h in ms/s
    private final static double MAX_DISTANCE_BETWEEN_SEQUENCES = MAX_SPEED * 2 * 60 ; // Distance with max speed in 2 minutes

    public static void main(String [] args) throws Exception {
        Stream<Integer> keysToLoad ;
        Map<Integer, Route> routesByShapeId = NetworkTopologyDS.getInstance().getNetworkByShapeId(null, true) ;

        String strShapeIdToLoad = Util.getEnv("SHAPE_TO_LOAD", false);
        if(strShapeIdToLoad != null) {
            keysToLoad = Arrays.stream(strShapeIdToLoad.split(",")).map(Integer::new) ;
        } else {
            keysToLoad = routesByShapeId.keySet().stream() ;
        }
        keysToLoad.sorted()
                .parallel()
                .forEach((shapeId) -> {
                    try {
                        Route route = routesByShapeId.get(shapeId) ;
                        String lineNumber = route.getLineNumber() ;
                        Collection<Route> routesForLineNumber = routesByShapeId.entrySet().stream()
                                .filter( item -> item.getValue().getLineNumber().equals(lineNumber) )
                                .map(Map.Entry::getValue)
                                .collect(Collectors.toList()) ;
                        MainGraphHeatmapDistanceBuses.generateGraph(route, routesForLineNumber, "2015.*\\.txt\\.gz", new LineNumberFilter(lineNumber), 3, null);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });
    }

    public static XYGraph generateGraph(Route route, Collection<Route> routesForLineNumber, String datePattern, DataFilter filter, int goodRate, SequenceGenerator<ProjectionInfo<SampleData>> projectionSequence) throws Exception {
        long start = System.currentTimeMillis() ;
        XYGraph xyGraph = MainGraphBase.createAndShowGUI("Distance heatmap Graph", XYGraph.XYGraphAxisType.METERS, XYGraph.XYGraphAxisType.SECONDS);
        String saveToFolder = Util.getEnv("SAVE_TO_FOLDER", false);

        final double routeLength = route.geometryLengthInMeters() ;
        String lineNumber = route.getLineNumber() ;

        logger.info("Considering line # {} for shape id {}", lineNumber, route.getShapeId());
        logger.info("MAX_DISTANCE_BETWEEN_SEQUENCES = {}", MAX_DISTANCE_BETWEEN_SEQUENCES) ;

        // Add bus-stops strips
        for (BusStop busStop : route.getBusStops()) {
            xyGraph.addVerticalMark(busStop.getDistanceFromBegin());
        }

        // Update graph boundary to full route length
        XYSample.XYSampleBoundary graphBoundary = new XYSample.XYSampleBoundary(0, 0, route.geometryLengthInMeters(), SECONDS_IN_A_DAY) ;
        xyGraph.setBoundary(graphBoundary);

        // Configure XY Graph
        xyGraph.setDrawAsDots(true);
        xyGraph.setBackgroundColor(Color.DARK_GRAY);
        xyGraph.setSequenceAlpha(255);
        xyGraph.setFontSize(20);

        // Configure legends
        int numLegends = 20 ;
        double goodDistance = routeLength / goodRate ;
        SortedMap<Float, String> legend = new TreeMap<>() ;
        String unitFormat ;
        int unit ;
        if (goodDistance / numLegends < 100) {
            unitFormat = "%.0fm" ;
            unit = 1 ;
        } else {
            unitFormat = "%.1fkm" ;
            unit = 1000 ;
        }
        for(int i=0; i<=numLegends; i++) {
            legend.put(i / (float) numLegends, String.format(unitFormat, i * goodDistance / numLegends / unit)) ;
        }
        xyGraph.setLegend(legend);
        xyGraph.setLegendForColorPercent(percent -> String.format(unitFormat, goodDistance * percent / unit));

        final Color[] heatColors = new Color[] {
                Color.WHITE,
                Color.RED,
                Color.YELLOW,
                Color.GREEN.darker(),
                Color.CYAN,
                Color.BLUE,
                new Color(0, 0, 0x33)} ;
        xyGraph.setColorProvider((float percent, int alpha) -> Util.gradientColor(heatColors, percent, alpha / 255.0f) );

        if(projectionSequence == null) {

            // Create Sample Reader...
            // Before BRS
            //        Iterator<SampleData> samples = new GZipSampleDataReader("2014((06[0-9][0-9])|(07[0-9][0-9])|(080[1-8]))\\.txt\\.gz");
            // After BRS
            //        Iterator<SampleData> samples = new GZipSampleDataReader("2014((08[1-3][0-9])|(09[0-9][0-9])|((100[0-9])))\\.txt\\.gz");
            //        GZipSampleDataReader samples = new GZipSampleDataReader();

            GZipSampleDataReader samples = new GZipSampleDataReader(datePattern);
            //        samples.setIncludeAnnotations(true);
            SequenceBreaker sequenceBreakers = new LeastIntervalBreaker(5 * 60 * 1000);    // Ignore samples that are 5min apart
            sequenceBreakers = new ChangeDayBreaker(sequenceBreakers);         // Break samples between 0h
            sequenceBreakers = new ChangeLineNumberBreaker(sequenceBreakers);    // Keep only same line number in the sequence
            sequenceBreakers = new BackwardTimeBreaker(sequenceBreakers);
            filter = new WorkdayFilter(filter);
            filter = new DaylightSavingTimeFixFilter(filter);

            SequenceGenerator<SampleData> samplesSequence = new SimpleSequenceGenerator<>(sequenceBreakers, filter, samples, SampleData::getOrder, SampleData::isAnnotation);

            // Convert Sample to Projection (snapping)
            projectionSequence = new SampleDataSequence2ProjectionSequence(routesForLineNumber, samplesSequence) ;
        }


        logger.info("Starting loading data...");
        Map<String, Double> positionByBus = new HashMap<>() ;
        Map<String, Long> lastSampleTimestamp = new HashMap<>() ;

        // Calculate heatmap "pixel" size
        float yResolutionInMinutes = 15 ;
        float yFactor = 24 * 60 / yResolutionInMinutes ;
        float graphHeight = 712+152 ;
        float graphWidth = 1696 ;
        double yReduceFactor = SECONDS_IN_A_DAY / yFactor ;
        xyGraph.setStrokeWidth(graphHeight / yFactor) ;
        double xReduceFactor = routeLength / (graphWidth / graphHeight * yFactor) ;

        // Create average map with +1 border on each side because we must include both boundaries
        MapAverage mapAverage = new MapAverage((int) (routeLength / xReduceFactor) + 1, (int) (SECONDS_IN_A_DAY / yReduceFactor) + 1) ;
        projectionSequence.readData(new SequenceReader<ProjectionInfo<SampleData>>() {
            AutoInstanceMap<String, XYSample> distancesByBus = new AutoInstanceMap<>(new HashMap<>(), LinkedList::new);
            long lastAnnotation = 0;
            long lastServerTime = 0 ;

            @Override
            public void beginAnnotation(ProjectionInfo<SampleData> annotation) {
                serverUpdate(annotation.getLocationData().getTimestamp()) ;
            }

            public void serverUpdate(long timestamp) {
                lastAnnotation = timestamp;
                if (!positionByBus.isEmpty()) {
                    // Remove old samples
                    Iterator<Map.Entry<String, Double>> iBusPositions = positionByBus.entrySet().iterator() ;
                    while(iBusPositions.hasNext()) {
                        Map.Entry<String, Double> entry = iBusPositions.next() ;
                        long entryTime = lastSampleTimestamp.get(entry.getKey()) ;
                        if (Math.abs(entryTime - lastAnnotation) > MAX_INTERVAL_FROM_ANNOTATION) {
                            iBusPositions.remove();
                            lastSampleTimestamp.remove(entry.getKey()) ;
                        }
                    }

                    // Create a sorted list of samples along the route
                    List<Map.Entry<String, Double>> sortedBusPosition = positionByBus.entrySet().stream()
                            .sorted(Comparator.comparing(Map.Entry::getValue))  // Sort by value
                            .collect(Collectors.toList());                      // Return as a list

                    int numPositions = sortedBusPosition.size();
                    for (int i = 1; i < numPositions; i++) {
                        Map.Entry<String, Double> lastBus = sortedBusPosition.get(i - 1);
                        Map.Entry<String, Double> currentBus = sortedBusPosition.get(i);
                        //Map.Entry<String, Double> nextBus = i < (numPositions - 1) ? sortedBusPosition.get(i + 1) : null;

                        double distanceBetweenCurrentAndLast;
                        //if (nextBus != null) {
                        //    distanceBetweenCurrentAndLast = ((currentBus.getValue() - lastBus.getValue()) + (nextBus.getValue() - currentBus.getValue())) / 2;
                        //} else {
                        distanceBetweenCurrentAndLast = currentBus.getValue() - lastBus.getValue();
                        //}

                        float color = Math.min((float) (distanceBetweenCurrentAndLast / goodDistance), 1.0f);

                        XYSample sequenceSample = new XYSample((currentBus.getValue() + lastBus.getValue())/2, timestamp % MS_IN_A_DAY / 1000, color);
                        mapAverage.add(sequenceSample.getX() / xReduceFactor, sequenceSample.getY() / yReduceFactor, color);


                        distancesByBus.putValue(currentBus.getKey(), sequenceSample);

                    }
                    //positionByBus.clear();
                }
            }

            int numSamples = 0 ;
            private SampleData updateServerTime(SampleData sample) {
                long serverTime = sample.getTimestamp() + sample.getDelay() * 1000 ;
                if (Math.abs(lastServerTime-serverTime) > 1000 * 30 ) {
                   // System.out.println("serverTime = " + serverTime + " with " + numSamples + " samples");
                    lastServerTime = serverTime ;

                    serverUpdate(serverTime) ;

                    numSamples = 0 ;
                }
                numSamples++ ;

                return sample ;
            }

            @Override
            public void beginSequence(ProjectionInfo<SampleData> newData) {
                updateServerTime(newData.getLocationData()) ;
            }

            @Override
            public void sequence(ProjectionInfo<SampleData> lastData, ProjectionInfo<SampleData> newData) {
                updateServerTime(newData.getLocationData()) ;
                SampleData lastSample = newData.getLocationData() ;
                if (Math.abs(lastSample.getTimestamp() - lastAnnotation) < MAX_INTERVAL_FROM_ANNOTATION) {
                    if (newData.getRoute().equals(route)) {
                        double distanceOnRoute = newData.distanceTraveledOnRoute();
                        if (distanceOnRoute > MIN_DISTANCE_FROM_BORDER) {
                            positionByBus.put(lastSample.getOrder(), distanceOnRoute);
                            lastSampleTimestamp.put(lastSample.getOrder(), lastSample.getTimestamp()) ;
                        }
                        if (distanceOnRoute > routeLength - MIN_DISTANCE_FROM_BORDER) {
                            endSequence(newData);
                        }
                    }
                }
            }

            @Override
            public void endSequence(ProjectionInfo<SampleData> lastData) {
                if (lastData.getRoute().equals(route)) {
                    String sampleOrder = lastData.getLocationData().getOrder();
                    positionByBus.remove(sampleOrder);
                    lastSampleTimestamp.remove(sampleOrder) ;

                    Deque<XYSample> samples = (Deque<XYSample>) distancesByBus.get(sampleOrder);

                    if (samples != null && samples.size() >= 4) {
                        xyGraph.addSampleSequence(samples);
                    }

                    distancesByBus.remove(sampleOrder);
                }
            }
        });

        LinkedList<XYSample> xySamples = new LinkedList<>() ;

        for(int x=0; x<mapAverage.getWidth(); x++) {
            for(int y=0; y<mapAverage.getHeight(); y++) {
                if (mapAverage.getCount(x, y) > 0) {
                    double result = mapAverage.getResult(x, y);
                    xySamples.add(new XYSample(x * xReduceFactor, y * yReduceFactor, (float) result));
                }
            }
        }

        // Update graph with heatmap average data
        xyGraph.clean();
        xyGraph.addSampleSequence(xySamples);

        long end = System.currentTimeMillis();

        logger.info("Finished loading all data in {} s", (end - start) / 1000);

        // Save file
        if (saveToFolder != null) {
            String fileBase = String.format("distance-heatmap-%s-%d", route.getLineNumber(), route.getShapeId()) ;

            xyGraph.saveData(new File(saveToFolder, fileBase + ".xyd"));
            xyGraph.saveImage(new File(saveToFolder, fileBase + ".png"), 1);

            JFrame frame = (JFrame) SwingUtilities.getRoot(xyGraph);
            frame.setVisible(false);
            frame.dispose();
        }

        return xyGraph ;
    }
}
