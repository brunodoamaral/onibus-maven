package br.pucrio.bguberfain.generator;

import br.pucrio.bguberfain.onibus.model.graph.v2.SampleData;
import br.pucrio.bguberfain.onibus.reader.GZipSampleDataReader;
import br.pucrio.bguberfain.onibus.reader.sequence.filter.DataFilter;
import br.pucrio.bguberfain.onibus.reader.sequence.filter.sampledata.DaylightSavingTimeFixFilter;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by Bruno on 01/03/2015.
 */
public class MainSamplesDiff {

    private static final int MS_IN_A_MINUTE = 1000 * 60 ;
    private static final int TRUNCATE_MS_VALUE = MS_IN_A_MINUTE / 2 ;      // Break every 30s
    private static final int MAX_INTERVAL = MS_IN_A_MINUTE * 60 ;          // 1h

    public static void main(String [] args)
    {
        // Check...
        if(MAX_INTERVAL % TRUNCATE_MS_VALUE != 0) {
            throw new RuntimeException("MAX_INTERVAL must divide TRUNCATE_MS_VALUE") ;
        }
        final int numSlots = (MAX_INTERVAL / TRUNCATE_MS_VALUE) ;
        final long []slots = new long[numSlots+1] ;

        Iterator<SampleData> iSamples = new GZipSampleDataReader();

        DataFilter<SampleData> filter = new DaylightSavingTimeFixFilter() ;
        Map<String, SampleData> lastSampleByOrder = new HashMap<>(20000) ;

        long count = 0 ;
        while (iSamples.hasNext()) {
            SampleData sample = filter.filter(iSamples.next()) ;
            SampleData lastSample = lastSampleByOrder.get(sample.getOrder()) ;

            if (lastSample != null) {
                long diff = sample.getTimestamp() - lastSample.getTimestamp() ;

                int slot = (int)(diff / TRUNCATE_MS_VALUE) ;
                if (slot > numSlots || slot < 0) {
                    slot = numSlots ;
                }
                slots[slot]++ ;
            }

            lastSampleByOrder.put(sample.getOrder(), sample) ;

            if (count++ % 9999999 == 0) {
                printSlots(slots) ;
            }
        }
    }

    private static void printSlots(long[] slots) {
        System.out.println("Status:");
        for (int i = 0; i < slots.length; i++) {
            int startSeconds = i * TRUNCATE_MS_VALUE / 1000 ;
            int endtSeconds = startSeconds + TRUNCATE_MS_VALUE / 1000 ;
            System.out.printf("%5d - %5d = %8d\n", startSeconds, endtSeconds, slots[i]);
        }
    }

}
