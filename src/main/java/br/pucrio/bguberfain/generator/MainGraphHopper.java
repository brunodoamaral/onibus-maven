package br.pucrio.bguberfain.generator;

import com.graphhopper.GHRequest;
import com.graphhopper.GHResponse;
import com.graphhopper.GraphHopper;
import com.graphhopper.routing.util.EncodingManager;
import com.graphhopper.util.PointList;

/**
 * Created by Bruno on 15/03/2015.
 */
public class MainGraphHopper {
    public static void main(String[] args) {
        GraphHopper hopper = new GraphHopper().forDesktop()
                .setOSMFile("H:\\onibus-data\\osm\\rio-de-janeiro_brazil.osm.bz2")
                .setGraphHopperLocation("H:\\onibus-data\\osm\\GraphHopper")
                .setEncodingManager(new EncodingManager("car"));

        // now this can take minutes if it imports or a few seconds for loading
        // of course this is dependent on the area you import
        System.out.println("Loading...");
        hopper.importOrLoad();

        // simple configuration of the request object, see the GraphHopperServlet classs for more possibilities.
        GHRequest req = new GHRequest(-22.958025066546313, -43.199355413214256,  -22.97845156811955, -43.225135696286976).
                setWeighting("fastest").
                setVehicle("car");

        System.out.println("Routing...");
        GHResponse rsp = hopper.route(req);

        // first check for errors
        if (rsp.hasErrors()) {
            // handle them!
            // rsp.getErrors()
            return;
        }

// route was found? e.g. if disconnected areas (like island)
// no route can ever be found
//        if (!rsp.isFound()) {
//            // handle properly
//            return;
//        }

        // points, distance in meters and time in millis of the full path
        PointList pointList = rsp.getPoints();
        double distance = rsp.getDistance();
        System.out.printf("distance = %.2f\n", distance);

        System.out.println(pointList);
    }
}
