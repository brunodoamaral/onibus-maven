package br.pucrio.bguberfain.generator;

import br.pucrio.bguberfain.onibus.gui.XYGraph;
import br.pucrio.bguberfain.onibus.model.graph.v2.ProjectionInfo;
import br.pucrio.bguberfain.onibus.model.graph.v2.Route;
import br.pucrio.bguberfain.onibus.model.graph.v2.SampleData;
import br.pucrio.bguberfain.onibus.reader.GZipSampleDataReader;
import br.pucrio.bguberfain.onibus.reader.sequence.SequenceGenerator;
import br.pucrio.bguberfain.onibus.reader.sequence.SequenceReader;
import br.pucrio.bguberfain.onibus.reader.sequence.SimpleSequenceGenerator;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.SequenceBreaker;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.sampledata.BackwardTimeBreaker;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.sampledata.ChangeDayBreaker;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.sampledata.ChangeLineNumberBreaker;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.sampledata.LeastIntervalBreaker;
import br.pucrio.bguberfain.onibus.reader.sequence.filter.DataFilter;
import br.pucrio.bguberfain.onibus.reader.sequence.filter.sampledata.DaylightSavingTimeFixFilter;
import br.pucrio.bguberfain.onibus.reader.sequence.filter.sampledata.FixedLineNumberFilter;
import br.pucrio.bguberfain.onibus.reader.sequence.filter.sampledata.WorkdayFilter;
import br.pucrio.bguberfain.onibus.reader.transformer.SampleDataSequence2ProjectionSequence;
import br.pucrio.bguberfain.onibus.util.Util;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.PrecisionModel;
import com.vividsolutions.jts.io.WKTReader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.geotools.referencing.crs.DefaultGeographicCRS;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.*;

/**
 * Created by Bruno on 22/04/2015.
 */
public class MainBRSBotafogoFast {
    private static final Logger logger = LogManager.getLogger(MainBRSBotafogoFast.class);

    // Source: http://www.brsrio.com.br/zona-sul/brs-botafogo-sao-clemente/
    private static final Set<String> linesNumbersSaoClemente = new HashSet<>(Arrays.asList("107", "119", "126", "129", "136", "143", "154", "155", "157",
            "161", "162", "170", "172", "173", "178", "190", "2014", "131", "409", "410", "416", "420", "423", "425", "426", "432",
            "434", "435", "438", "439", "440", "444*", "445*", "463", "464", "472", "511", "512", "538", "570", "573", "574", "583",
            "584", "309", "316", "317", "354", "404", "456", "457", "459", "483", "485", "486", "548", "957*", "1750D", "1775D",
            "2740D", "2750D", "740D", "741D", "750D", "751D", "775D"));

    private static final Set<String> linesNumbersVoluntarios = new HashSet<>(Arrays.asList("107", "119", "126", "129", "136", "143", "154", "155", "157",
            "161", "162", "170", "172", "173", "178", "190", "2014", "131", "409", "410", "416", "420", "421", "423", "425", "426",
            "432", "433", "434", "435", "438", "439", "440", "444", "445", "463", "464", "472", "512", "513", "538", "569", "570",
            "573", "574", "583", "584", "2018", "309", "316", "317", "354", "402", "404", "456", "457", "459", "483", "485", "486",
            "548", "957", "1750D", "1775D", "2740D", "2750D", "3721D", "721D", "740D", "741D", "750D", "751D", "775D"));

    private static final String brsSaoClementeCoordinates = "LINESTRING ( -43.1787401 -22.9411123, -43.1795888 -22.9415436, -43.1801192 -22.9418956, -43.1803676 -22.9420786, " +
            "-43.1804167 -22.942112, -43.180534 -22.9421903, -43.18095 -22.9425175, -43.1809991 -22.9425687, -43.1813642 -22.9430065, -43.1815578 -22.9432622, -43.1818625 -22.9438104, " +
            "-43.182064 -22.9442814, -43.1823513 -22.9450697, -43.1824691 -22.9455754, -43.1825041 -22.9458023, -43.1825153 -22.945852, -43.1825371 -22.9460129, -43.1825555 -22.9463529, " +
            "-43.1825575 -22.946662, -43.1825437 -22.9469111, -43.1824864 -22.947503, -43.1824817 -22.9475784, -43.1824629 -22.9477187, -43.1824251 -22.9479632, -43.1823982 -22.9480875, " +
            "-43.182357 -22.9484234, -43.182285 -22.948666, -43.1822717 -22.948774, -43.1822817 -22.9488531, -43.1823053 -22.9489289, -43.182348 -22.9489886, -43.1824008 -22.9490434, " +
            "-43.1824896 -22.949096, -43.1825732 -22.9491213, -43.1827112 -22.9491447, -43.1839 -22.9492567, -43.1841573 -22.949289, -43.184261 -22.949302, -43.1848898 -22.9493611, " +
            "-43.1854122 -22.9494112, -43.1856459 -22.9494345, -43.1856862 -22.9494385, -43.1857639 -22.9494412, -43.1862124 -22.9494627, -43.1862397 -22.949464, -43.1866279 -22.9494818, " +
            "-43.187003 -22.949499, -43.1873594 -22.9495154, -43.187753 -22.9495626, -43.1879179 -22.9495663, -43.188269 -22.9495869, -43.1888615 -22.9496497, -43.1891746 -22.9496824, " +
            "-43.189694 -22.9497367, -43.1902612 -22.9498627, -43.190385 -22.9498902, -43.1910546 -22.9500461, -43.191958 -22.950278, -43.19203 -22.9503026, -43.1922397 -22.9503605, " +
            "-43.1926198 -22.9504674, -43.1927866 -22.9505143, -43.1928793 -22.9505316, -43.1933404 -22.9507431, -43.1936304 -22.9509031, -43.1938536 -22.9510211, -43.1941812 -22.9511704, " +
            "-43.1944666 -22.9513054, -43.1945383 -22.9513393, -43.1951016 -22.9516057, -43.1957408 -22.9520771, -43.196297 -22.9525112, -43.1963713 -22.9525757, -43.1964079 -22.952624, " +
            "-43.1964479 -22.9526972, -43.1965081 -22.9528027, -43.1965499 -22.9528811, -43.1969194 -22.9536048, -43.1969806 -22.9537203, -43.1971163 -22.953994, -43.1973441 -22.9544558, " +
            "-43.1973946 -22.9545478, -43.1974247 -22.9545899, -43.1975124 -22.954726, -43.1977534 -22.9550892, -43.1978639 -22.9552476, -43.197984 -22.9554637, -43.1981941 -22.9559138, " +
            "-43.1983403 -22.9562191, -43.1985596 -22.9565892, -43.1986529 -22.9567648, -43.1987842 -22.9569834, -43.1989815 -22.9572589, -43.1990925 -22.9574203, -43.1991877 -22.9575587, " +
            "-43.1992581 -22.9576552, -43.1993209 -22.9577426, -43.1996351 -22.9581912, -43.2000101 -22.9586621, -43.2001181 -22.9587704, -43.2002608 -22.9588669, -43.2005939 -22.9590294, " +
            "-43.2023399 -22.9598811, -43.2026022 -22.9599783 )" ;
    private static final String brsVoluntariosCoordinates = "LINESTRING ( -43.2029636 -22.9602723, -43.2027893 -22.9602395, -43.2025179 -22.9601539, -43.2021014 -22.9599894, -43.2020004 -22.959948, " +
            "-43.2008735 -22.9594169, -43.2005601 -22.9592685, -43.2003035 -22.9591335, -43.2002099 -22.9590747, -43.200136 -22.9590283, -43.2000615 -22.9589668, -43.1992911 -22.9579185, " +
            "-43.1991475 -22.95775, -43.1990925 -22.9576854, -43.1990538 -22.957638, -43.1989156 -22.957469, -43.1985172 -22.9568152, -43.1983245 -22.9565063, -43.1980799 -22.9562745, " +
            "-43.19723 -22.9559884, -43.1967078 -22.9557955, -43.1966629 -22.9557789, -43.1965739 -22.9557475, -43.1962156 -22.9556296, -43.1952466 -22.9552768, -43.1945964 -22.9550441, " +
            "-43.194033 -22.9548348, -43.1929911 -22.9544685, -43.1929529 -22.9544546, -43.1928769 -22.9544284, -43.1927735 -22.9543904, -43.1927091 -22.9543657, -43.192264 -22.9542125, " +
            "-43.1914309 -22.9539148, -43.1909083 -22.9537329, -43.1906529 -22.9536414, -43.1898678 -22.9533484, -43.1897842 -22.9533172, -43.1887602 -22.9529923, -43.1887016 -22.9529737, " +
            "-43.1886107 -22.9529449, -43.1877323 -22.9526216, -43.1875043 -22.9525531, -43.187159 -22.9524253, -43.1871078 -22.9524063, -43.1870282 -22.9523768, -43.1866926 -22.9522542, " +
            "-43.1866436 -22.9522362, -43.1865983 -22.9522196, -43.1844298 -22.9514223, -43.1843911 -22.9514088, -43.1843606 -22.9513985, -43.1842446 -22.9513586, -43.184116 -22.9513144, " +
            "-43.1833284 -22.9510451, -43.1829288 -22.9509085, -43.1825197 -22.9507686, -43.1822699 -22.9506832, -43.1818571 -22.950542, -43.1818019 -22.9505235, -43.181755 -22.950505, " +
            "-43.1816573 -22.9504652, -43.1816064 -22.9504437, -43.1815082 -22.9503585, -43.1814603 -22.9502938, -43.1814142 -22.9501871, -43.1813888 -22.9500194, -43.1813954 -22.9499396, " +
            "-43.1814187 -22.9498522, -43.1814752 -22.9497022, -43.1816491 -22.9493104, -43.181726 -22.9491156, -43.1818663 -22.9487524, -43.1819541 -22.948498, -43.1820552 -22.9481574, " +
            "-43.1821318 -22.9478654, -43.1821405 -22.9477881, -43.1821441 -22.9477489, -43.1821489 -22.9477128, -43.1821678 -22.9472008, -43.182171 -22.9469458, -43.1821336 -22.9461605, " +
            "-43.1821302 -22.9461038, -43.182123 -22.9460126, -43.1820495 -22.9456091, -43.1819941 -22.9454271, -43.1816528 -22.9445582, -43.1815094 -22.9442494, -43.1813438 -22.9439525, " +
            "-43.1812174 -22.9437452, -43.180941 -22.9433679, -43.1807399 -22.9431118, -43.1804548 -22.942806, -43.1802928 -22.9426476 )" ;

    static final File outFolder = new File(Util.getEnv("SAVE_TO_DIR")) ;

    public static void main(String [] args) throws Exception {

        GeometryFactory geometryFactory = new GeometryFactory(new PrecisionModel(), 4326) ;
        WKTReader wktReader = new WKTReader(geometryFactory) ;
        LineString lsSaoClemente = (LineString) wktReader.read(brsSaoClementeCoordinates) ;
        LineString lsVoluntarios = (LineString) wktReader.read(brsVoluntariosCoordinates) ;

        Route brsSaoClemente = new Route(lsSaoClemente, "BRS-Botafogo", 1001, DefaultGeographicCRS.WGS84) ;
        Route brsVoluntarios = new Route(lsVoluntarios, "BRS-Botafogo", 1002, DefaultGeographicCRS.WGS84) ;

//        if (true) {
//            MainGraphHeatmapSpeed.generateGraph(brsSaoClemente, Arrays.asList(brsSaoClemente, brsVoluntarios), "2015.*\\.txt\\.gz", new FixedLineNumberFilter("BRS-Botafogo"), 10, null) ;
//
//            return ;
//        }

//        String fileBeforeBRS = "2014((06[0-9][0-9])|(07[0-9][0-9])|(080[1-8]))\\.txt\\.gz" ;
//        String fileAfterBRS = "2014((08[1-3][0-9])|(09[0-9][0-9])|((100[0-9])))\\.txt\\.gz" ;

        LocalDate currDate = LocalDate.of(2014, 06, 01) ;

        LocalDate now = LocalDate.now() ;

        String [] monthsToLoad = new String[] {"201501", "201504"} ;

        for (String fileSuffix : monthsToLoad) {

            String filePattern = fileSuffix + ".*\\.txt\\.gz" ;

            SequenceGenerator<ProjectionInfo<SampleData>> sequencer = generatorForDate(filePattern, Arrays.asList(brsSaoClemente, brsVoluntarios));

            // Space x Speed
            // -> Before
            runAsThread(() -> saveXYGraph(MainGraphAverageSpeed.generateGraph(brsSaoClemente, Arrays.asList(brsSaoClemente, brsVoluntarios), filePattern, new FixedLineNumberFilter("BRS-Botafogo"), sequencer), "space-speed-brs-sao-clemente-" + fileSuffix));

            // Space x Time
            // -> Before
            runAsThread(() -> saveXYGraph(MainGraphTimeSpace.generateGraph(brsSaoClemente, Arrays.asList(brsSaoClemente, brsVoluntarios), filePattern, new FixedLineNumberFilter("BRS-Botafogo"), sequencer), "space-time-brs-sao-clemente-" + fileSuffix));

            // -> Before
            //runAsThread(() -> saveXYGraph(MainGraphHeatmapDistanceBuses.generateGraph(brsSaoClemente, Arrays.asList(brsSaoClemente, brsVoluntarios), filePattern, new FixedLineNumberFilter("BRS-Botafogo"), 10, sequencer), "heatmap-brs-sao-clemente-" + fileSuffix));
        }

        logger.info("Finished");
    }

    private static SequenceGenerator<ProjectionInfo<SampleData>> generatorForDate(String datePattern, Collection<Route> routesForLineNumber) {
        // Create Sample Reader...
        GZipSampleDataReader samples = new GZipSampleDataReader(datePattern);
        //        samples.setIncludeAnnotations(true);
        SequenceBreaker sequenceBreakers = new LeastIntervalBreaker(5 * 60 * 1000);    // Ignore samples that are 5min apart
        sequenceBreakers = new ChangeDayBreaker(sequenceBreakers);         // Break samples between 0h
        sequenceBreakers = new ChangeLineNumberBreaker(sequenceBreakers);    // Keep only same line number in the sequence
        sequenceBreakers = new BackwardTimeBreaker(sequenceBreakers);
        DataFilter filter = new FixedLineNumberFilter("BRS-Botafogo") ;
        filter = new WorkdayFilter(filter);
        filter = new DaylightSavingTimeFixFilter(filter);

        SequenceGenerator<SampleData> samplesSequence = new SimpleSequenceGenerator<>(sequenceBreakers, filter, samples, SampleData::getOrder, SampleData::isAnnotation);
        final SequenceGenerator<ProjectionInfo<SampleData>> realSource = new SampleDataSequence2ProjectionSequence(routesForLineNumber, samplesSequence) ;
        final Collection<SequenceReader<ProjectionInfo<SampleData>>> readers = new LinkedList<>() ;
        final Object lock = new Object() ;

        new Thread(() -> {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            realSource.readData(new SequenceReader<ProjectionInfo<SampleData>>() {
                @Override
                public void beginSequence(ProjectionInfo<SampleData> firstData) {
                    readers.forEach(r -> r.beginSequence(firstData));
                }

                @Override
                public void sequence(ProjectionInfo<SampleData> lastData, ProjectionInfo<SampleData> newData) {
                    readers.forEach(r -> r.sequence(lastData, newData));
                }

                @Override
                public void endSequence(ProjectionInfo<SampleData> lastData) {
                    readers.forEach(r -> r.endSequence(lastData));
                }
            });

            synchronized (lock) {
                lock.notifyAll();
            }
        }).start() ;

        // Convert Sample to Projection (snapping)
        return new SequenceGenerator<ProjectionInfo<SampleData>>() {
            @Override
            public void readData(SequenceReader<ProjectionInfo<SampleData>> sequenceReader) {
                readers.add(sequenceReader) ;
                synchronized (lock) {
                    try {
                        lock.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void stop() {

            }
        };
    }

    private interface ThrowableRunnable {
        void run() throws Exception ;
    }

    private static void runAsThread(ThrowableRunnable task) {
        new Thread(() -> {
            try {
                task.run() ;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).start();
    }

    private static void saveXYGraph(XYGraph xyGraph, String fileBase) throws IOException {
        xyGraph.saveImage(new File(outFolder, fileBase + ".png"), 1);
        xyGraph.saveData(new File(outFolder, fileBase + ".xyd"));

//        JFrame frame = (JFrame) SwingUtilities.getRoot(xyGraph);
//        frame.setVisible(false);
//        frame.dispose();
    }
}
