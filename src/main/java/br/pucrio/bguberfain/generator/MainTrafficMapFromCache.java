package br.pucrio.bguberfain.generator;

import br.pucrio.bguberfain.onibus.gui.GeoImage;
import br.pucrio.bguberfain.onibus.gui.Region;
import br.pucrio.bguberfain.onibus.model.EdgeStatsExtended;
import br.pucrio.bguberfain.onibus.model.TrafficDataPeriod;
import br.pucrio.bguberfain.onibus.model.graph.v2.*;
import br.pucrio.bguberfain.onibus.util.AutoInstanceMap;
import br.pucrio.bguberfain.onibus.util.Util;
import com.vividsolutions.jts.geom.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Bruno on 10/03/2015.
 */
public class MainTrafficMapFromCache {

    private static final Logger logger = LogManager.getLogger(MainTrafficMapFromCache.class);
    private final static float MAX_SPEED = 90 / 3.6f ;
    private final static float MIN_SPEED = 80 / 3.6f ;
    private final static float STROKE_WIDTH = 3.0f ;
    private final static File saveToFolder = new File(Util.getEnv("SAVE_TO_DIR")) ;

    private static final int MINUTES_IN_A_DAY = 24 * 60 ;

    // Main parameters
    private final static Envelope REGION = Region.ATERRO ;
    private static final TrafficDataPeriod PERIOD = TrafficDataPeriod.DAILY ;
    private static final int PRECISION_MINUTES = 30 ;
    private static final int NUM_TIME_SLICES = PERIOD.getMinutes() / PRECISION_MINUTES ;

    private final static int IMAGE_WIDTH = Region.imageWidthForZoomLevel(16, REGION) ;
    private final static AutoInstanceMap<LineSegment, LineSegment> segmentCorrectPosition = new AutoInstanceMap<>(new HashMap<>(), LinkedList::new) ;

    public static void main(String [] args) throws Exception {
        logger.info("Loading edges...");
        File graphFolder = new File(Util.getEnv("GRAPH_DAT_FOLDER")) ;
        Map<LineSegment, EdgeStatsExtended> edgeStats = EdgeStatsExtended.loadEdgeStatsByLineString(graphFolder, PERIOD, PRECISION_MINUTES) ;

        // Start loading...
        // Detect two-way edges
        int countReverse = 0 ;
        for (LineSegment segment : edgeStats.keySet()) {
            LineSegment reverseSegment = new LineSegment(segment.p1, segment.p0) ;

            if(edgeStats.containsKey(reverseSegment)) {
                countReverse++ ;

                // Move the segment to the "right" (-pi/2) by 0.0.00008 degrees
                double moveAngle = segment.angle() - Math.PI/2 ;
                double plusX = Math.cos(moveAngle) * 0.00008;
                double plusY = Math.sin(moveAngle) * 0.00008;

                Coordinate from = segment.p0 ;
                Coordinate to = segment.p1 ;

                LineSegment lsMoved = new LineSegment(from.x+plusX, from.y+plusY, to.x+plusX, to.y+plusY) ;

                segmentCorrectPosition.putValue(segment, lsMoved) ;
            } else {
                segmentCorrectPosition.putValue(segment, segment) ;
            }
        }
        logger.info("Found {} two-way segments", countReverse);

        // Expanded region
        Envelope filterRegion = new Envelope(REGION) ;
        filterRegion.expandBy(0.01);

        // Display image
        logger.info("Generating images...");
        long startImgs = System.currentTimeMillis() ;

        // Save max speed image
        GeoImage imgMax = new GeoImage(REGION, 2*IMAGE_WIDTH) ;
        drawMaxSpeedGraphOnImage(edgeStats, imgMax);
        saveImage(imgMax.getImage(), String.format("%s-%s", Region.regionName(REGION).toLowerCase(), "max-speed"));
        imgMax = null ;
        int numTimeSlicesPerDay = NUM_TIME_SLICES ;
        int timeSlice = (int) (18.5 * numTimeSlicesPerDay / 24);
//        int slices[] = new int[] { timeSlice, timeSlice + numTimeSlicesPerDay, timeSlice + 2*numTimeSlicesPerDay, timeSlice + 3*numTimeSlicesPerDay, timeSlice + 4*numTimeSlicesPerDay, timeSlice + 5*numTimeSlicesPerDay, timeSlice + 6*numTimeSlicesPerDay } ;
        int slices[] = new int[] { timeSlice } ;
        for (int i : slices) {
//        for( int i=0; i< NUM_TIME_SLICES; i++ ) {
            GeoImage img = new GeoImage(REGION, IMAGE_WIDTH) ;
            drawGraphOnImages(edgeStats, img, i) ;

            String strHour = hourForSegment(i) ;
            saveImage(img.getImage(), strHour);
        }
        logger.info("Images generated in {}s", (System.currentTimeMillis() -startImgs) / 1000);
    }

    private static void saveImage(BufferedImage bufferedImage, String title) {
        try {
            File outFile = new File(saveToFolder, "traffic-" + title + ".png") ;
            logger.info("Saving file {}", outFile.getPath());
            ImageIO.write(bufferedImage, "png", outFile) ;
        } catch (IOException e) {
            logger.error("Error while saving image", e);
        }

    }

    private static String hourForSegment(int segment) {
        String time = null ;

//        segment = (segment + 3 * 60 / PRECISION_MINUTES + NUM_TIME_SLICES) % NUM_TIME_SLICES ;

        int totalMinutes = segment * PRECISION_MINUTES ;
        if (PERIOD.equals(TrafficDataPeriod.WEEKLY)) {
            int dow = totalMinutes / MINUTES_IN_A_DAY ;
            int minutesInTheDay = totalMinutes % MINUTES_IN_A_DAY ;
            int hours = minutesInTheDay / 60 ;
            int minutes = minutesInTheDay % 60 ;

            time = String.format("%1d-%02dh%02dm", dow, hours, minutes) ;

        } else if (PERIOD.equals(TrafficDataPeriod.DAILY)) {
            int hours = totalMinutes / 60 ;
            int minutes = totalMinutes % 60 ;

            time = String.format("%02dh%02dm", hours, minutes) ;
        } else if (PERIOD.equals(TrafficDataPeriod.YEARLY) ) {
            if (PRECISION_MINUTES == TrafficDataPeriod.YEARLY.getMinutes() / 12) {
                // Monthly
                time = String.format("%02d", totalMinutes / PRECISION_MINUTES + 1);
            } else {
                // Weekly
                time = String.format("%02d", totalMinutes / PRECISION_MINUTES);
            }
        }
        return String.format("%s-%s", Region.regionName(REGION).toLowerCase(), time) ;
    }

    private static void drawGraphOnImages(Map<LineSegment, EdgeStatsExtended> graph, GeoImage img, int idx)
    {
        img.setColor(new Color(0xfaf7ef));
        img.clear();
        img.setStrokeWidth(STROKE_WIDTH);
        Color defaultColor = new Color(0xc7cfc4) ;
        for (Map.Entry<LineSegment, EdgeStatsExtended> edge : graph.entrySet()) {
            EdgeStatsExtended edgeStats = edge.getValue();
            Color color;
            if (edgeStats.numSamples[idx] > 10) {
                double currentSpeed = edgeStats.meanByTimeSlice[idx];
                double maxSpeed = edgeStats.percentile98;
                if (Double.isNaN(maxSpeed)) {
                    color = defaultColor ;
                } else {
                    double relativeSpeed = currentSpeed / maxSpeed;
                    if (relativeSpeed > 1) relativeSpeed = 1;
//                    float stroke = (float) ((STROKE_WIDTH - MIN_STROKE_WIDTH) * (maxSpeed - MIN_SPEED) / (MAX_SPEED - MIN_SPEED) + MIN_STROKE_WIDTH);
//                    if (stroke < MIN_STROKE_WIDTH) {
//                        stroke = MIN_STROKE_WIDTH;
//                    }
//                    img.setStrokeWidth(stroke);

//                    color = SpeedColors.gradientColorFor(relativeSpeed) ;
                    color = SpeedColors.speedForRelativeOrAbsolute(currentSpeed, relativeSpeed);
                }
            } else {
//                img.setStrokeWidth(MIN_STROKE_WIDTH);
                color = defaultColor ;
            }
            img.setColor(color);
            drawEdgeOnImage(edge.getKey(), img);
        }
    }

    private static void drawMaxSpeedGraphOnImage(Map<LineSegment, EdgeStatsExtended> graph, GeoImage img)
    {
        img.setColor(new Color(0xfaf7ef));
        img.clear();
        img.setStrokeWidth(STROKE_WIDTH);
        for (Map.Entry<LineSegment, EdgeStatsExtended> edge : graph.entrySet()) {
            EdgeStatsExtended edgeStats = edge.getValue();
            LineSegment lsEdge = edge.getKey() ;
            double relativeSpeed = (edgeStats.percentile98-MIN_SPEED) / (MAX_SPEED-MIN_SPEED);
            if (relativeSpeed <0) relativeSpeed = 0 ;
            else if (relativeSpeed>1) relativeSpeed = 1 ;
            relativeSpeed = 1-relativeSpeed ;

            if (Double.isNaN(relativeSpeed)) continue ;
            Color color = SpeedColors.speedForRelativeOrAbsolute(edgeStats.percentile98, relativeSpeed) ;
            img.setColor(color);
            drawEdgeOnImage(edge.getKey(), img);

            // Draw speed
            img.setColor(Color.DARK_GRAY);
            String strSpeed = String.format("%.0f", edgeStats.percentile98 * 3.6) ;
            img.drawStringRotated(strSpeed, 0, lsEdge.midPoint().y, lsEdge.midPoint().x);
        }
    }

    private static int colorFromRGB(float r, float g, float b) {
        return ((int) (0xff * b)) | (((int) (0xff * g)) << 8 ) | (((int) (0xff * r)) << 16 ) ;
    }

    private static void drawEdgeOnImage(LineSegment edge, GeoImage img)
    {
        for (LineSegment segment : segmentCorrectPosition.get(edge)) {
            img.drawLine(segment.p0, segment.p1) ;
        }
    }
}
