package br.pucrio.bguberfain.generator;

import br.pucrio.bguberfain.onibus.model.graph.v2.SampleDataExtended;
import br.pucrio.bguberfain.onibus.util.Util;

import java.io.*;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * Created by bguberfain on 5/4/2015.
 */
public class MainConvertToBinary {
    public static void main(String [] args) throws Exception {
        File logFolder = new File(Util.getEnv("LOG_FOLDER")) ;
        File logFiles[] = logFolder.listFiles(pathname -> pathname.getName().matches("\\d{8}\\.txt\\.gz"));

        for (File logFile : logFiles) {
            GZIPInputStream gzIn = new GZIPInputStream(new BufferedInputStream(new FileInputStream(logFile), 64 * 1024 * 1024));

            BufferedReader dataIn = new BufferedReader(new InputStreamReader(gzIn), 64 * 1024 * 1024);

            File outFile = new File(logFile.getPath().replace(".txt.gz", ".dat.gz")) ;
            DataOutputStream dataOut = new DataOutputStream(new BufferedOutputStream(new GZIPOutputStream(new FileOutputStream(outFile)), 64 * 1024 * 1024)) ;

            System.out.printf("Converting %s to %s...\n", logFile.getName(), outFile.getName());

            String line ;
            LineTokenizer lineToken = new LineTokenizer("") ;
            while( (line = dataIn.readLine()) != null) {
                // Ignore comment
                if (line.charAt(0) != '#') {
                    lineToken = LineTokenizer.recicleTokenizer(lineToken, line) ;

                    String dataHora = lineToken.nextToken() ;
                    String ordem = lineToken.nextToken() ;
                    String linha = lineToken.nextToken() ;
                    double latitude = Double.parseDouble(lineToken.nextToken()) ;
                    double longitude = Double.parseDouble(lineToken.nextToken()) ;
                    String strSpeed = lineToken.nextToken() ;
                    String strDelay = lineToken.nextToken() ;

                    // Linha
                    // Remove ".0" by the end of string
                    if ( linha.endsWith(".0") ) {
                        linha = linha.substring(0, linha.length()-2) ;
                    }

                    // Velocidade
                    float speed = fastParseSpeed(strSpeed) ;

                    // Atraso
                    int delay = Integer.parseInt(strDelay) ;

                    SampleDataExtended sampleExt = new SampleDataExtended(dataHora, ordem, linha, latitude, longitude, speed, delay) ;
                    sampleExt.writeTo(dataOut);
                }
            }
            dataOut.flush();
            dataOut.close();
            dataIn.close();
        }
    }

    private static class LineTokenizer
    {
        String line ;
        int currPosition ;
        int len ;

        static LineTokenizer recicleTokenizer(LineTokenizer recicledTokenizer, String line) {
            recicledTokenizer.line = line ;
            recicledTokenizer.currPosition = -1 ;
            recicledTokenizer.len = line.length() ;
            return recicledTokenizer ;
        }

        public LineTokenizer(String line) {
            this.line = line;
            currPosition = -1 ;
            len = line.length() ;
        }

        public String nextToken()
        {
            int lastPosition = currPosition+1 ;
            currPosition = line.indexOf('\t', currPosition+1) ;
            if ( currPosition == -1 ) {
                return line.substring(lastPosition, len);
            } else {
                return line.substring(lastPosition, currPosition);
            }
        }
    }

    private static int fastParseSpeed(String strSpeed)
    {
        int value = 0 ;
        for(int i=0; i<strSpeed.length(); i++) {
            char c = strSpeed.charAt(i) ;
            if (c == '.') break ;
            value = value * 10 + (c-'0') ;
        }

        return value ;
    }
}
