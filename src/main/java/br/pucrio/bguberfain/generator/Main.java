package br.pucrio.bguberfain.generator;

import br.pucrio.bguberfain.onibus.model.graph.v2.SampleData;
import br.pucrio.bguberfain.onibus.reader.SampleDataReader;
import br.pucrio.bguberfain.onibus.reader.sequence.SequenceGenerator;
import br.pucrio.bguberfain.onibus.reader.sequence.SequenceReader;
import br.pucrio.bguberfain.onibus.reader.sequence.ThreadedSequenceGenerator;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.SequenceBreaker;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.sampledata.ChangeLineNumberBreaker;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.sampledata.LeastIntervalBreaker;
import com.vividsolutions.jts.geom.*;
import com.vividsolutions.jts.index.SpatialIndex;
import com.vividsolutions.jts.index.strtree.STRtree;
import com.vividsolutions.jts.linearref.LinearLocation;
import org.apache.commons.math3.stat.descriptive.SummaryStatistics;
import org.apache.commons.math3.stat.descriptive.SynchronizedSummaryStatistics;
import org.apache.commons.math3.stat.descriptive.moment.Mean;
import org.geotools.data.*;
import org.geotools.data.collection.ListFeatureCollection;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.data.simple.SimpleFeatureStore;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.feature.simple.SimpleFeatureTypeBuilder;
import org.geotools.geometry.jts.JTS;
import org.geotools.geometry.jts.JTSFactoryFinder;
import org.geotools.jdbc.JDBCDataStoreFactory;
import org.geotools.referencing.GeodeticCalculator;
import org.geotools.referencing.crs.DefaultGeographicCRS;
import org.geotools.util.NullProgressListener;
import org.opengis.feature.Feature;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.opengis.referencing.operation.TransformException;

import java.io.File;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;

public class Main {
	
    private static final double MAX_SEARCH_DISTANCE = 0.0005 ;	// In degrees

    private static final boolean USE_POSTGIS = true ;

	private static double lineStringLength(MultiLineString multiLineString, CoordinateReferenceSystem crs) throws TransformException
	{
		if ( multiLineString.getNumGeometries() != 1 ) {
			throw new IllegalArgumentException("MultiLineString must have only one geometry");
		}
		
		LineString ls = (LineString) multiLineString.getGeometryN(0) ;
        CoordinateSequence coords = ls.getCoordinateSequence();
        final int coordsCount = coords.size();

        GeodeticCalculator geoCalculator = new GeodeticCalculator(crs) ;
        
        geoCalculator.setStartingPosition(JTS.toDirectPosition(coords.getCoordinate(0), crs));
        double length = 0.0;
        for (int i = 1; i < coordsCount; i++) {
        	geoCalculator.setDestinationPosition(JTS.toDirectPosition(coords.getCoordinate(i), crs));
        	
        	length += geoCalculator.getOrthodromicDistance() ;
        	
            geoCalculator.setStartingPosition(geoCalculator.getDestinationPosition());
        }
        
        return length ;
	}
	
	public static void main(String[] args) throws Exception {
		// Le os dados das linhas
		final SimpleFeatureType projectionType = createProjectionFeatureType() ;
		
		DataStore store ;
		SimpleFeatureSource featureSource ;
		if ( USE_POSTGIS ) {
			// Load line shapes (from postgis)
			Map<String, Object> params = new HashMap<String, Object>(); 
			params.put(JDBCDataStoreFactory.DBTYPE.key, "postgis"); 
			params.put(JDBCDataStoreFactory.HOST.key, "localhost"); 
			params.put(JDBCDataStoreFactory.PORT.key, 5432); 
			params.put(JDBCDataStoreFactory.SCHEMA.key, "public"); 
			params.put(JDBCDataStoreFactory.DATABASE.key, "bus-routes"); 
			params.put(JDBCDataStoreFactory.USER.key, "postgres"); 
			params.put(JDBCDataStoreFactory.PASSWD.key, "postgres"); 
			store = DataStoreFinder.getDataStore(params);
			featureSource = store.getFeatureSource("routes_segmented");
		} else {
			// Load line shapes (from file)
			File routesShapeFile = new File("data/trajetos-shape/routes-segmented.shp");
			store = FileDataStoreFinder.getDataStore(routesShapeFile);
			featureSource = store.getFeatureSource("routes-segmented");
		}

        System.out.println("Loading shapesfile...");
        final Map<String, SpatialIndex> indexByLineNumber = new HashMap<>();
        final Map<String, Set<LocationIndexedLineWithGeometry>> routesByLineNumber = new HashMap<>();
        SimpleFeatureCollection featureCollection = featureSource.getFeatures() ;
        
        // Cria índices para busca nas edges
        final SpatialIndex allLinesIndex = new STRtree();
        featureCollection.accepts((Feature feature) -> {
            SimpleFeature simpleFeature = (SimpleFeature) feature;
            LineString geom = (LineString) simpleFeature.getDefaultGeometry();
            String edgeId = (String) simpleFeature.getAttribute("edgeId") ;
            String linesNumbers[] = ((String) simpleFeature.getAttribute("linesNumbers")).split(",") ;
            String shapesId[] = ((String) simpleFeature.getAttribute("shapesId")).split(",") ;
            String sequences[] = ((String) simpleFeature.getAttribute("sequences")).split(",") ;
            
            // check of consistency
            if ( linesNumbers.length != shapesId.length || linesNumbers.length != sequences.length ) {
            	throw new RuntimeException("Numero invalido de parametros") ;
            }
            
        	LocationIndexedLineWithGeometry locationIndex;
			try {
				locationIndex = new LocationIndexedLineWithGeometry(geom, projectionType.getCoordinateReferenceSystem());
				
				// Add to "all index"
				Envelope env = geom.getEnvelopeInternal();
				allLinesIndex.insert(env, locationIndex);
				
				// Add to each line-number index
				for( int i=0; i<linesNumbers.length; i++ ) {
					String lineNumber = linesNumbers[i] ;
					if ( !"".equals(lineNumber) ) {
						String shapeId = shapesId[i] ;
						String sequence = sequences[i] ;
						// Save LS by line number
						Set<LocationIndexedLineWithGeometry> routesSet = routesByLineNumber.get(lineNumber) ;
						if ( routesSet == null ) {
							routesSet = new HashSet<>();
							routesByLineNumber.put(lineNumber, routesSet);
						}
						routesSet.add(locationIndex) ;
						
						// Create or update the index for this line
						SpatialIndex index = indexByLineNumber.get(lineNumber);
						if ( index == null ) {
							index = new STRtree();
							indexByLineNumber.put(lineNumber, index);
						}
						if (!env.isNull()) {
							index.insert(env, locationIndex);
						}
					}
				}
			} catch (Exception e) {
				throw new RuntimeException(e) ;
			}
        	
        }, new NullProgressListener() );
        
        
		// Cria o tabela de projeções
        try {
        	store.removeSchema(projectionType.getName().getLocalPart());
        } catch( IllegalArgumentException e ) { } // Does nothing... relation simply does not exists
		store.createSchema(projectionType);
		
		// Cria store de saída das projeções
		Transaction transaction = new DefaultTransaction("create");
		SimpleFeatureStore projectionsDestination = (SimpleFeatureStore) store.getFeatureSource(projectionType.getName().getLocalPart());
		projectionsDestination.setTransaction(transaction);
		
        // Início do snap!
		SampleDataReader sampleReader = new SampleDataReader("20140724") ;
		SequenceBreaker qualityFilter = new LeastIntervalBreaker((int) 5 * 60 * 1000) ;	// Ignore samples that are 5min apart
//		qualityFilter = new LongDistanceBreaker(qualityFilter, 1000) ;	// Ignore samples of more than 1km
		qualityFilter = new ChangeLineNumberBreaker(qualityFilter) ;	// Keep only same line number in the sequence

		SequenceGenerator<SampleData> sequenceGenerator = new ThreadedSequenceGenerator<>(qualityFilter, null, sampleReader, SampleData::getOrder) ;

		final long startTime = System.currentTimeMillis() ;
        System.out.println("Reading log file...");
        sequenceGenerator.readData(new SequenceReader<SampleData>() {

        	AtomicLong processed = new AtomicLong(0) ;
        	AtomicLong countIndexNotFound = new AtomicLong(0) ;
        	SummaryStatistics statsForNumEdges = new SynchronizedSummaryStatistics() ;
        	SummaryStatistics statsForDistance = new SynchronizedSummaryStatistics() ;
        	Mean meanForMinDistance = new Mean() ;

        	List<SimpleFeature> projectedFeatures = new LinkedList<SimpleFeature>() ;

        	ThreadLocal<SimpleFeatureBuilder> featureBuilder = new ThreadLocal<SimpleFeatureBuilder>() {
        		@Override
        		protected SimpleFeatureBuilder initialValue() {
        			return new SimpleFeatureBuilder(projectionType);
        		}
        	} ;

        	ThreadLocal<GeometryFactory> geometryFactory = new ThreadLocal<GeometryFactory>() {
        		@Override
        		protected GeometryFactory initialValue() {
        			return JTSFactoryFinder.getGeometryFactory() ;
        		}
        	} ;

        	ThreadLocal<GeodeticCalculator> geodeticCalculator = new ThreadLocal<GeodeticCalculator>() {
        		@Override
        		protected GeodeticCalculator initialValue() {
        			return new GeodeticCalculator() ;
        		}
        	} ;

			@Override
			public void beginSequence(SampleData firstData) {} // Does nothing

			@Override
			public void sequence(SampleData lastData, SampleData newData)
			{
				try {
					String lineNumber = lastData.getLineNumber() ;
					if ( !"".equals(lineNumber) ) {
						SpatialIndex index = indexByLineNumber.get(lineNumber) ;


//						Set<LocationIndexedLineWithGeometry> routesSet = routesByLineç´0pNumber.get(lineNumber) ;
						if( index == null ) {
							index = allLinesIndex ;
							countIndexNotFound.incrementAndGet() ;
							return ;	// Ignore processing of lines without route
						}
						long localProcessed = processed.incrementAndGet() ;
						if ( localProcessed % 1000 == 0 ) {
							long localCountIndexNotFound = countIndexNotFound.get() ;
							System.out.printf("Procesed %10d samples at rate %5.2fsamples/ms with %4.2f%% of lines not found on index\n", localProcessed, localProcessed/(double)(System.currentTimeMillis()-startTime), localCountIndexNotFound*100.0/localProcessed);
							System.out.printf("Mean # of candidate edges: %.4f (StdDev: %.4f)\n", statsForNumEdges.getMean(), statsForNumEdges.getStandardDeviation());
							System.out.printf("Mean distance (x1000): %.4f (StdDev: %.4f)\n", 1000*statsForDistance.getMean(), 1000*statsForDistance.getStandardDeviation());
							System.out.printf("%% of different min distance: %.4f%%\n", 100*meanForMinDistance.getResult());

							synchronized(projectedFeatures) {
								if ( projectedFeatures.size() > 0 ) {
									SimpleFeatureCollection collection = new ListFeatureCollection(projectionType, projectedFeatures);
									System.out.printf("Adding %d features to database...\n", collection.size());
									projectionsDestination.addFeatures(collection);
									projectedFeatures.clear();
								}
							}
							try {
								System.out.println("Commiting...");
								transaction.commit();
							} catch (Exception problem) {
								problem.printStackTrace();
								transaction.rollback();
								throw new RuntimeException(problem);
							}
						}
						Set<LocationIndexedLineWithGeometry> routesSet = new HashSet<>() ;
						Envelope lastDataEnvelope = new Envelope(lastData.getCoordinate()) ;
						Envelope newDataEnvelope = new Envelope(newData.getCoordinate()) ;
						lastDataEnvelope.expandBy(MAX_SEARCH_DISTANCE);
						newDataEnvelope.expandBy(MAX_SEARCH_DISTANCE);
						routesSet.addAll( index.query( lastDataEnvelope ) ) ;
//						routesSet.retainAll( index.query( newDataEnvelope ) ) ;
						statsForNumEdges.addValue(routesSet.size());
						double minDistance = Double.MAX_VALUE ;
						LocationIndexedLineWithGeometry minRoute = null ;
						Coordinate minLastProjectedLocation = null ;
						for (LocationIndexedLineWithGeometry route : routesSet) {
			                LinearLocation lastDataLocation = route.project(lastData.getCoordinate());
			                Coordinate lastDataProjection = route.extractPoint(lastDataLocation) ;
			                double lastDataProjectionDistance = lastDataProjection.distance(lastData.getCoordinate()) ;
			                if ( lastDataProjectionDistance < minDistance ) {
			                	minDistance = lastDataProjectionDistance ;
			                	minRoute = route ;
			                	minLastProjectedLocation = lastDataProjection ;
			                }
			                statsForDistance.addValue(lastDataProjectionDistance);
//			                if ( lastDataProjectionDistance < MAX_SEARCH_DISTANCE ) {
//			                	LinearLocation newDataLocation = route.project(newData.getCoordinate());
//			                	Coordinate newDataProjection = route.extractPoint(newDataLocation) ;
//			                	
//			                	double newDataProjectionDistance = newDataProjection.distance(newData.getCoordinate()) ;
//				                if ( newDataProjectionDistance < MAX_SEARCH_DISTANCE ) {
//				                }
//			                }
						}

						// Salva o ponto projetado]
						if ( minLastProjectedLocation != null ) {
							// Calcula da data do sample
							Date date = new Date(lastData.getTimestamp());

//							double distanceTraveled = 

							Coordinate [] lsCoordinates = new Coordinate[] { lastData.getCoordinate(), minLastProjectedLocation } ;
							LineString ls = geometryFactory.get().createLineString(lsCoordinates) ;
			                featureBuilder.get().add(ls);							// the_geom
			                featureBuilder.get().add(minDistance);					// distance
			                featureBuilder.get().add(lineNumber);					// lineNumber
			                featureBuilder.get().add(date);							// date
			                featureBuilder.get().add(lastData.getSpeed());							// date
			                SimpleFeature lsProjection = featureBuilder.get().buildFeature(null);
			                synchronized(projectedFeatures) {
			                	projectedFeatures.add(lsProjection) ;
			                }
						}


						// Refaz o teste para uma distancia maior...
						routesSet = new HashSet<>() ;
						lastDataEnvelope = new Envelope(lastData.getCoordinate()) ;
						newDataEnvelope = new Envelope(newData.getCoordinate()) ;
						lastDataEnvelope.expandBy(4*MAX_SEARCH_DISTANCE);
						newDataEnvelope.expandBy(4*MAX_SEARCH_DISTANCE);
						routesSet.addAll( index.query( lastDataEnvelope ) ) ;
//						routesSet.retainAll( index.query( newDataEnvelope ) ) ;
						statsForNumEdges.addValue(routesSet.size());
						double minDistance2 = Double.MAX_VALUE ;
						LocationIndexedLineWithGeometry minRoute2 = null ;
						for (LocationIndexedLineWithGeometry route : routesSet) {
			                LinearLocation lastDataLocation = route.project(lastData.getCoordinate());
			                Coordinate lastDataProjection = route.extractPoint(lastDataLocation) ;
			                double lastDataProjectionDistance = lastDataProjection.distance(lastData.getCoordinate()) ;
			                if ( lastDataProjectionDistance < minDistance2 ) {
			                	minDistance2 = lastDataProjectionDistance ;
			                	minRoute2 = route ;
			                }
			                statsForDistance.addValue(lastDataProjectionDistance);
//			                if ( lastDataProjectionDistance < MAX_SEARCH_DISTANCE ) {
//			                	LinearLocation newDataLocation = route.project(newData.getCoordinate());
//			                	Coordinate newDataProjection = route.extractPoint(newDataLocation) ;
//			                	
//			                	double newDataProjectionDistance = newDataProjection.distance(newData.getCoordinate()) ;
//				                if ( newDataProjectionDistance < MAX_SEARCH_DISTANCE ) {
//				                }
//			                }
						}

						if ( minRoute2 != minRoute ) {
//							System.out.println("Min distance difference!");
							meanForMinDistance.increment(1);
						} else {
							meanForMinDistance.increment(0);
						}
					} else {
//							System.out.printf("Line route not found for line number %s\n", lineNumber);
					}
				} catch (Exception e) {
					e.printStackTrace();
					throw new RuntimeException(e) ;
				}
			}

			@Override
			public void endSequence(SampleData lastData) {} // Does nothing

        });

        transaction.close();
	}
	
    private static SimpleFeatureType createProjectionFeatureType() {
        SimpleFeatureTypeBuilder builder = new SimpleFeatureTypeBuilder();
        builder.setName("projections");
        builder.setCRS(DefaultGeographicCRS.WGS84); // <- Coordinate reference system

        // add attributes in order
        builder.add("the_geom", LineString.class);
        builder.add("distance", Double.class);
        builder.add("line_number", String.class);
        builder.add("date", Date.class);
        builder.add("instant_speed", Double.class);
//        builder.add("meanSpeed", Double.class);
        
        // build the type
        return builder.buildFeatureType();
    }

}
