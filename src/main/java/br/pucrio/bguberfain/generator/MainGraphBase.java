package br.pucrio.bguberfain.generator;

import br.pucrio.bguberfain.onibus.gui.XYGraph;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Created by Bruno on 22/02/2015.
 */
public class MainGraphBase {
    protected static final long SECONDS_IN_AN_HOUR = 60 * 60 ;
    protected static final long SECONDS_IN_A_DAY = SECONDS_IN_AN_HOUR * 24 ;
    protected static final long MS_IN_A_DAY = SECONDS_IN_A_DAY * 1000 ;

    protected static float timeToColorPercent(long time)
    {
        return Math.min(1.0f, (time % MS_IN_A_DAY) / (float) MS_IN_A_DAY);
    }

    private static SortedMap<Float, String> graphLegend()
    {
        SortedMap<Float, String> legend = new TreeMap<>() ;

        legend.put(  0 * SECONDS_IN_AN_HOUR / (float) SECONDS_IN_A_DAY, "00:00" ) ;
        legend.put(  3 * SECONDS_IN_AN_HOUR / (float) SECONDS_IN_A_DAY, "03:00" ) ;
        legend.put(  6 * SECONDS_IN_AN_HOUR / (float) SECONDS_IN_A_DAY, "06:00" ) ;
        legend.put(  9 * SECONDS_IN_AN_HOUR / (float) SECONDS_IN_A_DAY, "09:00" ) ;
        legend.put( 12 * SECONDS_IN_AN_HOUR / (float) SECONDS_IN_A_DAY, "12:00" ) ;
        legend.put( 15 * SECONDS_IN_AN_HOUR / (float) SECONDS_IN_A_DAY, "15:00" ) ;
        legend.put( 18 * SECONDS_IN_AN_HOUR / (float) SECONDS_IN_A_DAY, "18:00" ) ;
        legend.put( 21 * SECONDS_IN_AN_HOUR / (float) SECONDS_IN_A_DAY, "21:00" ) ;
        legend.put( 24 * SECONDS_IN_AN_HOUR / (float) SECONDS_IN_A_DAY, "24:00" ) ;

        return legend ;
    }

    protected static XYGraph createAndShowGUIFromFile(String title, File file) throws IOException {
        return createAndShowGUI(title, XYGraph.loadFromFile(file)) ;
    }

    protected static XYGraph createAndShowGUI(String title, XYGraph.XYGraphAxisType xAxis, XYGraph.XYGraphAxisType yAxis) {
        return createAndShowGUI(title, new XYGraph(xAxis, yAxis)) ;
    }

    private static XYGraph createAndShowGUI(String title, XYGraph xyGraph)
    {
        JFrame f = new JFrame(title);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        xyGraph.setLegendForColorPercent(percent -> {
            long dayMinutes = Math.round(percent * SECONDS_IN_A_DAY / SECONDS_IN_AN_HOUR * 60);
            int minutes = (int) (dayMinutes % 60);
            int hours = (int) (dayMinutes / 60);

            return String.format("%02d:%02d", hours, minutes);
        });
        xyGraph.setLegend(MainGraphBase.graphLegend());

        f.add(xyGraph);
        f.pack();
        f.setSize(1916, 806+152);
        f.setVisible(true);

        return xyGraph ;
    }
}
