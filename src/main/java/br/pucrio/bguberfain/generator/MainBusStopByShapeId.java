package br.pucrio.bguberfain.generator;

import br.pucrio.bguberfain.onibus.model.graph.v2.Localizable;
import br.pucrio.bguberfain.onibus.model.graph.v2.ProjectionInfo;
import br.pucrio.bguberfain.onibus.model.graph.v2.Route;
import br.pucrio.bguberfain.onibus.util.AutoInstanceMap;
import br.pucrio.bguberfain.onibus.util.NetworkTopologyDS;
import br.pucrio.bguberfain.onibus.util.Table;
import br.pucrio.bguberfain.onibus.util.Util;
import com.csvreader.CsvReader;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Point;
import org.apache.commons.math3.stat.descriptive.moment.Mean;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.geotools.data.DataStore;
import org.geotools.data.DataStoreFinder;
import org.geotools.data.DefaultTransaction;
import org.geotools.data.Transaction;
import org.geotools.data.collection.ListFeatureCollection;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureStore;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.feature.simple.SimpleFeatureTypeBuilder;
import org.geotools.geometry.jts.JTSFactoryFinder;
import org.geotools.jdbc.JDBCDataStoreFactory;
import org.geotools.referencing.crs.DefaultGeographicCRS;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;

import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Bruno on 01/02/2015.
 */
public class MainBusStopByShapeId
{
    private static final Logger logger = LogManager.getLogger(MainBusStopByShapeId.class);

    private static final double MAX_DISTANCE_FROM_ROUTE = 15;  // In meters
    private static final double MIN_DISTANCE_FROM_LAST_STOP = 15;  // In meters

    public static void main(String [] args) throws Exception
    {
        Map<Integer, Route> routeById = NetworkTopologyDS.getInstance().getNetworkByShapeId(null, false) ;
        Map<String, List<Route>> routesByLine = routeById.values().stream().collect(Collectors.groupingBy(Route::getLineNumber)) ;

        File trajectoriesFolder = new File(Util.getEnv("BUS_STOP_FOLDER"));
        File[] trajectories = trajectoriesFolder.listFiles(pathname -> pathname.getName().endsWith(".csv"));

        SimpleFeatureType busStopType = createBusStopFeatures() ;
        SimpleFeatureBuilder busStopBuilder = new SimpleFeatureBuilder(busStopType) ;
        GeometryFactory geometryFactory = JTSFactoryFinder.getGeometryFactory() ;

        // Create bus stop table
        DataStore store = getDataStore();

        try {
            store.removeSchema(busStopType.getName().getLocalPart());
        } catch( IllegalArgumentException e ) { } // Does nothing... relation simply does not exists
        store.createSchema(busStopType);
        // Cria store de saída das projeções
        Transaction transaction = new DefaultTransaction("create");
        SimpleFeatureStore busStopsStore = (SimpleFeatureStore) store.getFeatureSource(busStopType.getName().getLocalPart());
        busStopsStore.setTransaction(transaction);

        Mean percentOk = new Mean() ;

        AutoInstanceMap<Integer, BusStop> busStopsBySequence = new AutoInstanceMap<>(new TreeMap<>(), LinkedList::new) ;
        for (File file : trajectories) {
            CsvReader trajectoryData = new CsvReader(file.getAbsolutePath());

            String lineNumber = file.getName().substring(0, file.getName().indexOf('.')) ;

            busStopsBySequence.clear() ;

            trajectoryData.readHeaders() ;
            while( trajectoryData.readRecord() ) {
                int sequence = Integer.parseInt(trajectoryData.get("sequencia")) ;
                double latitude = Double.parseDouble(trajectoryData.get("latitude")) ;
                double longitude = Double.parseDouble(trajectoryData.get("longitude")) ;

                busStopsBySequence.putValue(sequence, new BusStop(new Coordinate(longitude, latitude), sequence)) ;
            }

            trajectoryData.close() ;

            logger.debug("Analyzing line {}", lineNumber);

            List<Route> routes = routesByLine.get(lineNumber) ;
            Map<Route, Deque<ProjectionInfo<BusStop>>> busStopsForRoute = new HashMap<>() ;
            List<SimpleFeature> projectedBusStops = new LinkedList<>() ;

            // Create first stop on the beginning of each route
            routes.forEach(route -> {
                ProjectionInfo<BusStop> firstPointProjection = ProjectionInfo.project(route, new BusStop(route.getGeometry().getCoordinateN(0), 0)) ;
                Deque<ProjectionInfo<BusStop>> busStopsForThisRoute = new LinkedList<>() ;
                busStopsForThisRoute.add(firstPointProjection) ;
                busStopsForRoute.put(route, busStopsForThisRoute) ;

                Point busStopPoint = geometryFactory.createPoint(firstPointProjection.getProjectedCoordinate()) ;
                double distanceSinceBegin = route.distanceInMetersUntil(firstPointProjection.getLocation()) ;

                busStopBuilder.add(busStopPoint);            // the_geom
                busStopBuilder.add(lineNumber);              // line_number
                busStopBuilder.add(route.getShapeId());      // shape_id
                busStopBuilder.add(0);                       // sequence
                busStopBuilder.add(distanceSinceBegin);      // distance_since_begin

                SimpleFeature newBusStop = busStopBuilder.buildFeature(null);

                projectedBusStops.add(newBusStop) ;
            });

            // Attach each bus stop (grouped and sorted by sequence) to a route
            for(Map.Entry<Integer, Collection<BusStop>> stopsOnSequence : busStopsBySequence.entrySet()) {

                // Create a map of bus stop by routes (it will be used latter to decide which route is most probable)
                Table<BusStop, Route, Double> distanceToLastStopOnRoute = new Table<>() ;
                Collection<BusStop> busStops = stopsOnSequence.getValue();
                for (BusStop busStop : busStops) {
                    for (Route route : routes) {
                        Deque<ProjectionInfo<BusStop>> busStopsForThisRoute = busStopsForRoute.get(route);
                        ProjectionInfo<BusStop> lastProjection = busStopsForThisRoute.getLast();

                        if (busStop.getSequence() > lastProjection.getLocationData().getSequence()) {
                            ProjectionInfo<BusStop> busStopProjection = ProjectionInfo.projectAfter(route, busStop, lastProjection);
                            double distanceSinceLast = route.distanceInMetersBetween(lastProjection.getLocation(), busStopProjection.getLocation());


                            // Note: ignore the MIN_DISTANCE_FROM_LAST_STOP value if it is the first bus stop
                            if ( (lastProjection.getLocationData().getSequence() == 0 || distanceSinceLast > MIN_DISTANCE_FROM_LAST_STOP) && busStopProjection.getProjectedDistance() < MAX_DISTANCE_FROM_ROUTE) {
                                distanceToLastStopOnRoute.put(busStop, route, distanceSinceLast);
                            }
                        }
                    }
                }

                // Find the route with smallest distance to the last stop
                distanceToLastStopOnRoute.forEachRow(entry -> {
                    BusStop busStop = entry.getKey();
                    Map<Route, Double> distanceOnRoute = entry.getValue() ;
                    Optional<Route> minRouteDistance = distanceOnRoute.entrySet().stream()
                            .min(Map.Entry.comparingByValue())
                            .map(Map.Entry::getKey);
                    if (minRouteDistance.isPresent()) {
                        Route route = minRouteDistance.get();
                        Deque<ProjectionInfo<BusStop>> busStopsForThisRoute = busStopsForRoute.get(route);
                        ProjectionInfo<BusStop> lastProjection = busStopsForThisRoute.getLast();
                        ProjectionInfo<BusStop> busStopProjection = ProjectionInfo.projectAfter(route, busStop, lastProjection);

                        busStopsForThisRoute.add(busStopProjection);

                        Point busStopPoint = geometryFactory.createPoint(busStopProjection.getProjectedCoordinate());
                        double distanceSinceBegin = route.distanceInMetersUntil(busStopProjection.getLocation());

                        busStopBuilder.add(busStopPoint);            // the_geom
                        busStopBuilder.add(lineNumber);              // line_number
                        busStopBuilder.add(route.getShapeId());      // shape_id
                        busStopBuilder.add(busStopsForThisRoute.size() - 1);   // sequence
                        busStopBuilder.add(distanceSinceBegin);      // distance_since_begin

                        SimpleFeature newBusStop = busStopBuilder.buildFeature(null);

                        projectedBusStops.add(newBusStop);

                    }
                });
            }

            int totalBusStops = busStopsBySequence.entrySet().stream()
                    .mapToInt(entry ->entry.getValue().size())
                    .sum()
                    + routes.size() ;   // Add the first stop added for each route

            if (projectedBusStops.size() != totalBusStops) {
                logger.warn("Not all stops where found for line {} (total:{}, projected: {})", lineNumber, totalBusStops, projectedBusStops.size());
                percentOk.increment(0);
            } else {
                logger.debug("Line {} processd: no erros found", lineNumber);
                percentOk.increment(1);
            }

            SimpleFeatureCollection collection = new ListFeatureCollection(busStopType, projectedBusStops);
            busStopsStore.addFeatures(collection);

            try {
                synchronized(transaction) {
                    transaction.commit();
                }
            } catch (Exception problem) {
                logger.error("Error while commiting", problem);
                transaction.rollback();
                throw new RuntimeException(problem);
            }

        }

        logger.printf(Level.INFO, "Percent ok %.1f%%", new Double(percentOk.getResult() * 100));
    }

    private static class BusStop implements Comparable<BusStop>, Localizable {
        private Coordinate coordinate ;
        int sequence ;

        public BusStop(Coordinate coordinate, int sequence) {
            this.coordinate = coordinate;
            this.sequence = sequence;
        }

        public Coordinate getCoordinate() {
            return coordinate;
        }

        public int getSequence() {
            return sequence;
        }

        @Override
        public int compareTo(BusStop other) {
            return sequence - other.sequence ;
        }
    }

    private static DataStore getDataStore() throws Exception
    {
        // Load line shapes (from postgis)
        Map<String, Object> params = new HashMap<>();
        params.put(JDBCDataStoreFactory.DBTYPE.key, "postgis");
        params.put(JDBCDataStoreFactory.HOST.key, "localhost");
        params.put(JDBCDataStoreFactory.PORT.key, 5432);
        params.put(JDBCDataStoreFactory.SCHEMA.key, "public");
        params.put(JDBCDataStoreFactory.DATABASE.key, "bus-routes");
        params.put(JDBCDataStoreFactory.USER.key, "postgres");
        params.put(JDBCDataStoreFactory.PASSWD.key, "postgres");
        return DataStoreFinder.getDataStore(params);
    }

    private static SimpleFeatureType createBusStopFeatures() {
        SimpleFeatureTypeBuilder builder = new SimpleFeatureTypeBuilder();
        builder.setName("bus_stops_new");
        builder.setCRS(DefaultGeographicCRS.WGS84); // <- Coordinate reference system

        // add attributes in order
        builder.add("the_geom", Point.class);
        builder.add("line_number", String.class);
        builder.add("shape_id", Integer.class);
        builder.add("sequence", Integer.class);
        builder.add("distance_since_begin", Double.class);

        // build the type
        return builder.buildFeatureType();
    }
}
