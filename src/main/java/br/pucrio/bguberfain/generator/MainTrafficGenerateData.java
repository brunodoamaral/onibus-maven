package br.pucrio.bguberfain.generator;

import br.pucrio.bguberfain.onibus.gui.Region;
import br.pucrio.bguberfain.onibus.model.DynamicEdgeStats;
import br.pucrio.bguberfain.onibus.model.EdgeStatsExtended;
import br.pucrio.bguberfain.onibus.model.TrafficDataPeriod;
import br.pucrio.bguberfain.onibus.model.graph.v2.*;
import br.pucrio.bguberfain.onibus.reader.GZipSampleDataReader;
import br.pucrio.bguberfain.onibus.reader.sequence.SequenceGenerator;
import br.pucrio.bguberfain.onibus.reader.sequence.SequenceReader;
import br.pucrio.bguberfain.onibus.reader.sequence.ThreadedSequenceGenerator2;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.SequenceBreaker;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.sampledata.BackwardTimeBreaker;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.sampledata.ChangeLineNumberBreaker;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.sampledata.LeastIntervalBreaker;
import br.pucrio.bguberfain.onibus.reader.sequence.filter.DataFilter;
import br.pucrio.bguberfain.onibus.reader.sequence.filter.sampledata.*;
import br.pucrio.bguberfain.onibus.reader.transformer.SampleDataSequence2ProjectionSequence;
import br.pucrio.bguberfain.onibus.util.NetworkTopologyDS;
import br.pucrio.bguberfain.onibus.util.Util;
import com.vividsolutions.jts.geom.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.geotools.geometry.jts.JTSFactoryFinder;
import org.geotools.graph.build.GraphBuilder;
import org.geotools.graph.build.line.BasicLineGraphGenerator;
import org.geotools.graph.build.line.LineStringGraphGenerator;
import org.geotools.graph.structure.Graph;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by Bruno on 10/03/2015.
 */
public class MainTrafficGenerateData {

    private static final Logger logger = LogManager.getLogger(MainTrafficGenerateData.class);
    private final static long MAX_INTERVAL_BETWEEN_SEQUENCES = 60 * 15 ; // 15 minutes in seconds
    private final static double MAX_SPEED = 100 / 3.6 ;     // km/h in m/s
    private final static double MAX_DISTANCE_BETWEEN_SEQUENCES = MAX_SPEED * 5 * 60 ; // Distance with max speed in 5 minutes
    private final static double MIN_DISTANCE_BETWEEN_SEQUENCES = 100 ; // Minimum distance to compute average speed
    private final static double MAX_DISTANCE_TO_ENDPOINTS = 400 ;   // In meters
    private final static Envelope REGION = Region.RIO_DE_JANEIRO ;
    private final static File saveToFolder = new File(Util.getEnv("SAVE_TO_DIR")) ;

    public static void main(String [] args) throws Exception {
        // Generate Daily configuration...
        new MainTrafficGenerateData(new int[]{ 15, 30, 60 }, new TrafficDataPeriod[] {TrafficDataPeriod.DAILY, TrafficDataPeriod.DAILY, TrafficDataPeriod.DAILY}).generate(new WorkdayFilter(), "2015.*\\.txt\\.gz");

        // Generate Weekly configuration...
        new MainTrafficGenerateData(new int[]{ 15, 30, 60 }, new TrafficDataPeriod[] {TrafficDataPeriod.WEEKLY, TrafficDataPeriod.WEEKLY, TrafficDataPeriod.WEEKLY}).generate(new HolidayFilter(), "2015.*\\.txt\\.gz");

        // Generate Yearly configuration...
        new MainTrafficGenerateData(
                new int[]{ TrafficDataPeriod.YEARLY.getMinutes() / 12 /* per month */, TrafficDataPeriod.WEEKLY.getMinutes() /* per week */},
                new TrafficDataPeriod[] {TrafficDataPeriod.YEARLY, TrafficDataPeriod.YEARLY}).generate(null, ".*\\.txt\\.gz");
    }

    int []precisionMinutes ;
    TrafficDataPeriod []periods ;
    int []numsTimeSlices ;

    private interface ConsumeConfiguration {
        void consume(int configIdx, int precisionMinute, TrafficDataPeriod period) throws Exception;
    }

    public MainTrafficGenerateData(int []precisionMinutes, TrafficDataPeriod []periods) throws Exception {
        if (precisionMinutes.length != periods.length) {
            throw new RuntimeException("Invalid configuration") ;
        }
        this.precisionMinutes = precisionMinutes;
        this.periods = periods;

        numsTimeSlices = new int[numConfigurations()] ;
        forEachConfiguration((configIdx, precisionMinute, period) -> numsTimeSlices[configIdx] = period.getMinutes() / precisionMinute);
    }

    private int numConfigurations() {
        return precisionMinutes.length ; // or periods.length
    }

    private void forEachConfiguration(ConsumeConfiguration consumer) {
        int numConfigs = numConfigurations() ;
        for( int idx=0; idx<numConfigs; idx++ ) {
            try {
                consumer.consume(idx, precisionMinutes[idx], periods[idx]);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void generate(DataFilter filter, String dataPattern) throws Exception {
        logger.info("Generating data for {} precisions and {} minutes", periods.length, precisionMinutes.length);

        // Load routes
        Map<Integer, Route> routesByShapeId = NetworkTopologyDS.getInstance().getNetworkByShapeId(null, false) ;

        // Expanded region
        Envelope filterRegion = new Envelope(REGION) ;
        filterRegion.expandBy(0.01);

        // Generate graph
        logger.info("Generating graph...");
        Graph graph = getGraphForRoutes(routesByShapeId.values(), filterRegion) ;

        // Generate map of LineSegment for Edge
        logger.info("Generating edge-by-segment map...");
        Map<LineSegment, BasicDirectedUniqueEdge<DynamicEdgeStats[]>> edgeBySegment = new HashMap<>() ;
        for (BasicDirectedUniqueEdge edge : (Collection<BasicDirectedUniqueEdge>) graph.getEdges()) {
            Coordinate from = ((Point)edge.getNodeA().getObject()).getCoordinate() ;
            Coordinate to = ((Point)edge.getNodeB().getObject()).getCoordinate() ;

            edgeBySegment.put( new LineSegment(from, to), edge ) ;
        }

        // Configure data loader
        Iterator<SampleData> samples = new GZipSampleDataReader(dataPattern);
        SequenceBreaker sequenceBreakers = new LeastIntervalBreaker(MAX_INTERVAL_BETWEEN_SEQUENCES * 1000) ;
        sequenceBreakers = new ChangeLineNumberBreaker(sequenceBreakers) ;	// Keep only same line number in the sequence
        sequenceBreakers = new BackwardTimeBreaker(sequenceBreakers) ;
        // Standard filters
        filter = new RegionFilter(filterRegion, filter) ;
        filter = new MaxDelayFilter(15 * 60, filter) ;
        filter = new DaylightSavingTimeFixFilter(filter) ;
        SequenceGenerator<SampleData> samplesSequence = new ThreadedSequenceGenerator2<>(sequenceBreakers, filter, samples, SampleData::getOrder) ;

        // Convert Sample to Projection (snapping)
        SequenceGenerator<ProjectionInfo<SampleData>> projectionSequence = new SampleDataSequence2ProjectionSequence(routesByShapeId.values(), samplesSequence) ;

        logger.info("Starting loading data...");
        long start = System.currentTimeMillis() ;
        projectionSequence.readData(new SequenceReader<ProjectionInfo<SampleData>>() {
            ThreadLocal<Map<String, ProjectionInfo<SampleData>>> fistSampleByOrder = new ThreadLocal<Map<String, ProjectionInfo<SampleData>>>() {
                @Override
                protected Map<String, ProjectionInfo<SampleData>> initialValue() {
                    return new HashMap<>();
                }
            } ;

            @Override
            public void beginSequence(ProjectionInfo<SampleData> newData) {
                String order = newData.getLocationData().getOrder() ;
                fistSampleByOrder.get().put(order, newData) ;
            }

            @Override
            public void sequence(ProjectionInfo<SampleData> lastData, ProjectionInfo<SampleData> newData) {
                SampleData newSample = newData.getLocationData() ;
                SampleData lastSample = lastData.getLocationData() ;
                int timeDiff = (int) ((newSample.getTimestamp() - lastSample.getTimestamp()) / 1000);
                if (timeDiff > 0) {
                    double distanceDiff = newData.distanceInMetersTraveledSince(lastData);

                    String order = lastData.getLocationData().getOrder() ;
                    ProjectionInfo<SampleData> firstSampleData = fistSampleByOrder.get().get(order) ;

                    // Distance is insufficient to calculate the average speed
                    if (distanceDiff < MIN_DISTANCE_BETWEEN_SEQUENCES) {
                        // Try to use an older sample (firstSample)
                        if (firstSampleData != null) {
                            // Check if we can use the firstSampleData (it must be on the same route as newData)
                            if(firstSampleData.getRoute().equals(newData.getRoute())) {
                                double distanceSinceFirstSample = newData.distanceInMetersTraveledSince(firstSampleData);

                                // Ignore 'newData', since it has not traveled the minimum required to calculate the average speed
                                // even since firstSample
                                if (distanceSinceFirstSample < MIN_DISTANCE_BETWEEN_SEQUENCES) {
                                    return;
                                } else {
                                    // Change lastData for firstSampleData (and update other variables for this change)
                                    lastData = firstSampleData ;
                                    lastSample = firstSampleData.getLocationData();
                                    distanceDiff = distanceSinceFirstSample ;
                                    timeDiff = (int) ((newSample.getTimestamp() - lastSample.getTimestamp()) / 1000);

                                    // Prevent firstSampleData to be used again
                                    fistSampleByOrder.get().remove(order) ;
                                }
                            } else {
                                // firstSampleData is on different route -> remove it
                                fistSampleByOrder.get().remove(order) ;
                            }

                        } else {
                            // No previously sample were available: set lastData to be used in the future
                            fistSampleByOrder.get().put(order, lastData) ;

                            // Skip processing this sequence
                            return ;
                        }
                    }

                    if (distanceDiff > 0 && distanceDiff < MAX_DISTANCE_BETWEEN_SEQUENCES) {
                        double avgSpeed = distanceDiff / timeDiff;

                        // Ignore if faster than MAX_SPEED
                        if (avgSpeed < MAX_SPEED) {
                            // Check for buses on the beginning or end of route
                            Route route = newData.getRoute();
                            double distanceSinceBegin = newData.distanceTraveledOnRoute();
                            if (distanceSinceBegin < MAX_DISTANCE_TO_ENDPOINTS ||
                                    route.geometryLengthInMeters() - distanceSinceBegin < MAX_DISTANCE_TO_ENDPOINTS) {
                                return;
                            }

                            // Update average speed of each edge
                            final ProjectionInfo<SampleData> finalLastData = lastData;
                            forEachConfiguration((configIdx, precisionMinute, period) -> {
                                int imgIdx = period.timeSliceForTimeStamp(newSample.getTimestamp(), precisionMinute) ;
                                LineString lsRoute = newData.getRoute().getGeometry();

                                // Update first and last segment using its fraction
                                if (finalLastData.getLocation().getSegmentFraction() > 0.5) {
                                    updateSegmentWithSpeed(finalLastData.getLocation().getSegmentIndex(), imgIdx, configIdx, lsRoute, avgSpeed);
                                }

                                if (newData.getLocation().getSegmentFraction() >0.5) {
                                    updateSegmentWithSpeed(newData.getLocation().getSegmentIndex(), imgIdx, configIdx, lsRoute, avgSpeed);
                                }

                                // Update segments between new and last with weight 1.0
                                for (int segment = finalLastData.getLocation().getSegmentIndex() + 1; segment < newData.getLocation().getSegmentIndex(); segment++) {
                                    updateSegmentWithSpeed(segment, imgIdx, configIdx, lsRoute, avgSpeed);
                                }
                            });
                        }
                    }
                }
            }

            @Override
            public void endSequence(ProjectionInfo<SampleData> lastData) {
                String order = lastData.getLocationData().getOrder() ;
                fistSampleByOrder.get().remove(order) ;
            }

            private void updateSegmentWithSpeed(int segment, int timeSlice, int configIdx, LineString lsRoute, double speed) {
                BasicDirectedUniqueEdge<DynamicEdgeStats[]> edge = edgeBySegment.get(getLineSegmentFrom(segment, lsRoute)) ;
                if (edge != null) {
                    synchronized (edge) {
                        edge.getUserObject()[configIdx].increment(timeSlice, speed);
                    }
                }
            }
        });

        long end = System.currentTimeMillis() ;
        logger.info("Finished loading all data in {} s", (end-start) / 1000);

        saveData(edgeBySegment) ;
    }

    private void saveData( Map<LineSegment, BasicDirectedUniqueEdge<DynamicEdgeStats[]>> edgeBySegment ) throws Exception {
        logger.info("Saving file...");

        forEachConfiguration((configIdx, precisionMinute, period) -> {
            Map<LineSegment, EdgeStatsExtended> edgeStatsByLs = new HashMap<>();

            // Convert edgeBySegment to edgeStatsByLs
            for (Map.Entry<LineSegment, BasicDirectedUniqueEdge<DynamicEdgeStats[]>> edgeEntry : edgeBySegment.entrySet()) {
                LineSegment lsEdge = edgeEntry.getKey();
                DynamicEdgeStats stats = edgeEntry.getValue().getUserObject()[configIdx];

                EdgeStatsExtended edgeStats = stats.snapshotExtended() ;
                edgeStatsByLs.put(lsEdge, edgeStats);
            }

            File outFile = new File(saveToFolder, "traffic-data-extended-" + period.name().toLowerCase() + "-" + precisionMinute + ".dat");
            logger.info("Saving serialized class to {}...", outFile.getPath());
            ObjectOutputStream out = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(outFile)));
            out.writeObject(edgeStatsByLs);
            out.flush();
            out.close();
        });
    }

    private Graph getGraphForRoutes(Collection<Route> routes, Envelope region) {
        // Graph builders....
        GraphBuilder graphBuilder = new BasicUniqueDirectedLineGraphBuilder();
        BasicLineGraphGenerator graphGenerator = new LineStringGraphGenerator() ;
        graphGenerator.setGraphBuilder(graphBuilder);

        // Geometry builder...
        GeometryFactory geometryFactory = JTSFactoryFinder.getGeometryFactory();

        // Create graph for each edge
        for (Route route : routes) {
            LineString lsRoute = route.getGeometry() ;
            Coordinate lastCoordinate = lsRoute.getCoordinateN(0) ;
            for(int i=0; i<lsRoute.getNumPoints(); i++) {
                Coordinate currentCoordinate = lsRoute.getCoordinateN(i) ;

                if (region.contains(currentCoordinate) && region.contains(lastCoordinate)) {
                    LineString lsSegment = geometryFactory.createLineString( new Coordinate[] {lastCoordinate, currentCoordinate} ) ;

                    graphGenerator.add(lsSegment) ;
                }

                lastCoordinate = currentCoordinate ;
            }
        }

        // Generate graph and add DynamicEdgeStats to edges
        Graph graph = graphGenerator.getGraph() ;
        graph.visitEdges(edge -> {
            DynamicEdgeStats[] statsForConfig = new DynamicEdgeStats[numConfigurations()] ;

            try {
                forEachConfiguration((configIdx, precisionMinute, period) -> statsForConfig[configIdx] = new DynamicEdgeStats(numsTimeSlices[configIdx]));
            } catch (Exception e) {
                e.printStackTrace();
            }

            ((BasicDirectedUniqueEdge) edge).setUserObject(statsForConfig);
            return Graph.PASS_AND_CONTINUE ;
        });


        return graph ;
    }

    private static LineSegment getLineSegmentFrom(int segment, LineString ls) {
        return new LineSegment(ls.getCoordinateN(segment), ls.getCoordinateN(segment+1)) ;
    }
}
