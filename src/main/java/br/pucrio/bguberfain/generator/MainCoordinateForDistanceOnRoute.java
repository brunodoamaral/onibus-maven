package br.pucrio.bguberfain.generator;

import br.pucrio.bguberfain.onibus.model.graph.v2.Route;
import br.pucrio.bguberfain.onibus.util.NetworkTopologyDS;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.PrecisionModel;
import com.vividsolutions.jts.io.WKTReader;
import org.geotools.referencing.crs.DefaultGeographicCRS;

import java.util.Map;

/**
 * Created by Bruno on 05/04/2015.
 */
public class MainCoordinateForDistanceOnRoute {

    private static final int SHAPE_ID = 26493284 ;
    private static final double DISTANCE_IN_METER_ON_ROUTE[] = { 16200, 45207, 47800 } ;

    public static void main(String [] args) throws Exception {
        Map<Integer, Route> routesByShapeId = NetworkTopologyDS.getInstance().getNetworkByShapeId(null, false) ;
        Route route = routesByShapeId.get(SHAPE_ID) ;

        for (double distance : DISTANCE_IN_METER_ON_ROUTE) {
            Coordinate pt = route.coordinateAtLength(distance) ;
            System.out.printf("%20.2f = %.5f, %.5f\n", distance, pt.y, pt.x) ;
        }

    }

}
