package br.pucrio.bguberfain.generator;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

import br.pucrio.bguberfain.onibus.model.graph.v2.ProjectionInfo;
import br.pucrio.bguberfain.onibus.reader.GZipSampleDataReader;
import br.pucrio.bguberfain.onibus.reader.sequence.SequenceGenerator;
import br.pucrio.bguberfain.onibus.reader.sequence.ThreadedSequenceGenerator2;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.sampledata.BackwardTimeBreaker;
import br.pucrio.bguberfain.onibus.reader.sequence.filter.sampledata.*;
import br.pucrio.bguberfain.onibus.reader.transformer.SampleDataSequence2ProjectionSequence;
import com.vividsolutions.jts.algorithm.Angle;
import org.apache.commons.math3.stat.descriptive.SummaryStatistics;
import org.apache.commons.math3.stat.descriptive.SynchronizedSummaryStatistics;
import org.geotools.data.DataStore;
import org.geotools.data.DataStoreFinder;
import org.geotools.data.DefaultTransaction;
import org.geotools.data.FileDataStoreFinder;
import org.geotools.data.Transaction;
import org.geotools.data.collection.ListFeatureCollection;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.data.simple.SimpleFeatureStore;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.feature.simple.SimpleFeatureTypeBuilder;
import org.geotools.geometry.jts.JTS;
import org.geotools.geometry.jts.JTSFactoryFinder;
import org.geotools.jdbc.JDBCDataStoreFactory;
import org.geotools.referencing.crs.DefaultGeographicCRS;
import org.geotools.util.NullProgressListener;
import org.opengis.feature.Feature;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;

import br.pucrio.bguberfain.onibus.model.graph.v2.SampleData;
import br.pucrio.bguberfain.onibus.reader.SampleDataReader;
import br.pucrio.bguberfain.onibus.reader.sequence.ThreadedSequenceGenerator;
import br.pucrio.bguberfain.onibus.reader.sequence.SequenceReader;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.sampledata.ChangeLineNumberBreaker;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.sampledata.LeastIntervalBreaker;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.SequenceBreaker;
import br.pucrio.bguberfain.onibus.reader.sequence.filter.DataFilter;
import br.pucrio.bguberfain.onibus_geotools.Util;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.index.SpatialIndex;
import com.vividsolutions.jts.index.strtree.STRtree;
import com.vividsolutions.jts.linearref.LinearLocation;

public class MainOSM {
	
    private static final double MAX_SEARCH_DISTANCE = 0.002 ;	// In degrees
    private static final double MIN_TRAVEL_DISTANCE = 0.001 ;	// In degrees

    private static final boolean USE_POSTGIS = true ;
    private static final boolean SAVE_TO_POSTGIS = true ;
    private static final boolean SAVE_TO_FILE = false ;
    
    private static final int MAX_DELAY_IN_SECONDS = 60 * 50 ;	// Ignore samples with delay above 50 minutes
	
	public static void main(String[] args) throws Exception {
		// Le os dados das linhas
		final SimpleFeatureType projectionType = createProjectionFeatureType() ;
		
		DataStore store ;
		SimpleFeatureSource featureSource ;
		if ( USE_POSTGIS ) {
			// Load line shapes (from postgis)
			Map<String, Object> params = new HashMap<String, Object>(); 
			params.put(JDBCDataStoreFactory.DBTYPE.key, "postgis"); 
			params.put(JDBCDataStoreFactory.HOST.key, "localhost"); 
			params.put(JDBCDataStoreFactory.PORT.key, 5432); 
			params.put(JDBCDataStoreFactory.SCHEMA.key, "public"); 
			params.put(JDBCDataStoreFactory.DATABASE.key, "bus-routes"); 
			params.put(JDBCDataStoreFactory.USER.key, "postgres"); 
			params.put(JDBCDataStoreFactory.PASSWD.key, "postgres"); 
			store = DataStoreFinder.getDataStore(params);
			featureSource = store.getFeatureSource("osm_rj_roads_segments");
		} else {
			// Load line shapes (from file)
			File routesShapeFile = new File("data/trajetos-shape/routes.shp");
			store = FileDataStoreFinder.getDataStore(routesShapeFile);
			featureSource = store.getFeatureSource("routes");
		}

        System.out.println("Loading shapesfile...");
        SimpleFeatureCollection featureCollection = featureSource.getFeatures() ;
        
        // Indexa os features
        final SpatialIndex geometryIndex = new STRtree();
        final int idPrefixLength = "osm_rj_roads_segments.".length() ;
        featureCollection.accepts((Feature feature) -> {
            SimpleFeature simpleFeature = (SimpleFeature) feature;
            LineString geom = (LineString) simpleFeature.getDefaultGeometry();
            Integer gid = new Integer(simpleFeature.getID().substring(idPrefixLength)) ;
            
            LocationIndexedLineWithGeometry locationIndex;
			try {
				locationIndex = new LocationIndexedLineWithGeometry(geom, projectionType.getCoordinateReferenceSystem());
			} catch (Exception e) {
				throw new RuntimeException(e) ;
			}
			
			locationIndex.setShapeId(gid);
			Envelope env = geom.getEnvelopeInternal();
			geometryIndex.insert(env, locationIndex);
        	
        }, new NullProgressListener() );
        
        // Create the index
        System.out.println("Creating index...");
        geometryIndex.query( new Envelope() ) ;
        
		// Cria o tabela de projeções
        SimpleFeatureStore projectionsDestination ;
        Transaction transaction ;
        if ( SAVE_TO_POSTGIS ) {
	        try {
	        	store.removeSchema(projectionType.getName().getLocalPart());
	        } catch( IllegalArgumentException e ) { } // Does nothing... relation simply does not exists
			store.createSchema(projectionType);
			// Cria store de saída das projeções
			transaction = new DefaultTransaction("create");
			projectionsDestination = (SimpleFeatureStore) store.getFeatureSource(projectionType.getName().getLocalPart());
			projectionsDestination.setTransaction(transaction);
        } else {
        	projectionsDestination = null ;
        	transaction = null ;
        }


        // Configure data loader
        Iterator<SampleData> samples = new GZipSampleDataReader();
        SequenceBreaker sequenceBreakers = new LeastIntervalBreaker(5 * 60 * 1000) ;	// Ignore samples that are 5min apart
        sequenceBreakers = new ChangeLineNumberBreaker(sequenceBreakers) ;	// Keep only same line number in the sequence
        sequenceBreakers = new BackwardTimeBreaker(sequenceBreakers) ;
        DataFilter filter = new WorkdayFilter() ;
        filter = new MaxDelayFilter(15 * 60, filter) ;
        filter = new DaylightSavingTimeFixFilter(filter) ;
        SequenceGenerator<SampleData> samplesSequence = new ThreadedSequenceGenerator2<>(sequenceBreakers, filter, samples, SampleData::getOrder) ;


		final long startTime = System.currentTimeMillis() ;
        System.out.println("Reading log file...");
        samplesSequence.readData(new SequenceReader<SampleData>() {
        	
        	AtomicLong processed = new AtomicLong(0) ;
        	SummaryStatistics statsForNumRoutes = new SynchronizedSummaryStatistics() ;
        	SummaryStatistics statsForDistance = new SynchronizedSummaryStatistics() ;
        	
        	ThreadLocal<List<SimpleFeature>> projectedFeaturesForThread = new ThreadLocal<List<SimpleFeature>>() {
        		@Override
        		protected List<SimpleFeature> initialValue() {
        			return new LinkedList<>();
        		}
        	} ;
        	
        	ThreadLocal<SimpleFeatureBuilder> featureBuilderForThread = new ThreadLocal<SimpleFeatureBuilder>() {
        		@Override
        		protected SimpleFeatureBuilder initialValue() {
        			return new SimpleFeatureBuilder(projectionType);
        		}
        	} ;
        	
        	ThreadLocal<GeometryFactory> geometryFactory = new ThreadLocal<GeometryFactory>() {
        		@Override
        		protected GeometryFactory initialValue() {
        			return JTSFactoryFinder.getGeometryFactory() ;
        		}
        	} ;

            Map<String, LocationIndexedLineWithGeometry> lastProjectionForOrder = new ConcurrentHashMap<>() ;

			@Override
			public void beginSequence(SampleData firstData) {
                lastProjectionForOrder.remove(firstData.getOrder()) ;
            }

			@Override
			public void sequence(SampleData lastData, SampleData newData)
			{
				try {
					String lineNumber = newData.getLineNumber() ;
					if ( "485".equals(lineNumber) && newData.getCoordinate().distance(lastData.getCoordinate()) > MIN_TRAVEL_DISTANCE ) {
						long localProcessed = processed.incrementAndGet() ;
						Envelope newDataEnvelope = new Envelope(newData.getCoordinate()) ;
						newDataEnvelope.expandBy(MAX_SEARCH_DISTANCE);
						@SuppressWarnings("unchecked")
						List<LocationIndexedLineWithGeometry> foundGeom = geometryIndex.query( newDataEnvelope ) ;
						
						// Log and save projectedFeatures
						List<SimpleFeature> projectedFeatures = projectedFeaturesForThread.get() ;
						if ( localProcessed % 5000 == 0 ) {
							System.out.printf("Procesed %10d samples at rate %5.2fsamples/s\n", localProcessed, localProcessed*1000.0/(System.currentTimeMillis()-startTime));
							System.out.printf("Mean # of candidate routes: %.4f (StdDev: %.4f)\n", statsForNumRoutes.getMean(), statsForNumRoutes.getStandardDeviation());
							System.out.printf("Mean distance (x1000): %.4f (StdDev: %.4f)\n", 1000*statsForDistance.getMean(), 1000*statsForDistance.getStandardDeviation());
							
							saveToPostGis(projectedFeaturesForThread.get(), projectionType, transaction, projectionsDestination) ;
						}
						statsForNumRoutes.addValue(foundGeom.size());


                        // Busca a rota mais provavel
                        double sequenceAngle = Angle.angle(lastData.getCoordinate(), newData.getCoordinate());
                        double minAngle = Double.MAX_VALUE;
                        double minDistance = Double.MAX_VALUE ;
                        double maxProb = Double.MAX_VALUE ;
						LocationIndexedLineWithGeometry minSegment = null ;
						Coordinate minProjectedLocation = null ;
						LinearLocation minNewDataLocation = null ;

                        LocationIndexedLineWithGeometry lastSegment = lastProjectionForOrder.get(lastData.getOrder()) ;

						for (LocationIndexedLineWithGeometry segment : foundGeom) {
		                	LinearLocation newDataLocation = segment.project(newData.getCoordinate());
		                	Coordinate newDataProjection = segment.extractPoint(newDataLocation) ;
		                	
		                	double newDataProjectionDistance = newDataProjection.distance(newData.getCoordinate()) ;
		                	statsForDistance.addValue(newDataProjectionDistance);
			                if ( newDataProjectionDistance < MAX_SEARCH_DISTANCE ) {
                                double projectedAngle ;
                                if (lastSegment != null) {
                                    projectedAngle = (lastSegment.getGeometryAngle() + segment.getGeometryAngle()) / 2 ;
                                } else {
                                    projectedAngle = segment.getGeometryAngle() ;
                                }
                                double diffAngle = Angle.diff(projectedAngle, sequenceAngle) ;

                                double prob = diffAngle / Math.PI * newDataProjectionDistance / MAX_SEARCH_DISTANCE ;

                                if( prob < maxProb ) {
                                    maxProb = prob ;
                                    minAngle = diffAngle ;
                                    minDistance = newDataProjectionDistance;
                                    minProjectedLocation = newDataProjection;
                                    minSegment = segment;
                                    minNewDataLocation = newDataLocation;
                                }
			                }
						}
						
						// Salva o ponto projetado
						if ( minProjectedLocation != null ) {
                            lastProjectionForOrder.put(newData.getOrder(), minSegment) ;
							// Calcula a data do sample
							Date date = new Date(newData.getTimestamp());
							
//							LineString lsRoute = (LineString) minRoute.getGeometry() ;
							
							double metersTraveled = minSegment.distanceInMetersUntil(minNewDataLocation) ;
							double percentTraveled = metersTraveled / minSegment.geometryLengthInMeters() ;

						    // ensure segment index is valid
//							int segmentIndex = minNewDataLocation.getSegmentIndex() ;
//						    int segIndex = segmentIndex;
//						    if (segmentIndex >= lsRoute.getNumPoints() - 1)
//						      segIndex = lsRoute.getNumPoints() - 2;

//						    Coordinate edgeFrom = lsRoute.getCoordinateN(segIndex);
//						    Coordinate edgeTo = lsRoute.getCoordinateN(segIndex + 1);
//						    
							Coordinate [] lsCoordinates = new Coordinate[] { newData.getCoordinate(), minProjectedLocation } ;
							// Distance to route in meters
							double distanceInMeters = JTS.orthodromicDistance(lsCoordinates[0], lsCoordinates[1], projectionType.getCoordinateReferenceSystem());

			                if ( SAVE_TO_POSTGIS ) {
			                	SimpleFeatureBuilder featureBuilder = featureBuilderForThread.get() ;
			                	
			                	LineString ls = geometryFactory.get().createLineString(lsCoordinates) ;
			                	featureBuilder.add(minSegment.getGeometry());   // the_geom
			                	featureBuilder.add(minDistance);				// distance
			                	featureBuilder.add(distanceInMeters);			// distance_in_meters
			                	
			                	featureBuilder.add(lineNumber);					// line_number
			                	featureBuilder.add(newData.getOrder());			// order_number
			                	featureBuilder.add(minSegment.getShapeId());		// gid
			                	featureBuilder.add(date);						// date
			                	featureBuilder.add(newData.getSpeed());			// instant_speed
			                	
			                	SimpleFeature lsProjection = featureBuilder.buildFeature(null);
			                	projectedFeatures.add(lsProjection) ;
			                }
		                	
		                	// Save to file
			                if( SAVE_TO_FILE ) {
			                	DataOutputStream projectionsOut = getOutputForDate(date) ;
//
//			                	synchronized(projectionsOut) {
//		                			// Timestamp
//		                			projectionsOut.writeLong(newData.getTimestamp());
//
//		                			// Identificador do onibus (ordem)
//		                			projectionsOut.writeInt(newData.getOrderId());
//
//		                			// Coordenadas originais
//		                			projectionsOut.writeDouble(newData.getCoordinate().y);	// latitude
//		                			projectionsOut.writeDouble(newData.getCoordinate().x);	// longitude
//
//		                			// Velocidade instantanea
//		                			projectionsOut.writeDouble(newData.getSpeed());
//
//		                			// Coordenadas projetadas
//		                			projectionsOut.writeDouble(minProjectedLocation.y);		// latitude
//		                			projectionsOut.writeDouble(minProjectedLocation.x);		// longitude
//
//		                			// Distancia da projecao (m)
//		                			projectionsOut.writeDouble(distanceInMeters);
//
//		                			// Id da linha e rota
//		                			projectionsOut.writeInt(newData.getLineNumberId());
//		                			projectionsOut.writeInt(minRoute.getShapeId()) ;
//
//		                			// Localização linear dentro do trajeto (antes e depois)
//		                			projectionsOut.writeInt(minNewDataLocation.getSegmentIndex());
//		                			projectionsOut.writeDouble(minNewDataLocation.getSegmentFraction());
//
//		                			// Dados relativos ao trajeto ja percorrido
//		                			projectionsOut.writeDouble(metersTraveled);
//		                			projectionsOut.writeDouble(percentTraveled);
//			                	}
			                }
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
					throw new RuntimeException(e) ;
				}
			}

			@Override
			public void endSequence(SampleData lastData) {} // Does nothing
        	
        });
        
        closeAllOutputs();
        
        if ( transaction != null ) {
        	transaction.close();
        }
        
        // Print filter stats
        DataFilter filterToPrint = filter ;
        System.out.println("Filter stats:");
        while (filterToPrint != null) {
        	System.out.printf(" Filter %s processed %d samples and filtered %d (%.2f%%)\n", filterToPrint.getClass().getName(),
        			filterToPrint.getTotalSamplesProcessed(), filterToPrint.getTotalSamplesFiltered(),
        			filterToPrint.getTotalSamplesFiltered() * 100.0 / filterToPrint.getTotalSamplesProcessed());
        	
        	filterToPrint = filterToPrint.getNextFilter() ;
        }
	}
	
	private static Map<String, DataOutputStream> outputForDate = new HashMap<String, DataOutputStream>() ;
	private static SimpleDateFormat outFileFormat = new SimpleDateFormat("yyyyMMdd");
	private static DataOutputStream getOutputForDate(Date date) throws IOException
	{
		String strDate = outFileFormat.format(date) ;
		
		DataOutputStream out = outputForDate.get(strDate) ;
		if ( out == null ) {
			synchronized(outputForDate) {
				// Try again in a synchronized scope
				out = outputForDate.get(strDate) ;
				if ( out == null )
				{
					File logFolder = new File(Util.getEnv("LOG_FOLDER")) ;
					File outFile = new File(logFolder, strDate + "-projected.dat") ;
					FileOutputStream fileOutStream = new FileOutputStream(outFile) ;
					BufferedOutputStream bufferOutStream = new BufferedOutputStream(fileOutStream, 20*1024*1024) ;
					out = new DataOutputStream(bufferOutStream) ;
					
					outputForDate.put(strDate, out);
					
					// Close older files opened...
					while ( outputForDate.size() > 3 )
					{
						String strToClose = outputForDate.keySet().stream().min((a,b) -> a.compareTo(b)).get() ;
						DataOutputStream outToClose = outputForDate.get(strToClose) ;
						outToClose.flush();
						outToClose.close();
						outputForDate.remove(strToClose) ;
					}
				}				
			}
		}
		
		return out ;
	}
	
	private static void closeAllOutputs()
	{
		outputForDate.values().forEach((out) -> {
			try {
				out.close() ;
			} catch (Exception e) {
				e.printStackTrace();
			}	
		});
	}
	
    private static SimpleFeatureType createProjectionFeatureType() {
        SimpleFeatureTypeBuilder builder = new SimpleFeatureTypeBuilder();
        builder.setName("projections_osm_roads_optimized");
        builder.setCRS(DefaultGeographicCRS.WGS84); // <- Coordinate reference system

        // add attributes in order
        builder.add("the_geom", LineString.class);
        builder.add("distance", Double.class);
        builder.add("distance_in_meters", Double.class);
        builder.add("line_number", String.class);
        builder.add("order_number", String.class);
        builder.add("gid", Integer.class);
        builder.add("date", Date.class);
        builder.add("instant_speed", Double.class);

        // build the type
        return builder.buildFeatureType();
    }

    private static void saveToPostGis(List<SimpleFeature> projectedFeatures, SimpleFeatureType projectionType, Transaction transaction, SimpleFeatureStore projectionsDestination) throws IOException
    {
		if ( SAVE_TO_POSTGIS ) {
			if ( projectedFeatures.size() > 0 ) {
				SimpleFeatureCollection collection = new ListFeatureCollection(projectionType, projectedFeatures);
				System.out.printf("Adding %d features to database...\n", collection.size());
				projectionsDestination.addFeatures(collection);
				projectedFeatures.clear();
			}
			try {
				synchronized(transaction) {
					System.out.println("Commiting...");
					transaction.commit();
				}
			} catch (Exception problem) {
				problem.printStackTrace();
				transaction.rollback();
				throw new RuntimeException(problem);
			}
		}
    }
}
