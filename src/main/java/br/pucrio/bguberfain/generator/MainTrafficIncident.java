package br.pucrio.bguberfain.generator;

import br.pucrio.bguberfain.onibus.gui.GeoImage;
import br.pucrio.bguberfain.onibus.model.EdgeStats;
import br.pucrio.bguberfain.onibus.model.EdgeStatsGenerator;
import br.pucrio.bguberfain.onibus.model.TrafficDataPeriod;
import br.pucrio.bguberfain.onibus.model.graph.v2.SpeedColors;
import br.pucrio.bguberfain.onibus.util.AutoInstanceMap;
import br.pucrio.bguberfain.onibus.util.Util;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.geom.LineSegment;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

/**
 * Created by bguberfain on 3/20/2015.
 */
public class MainTrafficIncident {
    private static final SimpleDateFormat inputDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm") ;


    private static final Logger logger = LogManager.getLogger(MainTrafficGenerateData.class);
    private final static long MS_IN_A_MINUTE = 60 * 1000 ;
    private final static long DATE_RANGE_FROM_EVENT = 120 * MS_IN_A_MINUTE ;
    private final static int IMAGE_WIDTH = 1024 ;
    private final static float MAX_STROKE_WIDTH = 2.0f ;
    private final static float MIN_STROKE_WIDTH = 0.5f ;
    private final static double MAX_SPEED = 80 / 3.6 ;     // km/h in m/s
    private final static double MIN_SPEED = 20 / 3.6 ;     // Used in stroke calculation

    private final static AutoInstanceMap<LineSegment, LineSegment> segmentCorrectPosition = new AutoInstanceMap<>(new HashMap<>(), LinkedList::new) ;

    public static void main(String [] args) throws Exception {
        // Program input
        Coordinate centerLocation = new Coordinate(-43.203608, -22.908914);
        String dateTimeStr = "19/03/2015 08:22";

        // Prepare input
        Date eventDateTime = inputDateFormat.parse(dateTimeStr) ;
        Date fromDate = new Date(eventDateTime.getTime() - DATE_RANGE_FROM_EVENT) ;
        Date toDate = new Date(eventDateTime.getTime() + DATE_RANGE_FROM_EVENT) ;
        Envelope eventRegion = new Envelope(centerLocation) ;
        eventRegion.expandBy(0.01);

        // Configure loader...
        EdgeStatsGenerator loader = new EdgeStatsGenerator() ;
        loader.setDateRange(fromDate, toDate);
        loader.setRegion(eventRegion);

        // Start loading...
        Map<LineSegment, EdgeStats> result = loader.process() ;

        // Detect two-way edges
        int countReverse = 0 ;
        for (LineSegment segment : result.keySet()) {
            LineSegment reverseSegment = new LineSegment(segment.p1, segment.p0) ;

            if(result.containsKey(reverseSegment)) {
                countReverse++ ;

                // Move the segment to the "right" (-pi/2)
                double moveAngle = segment.angle() - Math.PI/2 ;
                double plusX = Math.cos(moveAngle) * 0.00005;
                double plusY = Math.sin(moveAngle) * 0.00005;

                Coordinate from = segment.p0 ;
                Coordinate to = segment.p1 ;

                LineSegment lsMoved = new LineSegment(from.x+plusX, from.y+plusY, to.x+plusX, to.y+plusY) ;

                segmentCorrectPosition.putValue(segment, lsMoved) ;
            } else {
                segmentCorrectPosition.putValue(segment, segment) ;
            }
        }
        logger.info("Found {} two-way segments", countReverse);

        // Draw image
        File graphFolder = new File(Util.getEnv("GRAPH_DAT_FOLDER")) ;
        Map<LineSegment, EdgeStats> allStats = EdgeStats.loadEdgeStatsByLineString(graphFolder, TrafficDataPeriod.DAILY, 15) ;
        int fromTimeSlice = EdgeStatsGenerator.timeSliceForTimeStamp(fromDate.getTime()) ;
        int toTimeSlice = EdgeStatsGenerator.timeSliceForTimeStamp(toDate.getTime()) ;

        for( int i = fromTimeSlice; i<= toTimeSlice; i++) {
            // Generate the "absolute" image...
            GeoImage img = new GeoImage(eventRegion, IMAGE_WIDTH) ;
            drawGraphOnImages(result, null, img, i) ;
            File outFile = new File("F:\\www-pub\\processed-data\\incident_" + EdgeStatsGenerator.hourForSegment(i) + ".png") ;
            ImageIO.write(img.getImage(), "png", outFile) ;

            // Generate the "relative" image...
            drawGraphOnImages(result, allStats, img, i) ;
            outFile = new File("F:\\www-pub\\processed-data\\incident_relative_" + EdgeStatsGenerator.hourForSegment(i) + ".png") ;
            ImageIO.write(img.getImage(), "png", outFile) ;

        }
    }


    private static void drawGraphOnImages(Map<LineSegment, EdgeStats> graph, Map<LineSegment, EdgeStats> allStats, GeoImage img, int idx)
    {
        img.setColor(new Color(0xfaf7ef));
        img.clear();
        for (Map.Entry<LineSegment, EdgeStats> edge : graph.entrySet()) {
            EdgeStats stats = edge.getValue();
            Color color;
            if (stats.numSamples[idx] > 4) {
                if (allStats == null ) {
                    double currentSpeed = stats.meanByTimeSlice[idx];
                    double maxSpeed = stats.maxSpeed;
                    double relativeSpeed = currentSpeed / maxSpeed;
                    if (relativeSpeed > 1) relativeSpeed = 1;
                    float stroke = (float) ((MAX_STROKE_WIDTH - MIN_STROKE_WIDTH) * (maxSpeed - MIN_SPEED) / (MAX_SPEED - MIN_SPEED) + MIN_STROKE_WIDTH);
                    if (stroke < MIN_STROKE_WIDTH) {
                        stroke = MIN_STROKE_WIDTH;
                    }
                    img.setStrokeWidth(stroke);

                    color = SpeedColors.speedForRelativeOrAbsolute(currentSpeed, relativeSpeed);
                } else {
                    EdgeStats allEdgeStats = allStats.get(edge.getKey()) ;
                    double relativeSpeed = stats.meanByTimeSlice[idx] / allEdgeStats.meanByTimeSlice[idx] ;

                    SpeedColors speedColors = SpeedColors.MODERATE ;
                    if ( relativeSpeed > 1.2 ) {
                        speedColors = SpeedColors.LIGHT ;
                    } else if ( relativeSpeed < 0.8 ) {
                        speedColors = SpeedColors.HEAVY ;
                    } else if ( relativeSpeed < 0.5 ) {
                        speedColors = SpeedColors.VERY_HEAVY;
                    }

                    color = speedColors.getColor();
                }
            } else {
                img.setStrokeWidth(MIN_STROKE_WIDTH);
                color = new Color(0xc7cfc4) ;
            }
            img.setColor(color);
            drawEdgeOnImage(edge.getKey(), img);
        }
    }

    private static int colorFromRGB(float r, float g, float b) {
        return ((int) (0xff * b)) | (((int) (0xff * g)) << 8 ) | (((int) (0xff * r)) << 16 ) ;
    }

    private static void drawEdgeOnImage(LineSegment edge, GeoImage img)
    {
        for (LineSegment segment : segmentCorrectPosition.get(edge)) {
            img.drawLine(segment.p0, segment.p1) ;
        }
    }

}
