package br.pucrio.bguberfain.generator;

import java.util.ArrayList;
import java.util.List;

import com.vividsolutions.jts.algorithm.Angle;
import com.vividsolutions.jts.geom.LineSegment;
import org.geotools.geometry.jts.JTS;
import org.geotools.referencing.GeodeticCalculator;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.opengis.referencing.operation.TransformException;

import com.vividsolutions.jts.geom.CoordinateSequence;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.linearref.LinearLocation;
import com.vividsolutions.jts.linearref.LocationIndexedLine;

/**
 * @author bguberfain
 * 
 * LocationIndexedLine with access to original geometry
 *
 */
public class LocationIndexedLineWithGeometry extends LocationIndexedLine {
	
	private LineString geometry;
	String lineNumber ;
	String lineName ;
	Integer shapeId ;
	List<Double> distanceOfSegmentsToOrigin ;
	CoordinateReferenceSystem crs ;
    double geometryAngle ;

	public LocationIndexedLineWithGeometry(LineString linearGeom, CoordinateReferenceSystem crs) throws TransformException {
		super(linearGeom);

        if (linearGeom.getNumPoints() != 2) {
            throw new IllegalArgumentException("LocationIndexedLineWithGeometry somente suporta LineString com 2 pontos") ;
        }
		
		this.geometry = linearGeom ;
		this.crs = crs ;

        geometryAngle = Angle.angle(geometry.getCoordinateN(0), geometry.getCoordinateN(1)) ;
		
		// Calcula vetor de distancia da origem ate os pontos
		CoordinateSequence points = geometry.getCoordinateSequence() ;
		
		// Calcula as distancias de cada ponto ate a origem
		distanceOfSegmentsToOrigin = new ArrayList<Double>(points.size()-1) ;
		GeodeticCalculator geoDistance = new GeodeticCalculator(crs) ;
		double currDistance = 0.0 ;
		geoDistance.setStartingPosition(JTS.toDirectPosition(points.getCoordinate(0), crs));
		distanceOfSegmentsToOrigin.add(currDistance) ;
		for( int i=1; i<points.size(); i++ ) {
			geoDistance.setDestinationPosition(JTS.toDirectPosition(points.getCoordinate(i), crs));
			
			currDistance += geoDistance.getOrthodromicDistance() ;
			
			distanceOfSegmentsToOrigin.add(currDistance) ;
			
			geoDistance.setStartingPosition(geoDistance.getDestinationPosition());
		}
	}
	
	public String getLineNumber() {
		return lineNumber;
	}

	public void setLineNumber(String lineNumber) {
		this.lineNumber = lineNumber;
	}

	public String getLineName() {
		return lineName;
	}

	public void setLineName(String lineName) {
		this.lineName = lineName;
	}

	public Integer getShapeId() {
		return shapeId;
	}

	public void setShapeId(Integer shapeId) {
		this.shapeId = shapeId;
	}

	public LineString getGeometry() {
		return geometry;
	}

    public double getGeometryAngle() {
        return geometryAngle;
    }

    public double geometryLengthInMeters()
	{
		return distanceOfSegmentsToOrigin.get(geometry.getCoordinateSequence().size()-1) ;
	}
	
	public double distanceInMetersUntil(LinearLocation location)
	{
		
		// Caso trivial: segmento eh o ultimo -> retorna a distancia da rota inteira
		if ( location.getSegmentIndex() == geometry.getCoordinateSequence().size()-1 ) {
			return geometryLengthInMeters() ;
		}
		
		double distanceFromLocationSegment = distanceOfSegmentsToOrigin.get(location.getSegmentIndex()) ;
		double segmentDistance = (distanceOfSegmentsToOrigin.get(location.getSegmentIndex()+1)-distanceFromLocationSegment) * location.getSegmentFraction() ;
		
		return distanceFromLocationSegment + segmentDistance ;
	}
}