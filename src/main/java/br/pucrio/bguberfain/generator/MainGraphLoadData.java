package br.pucrio.bguberfain.generator;

import br.pucrio.bguberfain.onibus.gui.XYGraph;
import br.pucrio.bguberfain.onibus.model.graph.v2.XYSample;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.File;
import java.io.IOException;

/**
 * Created by Bruno on 22/02/2015.
 */
public class MainGraphLoadData extends MainGraphBase {
    public static void main(String[] args) throws IOException {
        File file = null;
        if (args.length == 1) {
            file = new File(args[0]);
        } else {
            final JFileChooser fc = new JFileChooser();
            fc.setFileFilter(new FileNameExtensionFilter("XYGraph Data", "xyd"));
            int ret = fc.showOpenDialog(null);
            if (ret == JFileChooser.APPROVE_OPTION) {
                file = fc.getSelectedFile();
            }
        }
        XYGraph graph = createAndShowGUIFromFile("Graph: " + file.getPath(), file);
        XYSample.XYSampleBoundary boundary = graph.getBoundary() ;

        if (graph.getyAxis().equals(XYGraph.XYGraphAxisType.METERS_PER_SECOND)) {
            boundary = new XYSample.XYSampleBoundary(boundary.getMinX(), boundary.getMinY(), boundary.getMaxX(), 100 / 3.6) ;
        }

        graph.setBoundary(boundary);
        graph.setFontSize(20);
        graph.setSequenceAlpha(0x10);
    }
}
