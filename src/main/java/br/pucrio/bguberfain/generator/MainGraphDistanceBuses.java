package br.pucrio.bguberfain.generator;

import br.pucrio.bguberfain.onibus.gui.XYGraph;
import br.pucrio.bguberfain.onibus.model.graph.v2.*;
import br.pucrio.bguberfain.onibus.reader.GZipSampleDataReader;
import br.pucrio.bguberfain.onibus.reader.sequence.SequenceGenerator;
import br.pucrio.bguberfain.onibus.reader.sequence.SequenceReader;
import br.pucrio.bguberfain.onibus.reader.sequence.SimpleSequenceGenerator;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.SequenceBreaker;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.sampledata.BackwardTimeBreaker;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.sampledata.ChangeLineNumberBreaker;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.sampledata.LeastIntervalBreaker;
import br.pucrio.bguberfain.onibus.reader.sequence.filter.DataFilter;
import br.pucrio.bguberfain.onibus.reader.sequence.filter.sampledata.DaylightSavingTimeFixFilter;
import br.pucrio.bguberfain.onibus.reader.sequence.filter.sampledata.LineNumberFilter;
import br.pucrio.bguberfain.onibus.reader.sequence.filter.sampledata.WorkdayFilter;
import br.pucrio.bguberfain.onibus.reader.transformer.SampleDataSequence2ProjectionSequence;
import br.pucrio.bguberfain.onibus.util.AutoInstanceMap;
import br.pucrio.bguberfain.onibus.util.NetworkTopologyDS;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Bruno on 25/01/2015.
 */
public class MainGraphDistanceBuses extends MainGraphBase {
    private static final Logger logger = LogManager.getLogger(MainGraphDistanceBuses.class);

    /*
    Max shapes after 8740001 samples:
        17376210, line 638 has 132573 entries
        17378017, line 371 has 113376 entries
        17613062, line 371 has 100290 entries
        17389243, line 397 has 93517 entries  - 397-CAMPO GRANDE X CARIOCA
        17726374, line 624 has 90469 entries
        17541675, line 638 has 86068 entries
        17339354, line 393 has 85308 entries
        17416262, line 607 has 80032 entries
        17378253, line 753 has 75054 entries
        17362666, line 607 has 74823 entries

    - Linha: 2336-CAMPO GRANDE X CASTELO (VIA AV. BRASIL)
        - Sentido Campo Grande: 17594567 (ou 17375751)
        - Sentido Centro: 17761930 (ou 17556438)
    */
    private final static Integer SHAPE_ID = 17619292 ;
    private final static long MIN_DISTANCE_FROM_BORDER = 500 ;
    private final static double MAX_SPEED = 100 / 3.6 ;     // 100 km/h in ms/s
    private final static double MAX_DISTANCE_BETWEEN_SEQUENCES = MAX_SPEED * 2 * 60 ; // Distance with max speed in 2 minutes

    public static void main(String[] args) throws Exception {
        long start = System.currentTimeMillis() ;
        XYGraph xyGraph = MainGraphBase.createAndShowGUI("Space-Speed Graph", XYGraph.XYGraphAxisType.METERS, XYGraph.XYGraphAxisType.METERS);

        Map<Integer, Route> routesByShapeId = NetworkTopologyDS.getInstance().getNetworkByShapeId(null, true) ;
        Route route = routesByShapeId.get(SHAPE_ID) ;
        String lineNumber = route.getLineNumber() ;
        logger.info("Considering line # {} for shape id {}", lineNumber, SHAPE_ID);
        logger.info("MAX_DISTANCE_BETWEEN_SEQUENCES = {}", MAX_DISTANCE_BETWEEN_SEQUENCES) ;

        // Add bus-stops strips
        for (BusStop busStop : route.getBusStops()) {
            xyGraph.addVerticalMark(busStop.getDistanceFromBegin());
        }

        // Update graph boundary to full route length
        XYSample.XYSampleBoundary graphBoundary = new XYSample.XYSampleBoundary(0, 0, route.geometryLengthInMeters(), 0) ;
        xyGraph.setBoundary(graphBoundary);


        // Create Sample Reader...
        GZipSampleDataReader samples = new GZipSampleDataReader("2015.*\\.txt\\.gz");
        samples.setIncludeAnnotations(true);
        SequenceBreaker sequenceBreakers = new LeastIntervalBreaker(5 * 60 * 1000) ;	// Ignore samples that are 5min apart
        sequenceBreakers = new ChangeLineNumberBreaker(sequenceBreakers) ;	// Keep only same line number in the sequence
        sequenceBreakers = new BackwardTimeBreaker(sequenceBreakers) ;
        DataFilter filter = new WorkdayFilter() ;
        filter = new DaylightSavingTimeFixFilter(filter) ;
        filter = new LineNumberFilter(filter, lineNumber) ;
        SequenceGenerator<SampleData> samplesSequence = new SimpleSequenceGenerator<>(sequenceBreakers, filter, samples, SampleData::getOrder, SampleData::isAnnotation) ;

        // Convert Sample to Projection (snapping)
        SequenceGenerator<ProjectionInfo<SampleData>> projectionSequence = new SampleDataSequence2ProjectionSequence(Collections.singleton(route), samplesSequence) ;

        logger.info("Starting loading data...");
        Map<String, Double> positionByBus = new HashMap<>() ;
        final double routeLength = route.geometryLengthInMeters() ;
        projectionSequence.readData(new SequenceReader<ProjectionInfo<SampleData>>()
        {
            AutoInstanceMap<String, XYSample> distancesByBus = new AutoInstanceMap<>(new HashMap<>(), LinkedList::new);

            @Override
            public void beginAnnotation(ProjectionInfo<SampleData> annotation) {
                if (!positionByBus.isEmpty()) {
                    List<Map.Entry<String, Double>> sortedBusPosition = positionByBus.entrySet().stream()
                            .sorted(Comparator.comparing(Map.Entry::getValue))  // Sort by value
                            .collect(Collectors.toList());                      // Return as a list

                    for (int i = 1; i < sortedBusPosition.size(); i++) {
                        Map.Entry<String, Double> lastBus = sortedBusPosition.get(i - 1);
                        Map.Entry<String, Double> currentBus = sortedBusPosition.get(i);

                        double distanceBetweenCurrentAndLast = currentBus.getValue() - lastBus.getValue();

                        if (distanceBetweenCurrentAndLast<0) {
                            throw new RuntimeException("Error on sorting list by value") ;
                        }
                        XYSample sequenceSample = new XYSample(currentBus.getValue(), distanceBetweenCurrentAndLast, MainGraphBase.timeToColorPercent(annotation.getLocationData().getTimestamp()));

                        distancesByBus.putValue(currentBus.getKey(), sequenceSample);

                    }
                    positionByBus.clear();
                }
            }

            @Override
            public void beginSequence(ProjectionInfo<SampleData> newData) {
            }

            @Override
            public void sequence(ProjectionInfo<SampleData> lastData, ProjectionInfo<SampleData> newData) {
                double distanceOnRoute = newData.distanceTraveledOnRoute() ;
                if (distanceOnRoute > MIN_DISTANCE_FROM_BORDER) {
                    positionByBus.put(newData.getLocationData().getOrder(), distanceOnRoute);
                } if( distanceOnRoute > routeLength - MIN_DISTANCE_FROM_BORDER ) {
                    endSequence(newData);
                }
            }

            @Override
            public void endSequence(ProjectionInfo<SampleData> lastData) {
                String sampleOrder = lastData.getLocationData().getOrder();
                positionByBus.remove(sampleOrder);

                Deque<XYSample> samples = (Deque<XYSample>) distancesByBus.get(sampleOrder);

                if (samples != null && samples.size() > 1) {
                    xyGraph.addSampleSequence(samples);
                }

                distancesByBus.remove(sampleOrder);
            }
        } );

        long end = System.currentTimeMillis();

        logger.info("Finished loading all data in {} s", (end - start) / 1000);
    }


    public class BusesOnRoute {
        Map<String, Double> positionByBus = new HashMap<>() ;

        public void updateBusPosition(SampleData sample, double position) {
            positionByBus.put(sample.getOrder(), position) ;
        }
    }
}
