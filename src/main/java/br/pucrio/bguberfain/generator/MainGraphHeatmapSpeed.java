package br.pucrio.bguberfain.generator;

import br.pucrio.bguberfain.onibus.gui.XYGraph;
import br.pucrio.bguberfain.onibus.model.EdgeStatsExtended;
import br.pucrio.bguberfain.onibus.model.MapAverage;
import br.pucrio.bguberfain.onibus.model.TrafficDataPeriod;
import br.pucrio.bguberfain.onibus.model.graph.v2.*;
import br.pucrio.bguberfain.onibus.reader.GZipSampleDataReader;
import br.pucrio.bguberfain.onibus.reader.sequence.SequenceGenerator;
import br.pucrio.bguberfain.onibus.reader.sequence.SequenceReader;
import br.pucrio.bguberfain.onibus.reader.sequence.SimpleSequenceGenerator;
import br.pucrio.bguberfain.onibus.reader.sequence.ThreadedSequenceGenerator2;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.SequenceBreaker;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.sampledata.BackwardTimeBreaker;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.sampledata.ChangeDayBreaker;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.sampledata.ChangeLineNumberBreaker;
import br.pucrio.bguberfain.onibus.reader.sequence.breaker.sampledata.LeastIntervalBreaker;
import br.pucrio.bguberfain.onibus.reader.sequence.filter.DataFilter;
import br.pucrio.bguberfain.onibus.reader.sequence.filter.sampledata.DaylightSavingTimeFixFilter;
import br.pucrio.bguberfain.onibus.reader.sequence.filter.sampledata.LineNumberFilter;
import br.pucrio.bguberfain.onibus.reader.sequence.filter.sampledata.WorkdayFilter;
import br.pucrio.bguberfain.onibus.reader.transformer.SampleDataSequence2ProjectionSequence;
import br.pucrio.bguberfain.onibus.util.AutoInstanceMap;
import br.pucrio.bguberfain.onibus.util.NetworkTopologyDS;
import br.pucrio.bguberfain.onibus.util.Util;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.LineSegment;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Bruno on 25/01/2015.
 */
public class MainGraphHeatmapSpeed extends MainGraphBase {
    private static final Logger logger = LogManager.getLogger(MainGraphHeatmapSpeed.class);

    private final static long MAX_INTERVAL_BETWEEN_SEQUENCES = 60 * 15 ; // 15 minutes in seconds
    private final static double MAX_SPEED = 40 / 3.6 ;     // 100 km/h in ms/s
    private final static double MAX_DISTANCE_BETWEEN_SEQUENCES = MAX_SPEED * 2 * 60 ; // Distance with max speed in 2 minutes

    private static final TrafficDataPeriod PERIOD = TrafficDataPeriod.DAILY ;
    private static final int PRECISION_MINUTES = 15 ;

    public static void main(String [] args) throws Exception {
        Stream<Integer> keysToLoad ;
        Map<Integer, Route> routesByShapeId = NetworkTopologyDS.getInstance().getNetworkByShapeId(null, true) ;

        String strShapeIdToLoad = Util.getEnv("SHAPE_TO_LOAD", false);
        if(strShapeIdToLoad != null) {
            keysToLoad = Arrays.stream(strShapeIdToLoad.split(",")).map(Integer::new) ;
        } else {
            keysToLoad = routesByShapeId.keySet().stream() ;
        }
        keysToLoad.sorted()
                .parallel()
                .forEach((shapeId) -> {
                    try {
                        Route route = routesByShapeId.get(shapeId) ;
                        String lineNumber = route.getLineNumber() ;
                        Collection<Route> routesForLineNumber = routesByShapeId.entrySet().stream()
                                .filter( item -> item.getValue().getLineNumber().equals(lineNumber) )
                                .map(Map.Entry::getValue)
                                .collect(Collectors.toList()) ;
                        MainGraphHeatmapSpeed.generateGraph(route, routesForLineNumber, ".*\\.txt\\.gz", new LineNumberFilter(lineNumber), 3, null);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });
    }

    public static XYGraph generateGraph(Route route, Collection<Route> routesForLineNumber, String datePattern, DataFilter filter, int goodRate, SequenceGenerator<ProjectionInfo<SampleData>> projectionSequence) throws Exception {
        long start = System.currentTimeMillis() ;
        XYGraph xyGraph = MainGraphBase.createAndShowGUI("Distance heatmap Graph", XYGraph.XYGraphAxisType.METERS, XYGraph.XYGraphAxisType.SECONDS);
        String saveToFolder = Util.getEnv("SAVE_TO_FOLDER", false);

        File graphFolder = new File(Util.getEnv("GRAPH_DAT_FOLDER")) ;
        Map<LineSegment, EdgeStatsExtended> edgeStats = EdgeStatsExtended.loadEdgeStatsByLineString(graphFolder, PERIOD, PRECISION_MINUTES) ;

        final double routeLength = route.geometryLengthInMeters() ;
        String lineNumber = route.getLineNumber() ;

        logger.info("Considering line # {} for shape id {}", lineNumber, route.getShapeId());
        logger.info("MAX_DISTANCE_BETWEEN_SEQUENCES = {}", MAX_DISTANCE_BETWEEN_SEQUENCES) ;

        // Add bus-stops strips
        for (BusStop busStop : route.getBusStops()) {
            xyGraph.addVerticalMark(busStop.getDistanceFromBegin());
        }

        // Update graph boundary to full route length
        XYSample.XYSampleBoundary graphBoundary = new XYSample.XYSampleBoundary(0, 0, route.geometryLengthInMeters(), SECONDS_IN_A_DAY) ;
        xyGraph.setBoundary(graphBoundary);
        xyGraph.setShouldUpdateBoundary(false);

        // Configure XY Graph
        xyGraph.setDrawAsDots(true);
        xyGraph.setBackgroundColor(Color.DARK_GRAY);
        xyGraph.setSequenceAlpha(255);
        xyGraph.setFontSize(20);

        // Configure legends
        int numLegends = 20 ;
        SortedMap<Float, String> legend = new TreeMap<>() ;
        for(int i=0; i<=numLegends; i++) {
            legend.put(i / (float) numLegends, String.format("%.0fkm/h", i * MAX_SPEED * 3.6 / numLegends)) ;
        }
        xyGraph.setLegend(legend);
        xyGraph.setLegendForColorPercent(percent -> String.format("%.0fkm/h", percent * MAX_SPEED * 3.6));

        final Color[] heatColors = new Color[] {
                Color.WHITE,
                Color.RED,
                Color.YELLOW,
                Color.GREEN.darker(),
                Color.CYAN,
                Color.BLUE,
                new Color(0, 0, 0x33)} ;
        xyGraph.setColorProvider((float percent, int alpha) -> Util.gradientColor(heatColors, percent, alpha / 255.0f) );

        if(projectionSequence == null) {

            // Create Sample Reader...
            // Before BRS
            //        Iterator<SampleData> samples = new GZipSampleDataReader("2014((06[0-9][0-9])|(07[0-9][0-9])|(080[1-8]))\\.txt\\.gz");
            // After BRS
            //        Iterator<SampleData> samples = new GZipSampleDataReader("2014((08[1-3][0-9])|(09[0-9][0-9])|((100[0-9])))\\.txt\\.gz");
            //        GZipSampleDataReader samples = new GZipSampleDataReader();

            GZipSampleDataReader samples = new GZipSampleDataReader(datePattern);
            //        samples.setIncludeAnnotations(true);
            SequenceBreaker sequenceBreakers = new LeastIntervalBreaker(5 * 60 * 1000);    // Ignore samples that are 5min apart
            sequenceBreakers = new ChangeDayBreaker(sequenceBreakers);         // Break samples between 0h
            sequenceBreakers = new ChangeLineNumberBreaker(sequenceBreakers);    // Keep only same line number in the sequence
            sequenceBreakers = new BackwardTimeBreaker(sequenceBreakers);
            filter = new WorkdayFilter(filter);
            filter = new DaylightSavingTimeFixFilter(filter);

            SequenceGenerator<SampleData> samplesSequence = new ThreadedSequenceGenerator2<>(sequenceBreakers, filter, samples, SampleData::getOrder) ;

            // Convert Sample to Projection (snapping)
            projectionSequence = new SampleDataSequence2ProjectionSequence(routesForLineNumber, samplesSequence) ;
        }


        logger.info("Starting loading data...");

        // Calculate heatmap "pixel" size
        float yResolutionInMinutes = 10 ;
        float yFactor = 24 * 60 / yResolutionInMinutes ;
        float graphHeight = 712+152 ;
        float graphWidth = 1696 ;
        double yReduceFactor = SECONDS_IN_A_DAY / yFactor ;
        xyGraph.setStrokeWidth(graphHeight / yFactor) ;
        double xReduceFactor = routeLength / (graphWidth / graphHeight * yFactor) ;

        /*
        // Calculate the expected speed at each point on route
        logger.info("Computing max speed for each segment length");
        int numSpeedDivisions = (int) ((routeLength / xReduceFactor) + 1);
        double maxSpeedAtLengthPercent[] = new double[numSpeedDivisions+1] ;

        for(int i=0; i<=numSpeedDivisions; i++) {
            double distanceOnRoute = routeLength * i / numSpeedDivisions ;
            final Coordinate position = route.coordinateAtLength(distanceOnRoute) ;
            Map.Entry<LineSegment, EdgeStatsExtended> minSegment = edgeStats.entrySet().parallelStream().min((a, b) -> Double.compare(a.getKey().distance(position), b.getKey().distance(position)) ).get() ;
            maxSpeedAtLengthPercent[i] = minSegment.getValue().percentile98 ;
        }

        // Fix bogus speeds (NaN)
        int i = 0 ;
        while (i <= numSpeedDivisions) {
            int firstNaN = i ;
            int firstValid = i ;
            while( Double.isNaN(maxSpeedAtLengthPercent[firstValid]) ) {
                firstValid++ ;
            }

            if (firstNaN != firstValid) {
                double from = maxSpeedAtLengthPercent[firstNaN-1] ;
                double to = maxSpeedAtLengthPercent[firstValid] ;
                double diff = to-from ;
                double num = firstValid-firstNaN ;
                for(int j=firstNaN; j<firstValid; j++) {
                    maxSpeedAtLengthPercent[j] = from + diff * (j-firstValid) / num ;
                }
            }

            i = firstValid+1 ;
        }

        for(i=0; i<=numSpeedDivisions; i++) {
            double distanceOnRoute = routeLength * i / numSpeedDivisions;
            logger.info("Max speed at {}m is {}", distanceOnRoute, maxSpeedAtLengthPercent[i]);
        }
*/

        // Create average map with +1 border on each side because we must include both boundaries
        MapAverage mapAverage = new MapAverage((int) (routeLength / xReduceFactor) + 1, (int) (SECONDS_IN_A_DAY / yReduceFactor) + 1) ;
        logger.info("Starting loading data...");
        Map<String, Deque<XYSample>> spaceTimeSequenceByOrder = new HashMap<>() ;
        projectionSequence.readData(new SequenceReader<ProjectionInfo<SampleData>>() {
            @Override
            public void beginSequence(ProjectionInfo<SampleData> newData) {
                if (newData.getRoute().equals(route)) {
                    Deque<XYSample> newSequence = new LinkedList<>();
                    SampleData newSample = newData.getLocationData();

                    spaceTimeSequenceByOrder.put(newSample.getOrder(), newSequence);
                }
            }

            @Override
            public void sequence(ProjectionInfo<SampleData> lastData, ProjectionInfo<SampleData> newData) {
                if (newData.getRoute().equals(route) && lastData.getRoute().equals(route)) {
                    SampleData newSample = newData.getLocationData();
                    SampleData lastSample = lastData.getLocationData();
                    int timeDiff = (int) ((newSample.getTimestamp() - lastSample.getTimestamp()) / 1000);
                    if (timeDiff > 0 && timeDiff < MAX_INTERVAL_BETWEEN_SEQUENCES) {
                        double distanceDiff = newData.distanceInMetersTraveledSince(lastData);

                        if (distanceDiff > 0 && distanceDiff < MAX_DISTANCE_BETWEEN_SEQUENCES) {
                            Deque<XYSample> currentSequence = spaceTimeSequenceByOrder.get(newSample.getOrder());

                            if (currentSequence == null) {
                                return;
                            }

                            double distanceTraveledSinceBeginRoute = newData.getRoute().distanceInMetersUntil(newData.getLocation());

                            double distanceTraveledSinceLastSample = 0;

                            if (!currentSequence.isEmpty()) {
                                double lastSampleDistanceTraveledSinceBeginRoute = currentSequence.peekLast().getX();

                                distanceTraveledSinceLastSample = distanceTraveledSinceBeginRoute - lastSampleDistanceTraveledSinceBeginRoute;
                            }

                            if (distanceTraveledSinceBeginRoute > 0 && distanceTraveledSinceLastSample >= 0 && distanceTraveledSinceLastSample < MAX_DISTANCE_BETWEEN_SEQUENCES) {
                                double avgSpeed = distanceDiff / timeDiff;

                                if (avgSpeed < MAX_SPEED) {
                                    double distance = distanceTraveledSinceBeginRoute ;
//                                    int speedPosition = ((int) (distance * numSpeedDivisions / routeLength)) ;
//                                    double maxSpeedAtSample = maxSpeedAtLengthPercent[speedPosition];
                                    float color = (float) (avgSpeed / MAX_SPEED);
                                    XYSample sequenceSample = new XYSample(distance, newSample.getTimestamp() % MS_IN_A_DAY / 1000, color);
                                    mapAverage.add(sequenceSample.getX() / xReduceFactor, sequenceSample.getY() / yReduceFactor, color);

                                    currentSequence.add(sequenceSample);
                                }
                            }
                        }
                    }
                }
            }

            @Override
            public void endSequence(ProjectionInfo<SampleData> lastData) {
                SampleData lastSample = lastData.getLocationData();
                Deque<XYSample> newSequence = spaceTimeSequenceByOrder.get(lastSample.getOrder());

                if (newSequence != null) {
                    addSampleSequenceToGraph(newSequence, lastSample.getOrder());

                    newSequence.remove(lastSample.getOrder());
                }
            }

            private synchronized void addSampleSequenceToGraph(Deque<XYSample> sequence, String order) {
                if (sequence.size() > 1) {
                    xyGraph.addSampleSequence(sequence);
                }
            }
        });

        LinkedList<XYSample> xySamples = new LinkedList<>() ;

        for(int x=0; x<mapAverage.getWidth(); x++) {
            for(int y=0; y<mapAverage.getHeight(); y++) {
                if (mapAverage.getCount(x, y) > 0) {
                    double result = mapAverage.getResult(x, y);
                    xySamples.add(new XYSample(x * xReduceFactor, y * yReduceFactor, (float) result));
                }
            }
        }

        // Update graph with heatmap average data
        xyGraph.clean();
        xyGraph.addSampleSequence(xySamples);

        long end = System.currentTimeMillis();

        logger.info("Finished loading all data in {} s", (end - start) / 1000);

        // Save file
        if (saveToFolder != null) {
            String fileBase = String.format("speed-heatmap-%s-%d", route.getLineNumber(), route.getShapeId()) ;

            xyGraph.saveData(new File(saveToFolder, fileBase + ".xyd"));
            xyGraph.saveImage(new File(saveToFolder, fileBase + ".png"), 1);

            JFrame frame = (JFrame) SwingUtilities.getRoot(xyGraph);
            frame.setVisible(false);
            frame.dispose();
        }

        return xyGraph ;
    }
}
